﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Pass
{
    public class PassMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public PassMgmt()
        {
        }

        #endregion

        #region Method

        #region GetPassAuthList
        /// <summary>
        /// GetPassAuthList 
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strPassType">string</param>
        /// <param name="strOperNm">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassAuthList(string strOrgCd, string strPassType, string strOperNm)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_PASSAUTHINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[1].Value = strPassType;

                parameters[2] = new MySqlParameter("@pOPERNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strOperNm;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetPassAuthInfo
        /// <summary>
        /// SetPassAuthInfo
        /// </summary>
        /// <param name="strPassType">string</param>
        /// <param name="strAuthOper">string</param>
        /// <param name="strOrgCd">string</param>
        /// <param name="strUpperYn">string</param>
        public void SetPassAuthInfo(string strPassType, string strAuthOper, string strOrgCd, string strUpperYn)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_PASSAUTHINFO_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pAUTHOPER", MySqlDbType.VarChar, 20);
                parameters[1].Value = strAuthOper;

                parameters[2] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strOrgCd;

                parameters[3] = new MySqlParameter("@pUPPERYN", MySqlDbType.VarChar, 1);
                parameters[3].Value = strUpperYn;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelPassAuthInfo
        /// <summary>
        /// DelPassAuthInfo
        /// </summary>
        /// <param name="strPassType">string</param>
        /// <param name="strAuthOper">string</param>
        public void DelPassAuthInfo(string strPassType, string strAuthOper)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_PASSAUTHINFO_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pAUTHOPER", MySqlDbType.VarChar, 20);
                parameters[1].Value = strAuthOper;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetPassSvcList
        /// <summary>
        /// GetPassSvcList 
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetPassSvcList()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_PASSSVCLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassBizAuthist
        /// <summary>
        /// GetPassBizAuthist 
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassBizAuthist(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PASSBIZAUTHLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetPassProcess
        /// <summary>
        /// SetPassProcess
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCustNm">string</param>
        /// <param name="strCustTelNo">string</param>
        /// <param name="strHopeDt">string</param>
        /// <param name="strRelType">string</param>
        /// <param name="strPassType">string</param>
        /// <param name="strSvcType">string</param>
        /// <param name="strConsult">string</param>
        /// <param name="strNoti">string</param>
        /// <param name="strCompEmpNo">string</param>
        /// <param name="strImgData">string</param>
        /// <param name="strProcId">string</param>
        public void SetPassProcess(string strEmpNo, string strCustNm, string strCustTelNo, string strHopeDt, string strRelType, string strPassType, string strSvcType, string strMotType, string strConsult, string strNoti, string strCompEmpNo, string strImgData, string strProcId)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[13];

                CommonData data = new CommonData("UP_PASSPROCINFO_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCUSTNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strCustNm;

                parameters[2] = new MySqlParameter("@pCUSTTELNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strCustTelNo;

                parameters[3] = new MySqlParameter("@pHOPEDT", MySqlDbType.VarChar, 8);
                parameters[3].Value = strHopeDt;

                parameters[4] = new MySqlParameter("@pRELTYPE", MySqlDbType.VarChar, 50);
                parameters[4].Value = strRelType;

                parameters[5] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[5].Value = strPassType;

                parameters[6] = new MySqlParameter("@pSVCTYPE", MySqlDbType.VarChar, 10);
                parameters[6].Value = strSvcType;

                parameters[7] = new MySqlParameter("@pMOTTYPE", MySqlDbType.VarChar, 1);
                parameters[7].Value = strMotType;

                parameters[8] = new MySqlParameter("@pTRTEMPNO", MySqlDbType.VarChar, 20);
                parameters[8].Value = strConsult;

                parameters[9] = new MySqlParameter("@pNOTI", MySqlDbType.VarChar, 8000);
                parameters[9].Value = strNoti;

                parameters[10] = new MySqlParameter("@pCOMPEMPNO", MySqlDbType.VarChar, 20);
                parameters[10].Value = strCompEmpNo;

                parameters[11] = new MySqlParameter("@pIMGDATA", MySqlDbType.VarChar, 8000);
                parameters[11].Value = strImgData;

                parameters[12] = new MySqlParameter("@pPROCID", MySqlDbType.Int32);
                parameters[12].Value = strProcId;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetPassProcessForProcId
        /// <summary>
        /// GetPassProcessForProcId 
        /// </summary>
        /// <param name="strProcId">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassProcessForProcId(string strProcId)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PASSPROCINFOFORPROCID_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPROCID", MySqlDbType.Int32);
                parameters[0].Value = strProcId;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassProcessSts
        /// <summary>
        /// GetPassProcessSts
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCustNm">string</param>
        /// <param name="strCustTelNo">string</param>
        /// <param name="strPassType">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassProcessSts(string strEmpNo, string strCustNm, string strCustTelNo, string strPassType, string strStDt, string strEnDt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_PASSPROCSTS_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCUSTNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strCustNm;

                parameters[2] = new MySqlParameter("@pCUSTTELNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strCustTelNo;

                parameters[3] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[3].Value = strPassType;

                parameters[4] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[4].Value = strStDt;

                parameters[5] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[5].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassProcessDetail
        /// <summary>
        /// GetPassProcessDetail
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCustNm">string</param>
        /// <param name="strCustTelNo">string</param>
        /// <param name="strPassType">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassProcessDetail(string strEmpNo, string strCustNm, string strCustTelNo, string strPassType, string strStDt, string strEnDt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_PASSPROCDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCUSTNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strCustNm;

                parameters[2] = new MySqlParameter("@pCUSTTELNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strCustTelNo;

                parameters[3] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[3].Value = strPassType;

                parameters[4] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[4].Value = strStDt;

                parameters[5] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[5].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelPassProcess
        /// <summary>
        /// DelPassProcess
        /// </summary>
        /// <param name="strProcId"></param>
        public void DelPassProcess(string strProcId)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PASSPROCINFO_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPROCID", MySqlDbType.Int32);
                parameters[0].Value = strProcId;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetPassEmpInfo
        /// <summary>
        /// GetPassEmpInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassEmpInfo(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PASSEMPNOINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassProcessNewBiz
        /// <summary>
        /// GetPassProcessNewBiz
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassProcessNewBiz(string strEmpNo, string strStDt, string strEnDt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_PASSPROCNEWBIZ_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[1].Value = strStDt;

                parameters[2] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetPassProcessNewBiz
        /// <summary>
        /// SetPassProcessNewBiz
        /// </summary>
        /// <param name="strProcId">string</param>
        /// <param name="strPassStat">string</param>
        /// <param name="strSuccYn">string</param>
        public void SetPassProcessNewBiz(string strProcId, string strSuccYn)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_PASSPROCNEWBIZ_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPROCID", MySqlDbType.Int32);
                parameters[0].Value = strProcId;

                parameters[1] = new MySqlParameter("@pSUCCYN", MySqlDbType.VarChar, 1);
                parameters[1].Value = strSuccYn;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetPassNewBizAuth
        /// <summary>
        /// GetPassNewBizAuth
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassNewBizAuth(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PASSOPERAUTH_NEWBIZ_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetUserList
        /// <summary>
        /// GetUserList 
        /// </summary>
        /// <param name="strEmpNm">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserList(string strEmpNm)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PASSUSERINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[0].Value = strEmpNm;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassAuthList
        /// <summary>
        /// GetPassAuthList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassAuthList(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PASSAUTHINFOFOREMPNO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassProcList
        /// <summary>
        /// GetPassProcList
        /// </summary>
        /// <param name="strPassType">string</param>
        /// <param name="strOrgCd">string</param>
        /// <param name="strEmpNo">string</param>
        /// <param name="strStdt">string</param>
        /// <param name="strEndt">string</param>
        /// <param name="strPassStat">string</param>
        /// <param name="strSuccYn">string</param>
        /// <param name="strTrtEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassProcList(string strPassType, string strOrgCd, string strEmpNo, string strStdt, string strEndt, string strPassStat, string strSuccYn, string strTrtEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[8];

                CommonData data = new CommonData("UP_PASSPROCLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[1].Value = strOrgCd;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[3].Value = strStdt;

                parameters[4] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[4].Value = strEndt;

                parameters[5] = new MySqlParameter("@pPASSSTAT", MySqlDbType.VarChar, 1);
                parameters[5].Value = strPassStat;

                parameters[6] = new MySqlParameter("@pSUCCYN", MySqlDbType.VarChar, 1);
                parameters[6].Value = strSuccYn;

                parameters[7] = new MySqlParameter("@pTRTEMPNO", MySqlDbType.VarChar, 20);
                parameters[7].Value = strTrtEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetPassTrtEmpNo
        /// <summary>
        /// SetPassTrtEmpNo
        /// </summary>
        /// <param name="strProcId">string</param>
        /// <param name="strTrtEmpNo">string</param>
        /// <param name="strSuccYn">string</param>
        /// <param name="strNoti2">string</param>
        /// <param name="strTrtEmpType">string</param>
        public void SetPassProcInfo(string strProcId, string strTrtEmpNo, string strSuccYn, string strNoti2, string strTrtEmpType)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_PASSPROCINFO_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPROCID", MySqlDbType.Int32);
                parameters[0].Value = strProcId;

                parameters[1] = new MySqlParameter("@pTRTEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strTrtEmpNo;

                parameters[2] = new MySqlParameter("@pSUCCYN", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSuccYn;

                parameters[3] = new MySqlParameter("@pNOTI2", MySqlDbType.VarChar, 500);
                parameters[3].Value = strNoti2;

                parameters[4] = new MySqlParameter("@pTRTEMPTYPE", MySqlDbType.VarChar, 1);
                parameters[4].Value = strTrtEmpType;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetPassProcMonthFin
        /// <summary>
        /// SetPassProcMonthFin
        /// </summary>
        /// <param name="strPassType"></param>
        /// <param name="strEmpNo"></param>
        /// <param name="strStDt"></param>
        /// <param name="strEnDt"></param>
        public void SetPassProcMonthFin(string strPassType, string strEmpNo, string strStDt, string strEnDt, string MonthFinYn)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_PASSPROCMONTHFIN_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strEmpNo;

                parameters[2] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[2].Value = strStDt;

                parameters[3] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[3].Value = strEnDt;

                parameters[4] = new MySqlParameter("@pFINYN", MySqlDbType.VarChar, 1);
                parameters[4].Value = MonthFinYn;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetPassProcStsByEmpNo
        /// <summary>
        /// GetPassProcStsByEmpNo
        /// </summary>
        /// <param name="strPassType">string</param>
        /// <param name="strOrgCd">string</param>
        /// <param name="strEmpNo">string</param>
        /// <param name="strStdt">string</param>
        /// <param name="strEndt">string</param>
        /// <returns></returns>
        public DataSet GetPassProcStsByEmpNo(string strPassType, string strOrgCd, string strEmpNo, string strStdt, string strEndt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_PASSPROCSTSBYEMPNO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[1].Value = strOrgCd;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[3].Value = strStdt;

                parameters[4] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[4].Value = strEndt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassProcStsByTrtEmpNo
        /// <summary>
        /// GetPassProcStsByTrtEmpNo
        /// </summary>
        /// <param name="strPassType">string</param>
        /// <param name="strOrgCd">string</param>
        /// <param name="strEmpNo">string</param>
        /// <param name="strStdt">string</param>
        /// <param name="strEndt">string</param>
        /// <returns></returns>
        public DataSet GetPassProcStsByTrtEmpNo(string strPassType, string strOrgCd, string strEmpNo, string strStdt, string strEndt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_PASSPROCSTSBYTRTEMPNO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[1].Value = strOrgCd;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[3].Value = strStdt;

                parameters[4] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[4].Value = strEndt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassProcStsByOrgLv
        /// <summary>
        /// GetPassProcStsByOrgLv
        /// </summary>
        /// <param name="strPassType">string</param>
        /// <param name="strOrgCd">string</param>
        /// <param name="strEmpNo">string</param>
        /// <param name="strStdt">string</param>
        /// <param name="strEndt">string</param>
        /// <param name="strOrgLv">string</param>
        /// <returns></returns>
        public DataSet GetPassProcStsByOrgLv(string strPassType, string strOrgCd, string strEmpNo, string strStdt, string strEndt, string strOrgLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_PASSPROCSTSBYORGLV_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 5);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[1].Value = strOrgCd;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[3].Value = strStdt;

                parameters[4] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[4].Value = strEndt;

                parameters[5] = new MySqlParameter("@pORGLV", MySqlDbType.VarChar, 3);
                parameters[5].Value = strOrgLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassProcessNewBiz
        /// <summary>
        /// GetPassProcessNewBiz
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassProcessCompList(string strEmpNo, string strStDt, string strEnDt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_PASSPROCCOMPLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 20);
                parameters[1].Value = strStDt;

                parameters[2] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
