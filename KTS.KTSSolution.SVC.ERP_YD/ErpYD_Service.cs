﻿using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using System.Runtime.Remoting;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Util;
using KTS.KTSSolution.BSL.Erp_Yd;

namespace KTS.KTSSolution.SVC.ERP_YD
{
    public partial class ErpYD_Service : ServiceBase
    {
        #region fields

        private Timer timer;
        private bool bTimerStat = false;
        private bool bRunSvcStat = true;
        private ErpYD_Service oSvc;

        #endregion

        public ErpYD_Service()
        {
            InitializeComponent();
        }

        #region Method

        #region OnStart
        /// <summary>
        /// OnStart
        /// </summary>
        /// <param name="args">string[]</param>
        protected override void OnStart(string[] args)
        {
            string strConfig = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            RemotingConfiguration.Configure(strConfig, false);

            oSvc = new ErpYD_Service();
            oSvc.Start();
        }
        #endregion


        #region OnStop
        /// <summary>
        /// OnStop
        /// </summary>
        protected override void OnStop()
        {
            if (bTimerStat)
            {
                timer.Dispose();
                bTimerStat = false;
            }
        }
        #endregion

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        public void Start()
        {
            int ntimerTick = 60 * 1000;

            try
            {
                int.TryParse(ConfigurationSettings.AppSettings["timerTick"], out ntimerTick);

                timer = new Timer(new TimerCallback(StartService), null, 100, ntimerTick * 1000);
                bTimerStat = true;
            }
            catch (Exception ex)
            {
                KTSException.HandleSVCException(SubSystemType.KTSSolution, ex, this.GetType(), false);
            }
        }
#endregion

#region StartService
        /// <summary>
        /// StartService
        /// </summary>
        /// <param name="s">object</param>
        public void StartService(object s)
        {
            DataSet ds = new DataSet();
            bool bResult = false;
            TimeStamp timestamp = new TimeStamp();

            try
            {
                timestamp.TimeStampStart();

                if (bRunSvcStat)
                {
                    bRunSvcStat = false;
                    timestamp.TimeStampEnd(this, "=====서비스 시작");

                    using (Erp_Yd oErpYd = new Erp_Yd())
                    {
                        ds = oErpYd.GetYdLIst();
                    }

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            switch (dr["YD_TBLNM"].ToString())
                            {
                                case "1069_OC_EMP_NO":
                                    bResult = SetOC_EMP_NO();
                                    break;
                                case "1069_ORG_CODE":
                                    bResult = SetORG_CODE();
                                    break;
                                case "1069_ORG_GROUP":
                                    bResult = SetORG_GROUP();
                                    break;
                                case "1069_ORG_USER":
                                    bResult = SetORG_USER();
                                    break;
                                default:
                                    break;
                            }
                            timestamp.TimeStampEnd(this, "=====연동 결과 >>" + dr["YDNM"].ToString() + " : " + bResult.ToString());
                        }
                    }
                    bRunSvcStat = true;
                }
                else
                {
                    timestamp.TimeStampEnd(this, ">> 종전 서비스 수행중으로 서비스 종료.");
                }
            }
            catch (Exception ex)
            {
                bRunSvcStat = true;

                timestamp = new TimeStamp();
                timestamp.TimeStampStart();
                timestamp.TimeStampEnd(this, "=====오류 발생 >> " + ex.ToString());
                KTSException.HandleSVCException(SubSystemType.KTSSolution, ex, this.GetType(), false);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                timestamp = new TimeStamp();
                timestamp.TimeStampStart();
                timestamp.TimeStampEnd(this, "=====서비스종료 결과 >>" + bResult.ToString());
            }
        }
#endregion

#region SetOC_EMP_NO
        /// <summary>
        /// SetOC_EMP_NO
        /// </summary>
        /// <returns>bool</returns>
        private bool SetOC_EMP_NO()
        {
            DataSet ds = new DataSet();
            bool bResult = false;

            try
            {
                string strSql_Select = "SELECT EMP_NO, OC_EMP_NO FROM 1069_OC_EMP_NO";

                using (Erp_Yd oErpYd = new Erp_Yd())
                {
                    ds = oErpYd.GetYdData(strSql_Select);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("DELETE FROM tb_1069_oc_emp_no WHERE 1 = 1;");

                            sb.Append("INSERT INTO tb_1069_oc_emp_no ");
                            sb.Append(" (EMP_NO, OC_EMP_NO) ");
                            sb.Append(" VALUES ");

                            string strData = "";

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strData = "(" + ds.Tables[0].Rows[i]["EMP_NO"].ToString() + ", " + ds.Tables[0].Rows[i]["OC_EMP_NO"].ToString() + "), ";

                                if (i + 1 < ds.Tables[0].Rows.Count)
                                {
                                    strData += strData + ", ";
                                }
                                sb.Append(strData);
                            }
                            sb.Append(";");

                            ds = oErpYd.SetYdData(strData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bResult;
        }
#endregion

#region SetORG_CODE
        /// <summary>
        /// SetORG_CODE
        /// </summary>
        /// <returns>bool</returns>
        private bool SetORG_CODE()
        {
            DataSet ds = new DataSet();
            bool bResult = false;

            try
            {
                string strSql_Select = "SELECT GroupType, Code, Name, FormOrder, CompanyCD FROM 1069_ORG_CODE";

                using (Erp_Yd oErpYd = new Erp_Yd())
                {
                    ds = oErpYd.GetYdData(strSql_Select);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("DELETE FROM tb_1069_org_code WHERE 1 = 1;");

                            sb.Append("INSERT INTO tb_1069_org_code ");
                            sb.Append(" (GroupType, Code, Name, FormOrder, CompanyCD) ");
                            sb.Append(" VALUES ");

                            string strData = "";

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strData = "(" + ds.Tables[0].Rows[i]["GroupType"].ToString() 
                                        + ", " + ds.Tables[0].Rows[i]["Code"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["Name"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["FormOrder"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["CompanyCD"].ToString()
                                        + "), ";

                                if (i + 1 < ds.Tables[0].Rows.Count)
                                {
                                    strData += strData + ", ";
                                }
                                sb.Append(strData);
                            }
                            sb.Append(";");

                            ds = oErpYd.SetYdData(strData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bResult;
        }
#endregion

#region SetORG_GROUP
        /// <summary>
        /// SetORG_GROUP
        /// </summary>
        /// <returns>bool</returns>
        private bool SetORG_GROUP()
        {
            DataSet ds = new DataSet();
            bool bResult = false;

            try
            {
                string strCol = "Code, Name, ExOrgEngNM, ExCaptainNumber, ExenterDT, ExOrgLevel"
                                + ", ExOrgOrder, ExLowerLevelGB, ExUpperOrgCD, ExFormOrder, ExZipCode, ExAddress1"
                                + ", ExAddress2, ExPhoneNO, ExFaxNO, ExCompanyCD";

                string strSql_Select = "SELECT " + strCol + "  FROM 1069_ORG_GROU";

                using (Erp_Yd oErpYd = new Erp_Yd())
                {
                    ds = oErpYd.GetYdData(strSql_Select);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("DELETE FROM tb_1069_org_group WHERE 1 = 1;");

                            sb.Append("INSERT INTO tb_1069_org_group ");
                            sb.Append(" (" + strCol + ") ");
                            sb.Append(" VALUES ");

                            string strData = "";

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strData = "(" + ds.Tables[0].Rows[i]["Code"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["Name"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExOrgEngNM"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExCaptainNumber"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExenterDT"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExOrgLevel"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExOrgOrder"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExLowerLevelGB"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExUpperOrgCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExFormOrder"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExZipCode"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExAddress1"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExAddress2"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExPhoneNO"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExFaxNO"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExCompanyCD"].ToString()
                                        + "), ";

                                if (i + 1 < ds.Tables[0].Rows.Count)
                                {
                                    strData += strData + ", ";
                                }
                                sb.Append(strData);
                            }
                            sb.Append(";");

                            ds = oErpYd.SetYdData(strData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bResult;
        }
#endregion

#region SetORG_USER
        /// <summary>
        /// SetORG_USER
        /// </summary>
        /// <returns>bool</returns>
        private bool SetORG_USER()
        {
            DataSet ds = new DataSet();
            bool bResult = false;

            try
            {
                string strCol = "Code, Name, ExNameE, ExNameH, ExSex, ExSocialNo, Phone, Mobile, Email"
                                + ", ExTitleCD, ExTitleNM, ExLevelCD, ExLevelNM, ExJobCD, ExJobNM, ExJobDesc"
                                + ", ExAgencyCD, ExAgencyNM, ExDeptCD, ExDeptNM, ExManagerFlag, ExMailUseYN"
                                + ", ExOCUseYN, ExPhoneUseYN, ExMailType, ExPRSKindCD, ExCompanyCD, exskypeuseyn";

                string strSql_Select = "SELECT " + strCol + "  FROM 1069_ORG_USER";

                using (Erp_Yd oErpYd = new Erp_Yd())
                {
                    ds = oErpYd.GetYdData(strSql_Select);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("DELETE FROM tb_1069_org_user WHERE 1 = 1;");

                            sb.Append("INSERT INTO tb_1069_org_user ");
                            sb.Append(" (" + strCol + ") ");
                            sb.Append(" VALUES ");

                            string strData = "";

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strData = "(" + ds.Tables[0].Rows[i]["Code"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["Name"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExNameE"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExNameH"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExSex"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExSocialNo"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["Phone"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["Mobile"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["Email"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExTitleCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExTitleNM"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExLevelCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExLevelNM"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExJobCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExJobNM"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExJobDesc"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExAgencyCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExAgencyNM"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExDeptCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExDeptNM"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExManagerFlag"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExMailUseYN"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExOCUseYN"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExPhoneUseYN"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExMailType"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExPRSKindCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["ExCompanyCD"].ToString()
                                        + ", " + ds.Tables[0].Rows[i]["exskypeuseyn"].ToString()
                                        + "), ";

                                if (i + 1 < ds.Tables[0].Rows.Count)
                                {
                                    strData += strData + ", ";
                                }
                                sb.Append(strData);
                            }
                            sb.Append(";");

                            ds = oErpYd.SetYdData(strData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bResult;
        }
#endregion

#endregion
    }
}
