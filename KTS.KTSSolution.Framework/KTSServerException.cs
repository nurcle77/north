﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Runtime.Serialization;

namespace KTS.KTSSolution.Framework.ExceptionManager
{
    [Serializable]
    public class KTSServerException : System.Exception, ISerializable
	{
		#region Fields

		private string bizName = "";
		private string layerName = "";
		private string code = "";
		private string message = "";
		private string location = "";
		private string errorLog = "";

		#endregion

		#region Properties

		#region ToString()
		public override string ToString()
		{

			return ".KTSException " +
				bizName + "|" +
				layerName + "|" +
				code + "|" +
				message + "|" +
				location + "|" +
				errorLog + "||";
		}
        #endregion

        #region BizName
        public string BizName
		{
			get
			{
				return bizName;
			}
		}
        #endregion

        #region LayerName
        public string LayerName
		{
			get
			{
				return layerName;
			}
		}
        #endregion

        #region Code
        public string Code
		{
			get
			{
				return code;
			}
		}
        #endregion

        #region Msg
        public string Msg
		{
			get
			{
				return message;
			}
			set
			{
				message = value;
			}
		}
        #endregion

        #region Location
        public string Location
		{
			get
			{
				return location;

			}
			set
			{
				location = value;
			}
		}
        #endregion

        #region ErrorLog
        public string ErrorLog
		{
			get
			{
				return errorLog;
			}
		}
		#endregion

		#region EventLogMessage
		/// <summary>
		/// EventLogMessage
		/// </summary>
		//SystemLog를 입력하기 위한것으로, ToString을 사용한다.
		public string EventLogMessage
		{
			get
			{
				string str = string.Format("\r\n.COMOSException : \r\nCode:{0}\r\nBiz:{1}\r\nLayer:{2}\r\nMessage:\r\n{3}\r\nLocation:{4}\r\nLog:{5}",
											code, bizName, layerName, message, location, errorLog);
				return str;
			}
		}
		#endregion

		#endregion

		#region Method

		#region KTSServerException(string bizName, string layerName, string code, string message) : base(message)
		/// <summary>
		/// KTSServerException(string bizName, string layerName, string code, string message) : base(message)
		/// </summary>
		/// <param name="bizName">string</param>
		/// <param name="layerName">string</param>
		/// <param name="code">string</param>
		/// <param name="message">string</param>
		public KTSServerException(string bizName, string layerName, string code, string message) : base(message)
		{
			this.bizName = bizName;
			this.layerName = layerName;
			this.code = code;
			this.message = message;
		}
		#endregion

		#region KTSServerException(string bizName, string layerName, string code, string message, string location, string errorLog) : base(message)
		/// <summary>
		/// KTSServerException(string bizName, string layerName, string code, string message, string location, string errorLog) : base(message)
		/// </summary>
		/// <param name="bizName">string</param>
		/// <param name="layerName">string</param>
		/// <param name="code">string</param>
		/// <param name="message">string</param>
		/// <param name="location">string</param>
		/// <param name="errorLog">string</param>
		public KTSServerException(string bizName, string layerName, string code, string message, string location, string errorLog) : base(message)
		{
			this.bizName = bizName;
			this.layerName = layerName;
			this.code = code;
			this.message = message;
			this.location = location;
			this.errorLog = errorLog;
		}
		#endregion

		#region KTSServerException(SerializationInfo info, StreamingContext c) : base(info, c)
		/// <summary>
		/// KTSServerException(SerializationInfo info, StreamingContext c) : base(info, c)
		/// </summary>
		/// <param name="info"><SerializationInfo/param>
		/// <param name="c">StreamingContext</param>
		private KTSServerException(SerializationInfo info, StreamingContext c) : base(info, c)
		{

			bizName = (string)info.GetValue("bn", bizName.GetType());
			layerName = (string)info.GetValue("ln", layerName.GetType());
			code = (string)info.GetValue("cd", code.GetType());
			message = (string)info.GetValue("ms", message.GetType());
			location = (string)info.GetValue("lc", location.GetType());
			errorLog = (string)info.GetValue("el", errorLog.GetType());
		}
		#endregion

		#region GetObjectData(SerializationInfo info, StreamingContext context)
		/// <summary>
		/// GetObjectData(SerializationInfo info, StreamingContext context)
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bn", bizName, bizName.GetType());
			info.AddValue("ln", layerName, layerName.GetType());
			info.AddValue("cd", code, code.GetType());
			info.AddValue("ms", message, message.GetType());
			info.AddValue("lc", location, location.GetType());
			info.AddValue("el", errorLog, errorLog.GetType());
		}
		#endregion

		#endregion
	}
}
