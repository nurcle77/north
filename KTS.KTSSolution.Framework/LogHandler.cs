﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Text;
using System.Diagnostics;

using MySql.Data;
using MySql.Data.MySqlClient;

namespace KTS.KTSSolution.Framework.ExceptionManager
{
    public class LogHandler
	{
		#region Constructor

		public LogHandler()
        {
        }

        #endregion

        #region Method

        #region Publish

        #region PublishNew(EventLogEntryType entryType, string SystemName, string Layer, Exception exception)
        /// <summary>
        /// PublishNew(EventLogEntryType entryType, string SystemName, string Layer, Exception exception)
        /// </summary>
        /// <param name="entryType">EventLogEntryType</param>
        /// <param name="SystemName">string</param>
        /// <param name="Layer">string</param>
        /// <param name="exception">Exception</param>
        public static void PublishNew(EventLogEntryType entryType, string SystemName, string Layer, Exception exception)
        {
            PublishNew(entryType, SystemName, Layer, exception, 0);
        }
		#endregion

		#region PublishNew(EventLogEntryType entryType, string SystemName, string Layer, Exception exception, int eventId)
		/// <summary>
		/// PublishNew(EventLogEntryType entryType, string SystemName, string Layer, Exception exception, int eventId)
		/// </summary>
		/// <param name="entryType">EventLogEntryType</param>
		/// <param name="SystemName">string</param>
		/// <param name="Layer">string</param>
		/// <param name="exception">Exception</param>
		/// <param name="eventId">int</param>
		public static void PublishNew(EventLogEntryType entryType, string SystemName, string Layer, Exception exception, int eventId)
		{
			StringBuilder msg = new StringBuilder();

			short category = 0;
			StackTrace st1 = new StackTrace(1, true);
			string[] strTitles = new string[st1.FrameCount];
			msg.Append("[CallStackTrace]\r\n");

			for (int i = 0; i < st1.FrameCount; i++)
			{
				StackFrame sf = st1.GetFrame(i);
				string tmp = sf.GetMethod().DeclaringType.FullName + "." + sf.GetMethod().Name;
				strTitles[i] = tmp;

				if (tmp.IndexOf("System") != 0 && tmp.IndexOf("Microsoft") != 0)
				{
					msg.Append(tmp);
					msg.Append(" : (" + sf.GetFileLineNumber() + ")");
					msg.Append("\r\n");
				}
			}

			StackTrace st2 = new StackTrace(exception, true);

			msg.Append("\r\n[ErrStackTrace]\r\n");

			for (int i = 0; i < st2.FrameCount; i++)
			{
				StackFrame sf = st2.GetFrame(i);
				string tmp = sf.GetMethod().DeclaringType.FullName + "." + sf.GetMethod().Name;
				if (tmp.IndexOf("System") != 0 && tmp.IndexOf("Microsoft") != 0)
				{
					msg.Append(tmp);
					msg.Append(" : (" + sf.GetFileLineNumber() + ")");
					msg.Append("\r\n");
				}
			}

			msg.Append("\r\n[DateTime] : " + DateTime.Now.ToString() + "\r\n");

			if (exception.GetType() == typeof(MySqlException))
			{
				MySqlException sqlErr = (MySqlException)exception;
				msg.Append("\r\n[MySqlException] ");
				msg.Append("\r\nException Type: ").Append(sqlErr.GetType());
				msg.Append("\r\nErrorCode: ").Append(sqlErr.ErrorCode);
				msg.Append("\r\nMessage: ").Append(sqlErr.Message);
				msg.Append("\r\nNumber: ").Append(sqlErr.Number);
				if (sqlErr.Number >= 50000)
				{
					eventId = 1;
					category = 100;
				}

				msg.Append("\r\nState: ").Append(sqlErr.SqlState);
				msg.Append("\r\nSource: ").Append(sqlErr.Source);
				msg.Append("\r\nTargetSite: ").Append(sqlErr.TargetSite);
				msg.Append("\r\nHelpLink: ").Append(sqlErr.HelpLink);
			}
			else
			{
				msg.Append("\r\n[Exception] ");
                if (exception is KTSServerException)
                {
                    KTSServerException nex = (KTSServerException)exception;

                    msg.Append("\r\n" + "DetailMsg: " + nex.EventLogMessage);
                }
                else
                    msg.Append("\r\n" + "DetailMsg: " + exception.Message);
            }

			string strTitle = "";
			for (int i = strTitles.Length - 1; i > -1; i--)
			{

				if (strTitles[i].ToLower().IndexOf("KTS") > 0)
				{
					strTitle = strTitles[i];

					if (strTitles[i].ToLower().IndexOf("svcbasemulti.runjob") <= 0)
					{
						break;
					}
				}
			}

            if (eventId == 1)
                WriteLogNew(strTitle, msg.ToString(), SystemName, Layer, EventLogEntryType.Warning, eventId, category);
            else
                WriteLogNew(strTitle, msg.ToString(), SystemName, Layer, entryType, eventId, category);
        }
		#endregion

		#region Publish(string SystemName, string Layer, Exception exception)
		/// <summary>
		/// Publish(string SystemName, string Layer, Exception exception)
		/// </summary>
		/// <param name="SystemName">string</param>
		/// <param name="Layer">string</param>
		/// <param name="exception">Exception</param>
		public static void Publish(string SystemName, string Layer, Exception exception)
		{
			StringBuilder msg = new StringBuilder();

			int eventId = 0;
			short category = 0;

			StackTrace st1 = new StackTrace(1, true);

			msg.Append("[CallStackTrace]\r\n");

			for (int i = 0; i < st1.FrameCount; i++)
			{
				StackFrame sf = st1.GetFrame(i);
				string tmp = sf.GetMethod().DeclaringType.FullName + "." + sf.GetMethod().Name;
				if (tmp.IndexOf("System") != 0 && tmp.IndexOf("Microsoft") != 0)
				{
					msg.Append(tmp);
					msg.Append(" : (" + sf.GetFileLineNumber() + ")");
					msg.Append("\r\n");
				}
			}

			StackTrace st2 = new StackTrace(exception, true);

			msg.Append("\r\n[ErrStackTrace]\r\n");

			for (int i = 0; i < st2.FrameCount; i++)
			{
				StackFrame sf = st2.GetFrame(i);
				string tmp = sf.GetMethod().DeclaringType.FullName + "." + sf.GetMethod().Name;
				if (tmp.IndexOf("System") != 0 && tmp.IndexOf("Microsoft") != 0)
				{
					msg.Append(tmp);
					msg.Append(" : (" + sf.GetFileLineNumber() + ")");
					msg.Append("\r\n");
				}
			}

			msg.Append("\r\n[DateTime] : " + DateTime.Now.ToString() + "\r\n");

			if (exception.GetType() == typeof(MySql.Data.MySqlClient.MySqlException))
			{
				MySqlException sqlErr = (MySqlException)exception;
				msg.Append("\r\n[MySqlException] ");
				msg.Append("\r\nException Type: ").Append(sqlErr.GetType());
				msg.Append("\r\nErrorCode: ").Append(sqlErr.ErrorCode);
				msg.Append("\r\nMessage: ").Append(sqlErr.Message);
				msg.Append("\r\nNumber: ").Append(sqlErr.Number);
				if (sqlErr.Number >= 50000)
				{
					// 오류 아님 (Warning)
					// 사용자 정의 오류 발생 (로직 오류)
					eventId = 1;
					category = 100;
				}

				msg.Append("\r\nState: ").Append(sqlErr.SqlState);
				msg.Append("\r\nSource: ").Append(sqlErr.Source);
				msg.Append("\r\nTargetSite: ").Append(sqlErr.TargetSite);
				msg.Append("\r\nHelpLink: ").Append(sqlErr.HelpLink);
			}
			else
			{
				msg.Append("\r\n[Exception] ");
				if (exception is KTSServerException)
				{
					KTSServerException nex = (KTSServerException)exception;

					msg.Append("\r\n" + "DetailMsg: " + nex.EventLogMessage);
				}
				else
					msg.Append("\r\n" + "DetailMsg: " + exception.Message);
			}

			if (eventId > 0)
				WriteLog(msg.ToString(), SystemName, Layer, EventLogEntryType.Warning, eventId, category);
			else
				WriteLog(msg.ToString(), SystemName, Layer);
		}
		#endregion

		#region Publish(string systemName, string layer, string message, int eventId, short category)
		/// <summary>
		/// Publish(string systemName, string layer, string message, int eventId, short category)
		/// </summary>
		/// <param name="systemName">string</param>
		/// <param name="layer">string</param>
		/// <param name="message">string</param>
		/// <param name="eventId">int</param>
		/// <param name="category">short</param>
		public static void Publish(string systemName, string layer, string message, int eventId, short category)
		{
			StringBuilder stb = new StringBuilder();
			stb.Append("\r\n[EventInfo]:");
			stb.Append("\r\nEventid:").Append(eventId);
			stb.Append("\tCategory:").Append(category);

			stb.Append("\r\n\r\n[Information]:");
			stb.Append("\r\nBiz:").Append(systemName);
			stb.Append("\tLayer:").Append(layer);
			stb.Append("\r\nMessage:");
			stb.Append("\r\n").Append(message);

			WriteLog(stb.ToString(), systemName, layer, eventId, category);
		}
		#endregion

		#endregion

		#region WriteLog

		#region WriteLogNew(string strTitle, string sMessage, string sSubSystem, string sLayer, EventLogEntryType evtType, int eventId, short category)
		/// <summary>
		/// WriteLogNew(string strTitle, string sMessage, string sSubSystem, string sLayer, EventLogEntryType evtType, int eventId, short category)
		/// </summary>
		/// <param name="strTitle">string</param>
		/// <param name="sMessage">string</param>
		/// <param name="sSubSystem">string</param>
		/// <param name="sLayer">string</param>
		/// <param name="evtType">EventLogEntryType</param>
		/// <param name="eventId">int</param>
		/// <param name="category">short</param>
		private static void WriteLogNew(string strTitle, string sMessage, string sSubSystem, string sLayer, EventLogEntryType evtType, int eventId, short category)
		{
			try
			{
				string sSource = sSubSystem + "." + sLayer;
				string sLog = sLayer + "//" + strTitle;

				if (!EventLog.SourceExists(sSource))
				{
					EventLog.CreateEventSource(sSource, sSource);
				}

				EventLog evtLog = new EventLog(sSource, ".", sLog);

				evtLog.WriteEntry(sMessage, evtType, eventId, category);

				evtLog.Dispose();
				evtLog.Close();
			}
			catch
			{
			}
		}
		#endregion

		#region WriteLog(string sMessage, string sSubSystem, string sLayer)
		/// <summary>
		/// WriteLog(string sMessage, string sSubSystem, string sLayer)
		/// </summary>
		/// <param name="sMessage">string</param>
		/// <param name="sSubSystem">string</param>
		/// <param name="sLayer">string</param>
		private static void WriteLog(string sMessage, string sSubSystem, string sLayer)
		{
			try
			{
				string sSource = sSubSystem + "." + sLayer;

				if (!EventLog.SourceExists(sSource))
				{
					EventLog.CreateEventSource(sSource, sSource);
				}

				EventLog Log = new EventLog();
				Log.Source = sSource;
				Log.WriteEntry(sMessage, EventLogEntryType.Error);

				Log.Dispose();
				Log.Close();
			}
			catch
			{
			}
		}
		#endregion

		#region WriteLog(string sMessage, string sSubSystem, string sLayer, int eventId, short category)
		/// <summary>
		/// WriteLog(string sMessage, string sSubSystem, string sLayer, int eventId, short category)
		/// </summary>
		/// <param name="sMessage">string</param>
		/// <param name="sSubSystem">string</param>
		/// <param name="sLayer">string</param>
		/// <param name="eventId">int</param>
		/// <param name="category">short</param>
		private static void WriteLog(string sMessage, string sSubSystem, string sLayer, int eventId, short category)
		{
			try
			{
				string sSource = sSubSystem + "." + sLayer;

				if (!EventLog.SourceExists(sSource))
				{
					EventLog.CreateEventSource(sSource, sSource);
				}

				EventLog evtLog = new EventLog();
				evtLog.Source = sSource;

				evtLog.WriteEntry(sMessage, EventLogEntryType.Information, eventId, category);

				evtLog.Dispose();
				evtLog.Close();
			}
			catch
			{
			}
		}
		#endregion

		#region WriteLog(string sMessage, string sSubSystem, string sLayer, EventLogEntryType evtType, int eventId, short category)
		/// <summary>
		/// WriteLog(string sMessage, string sSubSystem, string sLayer, EventLogEntryType evtType, int eventId, short category)
		/// </summary>
		/// <param name="sMessage">string</param>
		/// <param name="sSubSystem">string</param>
		/// <param name="sLayer">string</param>
		/// <param name="evtType">EventLogEntryType</param>
		/// <param name="eventId">int</param>
		/// <param name="category">short</param>
		private static void WriteLog(string sMessage, string sSubSystem, string sLayer, EventLogEntryType evtType, int eventId, short category)
		{
			try
			{
				string sSource = sSubSystem + "." + sLayer;

				if (!EventLog.SourceExists(sSource))
				{
					EventLog.CreateEventSource(sSource, sSource);
				}

				EventLog evtLog = new EventLog();
				evtLog.Source = sSource;

				evtLog.WriteEntry(sMessage, evtType, eventId, category);

				evtLog.Dispose();
				evtLog.Close();
			}
			catch
			{
			}
		}
		#endregion

		#endregion

		#endregion
	}
}
