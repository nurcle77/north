﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Collections;

namespace KTS.KTSSolution.Framework.ExceptionManager
{
    public class MessageHandler
    {
        #region Properties

        private static Hashtable messageTable = new Hashtable();

        #region MessageObject
        /// <summary>
        /// MessageObject
        /// </summary>
        public class MessageObject
        {
            #region Fields

            private string bizName;
            private string layerName;
            private string code;
            private string msg;
            private string location;
            private string errorLog;
            private string reason;
            private string treatmentPlan;
            private string charger;

            #endregion

            #region MessageObject(string bizName, string layerName, string code, string msg, string location, string errorLog)
            /// <summary>
            /// MessageObject(string bizName, string layerName, string code, string msg, string location, string errorLog)
            /// </summary>
            /// <param name="bizName">string</param>
            /// <param name="layerName">string</param>
            /// <param name="code">string</param>
            /// <param name="msg">string</param>
            /// <param name="location">string</param>
            /// <param name="errorLog">string</param>
            public MessageObject(string bizName, string layerName, string code, string msg, string location, string errorLog)
            {
                this.bizName = bizName;
                this.layerName = layerName;
                this.code = code;
                this.msg = msg;
                this.location = location;
                this.errorLog = errorLog;
            }
            #endregion

            #region BizName
            /// <summary>
            /// BizName
            /// </summary>
            public string BizName
            {
                get
                {
                    return bizName;
                }
            }
            #endregion

            #region LayerName
            /// <summary>
            /// LayerName
            /// </summary>
            public string LayerName
            {
                get
                {
                    return layerName;
                }
            }
            #endregion

            #region Code
            /// <summary>
            /// Code
            /// </summary>
            public string Code
            {
                get
                {
                    return code;
                }
            }
            #endregion

            #region Msg
            /// <summary>
            /// Msg
            /// </summary>
            public string Msg
            {
                get
                {
                    return msg;
                }
                set
                {
                    msg = value;
                }
            }
            #endregion

            #region Location
            /// <summary>
            /// Location
            /// </summary>
            public string Location
            {
                get
                {
                    return location;
                }
                set
                {
                    location = value;
                }
            }
            #endregion

            #region ErrorLog
            /// <summary>
            /// ErrorLog
            /// </summary>
            public string ErrorLog
            {
                get
                {
                    return errorLog;
                }
            }
            #endregion

            #region Reason
            /// <summary>
            /// Reason
            /// </summary>
            public string Reason
            {
                get
                {
                    return reason;
                }
                set
                {
                    reason = value;
                }
            }
            #endregion

            #region TreatmentPlan
            /// <summary>
            /// TreatmentPlan
            /// </summary>
            public string TreatmentPlan
            {
                get
                {
                    return treatmentPlan;
                }
                set
                {
                    treatmentPlan = value;
                }
            }
            #endregion

            #region Charger
            /// <summary>
            /// Charger
            /// </summary>
            public string Charger
            {
                get
                {
                    return charger;
                }
                set
                {
                    charger = value;
                }
            }
            #endregion

            #region ToString()
            /// <summary>
            /// ToString()
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                //return base.ToString ();
                return string.Format(".KTSException{8} : |{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}", bizName, layerName, code, msg, location, errorLog, charger, treatmentPlan, reason);

            }
            #endregion
        }
        #endregion

        #region MsgTableObject
        /// <summary>
        /// MsgTableObject
        /// </summary>
        private class MsgTableObject
        {
            #region Fields

            private DateTime lastWriteTime;
            private Hashtable subMessageTable;

            #endregion

            #region LastWriteTime
            /// <summary>
            /// LastWriteTime
            /// </summary>
            public DateTime LastWriteTime
            {
                get
                {
                    return lastWriteTime;
                }
                set
                {
                    lastWriteTime = value;
                }
            }
            #endregion

            #region SubMessageTable
            /// <summary>
            /// SubMessageTable
            /// </summary>
            public Hashtable SubMessageTable
            {
                get
                {
                    return subMessageTable;
                }
                set
                {
                    subMessageTable = value;
                }
            }
            #endregion
        }
        #endregion

        #endregion

        #region Constructor

        public MessageHandler() 
        { 
        }

        #endregion

        #region Method

        #region ReturnFileName(SubSystemType sub)
        /// <summary>
        /// ReturnFileName(SubSystemType sub)
        /// </summary>
        /// <param name="sub">SubSystemType</param>
        /// <returns>string</returns>
        public static string ReturnFileName(SubSystemType sub)
        {
            string filename = "KTSSolution";
            switch (sub)
            {
                case SubSystemType.KTSSolution:
                    filename = "KTSSolution";
                    break;
                default:
                    filename = "KTSSolution";
                    break;
            }

            return filename;
        }
        #endregion

        #region GetFullSubSystemTypeName(string subSystemName)
        /// <summary>
        /// GetFullSubSystemTypeName(string subSystemName)
        /// </summary>
        /// <param name="subSystemName">string</param>
        /// <returns>string</returns>
        public static string GetFullSubSystemTypeName(string subSystemName)
        {
            string filename = "KTSSolution";
            switch (subSystemName)
            {
                case "KTS":
                    filename = "KTSSolution";
                    break;
                default:
                    break;
            }
            return filename;
        }
        #endregion

        #region ParseErrorMessage(string errorMessage)
        /// <summary>
        /// ParseErrorMessage(string errorMessage)
        /// </summary>
        /// <param name="errorMessage">string</param>
        /// <returns>MessageObject</returns>
        public static MessageObject ParseErrorMessage(string errorMessage)
        {
            string[] splits = null;
            MessageObject msgObject = null;

            splits = errorMessage.Split('|');

            if (splits.Length < 7)
            {
                string[] strRuntimeError = errorMessage.Split('\r');

                if (strRuntimeError.Length > 1)
                {
                    string strLocation = errorMessage.Substring(strRuntimeError[0].Length);
                    msgObject = new MessageObject("", "", "", strRuntimeError[0], strLocation, "");
                }
                else
                    msgObject = new MessageObject("", "", "", strRuntimeError[0], "", "");
            }
            else
            {
                msgObject = new MessageObject(splits[1], splits[2], splits[3], splits[4], splits[5], splits[6]);
            }

            return msgObject;
        }
        #endregion

        #endregion

    }
}
