﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Car
{
    public class CarMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public CarMgmt()
        {
        }

        #endregion

        #region Method

        #region GetCarInfoList
        /// <summary>
        /// GetCarInfoList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetCarInfoList(string strOrgCd)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_CARINFOLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetCarNumberCheck
        /// <summary>
        /// GetCarNumberCheck
        /// </summary>
        /// <param name="strCarNum">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetCarNumberCheck(string strCarNum)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_CARNUMBERCHECK_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strCarNum;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetCarInfo
        /// <summary>
        /// SetCarInfo
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strStkm">string</param>
        /// <param name="strEmpNo">string</param>
        public void SetCarInfo(string strOrgCd, string strCarNum, string strStkm, string strEmpNo, string strNotice)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_CARINFOLIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCARORGCD", MySqlDbType.VarChar, 6);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[1].Value = strCarNum;

                parameters[2] = new MySqlParameter("@pSTKM", MySqlDbType.VarChar, 11);
                parameters[2].Value = strStkm;

                parameters[3] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[3].Value = strEmpNo;

                parameters[4] = new MySqlParameter("@pNOTICE", MySqlDbType.VarChar, 1500);
                parameters[4].Value = strNotice;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetCarNotiInfo
        /// <summary>
        /// SetCarNotiInfo
        /// </summary>
        /// <param name="strCarNum"></param>
        /// <param name="strNotice"></param>
        public void SetCarNotiInfo(string strCarNum, string strNotice)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_CARINFONOTICE_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strCarNum;

                parameters[1] = new MySqlParameter("@pNOTICE", MySqlDbType.VarChar, 1500);
                parameters[1].Value = strNotice;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetDelCarInfoCheck
        /// <summary>
        /// GetDelCarInfoCheck
        /// </summary>
        /// <param name="strCarNum"></param>
        /// <returns>DataSet</returns>
        public DataSet GetDelCarInfoCheck(string strCarNum)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_CARINFODELCHECK_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strCarNum;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelCarInfo
        /// <summary>
        /// DelCarInfo
        /// </summary>
        /// <param name="strCarNum"></param>
        public void DelCarInfo(string strCarNum)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_CARINFOLIST_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strCarNum;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetUserCarNum
        /// <summary>
        /// GetUserCarNum
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserCarNum(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_USERCARNUM_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetUserCarInfoCheck
        /// <summary>
        /// GetUserCarInfoCheck
        /// </summary>
        /// <param name="strCarNum">string</param>
        /// <returns></returns>
        public DataSet GetUserCarInfoCheck(string strCarNum)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_USERCARINFOCHECK_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;
                
                parameters[0] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strCarNum;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetUserCarInfo
        /// <summary>
        /// SetUserCarInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strWorkDt">string</param>
        /// <param name="strEnkm">string</param>
        public void SetUserCarInfo(string strEmpNo, string strCarNum, string strWorkDt, string strFuels, string strEnkm, string strLeaveYn)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_USERCARINFO_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[1].Value = strCarNum;

                parameters[2] = new MySqlParameter("@pWORKDT", MySqlDbType.VarChar, 8);
                parameters[2].Value = strWorkDt;

                parameters[3] = new MySqlParameter("@pFUELS", MySqlDbType.VarChar, 11);
                parameters[3].Value = strFuels;

                parameters[4] = new MySqlParameter("@pENKM", MySqlDbType.VarChar, 11);
                parameters[4].Value = strEnkm;

                parameters[5] = new MySqlParameter("@pLEAVEYN", MySqlDbType.VarChar, 1);
                parameters[5].Value = strLeaveYn;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetUserCarInfoList
        /// <summary>
        /// GetUserCarInfoList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserCarInfoList(string strOrgCd, string strEmpNo, string strCarNum, string strStDt, string strEnDt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_USERCARINFOLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strEmpNo;

                parameters[2] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[2].Value = strCarNum;

                parameters[3] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 8);
                parameters[3].Value = strStDt;

                parameters[4] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 8);
                parameters[4].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetUserCarLeaveKm
        /// <summary>
        /// SetUserCarLeaveKm
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strWorkDt">string</param>
        /// <param name="strLeaveKm">string</param>
        /// <param name="strFuels">string</param>
        public void SetUserCarLeaveKm(string strEmpNo, string strCarNum, string strWorkDt, string strLeaveKm, string strFuels)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_USERCARLEAVEKM_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[1].Value = strCarNum;

                parameters[2] = new MySqlParameter("@pWORKDT", MySqlDbType.VarChar, 8);
                parameters[2].Value = strWorkDt;

                parameters[3] = new MySqlParameter("@pLEAVEKM", MySqlDbType.VarChar, 11);
                parameters[3].Value = strLeaveKm;

                parameters[4] = new MySqlParameter("@pFUELS", MySqlDbType.VarChar, 11);
                parameters[4].Value = strFuels;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetUserCarLeaveConfirm
        /// <summary>
        /// SetUserCarLeaveConfirm
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strWorkDt">string</param>
        /// <param name="strLeaveYn">string</param>
        /// <param name="strConfirmUser">string</param>
        public void SetUserCarLeaveConfirm(string strEmpNo, string strCarNum, string strWorkDt, string strLeaveYn, string strConfirmUser)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_USERCARLEAVECONFIRM_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[1].Value = strCarNum;

                parameters[2] = new MySqlParameter("@pWORKDT", MySqlDbType.VarChar, 8);
                parameters[2].Value = strWorkDt;

                parameters[3] = new MySqlParameter("@pLEAVEYN", MySqlDbType.VarChar, 1);
                parameters[3].Value = strLeaveYn;

                parameters[4] = new MySqlParameter("@pCONFIRMUSER", MySqlDbType.VarChar, 20);
                parameters[4].Value = strConfirmUser;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetUserCarEnKm
        /// <summary>
        /// SetUserCarEnKm
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strWorkDt">string</param>
        /// <param name="strEnkm">string</param>
        /// <param name="strFuels">string</param>
        public void SetUserCarEnKm(string strEmpNo, string strCarNum, string strWorkDt, string strEnkm, string strFuels)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_USERCARENKM_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[1].Value = strCarNum;

                parameters[2] = new MySqlParameter("@pWORKDT", MySqlDbType.VarChar, 8);
                parameters[2].Value = strWorkDt;

                parameters[3] = new MySqlParameter("@pENKM", MySqlDbType.VarChar, 11);
                parameters[3].Value = strEnkm;

                parameters[4] = new MySqlParameter("@pFUELS", MySqlDbType.VarChar, 11);
                parameters[4].Value = strFuels;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetUserCarInfoList_ByEmpUser
        /// <summary>
        /// GetUserCarInfoList_ByEmpUser
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strKtsEmpNo">string</param>
        /// <param name="strEmpNm">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserCarInfoList_ByEmpUser(string strOrgCd, string strKtsEmpNo, string strEmpNm, string strCarNum, string strStDt, string strEnDt)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_USERCARINFOLIST_BYEMPUSER_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pKTSEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strKtsEmpNo;

                parameters[2] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strEmpNm;

                parameters[3] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[3].Value = strCarNum;

                parameters[4] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 8);
                parameters[4].Value = strStDt;

                parameters[5] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 8);
                parameters[5].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetUserOrgInfo
        /// <summary>
        /// GetUserOrgInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserOrgInfo(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_USERORGINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelUserCarMoveInfo
        /// <summary>
        /// DelUserCarMoveInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCarNum">string</param>
        /// <param name="strWorkDt">string</param>
        public void DelUserCarMoveInfo(string strEmpNo, string strCarNum, string strWorkDt)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_USERCARINFOLIST_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pCARNUM", MySqlDbType.VarChar, 20);
                parameters[1].Value = strCarNum;

                parameters[2] = new MySqlParameter("@pWORKDT", MySqlDbType.VarChar, 8);
                parameters[2].Value = strWorkDt;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
