﻿using System;
using System.Linq;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Util;
using KTS.KTSSolution.Framework.SharedType;
using System.Collections.Generic;
using System.Collections;

namespace KTS.KTSSolution.BSL.ExcelImport
{
    public class ExcelImport : IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public ExcelImport()
        {
        }

        #endregion

        #region Method

        #region
        private DataSet GetExcelUploadTarget()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_EXCELUPLOADTARGET_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion 

        #region XlsxImport
        /// <summary>
        /// XlsxImport
        /// </summary>
        /// <returns>bool</returns>
        public bool XlsxImport()
        {
            bool bResult = false;
            TimeStamp timestamp = new TimeStamp();
            DataSet ds = new DataSet();

            try
            {
                timestamp.TimeStampStart();

                string strDir = ConfigurationSettings.AppSettings["XlsxUploadPath"];
                string strCsvTemp = ConfigurationSettings.AppSettings["CsvPath"];

                DirectoryInfo dirinfo = new DirectoryInfo(strDir);

                if (!dirinfo.Exists)
                {
                    bResult = false;
                    timestamp.TimeStampEnd(this, ">> 엑셀 업로드 폴더 없음.");
                }
                else
                {
                    ds = GetExcelUploadTarget();

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string strUploadName = dr["UPLOADNAME"].ToString();
                            string strTargetTable = dr["TARGETTABLE"].ToString();
                            int nStartRow = int.Parse(dr["STARTROW"].ToString());
                            int nColCnt = int.Parse(dr["COLCNT"].ToString());
                            string strDelYn = dr["DELYN"].ToString();
                            string strDelTargetColNm = dr["DELTARGET"].ToString();

                            if (strDelYn.Equals("Y"))
                            {
                                if (strDelTargetColNm.Length == 0)
                                {
                                    {
                                        timestamp.TimeStampEnd(this, "=====오류 발생 >> " + strUploadName + " : 삭제 타겟 컬럼 미지정");

                                        InsExcelUploadHist(strUploadName, "N", strUploadName + " : 엑셀 양식 오류(컬럼수)");
                                        //컬럼수 오류 저장필요
                                        continue;
                                    }
                                }
                            }

                            string strUploadPath = dr["UPLOADPATH"].ToString().Replace("|", "\\");
                            string strDirExcel = strDir + "\\" + strUploadPath;

                            DirectoryInfo dirExcelinfo = new DirectoryInfo(strDirExcel);

                            if (!dirExcelinfo.Exists)
                            {
                                continue;
                            }

                            FileInfo[] xlFiles = dirExcelinfo.GetFiles("*.xlsx", SearchOption.TopDirectoryOnly);
                            
                            foreach (FileInfo file in xlFiles)
                            {

                                bool bValChk = true;
                                int nRow = 0;
                                int nCol = 0;

                                try
                                {
                                    using (ExcelPackage excelPackage = new ExcelPackage(file))
                                    {
                                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                        int endRowNum = worksheet.Dimension.End.Row;

                                        if (nColCnt != worksheet.Dimension.End.Column)
                                        {
                                            timestamp.TimeStampEnd(this, "=====오류 발생 >> " + file.FullName + " : 엑셀 양식 오류(컬럼수)");

                                            InsExcelUploadHist(strUploadName, "N", strUploadName + " : 엑셀 양식 오류(컬럼수)");
                                            //컬럼수 오류 저장필요
                                            continue;
                                        }

                                        string rowVal = "";

                                        int nQuery = (endRowNum / 1000) + 1;
                                        int nVal = 0;

                                        string[] strQuerys = new string[nQuery];

                                        for (int rowNumber = nStartRow; rowNumber <= endRowNum; rowNumber++)
                                        {
                                            nRow = rowNumber;
                                            nCol = 0;

                                            var row = worksheet.Cells[rowNumber, 1, rowNumber, worksheet.Dimension.End.Column];

                                            string strRowVal = "";

                                            //foreach (ExcelRangeBase cell in row)
                                            for (int i = 0; i < row.Columns; i++)
                                            {
                                                nCol += 1;
                                                string strVal = ((object[,])row.Value)[0, i] == null ? "0" : ((object[,])row.Value)[0, i].ToString().Trim();

                                                if (strVal.Equals("-"))
                                                    strVal = strVal.Replace("-", "0");

                                                //이부분에서 데이터 검증 가능
                                                DateTime DateChk;
                                                float fVal = 0;

                                                if (float.TryParse(strVal, out fVal))
                                                {
                                                    //숫자 체크
                                                    strRowVal += "'" + strVal + "',";
                                                }
                                                else if (DateTime.TryParse(strVal, out DateChk))
                                                {
                                                    //날짜체크
                                                    strRowVal += "'" + DateChk.ToString("yyyy-MM-dd") + "',";
                                                }
                                                else
                                                {
                                                    strRowVal += "'" + strVal + "',";
                                                }

                                                if (i == 0)
                                                {
                                                    strRowVal = "'" + rowNumber.ToString() + "'," + strRowVal;
                                                }
                                            }

                                            strRowVal = strRowVal.Substring(0, strRowVal.Length - 1);

                                            if (strRowVal.Replace(",", "").Length == 0)
                                            {
                                                continue;
                                            }

                                            if (rowVal.Length > 0)
                                                rowVal += ",";

                                            rowVal += "(" + strRowVal + ")";

                                            if (((rowNumber - (nStartRow - 1)) % 1000) == 0)
                                            {
                                                strQuerys[nVal] = rowVal;

                                                rowVal = "";
                                                nVal += 1;
                                            }

                                            if (rowNumber == endRowNum)
                                            {
                                                strQuerys[nVal] = rowVal;
                                            }
                                        }

                                        List<CommonData> DataList = new List<CommonData>();

                                        CommonData data = null;

                                        if (strDelYn.Equals("Y"))
                                        {
                                            //대상테이블 중복삭제
                                            string strDelete = " DELETE FROM " + strTargetTable.Replace("_temp", "") + " WHERE " + strDelTargetColNm + " = '" + DateTime.Now.AddMonths(-1).ToString("yyyyMM") + "'";

                                            data = new CommonData(strDelete);
                                            data.commandType = CommandType.Text;

                                            using (DSBase dsBase = new DSBase())
                                            {
                                                dsBase.Inquiry(data);
                                            }
                                        }

                                        string strQuery = "DELETE FROM " + strTargetTable + " WHERE 1 = 1 ; ";
                                        string strInsert = " INSERT INTO " + strTargetTable + " VALUES ";

                                        foreach (string strInsQuery in strQuerys)
                                        {
                                            //임시테이블 데이터 삭제
                                            data = new CommonData(strQuery);
                                            data.commandType = CommandType.Text;

                                            DataList.Add(data);

                                            data = new CommonData(strInsert + strInsQuery);
                                            data.commandType = CommandType.Text;

                                            DataList.Add(data);
                                        }

                                        using (DSBase dsBase = new DSBase())
                                        {
                                            dsBase.MultiRegister(DataList, true);
                                        }

                                        InsExcelUploadHist(strUploadName, "Y", "업로드 성공");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    timestamp.TimeStampEnd(this, "=====오류 발생 >> " + ex.ToString());

                                    string strFailMsg = "";

                                    //업로드 에러 로그 DB저장 추가
                                    if (!bValChk)
                                    {
                                        //유효성오류체크
                                        //nRow, nCol 오류 위치
                                        strFailMsg = nRow.ToString() + " 줄 " + nCol + " 컬럼 데이터 이상.";
                                    }
                                    else
                                    {
                                        strFailMsg = strUploadName + " 업로드 실패";
                                    }

                                    InsExcelUploadHist(strUploadName, "N", strFailMsg);

                                    continue;
                                }
                            }

                            //작업이 끝난 파일 삭제
                            foreach (FileInfo file in xlFiles)
                            {
                                if (file.Exists)
                                {
                                    file.Delete();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                timestamp = new TimeStamp();
                timestamp.TimeStampStart();
                timestamp.TimeStampEnd(this, "=====오류 발생 >> " + ex.ToString());


                //string strFailMsg = "시스템오류 : " + ex.Message.ToString();
                InsExcelUploadHist("엑셀 업로드 시스템 오류", "N", "");
            }            

            return bResult;
        }
        #endregion

        #region ExcelUpLoad
        /// <summary>
        /// ExcelUpLoad
        /// </summary>
        /// <param name="stream">Stream</param>
        public void ExcelUpLoad(string strUploadName, ExcelWorksheet worksheet, string strTableNm)
        {
            try
            {

                int endRowNum = worksheet.Dimension.End.Row;
                int endColNum = worksheet.Dimension.End.Column;

                StringBuilder sb = new StringBuilder();

                sb.Append("INSERT INTO " + strTableNm + " VALUES ");

                if (endRowNum >= 2)
                {
                    //1 = header 
                    //2 >= data
                    for (int rowNumber = 2; rowNumber <= endRowNum; rowNumber++)
                    {
                        string rowVal = "";
                        string strValChk = "";

                        var row = worksheet.Cells[rowNumber, 1, rowNumber, worksheet.Dimension.End.Column];

                        for (int colNumber = 1; colNumber <= endColNum; colNumber++)
                        {
                            string Val = worksheet.Cells[rowNumber, colNumber].Value == null ? "" : worksheet.Cells[rowNumber, colNumber].Value.ToString();
                            //이부분에서 데이터 검증 가능
                            rowVal += "'" + Val + "',";
                            strValChk += Val;
                        }

                        if (strValChk.Length == 0)
                        {
                            continue;
                        }
                        else
                        {
                            rowVal = "(" + rowVal.Substring(0, rowVal.Length - 1) + ") ";

                            if (rowNumber < endRowNum)
                            {
                                rowVal += ",";
                            }
                        }

                        sb.Append(rowVal);
                    }

                    try
                    {
                        string strQuery = sb.ToString();

                        if (strQuery.Substring(strQuery.Length - 1).Equals(","))
                            strQuery = strQuery.Replace("\r\n", "").Substring(0, strQuery.Length - 2);

                        CommonData dataDel = new CommonData("DELETE FROM " + strTableNm + " WHERE 1 = 1;");
                        dataDel.commandType = CommandType.Text;

                        CommonData data = new CommonData(strQuery);
                        data.commandType = CommandType.Text;

                        using (DSBase dsBase = new DSBase())
                        {
                            dsBase.Register(dataDel);

                            dsBase.Register(data);
                        }

                        InsExcelUploadHist(strUploadName, "Y", "업로드 성공");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                string strFailMsg = "업로드 실패 : " + ex.Message.ToString();
                InsExcelUploadHist(strUploadName, "N", strFailMsg);
            }
        }
        #endregion

        #region
        private void InsExcelUploadHist(string strUploadName, string strResultYn, string strMessage)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_EXCELUPLOADHIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pUPLOADNAME", MySqlDbType.VarChar, 200);
                parameters[0].Value = strUploadName;

                parameters[1] = new MySqlParameter("@pRESULTYN", MySqlDbType.VarChar, 1);
                parameters[1].Value = strResultYn;

                parameters[2] = new MySqlParameter("@pMESSAGE", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strMessage;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
