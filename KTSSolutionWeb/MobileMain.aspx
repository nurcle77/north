﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="MobileMain.aspx.cs" Inherits="KTSSolutionWeb.MobileMain" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<div class="page-main">
			<ul>
				<li><a href="#" onclick="window.location.href='/Mobile/UserCarMoveInfo';"></a></li>
				<li><a href="#" onclick="window.location.href='/Mobile/WorkingClothInfo';"></a></li>
				<li><a href="#" onclick="window.location.href='/Mobile/PassRegM';"></a></li>
				<li><a href="#" onclick="window.location.href='/Mobile/PassInfoM';"></a></li>
				<li><a href="#" onclick="window.location.href='/Mobile/PassNewBizInfoM';"></a></li>
				<li><a href="#" onclick="window.location.href='/Mobile/PassCompInfoM';"></a></li>
			</ul>
			<%--<button type="button" onclick="window.location.href='/Default'" title="PC버전보기" class="btn-gray">PC버전보기</button>--%>
		</div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>