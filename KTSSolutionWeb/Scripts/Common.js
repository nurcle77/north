﻿function createForm(url, obj) {
    var form = document.createElement("form");

    form.setAttribute("method", "post");
    form.setAttribute("action", url);

    if (obj != null) {
        for (var name in obj) {
            var valObj = obj[name];
            addInput(form, name, valObj);
        }
    }

    document.body.appendChild(form);

    return form;
}

function addInput(form, name, value) {
    var input = document.createElement("input");

    input.setAttribute("name", name);
    input.setAttribute("type", "hidden");
    input.setAttribute("value", value);
    
    form.appendChild(input);
}

function requestDataForm(url, param, fn_Seccess, fn_Error) {
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        success: function () {
            fn_Seccess;
        },
        error: function () {
            fn_Error;
        }
    });
    
}

function OnlyNumber(event) {
    event = event || window.event;
    var keyid = (event.which) ? event.which : event.keyCode;
    if ((keyid < 48) || (keyid > 57)) {
        return false;
    }
}

function OnlyNumber2(event, obj) {
    var val = obj.value;

    event = event || window.event;
    var keyid = (event.which) ? event.which : event.keyCode;
    if ((keyid >= 48 && keyid <= 57) || keyid == 46) {
        if (keyid == 46) {
            if (val.indexOf(".") < 1) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    }
    else {
        return false;
    }
}

function ReplaceKorean(obj) {
    if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 18 || event.keyCode == 46)
        return;

    obj.value = obj.value.replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, '');
}