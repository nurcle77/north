﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KtsErrorPage.aspx.cs" Inherits="KTSSolutionWeb.KtsErrorPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>kt Service 북부 업무지원시스템</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="wrap">
                    <div class="page-error">
                        <div class="error-table">
                            <div class="error-cell">
                                <div class="error-contents" style="width:70%;height:300px;">
                                    <h2 class="error-logo" style="margin-bottom:50px;">
                                        <img src="/Resource/images/icon_login_logo.png" alt="KT서비스 북부 오피스">
                                    </h2>
                                    <p class="error">네트워크 또는 서비스에 일시적인 오류가 발생하였거나 페이지 주소의 변경 혹은 삭제로 인해 현재 사용할 수 없습니다. </p>
                                    <div class="button-wrapper" style="margin-top:150px">
                                        <asp:Button id="btnRefer" runat="server" OnClick="btnRefer_Click" class="login-btn" Text="이전페이지" BorderStyle="None"></asp:Button>
                                    </div>
                                </div>
                                    <!-- // E : login-contents -->
                            </div>
                            <!-- // E : login-cell -->
                        </div>
                        <!-- // E : login-table -->
                        <p class="copyright">Copyright© 2020 kt service 북부 corp. all rights reserved</p>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
