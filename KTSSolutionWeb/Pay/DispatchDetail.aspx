﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DispatchDetail.aspx.cs" Inherits="KTSSolutionWeb.DispatchDetail" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>실적급 월별 상세조회 - 파견</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        
		<!-- S: windowpop-wrap -->
        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>실적급 월별 상세 - 파견</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <div class="pull-right">
                            <div class="btnset" style="margin-right:10px;">
                                <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                            </div>'
                        </div>
                    </div>
                    <!-- E:list-top -->
			        <!-- S:scrollbox -->
			        <div class="scrollbox" style="height:500px;overflow-y:auto;">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th rowspan="2">No.</th>
                                            <th colspan="3">소속</th>
                                            <th rowspan="2">파견지</th>                                                                                                                                                       
                                            <th rowspan="2">사번</th>
                                            <th rowspan="2">이름</th>
                                            <th colspan="4">매출</th>
                                            <th colspan="4">근무일수</th>
                                            <th rowspan="2">일매출</th>
                                            <th rowspan="2">파견일수</th>
                                            <th rowspan="2">적용매출</th>
                                            <th rowspan="2">포인트</th>
                                            <th rowspan="2">파견기간</th>
                                        </tr>
                                        <tr>
                                            <th>본부</th>
                                            <th>지사</th>
                                            <th>지점</th>
                                            <th>전전전월 매출</th>
                                            <th>전전월 매출</th>
                                            <th>전월 매출</th>
                                            <th>매출 계</th>
                                            <th>전전전월 근무일수</th>
                                            <th>전전월 근무일수</th>
                                            <th>전월 근무일수</th>
                                            <th>근무일수 계</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("NUM") %></td>
                                                    <td><%# Eval("ORGLV2NM") %></td>
                                                    <td><%# Eval("ORGLV3NM") %></td>
                                                    <td><%# Eval("ORGLV4NM") %></td>
                                                    <td><%# Eval("DISPACTH_PLACE") %></td>
                                                    <td><%# Eval("KTSEMPNO") %></td>
                                                    <td><%# Eval("EMPNM") %></td>
                                                    <td><%# Eval("PRE3MONTHSALES") %></td>
                                                    <td><%# Eval("PRE2MONTHSALES") %></td>
                                                    <td><%# Eval("PREMONTHSALES") %></td>
                                                    <td><%# Eval("TOTALSALES") %></td>
                                                    <td><%# Eval("PRE3WORKDAYS") %></td>
                                                    <td><%# Eval("PRE2WORKDAYS") %></td>
                                                    <td><%# Eval("PREWORKDAYS") %></td>
                                                    <td><%# Eval("TOTALWORKDAYS") %></td>
                                                    <td><%# Eval("DAY_SALES") %></td>
                                                    <td><%# Eval("DISPATCHDAYS") %></td>
                                                    <td><%# Eval("APPLYSALES") %></td>
                                                    <td><%# Eval("POINTS") %></td>
                                                    <td><%# Eval("TERM") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="paging" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
			        <!-- E:scrollbox -->
                    <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
                </div>
                <!-- E:datalist -->
            </div>
        </div>
    </form>
</body>
</html>
