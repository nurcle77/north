﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TurnkeyTotalList.aspx.cs" Inherits="KTSSolutionWeb.TurnkeyTotalList" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        window.onload = setChartWidth;

        function setChartWidth() {
            var width = document.documentElement.clientWidth - 180;
            $("#<%= hfChartWidth.ClientID %>").val(width);
        }

        function PopupOrgTree(orgcd, empno) {

            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: SumupMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_BComp", param);

                Popupform.target = "OrgTree_BComp";
                var win = window.open("", "OrgTree_BComp", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function PopupTurnKeyDetail(orgcd, orgnm, sumupmonth, orglv) {

            var param = {
                pORGCD: orgcd,
                pORGNM: orgnm,
                pSUMUPMONTH: sumupmonth,
                pORGLV: orglv
            };
            var nWidth = 1200;
            var nHeight = 760;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Pay/TurnKeyDetail", param);

            Popupform.target = "PopupDetail";
            var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupTurnKeyEventDetail(orgcd, orgnm, sumupmonth, cnt, orglv) {

            if (cnt != '0' && cnt != '0.00') {
                var param = {
                    pORGCD: orgcd,
                    pORGNM: orgnm,
                    pSUMUPMONTH: sumupmonth,
                    pORGLV: orglv
                };
                var nWidth = 1200;
                var nHeight = 760;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var Popupform = createForm("/Pay/TurnKeyEventDetail", param);

                Popupform.target = "PopupDetail";
                var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
            else {
                return;
            }
        }

        function SearchChk() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();
            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SetOrgCode(orgcd) {

            this.focus();

            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelSearch)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>
                <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox" style="margin-right:20px;">
                            <label>조회일자</label>
                            <asp:DropDownList ID="ddlSumupMonth" runat="server" Width="120px" OnSelectedIndexChanged="ddlSumupMonth_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
			            </span>
				        <span class="inpbox" style="margin-right:10px;">
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" style="margin-right:10px;width:40px;height:40px;"/>
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
				        </span>
                        <span class="optionbox">
                            <label>조건</label>
                            <asp:DropDownList ID="ddlValType" runat="server" Width="100px">
                                <asp:ListItem Text="총계" Value="TOTALPRICE" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="턴키금액" Value="SUBTOTAL1"></asp:ListItem>
                                <asp:ListItem Text="추가금액" Value="SUBTOTAL2"></asp:ListItem>
                            </asp:DropDownList>
			            </span>
                        <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlSumupMonth" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
		    </fieldset>
        </div>
        <br />
		<!-- //E:searchbox -->        
                
		<!-- S:datalist -->
        <div class="chartbox">
            <div style="width:100%;height:420px;overflow-x:hidden;overflow-y:hidden;"> <!-- 차트 사이즈 조정 -->
                <asp:HiddenField ID="hfChartWidth" runat="server" />
                <asp:UpdatePanel ID="updPanelChart1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:chart id="chart1" runat="server" Height="420px">
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                    <AxisX>
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineColor="#000" LineDashStyle="Solid" />
                                    </AxisX>
                                    <AxisY IsLabelAutoFit="False" IsMarginVisible="False" LabelAutoFitStyle="None">
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineDashStyle="NotSet"/>
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <Legends>
                                <asp:Legend Name="Legend1"></asp:Legend>
                            </Legends>
                        </asp:chart>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
		<!-- //E:datalist -->

		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>턴키 월별 조회결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
                </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th rowspan="2">No.</th>
                                    <th rowspan="2">조직명</th>
                                    <th rowspan="2" style="border-right:1px solid #dfdfdf;">일자</th>
                                    <th colspan="16" style="border-right:1px solid #dfdfdf;">턴키금액</th>
                                    <th colspan="22" style="border-right:1px solid #dfdfdf;">추가금액</th>
                                    <th rowspan="2">총계</th>
                                </tr>
                                <tr>
                                    <th>개통</th>
                                    <th>AS</th>
                                    <th>FM인건비</th>
                                    <th>10G노트북</th>
                                    <th>자재인건비</th>
                                    <th>고소차렌탈비</th>
                                    <th>다온플랜</th>
                                    <th>사무실</th>
                                    <th>자재창고</th>
                                    <th>샤워장</th>
                                    <th>주차비</th>
                                    <th>고객회선<br />관리실</th>
                                    <th>자가개통<br />운영비</th>
                                    <th>특수지역<br />정산</th>
                                    <th>품질관리평가</th>
                                    <th style="border-right:1px solid #dfdfdf;">소계</th>

                                    <th>C-TYPE<br />이너텔</th>
                                    <th>자가망</th>
                                    <th>기가아이즈<br />(개통)</th>
                                    <th>기가아이즈<br />(수리)</th>
                                    <th>수작업정산<br />(개통)</th>
                                    <th>수작업정산<br />(수리)</th>
                                    <th>초고속품질개선비</th>
                                    <th>기가지니<br />앱페어링차감</th>
                                    <th>IOT신상품</th>
                                    <th>핵심고객care서비스</th>
                                    <th>개통</th>
                                    <th>수리</th>
                                    <th>개통/AS</th>
                                    <th>단말관리</th>
                                    <th>기타</th>
                                    <th>SLA개통</th>
                                    <th>SLA수리</th>
                                    <th>개통/AS물자비</th>
                                    <th>모뎀물자비</th>
                                    <th>이벤트</th>
                                    <th>단말회수</th>
                                    <th style="border-right:1px solid #dfdfdf;">소계</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr id="tr" runat="server">
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td>
                                                <asp:Label ID="lblSumupMonth" runat="server" Text='<%# Eval("SUMUPMONTH") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("OPENPRICE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("ASPRICE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("FM") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("LAPTOP10G") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("MATERIAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("RENTAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("DAONPLAN") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("OFFICE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("MATERIALSTORED") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("SHOWERROOM") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("PARKING") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("CUSTLINE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("SELFOPEN") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("SPECIALPLACECOST") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("QUALITYMGMT") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ORGLV")%>');"><%# Eval("SUBTOTAL1") %></a>
                                            </td>
                                            
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("CTYPE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("SELFLINE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("GEYEOPEN") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("GEYEAS") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("MANUALOPEN") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("MANUALAS") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("IMPROVE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("APP_PAIRRING") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("IOT") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("VIPCARESVC") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("OPENCOST") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("ASCOST") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("OPENASCOST") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("TERMINAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("ETC") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("SLAOPEN") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("SLAAS") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("OPENASMATERIAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("MODEM") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyEventDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("EVENTSUM") %>');"><%# Eval("EVENTSUM") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("RETURNTERMINAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("SUBTOTAL2") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupTurnKeyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>');"><%# Eval("TOTALPRICE") %></a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
