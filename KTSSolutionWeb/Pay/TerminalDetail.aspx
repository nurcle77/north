﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TerminalDetail.aspx.cs" Inherits="KTSSolutionWeb.TerminalDetail" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>실적급 월별 상세조회 - 단말</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        
		<!-- S: windowpop-wrap -->
        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>실적급 월별 상세 - 단말</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <div class="pull-right">
                            <div class="btnset" style="margin-right:10px;">
                                <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                            </div>'
                        </div>
                    </div>
                    <!-- E:list-top -->
			        <!-- S:scrollbox -->
			        <div class="scrollbox" style="height:500px;overflow-y:auto;">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <th>No.</th>
                                        <th>현황월</th>
                                        <th>국사코드</th>
                                        <th>지점</th>
                                        <th>국사</th>
                                        <th>법인코드</th>
                                        <th>협력사코드</th>
                                        <th>법인명</th>
                                        <th>구분</th>
                                        <th>kt사번</th>
                                        <th>이름</th>
                                        <th>임대단말 제외건</th>
                                        <th>임대단말 정산건</th>
                                        <th>임대단말 정산액</th>
                                        <th>안테나 제외건</th>
                                        <th>안테나 회수건</th>
                                        <th>안테나 회수금액(정산액 아님)</th>
                                        <th>판매단말 제외건</th>
                                        <th>판매단말 정산건</th>
                                        <th>판매단말 정산액</th>
                                        <th>정산건수</th>
                                        <th>정산금액</th>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("NUM") %></td>
                                                    <td><%# Eval("STATEMONTH") %></td>
                                                    <td><%# Eval("OFFICECD") %></td>
                                                    <td><%# Eval("ORGLV4NM") %></td>
                                                    <td><%# Eval("OFFICENM") %></td>
                                                    <td><%# Eval("CORCD") %></td>
                                                    <td><%# Eval("COMPANYCD") %></td>
                                                    <td><%# Eval("CORNM") %></td>
                                                    <td><%# Eval("GBN") %></td>
                                                    <td><%# Eval("EMPNO") %></td>
                                                    <td><%# Eval("EMPNM") %></td>
                                                    <td><%# Eval("RENT_OUTCNT") %></td>
                                                    <td><%# Eval("RENT_CALCNT") %></td>
                                                    <td><%# Eval("RENT_CALPRICE") %></td>
                                                    <td><%# Eval("ANTENNA_OUTCNT") %></td>
                                                    <td><%# Eval("ANTENNA_RETURNCNT") %></td>
                                                    <td><%# Eval("ANTENNA_RETURNPRICE") %></td>
                                                    <td><%# Eval("SELL_OUTCNT") %></td>
                                                    <td><%# Eval("SELL_CALCNT") %></td>
                                                    <td><%# Eval("SELL_CALPRICE") %></td>
                                                    <td><%# Eval("TOT_CALCNT") %></td>
                                                    <td><%# Eval("TOT_CALPRICE") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="paging" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
			        <!-- E:scrollbox -->
                    <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
                </div>
                <!-- E:datalist -->
            </div>
        </div>
    </form>
</body>
</html>
