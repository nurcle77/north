﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;
using KTS.KTSSolution.BSL.Pay;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Security;

namespace KTSSolutionWeb
{
    public partial class BCompanyState : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private string ORGLV
        {
            get
            {
                if (ViewState["ORGLV"] != null)
                    return ViewState["ORGLV"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGLV"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();


                GetSumupMonth();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;
                }

                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strEmpNo, string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperBCompanyOrgMonthList(strEmpNo, strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetSumupMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (BCompanyPayMgmt mgmt = new BCompanyPayMgmt())
                {
                    ds = mgmt.GetSumupMonth();
                }

                ddlSumupMonth.Items.Clear();
                ddlSumupMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlSumupMonth.Items.Add(new ListItem(ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString(), ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hfOrgCd.Value;
                string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");

                if (strSumMonth.Length > 0 && strOrgCd.Length > 0)
                {
                    using (BCompanyPayMgmt mgmt = new BCompanyPayMgmt())
                    {
                        ds = mgmt.GetBCompanyState(strOrgCd, strSumMonth);
                    }

                    paging.PageNumber = 0;
                    paging.PageSize = 30;

                    paging.Dt = null;

                    if (ds.Tables.Count > 0)
                    {
                        paging.TotalRows = ds.Tables[0].Rows.Count;
                        paging.Dt = ds.Tables[0];

                        ORGLV = ds.Tables[1].Rows[0]["ORGLV"].ToString();
                    }

                    paging.SetPagingDataList();

                    SetChart();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetChart()
        {
            try
            {
                try
                {
                    chart1.Width = Unit.Parse(hfChartWidth.Value + "px");
                }
                catch
                {
                    chart1.Width = Unit.Parse("2000px");
                }

                string strValType = this.ddlValType.SelectedValue;
                string strValName = this.ddlValType.SelectedItem.Text;

                if (strValType.Equals("ALL"))
                {
                    Color[] colors = { ColorTranslator.FromHtml("#00c0a9")
                                     , ColorTranslator.FromHtml("#029f88")
                                     , ColorTranslator.FromHtml("#8ac8fc")
                                     , ColorTranslator.FromHtml("#6ad7c4")
                                     , ColorTranslator.FromHtml("#ed8a1d") //ee1c25
                                     , ColorTranslator.FromHtml("#d5b911") //ed8a1d
                                     , ColorTranslator.FromHtml("#985105")};

                    string[] legendText = { "개통", "AS", "보정", "단말", "공사", "도서", "기가아이즈" };

                    chart1.Legends[0].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;

                    for (int j = 0; j < 7; j++)
                    {
                        Series series = new Series("series" + j.ToString());
                        series.ChartType = SeriesChartType.StackedColumn;
                        series.YValueType = ChartValueType.Double;
                        series.BorderWidth = 1;
                        series.Color = colors[j];
                        series.LegendText = legendText[j];
                        series.SmartLabelStyle.Enabled = false;

                        chart1.Series.Add(series);
                    }

                    for (int i = 0; i < paging.Dt.Rows.Count; i++)
                    {
                        if (!ORGLV.Equals("3") && i == 0)
                        {
                            continue;
                        }

                        string[] tooltip = { "개통 : " + paging.Dt.Rows[i]["SALES_OPEN"].ToString()
                                       , "AS : " + paging.Dt.Rows[i]["SALES_AS"].ToString()
                                       , "보정 : " + paging.Dt.Rows[i]["SALES_REVICE"].ToString()
                                       , "단말 : " + paging.Dt.Rows[i]["SALES_TERMINAL"].ToString()
                                       , "공사 : " + paging.Dt.Rows[i]["SALES_CONST"].ToString()
                                       , "도서 : " + paging.Dt.Rows[i][ "SALES_ISLAND"].ToString()
                                       , "기가아이즈 : " + paging.Dt.Rows[i]["SALES_GEYE"].ToString() };


                        double[] data = { double.Parse(paging.Dt.Rows[i]["SALES_OPEN"].ToString().Replace(",", ""))
                                    , double.Parse(paging.Dt.Rows[i]["SALES_AS"].ToString().Replace(",", ""))
                                    , double.Parse(paging.Dt.Rows[i]["SALES_REVICE"].ToString().Replace(",", ""))
                                    , double.Parse(paging.Dt.Rows[i]["SALES_TERMINAL"].ToString().Replace(",", ""))
                                    , double.Parse(paging.Dt.Rows[i]["SALES_CONST"].ToString().Replace(",", ""))
                                    , double.Parse(paging.Dt.Rows[i]["SALES_ISLAND"].ToString().Replace(",", ""))
                                    , double.Parse(paging.Dt.Rows[i]["SALES_GEYE"].ToString().Replace(",", "")) };

                        for (int j = 0; j < 7; j++)
                        {
                            chart1.Series[j].Points.AddY(data[j]);

                            if (data[j] != 0)
                            {
                                if (ORGLV.Equals("3"))
                                {
                                    chart1.Series[j].Points[i].ToolTip = tooltip[j];
                                    chart1.Series[j].Points[i].Label = tooltip[j];
                                    chart1.Series[j].Points[i].LabelForeColor = Color.Black;//Color.White;
                                }
                                else
                                {
                                    chart1.Series[j].Points[i - 1].ToolTip = tooltip[j];
                                    chart1.Series[j].Points[i - 1].Label = tooltip[j];
                                    chart1.Series[j].Points[i - 1].LabelForeColor = Color.Black;//Color.White;
                                }
                                chart1.Series[j].SmartLabelStyle.Enabled = true;
                            }
                        }

                        if (ORGLV.Equals("3"))
                        {
                            chart1.Series[0].Points[i].AxisLabel = paging.Dt.Rows[i]["ORGNM"].ToString();
                        }
                        else
                        {
                            chart1.Series[0].Points[i - 1].AxisLabel = paging.Dt.Rows[i]["ORGNM"].ToString();
                        }
                    }
                }
                else
                {
                    chart1.Legends.Remove(chart1.Legends[0]);

                    Series series = new Series("series");
                    series.ChartType = SeriesChartType.StackedColumn;
                    series.YValueType = ChartValueType.Double;
                    series.BorderWidth = 1;
                    series.SmartLabelStyle.Enabled = false;

                    chart1.Series.Add(series);


                    for (int i = 0; i < paging.Dt.Rows.Count; i++)
                    {
                        if (!ORGLV.Equals("3") && i == 0)
                        {
                            continue;
                        }

                        chart1.Series[0].Points.AddY(double.Parse(paging.Dt.Rows[i][strValType].ToString()));

                        if (ORGLV.Equals("3"))
                        {
                            chart1.Series[0].Points[i].AxisLabel = paging.Dt.Rows[i]["ORGNM"].ToString();
                        }
                        else
                        {
                            chart1.Series[0].Points[i - 1].AxisLabel = paging.Dt.Rows[i]["ORGNM"].ToString();
                        }

                        if (!paging.Dt.Rows[i][strValType].ToString().Equals("0") && !paging.Dt.Rows[i][strValType].ToString().Equals("0.00"))
                        {
                            if (ORGLV.Equals("3"))
                            {
                                chart1.Series[0].Points[i].ToolTip = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                                chart1.Series[0].Points[i].Label = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                                chart1.Series[0].Points[i].LabelForeColor = Color.Black;//Color.White;
                                chart1.Series[0].Points[i].Color = double.Parse(paging.Dt.Rows[i][strValType].ToString()) > 0 ? ColorTranslator.FromHtml("#00c0a9") : ColorTranslator.FromHtml("#ed8a1d");
                            }
                            else
                            {
                                chart1.Series[0].Points[i - 1].ToolTip = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                                chart1.Series[0].Points[i - 1].Label = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                                chart1.Series[0].Points[i - 1].LabelForeColor = Color.Black;//Color.White;
                                chart1.Series[0].Points[i - 1].Color = double.Parse(paging.Dt.Rows[i][strValType].ToString()) > 0 ? ColorTranslator.FromHtml("#00c0a9") : ColorTranslator.FromHtml("#ed8a1d");
                            }
                            chart1.Series[0].SmartLabelStyle.Enabled = true;
                        }
                    }
                }
                    
                chart1.ChartAreas[0].AxisX.Interval = 1;
                chart1.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;

                chart1.ChartAreas[0].AxisY.Crossing = 0;

                updPanelChart1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[20];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGFULLNM";
            ArrHeader[3] = "SUMUPMONTH";                                            
            ArrHeader[4] = "SALES_TOTAL";
            ArrHeader[5] = "ALLOWANCE_TOTAL";
            ArrHeader[6] = "SALES_OPEN";
            ArrHeader[7] = "ALLOWANCE_OPEN";
            ArrHeader[8] = "SALES_AS";
            ArrHeader[9] = "ALLOWANCE_AS";
            ArrHeader[10] = "SALES_REVICE";
            ArrHeader[11] = "ALLOWANCE_REVICE";
            ArrHeader[12] = "SALES_TERMINAL";
            ArrHeader[13] = "ALLOWANCE_TERMINAL";
            ArrHeader[14] = "SALES_CONST";
            ArrHeader[15] = "ALLOWANCE_CONST";
            ArrHeader[16] = "SALES_ISLAND";
            ArrHeader[17] = "ALLOWANCE_ISLAND";                                            
            ArrHeader[18] = "SALES_GEYE";
            ArrHeader[19] = "ALLOWANCE_GEYE";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[12];
            
            ArrHeader[0] = "No.";
            ArrHeader[1] = "조직코드";
            ArrHeader[2] = "조직명";
            ArrHeader[3] = "일자";
            ArrHeader[4] = "합계";
            ArrHeader[5] = "개통";
            ArrHeader[6] = "AS";
            ArrHeader[7] = "보정";
            ArrHeader[8] = "단말";
            ArrHeader[9] = "공사";
            ArrHeader[10] = "도서";
            ArrHeader[11] = "기가아이즈";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[20];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "조직코드";
            ArrHeader[2] = "조직명";
            ArrHeader[3] = "일자";
            ArrHeader[4] = "공사/";
            ArrHeader[5] = "정산/";
            ArrHeader[6] = "공사/";
            ArrHeader[7] = "정산/";
            ArrHeader[8] = "공사/";
            ArrHeader[9] = "정산/";
            ArrHeader[10] = "공사/";
            ArrHeader[11] = "정산/";
            ArrHeader[12] = "공사/";
            ArrHeader[13] = "정산/";
            ArrHeader[14] = "공사/";
            ArrHeader[15] = "정산/";
            ArrHeader[16] = "공사/";
            ArrHeader[17] = "정산/";
            ArrHeader[18] = "공사/";
            ArrHeader[19] = "정산/";

            return ArrHeader;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        if (rptResult.Items.Count > 0)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult.Items[0].FindControl("tr");

                            if (!ORGLV.Equals("3") && paging.PageNumber == 0)
                            {
                                tr.Style.Add("background-color", "#e6e6e6");
                            }
                        }
                    }

                    updPanel1.Update();

                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }


        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {

                    string strOrgCd = this.hfOrgCd.Value;
                    string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");

                    if (strSumMonth.Length > 0 && strOrgCd.Length > 0)
                    {
                        using (BCompanyPayMgmt mgmt = new BCompanyPayMgmt())
                        {
                            ds = mgmt.GetBCompanyState(strOrgCd, strSumMonth);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                        return;
                    }
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();
                    excel.MergeCell = "4/2,5/2,6/2,7/2,8/2,9/2,10/2,11/2";

                    excel.ExcelDownLoad(this.Page, dt, "B협력사 정산 결과 조회");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlSumupMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = ddlSumupMonth.SelectedValue.Replace("-", "");

                if (!strMonth.Equals(""))
                {
                    if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                    {
                        SetOrgList("", strMonth);
                    }
                    else
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    using (KTSUser user = new KTSUser())
                    {
                        ds = user.GetUserViewOrgMonth(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strOrgCd = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";

                            strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        }

                        this.hfOrgCd.Value = strOrgCd;
                        this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                    }
                    else
                    {
                        this.hfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}