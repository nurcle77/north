﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Data;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Pay;

namespace KTSSolutionWeb
{
    public partial class DispatchDetail : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }
        private string SUMUPMONTH
        {
            get
            {
                if (ViewState["SUMUPMONTH"] != null)
                    return ViewState["SUMUPMONTH"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SUMUPMONTH"] = value;
            }
        }

        private string SELECTLV
        {
            get
            {
                if (ViewState["SELECTLV"] != null)
                    return ViewState["SELECTLV"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SELECTLV"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                    SUMUPMONTH = Request.Form["pSUMUPMONTH"] == null ? "" : Request.Form["pSUMUPMONTH"].ToString().Replace("-", "");
                    SELECTLV = Request.Form["pSELECTLV"] == null ? "" : Request.Form["pSELECTLV"].ToString().Replace("-", "");

                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (MeritPayMgmt mgmt = new MeritPayMgmt())
                {
                    ds = mgmt.GetMeritPayDispatchDetailList(ORGCD, SUMUPMONTH, SELECTLV);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn(DataTable dt)
        {
            string[] ArrHeader = new string[dt.Columns.Count];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ArrHeader[i] = dt.Columns[i].ColumnName;
            }
            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {

            string[] ArrHeader = new string[12];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "소속";
            ArrHeader[2] = "파견지";
            ArrHeader[3] = "사번";
            ArrHeader[4] = "이름";
            ArrHeader[5] = "매출";
            ArrHeader[6] = "근무일수";
            ArrHeader[7] = "일매출";
            ArrHeader[8] = "파견일수";
            ArrHeader[9] = "적용매출";
            ArrHeader[10] = "포인트";
            ArrHeader[11] = "파견기간";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[20];
            
            ArrHeader[0] = "No.";
            ArrHeader[1] = "본부";
            ArrHeader[2] = "지사";
            ArrHeader[3] = "지점";
            ArrHeader[4] = "파견지";
            ArrHeader[5] = "사번";
            ArrHeader[6] = "이름";
            ArrHeader[7] = "전전전월 매출";
            ArrHeader[8] = "전전월 매출";
            ArrHeader[9] = "전월 매출";
            ArrHeader[10] = "매출 계";
            ArrHeader[11] = "전전전월 근무일수";
            ArrHeader[12] = "전전월 근무일수";
            ArrHeader[13] = "전월 근무일수";
            ArrHeader[14] = "근무일수 계";
            ArrHeader[15] = "일매출";
            ArrHeader[16] = "파견일수";
            ArrHeader[17] = "적용매출";
            ArrHeader[18] = "포인트";
            ArrHeader[19] = "파견기간";

            return ArrHeader;
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    using (MeritPayMgmt mgmt = new MeritPayMgmt())
                    {
                        ds = mgmt.GetMeritPayDispatchDetailList(ORGCD, SUMUPMONTH, SELECTLV);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn(dt);
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();
                    excel.MergeCell = "1/3,5/4,6/4";

                    excel.ExcelDownLoad(this.Page, dt, "실적급 월별 상세조회-파견");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }

        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}