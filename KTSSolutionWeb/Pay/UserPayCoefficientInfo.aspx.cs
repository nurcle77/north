﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;
using KTS.KTSSolution.BSL.Pay;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Globalization;

namespace KTSSolutionWeb
{
    public partial class UserPayCoefficientInfo : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private bool bValChk
        {
            get
            {
                if (ViewState["bValChk"] != null)
                    return (bool)ViewState["bValChk"];
                else
                    return false;
            }
            set
            {
                ViewState["bValChk"] = value;
            }
        }

        private double MinVal
        {
            get
            {
                if (ViewState["MinVal"] != null)
                    return (double)ViewState["MinVal"];
                else
                    return 0;
            }
            set
            {
                ViewState["MinVal"] = value;
            }
        }

        private double MaxVal
        {
            get
            {
                if (ViewState["MaxVal"] != null)
                    return (double)ViewState["MaxVal"];
                else
                    return 0;
            }
            set
            {
                ViewState["MaxVal"] = value;
            }
        }

        private double LimitVal
        {
            get
            {
                if (ViewState["LimitVal"] != null)
                    return (double)ViewState["LimitVal"];
                else
                    return 0;
            }
            set
            {
                ViewState["LimitVal"] = value;
            }
        }

        private bool bCalc 
        {
            get
            {
                if (ViewState["bCalc"] != null)
                    return (bool)ViewState["bCalc"];
                else
                    return false;
            }
            set
            {
                ViewState["bCalc"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetSumupMonth();

                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");

                updPanelOrg.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetSumupMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                {
                    ds = mgmt.GetSumupMonth();
                }

                ddlSumupMonth.Items.Clear();
                ddlSumupMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlSumupMonth.Items.Add(new ListItem(ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString(), ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSumupMonth.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;
                bCalc = false;
                bValChk = false;

                MinVal = 0;
                MaxVal = 0;
                LimitVal = 0.5;

                bool bValidationChk = false;
                bool bSum = false;
                string strOrgCd = this.hdfOrgCd.Value;
                string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");

                DataRow[] dr = DtOrgList.Select(string.Format("ORGCD = '{0}'", strOrgCd));

                if (dr.Length > 0)
                {
                    if (dr[0]["ORGLEVEL"].ToString().Equals("3") && strOrgCd.Length == 6)
                        bSum = true;

                    if (bSum && (Session["AUTHID"].ToString().Equals("AUTH0001") || Session["AUTHID"].ToString().Equals("AUTH0002") 
                                || Session["AUTHID"].ToString().Equals("AUTH0003") || Session["AUTHID"].ToString().Equals("AUTH0004")))
                    {
                        using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                        {
                            ds = mgmt.GetCoefficientLimitVal(strSumMonth);
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            MinVal = double.Parse(ds.Tables[0].Rows[0]["MINVAL"].ToString());
                            MaxVal = double.Parse(ds.Tables[0].Rows[0]["MAXVAL"].ToString());
                            LimitVal = double.Parse(ds.Tables[0].Rows[0]["SUMVAL"].ToString());

                            bValChk = true;

                            if (ds.Tables[0].Rows[0]["UPDATEYN"].ToString().Equals("Y"))
                            {
                                bCalc = true;
                            }
                            else
                            {
                                bCalc = false;
                            }
                        }
                        else
                        {
                            MinVal = 0;
                            MaxVal = 0;
                            LimitVal = 0.5;

                            bValChk = false;
                            bCalc = true;
                        }
                    }
                }

                if (strSumMonth.Length > 0)
                {
                    bValidationChk = true;
                }

                if (bValidationChk)
                {
                    using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                    {
                        ds = mgmt.GetUserPayConficientList(strOrgCd, strSumMonth);
                    }

                    paging.PageNumber = 0;

                    if (bCalc)
                    {
                        paging.PageSize = 1000;
                    }
                    else
                    {
                        paging.PageSize = 30;
                    }

                    paging.Dt = null;

                    if (ds.Tables.Count > 0)
                    {
                        if (bSum)
                        {
                            DataTable dt = new DataTable();

                            dt = ds.Tables[0].Copy();

                            int iNum = 0;
                            double dOpenPoint = 0;
                            double dAsPoint = 0;
                            double dOpenPoint_C = 0;
                            double dAsPoint_C = 0;
                            double dSumPoint = 0;
                            double dManual = 0;
                            double dDispatch = 0;
                            double dEvent1 = 0;
                            double dEvent2 = 0;
                            double dEvent3 = 0;
                            int iSumprice = 0;

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                iNum = int.Parse(dt.Rows[i]["NUM"].ToString());
                                dOpenPoint += double.Parse(dt.Rows[i]["OPENPOINT"].ToString());
                                dAsPoint += double.Parse(dt.Rows[i]["ASPOINT"].ToString());
                                dOpenPoint_C += double.Parse(dt.Rows[i]["OPENPOINT_C"].ToString());
                                dAsPoint_C += double.Parse(dt.Rows[i]["ASPOINT_C"].ToString());
                                dManual += double.Parse(dt.Rows[i]["MANUAL"].ToString());
                                dDispatch += double.Parse(dt.Rows[i]["DISPATCH"].ToString());
                                dEvent1 += double.Parse(dt.Rows[i]["EVENT1"].ToString());
                                dEvent2 += double.Parse(dt.Rows[i]["EVENT2"].ToString());
                                dEvent3 += double.Parse(dt.Rows[i]["EVENT3"].ToString());
                            }

                            dSumPoint = dOpenPoint_C + dAsPoint_C;
                            iSumprice = (int)(dOpenPoint_C + dAsPoint_C) * 31000;
                            DataRow drNew = dt.NewRow();

                            drNew["NUM"] = iNum + 1;
                            drNew["ORGFULLNM"] = "합계";
                            drNew["SUMUPMONTH"] = "";
                            drNew["EMPNO"] = "";
                            drNew["KTSEMPNO"] = "";
                            drNew["EMPNM"] = "";
                            drNew["OPENPOINT"] = dOpenPoint;
                            drNew["ASPOINT"] = dAsPoint;
                            drNew["OPEN_CONFFICIENT"] = "";
                            drNew["AS_CONFFICIENT"] = "";
                            drNew["OPENPOINT_C"] = dOpenPoint_C;
                            drNew["ASPOINT_C"] = dAsPoint_C;
                            drNew["MANUAL"] = dManual;
                            drNew["DISPATCH"] = dDispatch;
                            drNew["EVENT1"] = dEvent1;
                            drNew["EVENT2"] = dEvent2;
                            drNew["EVENT3"] = dEvent3;
                            drNew["SUMPOINT"] = dSumPoint;
                            drNew["SUMPRICE"] = iSumprice;

                            dt.Rows.Add(drNew);

                            paging.TotalRows = dt.Rows.Count;
                            paging.Dt = dt;
                        }
                        else
                        {
                            paging.TotalRows = ds.Tables[0].Rows.Count;
                            paging.Dt = ds.Tables[0];
                        }
                    }

                    paging.SetPagingDataList();

                    if (bCalc)
                    {
                        divOpen.Visible = true;
                        divAs.Visible = true;
                        divBtn.Visible = true;
                        divCalYn.Visible = false;
                    }
                    else
                    {
                        divOpen.Visible = false;
                        divAs.Visible = false;
                        divBtn.Visible = false;

                        if (bSum)
                        {
                            divCalYn.Visible = true;
                        }
                        else
                        {
                            divCalYn.Visible = false;
                        }
                    }

                    txbOpenPointC.Text = "";
                    txbAsPointC.Text = "";

                    txbAsPointC.Style.Remove("background-color");
                    txbOpenPointC.Style.Remove("background-color");

                    this.hfCalcResult.Value = "";

                    this.hfMinVal.Value = MinVal.ToString();
                    this.hfMaxVal.Value = MaxVal.ToString();
                    this.hfLimitVal.Value = LimitVal.ToString();

                    updPanelCalc.Update();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[20];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGFULLNM";
            ArrHeader[3] = "SUMUPMONTH";
            ArrHeader[4] = "EMPNO";
            ArrHeader[5] = "KTSEMPNO";
            ArrHeader[6] = "EMPNM";
            ArrHeader[7] = "OPENPOINT";
            ArrHeader[8] = "ASPOINT";
            ArrHeader[9] = "OPEN_CONFFICIENT";
            ArrHeader[10] = "AS_CONFFICIENT";
            ArrHeader[11] = "OPENPOINT_C";
            ArrHeader[12] = "ASPOINT_C";
            ArrHeader[13] = "MANUAL";
            ArrHeader[14] = "DISPATCH";
            ArrHeader[15] = "EVENT1";
            ArrHeader[16] = "EVENT2";
            ArrHeader[17] = "EVENT3";
            ArrHeader[18] = "SUMPOINT";
            ArrHeader[19] = "SUMPRICE";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[20];

            ArrHeader[0] = "No";
            ArrHeader[1] = "소속코드";
            ArrHeader[2] = "소속";
            ArrHeader[3] = "일자";
            ArrHeader[4] = "사번";
            ArrHeader[5] = "KTS사번";
            ArrHeader[6] = "이름";
            ArrHeader[7] = "조정전 개통";
            ArrHeader[8] = "조정전 AS";
            ArrHeader[9] = "개통 구역계수";
            ArrHeader[10] = "AS 구역계수";
            ArrHeader[11] = "조정후 개통";
            ArrHeader[12] = "조정후 AS";
            ArrHeader[13] = "수작업";
            ArrHeader[14] = "파견";
            ArrHeader[15] = "EVENT1";
            ArrHeader[16] = "EVENT2";
            ArrHeader[17] = "EVENT3";
            ArrHeader[18] = "합계";
            ArrHeader[19] = "매출액";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult.Items[i].FindControl("tr");

                            Label lblOpenPoint = (Label)rptResult.Items[i].FindControl("lblOpenPoint");
                            Label lblASPoint = (Label)rptResult.Items[i].FindControl("lblASPoint");
                            Label lblOpenConfficient = (Label)rptResult.Items[i].FindControl("lblOpenConfficient");
                            Label lblAsConfficient = (Label)rptResult.Items[i].FindControl("lblAsConfficient");
                            TextBox txbOpenConfficient = (TextBox)rptResult.Items[i].FindControl("txbOpenConfficient");
                            TextBox txbAsConfficient = (TextBox)rptResult.Items[i].FindControl("txbAsConfficient");

                            Label lblSumupMonth = (Label)rptResult.Items[i].FindControl("lblSumupMonth");

                            if (bCalc)
                            {
                                if (lblSumupMonth.Text.Length > 0)
                                {
                                    tr.Style.Remove("background-color");

                                    lblOpenConfficient.Visible = false;
                                    lblAsConfficient.Visible = false;
                                    txbOpenConfficient.Visible = true;
                                    txbAsConfficient.Visible = true;
                                }
                                else
                                {
                                    tr.Style.Add("background-color", "#e6e6e6");

                                    lblOpenConfficient.Visible = true;
                                    lblAsConfficient.Visible = true;
                                    txbOpenConfficient.Visible = false;
                                    txbAsConfficient.Visible = false;

                                    txbOpenPoint.Text = lblOpenPoint.Text;
                                    txbAsPoint.Text = lblASPoint.Text;
                                }
                            }
                            else
                            {
                                if (lblSumupMonth.Text.Length > 0)
                                {
                                    tr.Style.Remove("background-color");
                                }
                                else
                                {
                                    tr.Style.Add("background-color", "#e6e6e6");
                                }

                                lblOpenConfficient.Visible = true;
                                lblAsConfficient.Visible = true;
                                txbOpenConfficient.Visible = false;
                                txbAsConfficient.Visible = false;
                            }
                        }
                    }
                    
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            try
            {
                string strResult = "";

                if (bCalc)
                {
                    if (rptResult.Items.Count > 0)
                    {
                        double dOpenPoint = double.Parse(txbOpenPoint.Text);
                        double dAsPoint = double.Parse(txbAsPoint.Text);

                        double dOpenMinChk = dOpenPoint - (dOpenPoint * (LimitVal * 0.01));
                        double dOpenMaxChk = dOpenPoint + (dOpenPoint * (LimitVal * 0.01));

                        double dAsMinChk = dAsPoint - (dAsPoint * (LimitVal * 0.01));
                        double dAsMaxChk = dAsPoint + (dAsPoint * (LimitVal * 0.01));

                        double dOpenPoint_C = 0;
                        double dAsPoint_C = 0;

                        for (int i = 0; i < rptResult.Items.Count - 1; i++)
                        {
                            Label lblOpenPoint = (Label)rptResult.Items[i].FindControl("lblOpenPoint");
                            Label lblASPoint = (Label)rptResult.Items[i].FindControl("lblASPoint");
                            TextBox txbOpenConfficient = (TextBox)rptResult.Items[i].FindControl("txbOpenConfficient");
                            TextBox txbAsConfficient = (TextBox)rptResult.Items[i].FindControl("txbAsConfficient");

                            double OPEN = 0;
                            double AS = 0;

                            double.TryParse(txbOpenConfficient.Text, out OPEN);
                            double.TryParse(txbAsConfficient.Text, out AS);

                            if (bValChk && MinVal > 0 && MaxVal > 0)
                            {
                                if ((MinVal > OPEN) || (MaxVal < OPEN) || (MinVal > AS) || (MaxVal < AS))
                                {
                                    strResult = "V";
                                    hfCalcResult.Value = strResult;
                                    updPanelCalc.Update();

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('구역계수는 최소 " + MinVal + " 이상, 최대 " + MaxVal + " 이하 여야 합니다.');", true);

                                    return;
                                }
                            }

                            dOpenPoint_C += double.Parse(lblOpenPoint.Text) * OPEN;
                            dAsPoint_C += double.Parse(lblASPoint.Text) * AS;
                        }

                        txbOpenPointC.Text = dOpenPoint_C.ToString();
                        txbAsPointC.Text = dAsPoint_C.ToString();

                        if ((dOpenPoint_C >= dOpenMinChk && dOpenPoint_C <= dOpenMaxChk) && (dAsPoint_C >= dAsMinChk && dAsPoint_C <= dAsMaxChk))
                        {
                            strResult = "T";
                        }
                        else
                        {
                            strResult = "F";
                        }

                        if (dOpenPoint_C >= dOpenMinChk && dOpenPoint_C <= dOpenMaxChk)
                        {
                            txbOpenPointC.Style.Remove("background-color");
                            txbOpenPointC.Style.Add("background-color", "#00b3ff");
                        }
                        else
                        {
                            txbOpenPointC.Style.Remove("background-color");
                            txbOpenPointC.Style.Add("background-color", "red");
                        }

                        if (dAsPoint_C >= dAsMinChk && dAsPoint_C <= dAsMaxChk)
                        {
                            txbAsPointC.Style.Remove("background-color");
                            txbAsPointC.Style.Add("background-color", "#00b3ff");
                        }
                        else
                        {
                            txbAsPointC.Style.Remove("background-color");
                            txbAsPointC.Style.Add("background-color", "red");
                        }
                    }
                    else
                    {
                        strResult = "X";
                    }
                }
                else
                {
                    strResult = "E";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }

                hfCalcResult.Value = strResult;
                updPanelCalc.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnReg_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfCalcResult.Value.Equals("T"))
                {
                    List<string> lstData = new List<string>();

                    for (int i = 0; i < rptResult.Items.Count; i++)
                    {
                        Label lblSumupMonth = (Label)rptResult.Items[i].FindControl("lblSumupMonth");
                        Label lblIdmsEmpNo = (Label)rptResult.Items[i].FindControl("lblIdmsEmpNo");

                        string strSumupMonth = lblSumupMonth.Text;
                        string strEmpNo = lblIdmsEmpNo.Text;


                        if (strSumupMonth.Length > 0 && strEmpNo.Length > 0)
                        {
                            Label lblOpenPoint = (Label)rptResult.Items[i].FindControl("lblOpenPoint");
                            Label lblASPoint = (Label)rptResult.Items[i].FindControl("lblASPoint");
                            TextBox txbOpenConfficient = (TextBox)rptResult.Items[i].FindControl("txbOpenConfficient");
                            TextBox txbAsConfficient = (TextBox)rptResult.Items[i].FindControl("txbAsConfficient");

                            double dOpenPoint = 0;
                            double dOpenConff = 0;

                            double.TryParse(lblOpenPoint.Text, out dOpenPoint);
                            double.TryParse(txbOpenConfficient.Text, out dOpenConff);

                            string strOpenConfficient = string.Format("{0:0.00000}", dOpenConff);
                            string strOpenPoint_C = string.Format("{0:0.00000}", dOpenPoint * dOpenConff);

                            double dAsPoint = 0;
                            double dAsConff = 0;

                            double.TryParse(lblASPoint.Text, out dAsPoint);
                            double.TryParse(txbAsConfficient.Text, out dAsConff);

                            string strAsConfficient = string.Format("{0:0.00000}", dAsConff);
                            string strAsPoint_C = string.Format("{0:0.00000}", dAsPoint * dAsConff);

                            string strData = strSumupMonth + "|" + strEmpNo + "|" + strOpenConfficient + "|" + strAsConfficient + "|" + strOpenPoint_C + "|" + strAsPoint_C;

                            lstData.Add(strData);
                        }
                    }

                    using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                    {
                        mgmt.UpdUserPayCofficient(lstData);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOk", "alert('최종저장을 완료했습니다.');", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    bool bValidationChk = false;
                    bool bSum = false;
                    string strOrgCd = this.hdfOrgCd.Value;
                    string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");

                    DataRow[] dr = DtOrgList.Select(string.Format("ORGCD = '{0}'", strOrgCd));

                    if (dr.Length > 0)
                    {
                        if (dr[0]["ORGLEVEL"].ToString().Equals("3") && strOrgCd.Length == 6)
                            bSum = true;
                    }

                    if (strSumMonth.Length > 0)
                    {
                        bValidationChk = true;
                    }

                    if (bValidationChk)
                    {
                        using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                        {
                            ds = mgmt.GetUserPayConficientList(strOrgCd, strSumMonth);
                        }

                        if (ds.Tables.Count > 0)
                        {
                            if (bSum)
                            {
                                dt = ds.Tables[0].Copy();

                                int iNum = 0;
                                double dOpenPoint = 0;
                                double dAsPoint = 0;
                                double dOpenPoint_C = 0;
                                double dAsPoint_C = 0;
                                double dSumPoint = 0;
                                double dManual = 0;
                                double dDispatch = 0;
                                double dEvent1 = 0;
                                double dEvent2 = 0;
                                double dEvent3 = 0;
                                int iSumprice = 0;

                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    iNum = int.Parse(dt.Rows[i]["NUM"].ToString());
                                    dOpenPoint += double.Parse(dt.Rows[i]["OPENPOINT"].ToString());
                                    dAsPoint += double.Parse(dt.Rows[i]["ASPOINT"].ToString());
                                    dOpenPoint_C += double.Parse(dt.Rows[i]["OPENPOINT_C"].ToString());
                                    dAsPoint_C += double.Parse(dt.Rows[i]["ASPOINT_C"].ToString());
                                    dManual += double.Parse(dt.Rows[i]["MANUAL"].ToString());
                                    dDispatch += double.Parse(dt.Rows[i]["DISPATCH"].ToString());
                                    dEvent1 += double.Parse(dt.Rows[i]["EVENT1"].ToString());
                                    dEvent2 += double.Parse(dt.Rows[i]["EVENT2"].ToString());
                                    dEvent3 += double.Parse(dt.Rows[i]["EVENT3"].ToString());
                                }

                                dSumPoint = dOpenPoint_C + dAsPoint_C;
                                iSumprice = (int)(dOpenPoint_C + dAsPoint_C) * 31000;
                                DataRow drNew = dt.NewRow();

                                drNew["NUM"] = iNum + 1;
                                drNew["ORGFULLNM"] = "합계";
                                drNew["SUMUPMONTH"] = "";
                                drNew["EMPNO"] = "";
                                drNew["EMPNM"] = "";
                                drNew["OPENPOINT"] = dOpenPoint;
                                drNew["ASPOINT"] = dAsPoint;
                                drNew["OPEN_CONFFICIENT"] = "";
                                drNew["AS_CONFFICIENT"] = "";
                                drNew["OPENPOINT_C"] = dOpenPoint_C;
                                drNew["ASPOINT_C"] = dAsPoint_C;
                                drNew["MANUAL"] = dManual;
                                drNew["DISPATCH"] = dDispatch;
                                drNew["EVENT1"] = dEvent1;
                                drNew["EVENT2"] = dEvent2;
                                drNew["EVENT3"] = dEvent3;
                                drNew["SUMPOINT"] = dSumPoint;
                                drNew["SUMPRICE"] = iSumprice;

                                dt.Rows.Add(drNew);
                            }
                            else
                            {
                                dt = ds.Tables[0];
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                        return;
                    }
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "파견내역 조회 결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlSumupMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = ddlSumupMonth.SelectedValue.Replace("-", "");

                if (!strMonth.Equals(""))
                {
                    SetOrgList(strMonth);

                    using (KTSUser user = new KTSUser())
                    {
                        ds = user.GetUserViewOrgMonth(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strOrgCd = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";

                            strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        }

                        this.hdfOrgCd.Value = strOrgCd;
                        this.txbTeam.Text = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);
                    }
                    else
                    {
                        this.hdfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hdfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}