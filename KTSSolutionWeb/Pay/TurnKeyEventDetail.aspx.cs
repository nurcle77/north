﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Data;
using KTS.KTSSolution.BSL.Pay;

namespace KTSSolutionWeb
{
    public partial class TurnKeyEventDetail : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }
        private string ORGCHK
        {
            get
            {
                if (ViewState["ORGCHK"] != null)
                    return ViewState["ORGCHK"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCHK"] = value;
            }
        }

        private string SUMUPMONTH
        {
            get
            {
                if (ViewState["SUMUPMONTH"] != null)
                    return ViewState["SUMUPMONTH"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SUMUPMONTH"] = value;
            }
        }

        private string[] COLUNMS
        {
            get
            {
                if (ViewState["COLUNMS"] != null)
                    return (string[])ViewState["COLUNMS"];
                else
                    return null;
            }
            set
            {
                ViewState["COLUNMS"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                    string strOrgNm = Request.Form["pORGNM"] == null ? "" : Request.Form["pORGNM"].ToString();
                    SUMUPMONTH = Request.Form["pSUMUPMONTH"] == null ? "" : Request.Form["pSUMUPMONTH"].ToString().Replace("-", "");
                    ORGCHK = strOrgNm.Length == 0 ? "Y" : "N";

                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                {
                    ds = mgmt.GetTurnkeyEventDetail(ORGCD, ORGCHK, SUMUPMONTH);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                TableCell thNum = new TableCell();
                thNum.Style.Add("font-weight", "800");
                thNum.Text = "No.";
                thead.Controls.Add(thNum);

                TableCell thSumupMonth = new TableCell();
                thSumupMonth.Style.Add("font-weight", "800");
                thSumupMonth.Text = "일자";
                thead.Controls.Add(thSumupMonth);

                TableCell thOrgLv2 = new TableCell();
                thOrgLv2.Style.Add("font-weight", "800");
                thOrgLv2.Text = "본부";
                thead.Controls.Add(thOrgLv2);

                TableCell thCompanyNm = new TableCell();
                thCompanyNm.Style.Add("font-weight", "800");
                thCompanyNm.Text = "업체명";
                thead.Controls.Add(thCompanyNm);

                TableCell thCompanyCd = new TableCell();
                thCompanyCd.Style.Add("font-weight", "800");
                thCompanyCd.Text = "업체코드";
                thead.Controls.Add(thCompanyCd);

                TableCell thOfficeNm = new TableCell();
                thOfficeNm.Style.Add("font-weight", "800");
                thOfficeNm.Text = "국사명";
                thead.Controls.Add(thOfficeNm);

                TableCell thOfficeCd = new TableCell();
                thOfficeCd.Style.Add("font-weight", "800");
                thOfficeCd.Text = "국사코드";
                thead.Controls.Add(thOfficeCd);

                TableCell thEventSum = new TableCell();
                thEventSum.Style.Add("font-weight", "800");
                thEventSum.Text = "이벤트합계";
                thead.Controls.Add(thEventSum);

                COLUNMS = new string[ds.Tables[1].Columns.Count - 1];

                for (int i = 1; i < ds.Tables[1].Columns.Count; i++)
                {
                    TableCell thValue = new TableCell();
                    thValue.Style.Add("font-weight", "800");
                    thValue.Text = ds.Tables[1].Rows[0]["VALUE" + i.ToString()].ToString();
                    thead.Controls.Add(thValue);

                    COLUNMS[i - 1] = ds.Tables[1].Rows[0]["VALUE" + i.ToString()].ToString();
                }

                updPanel1.Update();

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[8 + COLUNMS.Length];
            
            ArrHeader[0] = "NUM";
            ArrHeader[1] = "SUMUPMONTH";
            ArrHeader[2] = "ORGLV2NM";
            ArrHeader[3] = "COMPANYNM";
            ArrHeader[4] = "COMPANYCD";
            ArrHeader[5] = "OFFICENM";
            ArrHeader[6] = "OFFICECD";
            ArrHeader[7] = "EVENTSUM";

            for (int i = 0; i < COLUNMS.Length; i++)
            {
                ArrHeader[i + 8] = "VALUE" + (i + 1).ToString();
            }

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {

            string[] ArrHeader = new string[8 + COLUNMS.Length];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "일자";
            ArrHeader[2] = "본부";
            ArrHeader[3] = "업체명";
            ArrHeader[4] = "업체코드";
            ArrHeader[5] = "국사명";
            ArrHeader[6] = "국사코드";
            ArrHeader[7] = "이벤트합계";

            for (int i = 0; i < COLUNMS.Length; i++)
            {
                ArrHeader[i + 8] = COLUNMS[i];
            }

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                    {
                        ds = mgmt.GetTurnkeyEventDetail(ORGCD, ORGCHK, SUMUPMONTH);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "턴키 월별 이벤트 상세조회");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }

        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}