﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserPayCoefficientInfo.aspx.cs" Inherits="KTSSolutionWeb.UserPayCoefficientInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function PopupOrgTree(orgcd, empno) {

            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("정산일을 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMDATE: SumupMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function sethfCalcResult() {
            $("#<%= hfCalcResult.ClientID %>").val('');
        }

        function btnRegChk() {
            var CalcResult = $("#<%= hfCalcResult.ClientID %>").val();

            if (CalcResult.length == 0 || CalcResult == "N") {
                alert("계산이 되지 않았습니다. 계산을 먼저 해주세요");
                return false;
            } else if (CalcResult == "F") {
                alert("계산된 포인트가 조정 범위에 포함되지 않습니다.");
                return false;
            } else if (CalcResult == "X") {
                alert("조회된 데이터가 없습니다.");
                return false;
            } else if (CalcResult == "T") {
                return true;
            } else {
                alert("잘못된 접근입니다.");
                return false;
            }
        }

        function ValChk(obj) {
            var val = obj.value;
            var minVal = $("#<%= hfMinVal.ClientID %>").val();
            var maxVal = $("#<%= hfMaxVal.ClientID %>").val();
            
            if (val > 0) {
                if ((val < minVal) || val > maxVal) {
                    alert("구역계수는 최소 " + minVal + " 이상, 최대 " + maxVal + " 이하 여야 합니다.");
                    return false;
                }
                else {
                    return true;
                }
            }
        }

        function SearchChk() {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("정산일자를 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SetOrgCd(orgcd, valtype) {

            this.focus();

            document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelOrg)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>
                <span class="optionbox">
                    <asp:UpdatePanel ID="updPanelSumupMonth" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>정산일</label>
                            <asp:DropDownList ID="ddlSumupMonth" runat="server" Width="120px" OnSelectedIndexChanged="ddlSumupMonth_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
			    </span>
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
                            <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlSumupMonth" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>구역계수 조회 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
                </div>
			    <div class="pull-right">
                    <asp:UpdatePanel ID="updPanelCalc" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="inpbox" id="divOpen" runat="server" visible="false">
					            <label>개통포인트</label>
                                <asp:TextBox ID="txbOpenPoint" runat="server" Width="100px" ReadOnly="true" style="margin-right:0px"></asp:TextBox>
                                <asp:TextBox ID="txbOpenPointC" runat="server" Width="100px" ReadOnly="true" style="margin-right:10px;background-color:white;"></asp:TextBox>
                            </div>
                            <div class="inpbox" id="divAs" runat="server" visible="false">
					            <label>AS포인트</label>
                                <asp:TextBox ID="txbAsPoint" runat="server" Width="100px" ReadOnly="true" style="margin-right:0px"></asp:TextBox>
                                <asp:TextBox ID="txbAsPointC" runat="server" Width="100px" ReadOnly="true" style="background-color:white;"></asp:TextBox>
                                <asp:HiddenField ID="hfCalcResult" runat="server" />
                                <asp:HiddenField ID="hfMinVal" runat="server" />
                                <asp:HiddenField ID="hfMaxVal" runat="server" />
                                <asp:HiddenField ID="hfLimitVal" runat="server" />
                            </div>
				            <div class="btnset" style="float:right;margin-left:10px;margin-right:10px;" id="divBtn" runat="server" visible="false">
                                <asp:Button ID="btnCalc" runat="server" class="btn-green" OnClick="btnCalc_Click" Text="계산" />
                                <asp:Button ID="btnReg" runat="server" class="btn-green" OnClientClick="return btnRegChk();" OnClick="btnReg_Click" Text="최종저장" />
				            </div>
                            <div class="inpbox" id="divCalYn" runat="server" style="margin-top:8px;margin-right:20px;" visible="false">
					            <label>구역계수 수정 마감일이 지났습니다.</label>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCalc" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>소속</th>
                                    <th>일자</th>
                                    <th>사번</th>
                                    <th>KTS사번</th>
                                    <th>이름</th>
                                    <th>조정전 개통</th>
                                    <th>조정전 AS</th>
                                    <th>개통 구역계수</th>
                                    <th>AS 구역계수</th>
                                    <th>조정후 개통</th>
                                    <th>조정후 AS</th>
                                    <th>수작업</th>
                                    <th>파견</th>
                                    <th>EVENT1</th>
                                    <th>EVENT2</th>
                                    <th>EVENT3</th>
                                    <th>합계</th>
                                    <th>매출액</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr id="tr" runat="server">
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td>
                                                <asp:Label ID="lblSumupMonth" runat="server" Text='<%# Eval("SUMUPMONTH") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("EMPNO") %>
                                                <asp:Label ID="lblIdmsEmpNo" runat="server" Visible="false" Text='<%# Eval("IDMSEMPNO") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td>
                                                <asp:Label ID="lblOpenPoint" runat="server" Text='<%# Eval("OPENPOINT") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblASPoint" runat="server" Text='<%# Eval("ASPOINT") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <div class="txtbox" style="max-width:150px">
                                                    <asp:Label ID="lblOpenConfficient" runat="server" style="max-width:140px" Text='<%# Eval("OPEN_CONFFICIENT") %>'></asp:Label>
                                                    <asp:TextBox ID="txbOpenConfficient" runat="server" style="max-width:140px;text-align:center;" onkeypress="sethfCalcResult();return OnlyNumber2(event, this);" MaxLength="10" onkeydown="ReplaceKorean(this);" onfocusout="return ValChk(this);" Text='<%# Eval("OPEN_CONFFICIENT") %>' Visible="false"></asp:TextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="txtbox" style="max-width:150px">
                                                    <asp:Label ID="lblAsConfficient" runat="server" style="max-width:140px" Text='<%# Eval("AS_CONFFICIENT") %>'></asp:Label>
                                                    <asp:TextBox ID="txbAsConfficient" runat="server" style="max-width:140px;text-align:center;" onkeypress="sethfCalcResult();return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" MaxLength="10" onfocusout="return ValChk(this);" Text='<%# Eval("AS_CONFFICIENT") %>' Visible="false"></asp:TextBox>
                                                </div>
                                            </td>
                                            <td><%# Eval("OPENPOINT_C") %></td>
                                            <td><%# Eval("ASPOINT_C") %></td>
                                            <td><%# Eval("MANUAL") %></td>
                                            <td><%# Eval("DISPATCH") %></td>
                                            <td><%# Eval("EVENT1") %></td>
                                            <td><%# Eval("EVENT2") %></td>
                                            <td><%# Eval("EVENT3") %></td>
                                            <td><%# Eval("SUMPOINT") %></td>
                                            <td><%# Eval("SUMPRICE") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
