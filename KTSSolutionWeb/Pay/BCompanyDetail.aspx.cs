﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Data;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Pay;

namespace KTSSolutionWeb
{
    public partial class BCompanyDetail : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }
        private string ORGNM
        {
            get
            {
                if (ViewState["ORGNM"] != null)
                    return ViewState["ORGNM"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGNM"] = value;
            }
        }

        private string SUMUPMONTH
        {
            get
            {
                if (ViewState["SUMUPMONTH"] != null)
                    return ViewState["SUMUPMONTH"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SUMUPMONTH"] = value;
            }
        }

        private string WORKTYPE
        {
            get
            {
                if (ViewState["WORKTYPE"] != null)
                    return ViewState["WORKTYPE"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["WORKTYPE"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                    ORGNM = Request.Form["pORGNM"] == null ? "" : Request.Form["pORGNM"].ToString();
                    SUMUPMONTH = Request.Form["pSUMUPMONTH"] == null ? "" : Request.Form["pSUMUPMONTH"].ToString().Replace("-", "");
                    WORKTYPE = Request.Form["pWORKTYPE"] == null ? "" : Request.Form["pWORKTYPE"].ToString();
                    
                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (BCompanyPayMgmt mgmt = new BCompanyPayMgmt())
                {
                    ds = mgmt.GetBCompanyDetailList(ORGCD, ORGNM, SUMUPMONTH, WORKTYPE);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn(DataTable dt)
        {
            string[] ArrHeader = new string[dt.Columns.Count];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ArrHeader[i] = dt.Columns[i].ColumnName;
            }
            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {

            string[] ArrHeader = new string[14];            

            ArrHeader[0] = "No.";
            ArrHeader[1] = "일자";
            ArrHeader[2] = "본부";
            ArrHeader[3] = "국코드";
            ArrHeader[4] = "국사";
            ArrHeader[5] = "업체명";
            ArrHeader[6] = "이름";
            ArrHeader[7] = "사번";
            ArrHeader[8] = "건수";
            ArrHeader[9] = "매출";
            ArrHeader[10] = "업무구분";
            ArrHeader[11] = "보정항목";
            ArrHeader[12] = "요율";
            ArrHeader[13] = "지급액";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    using (BCompanyPayMgmt mgmt = new BCompanyPayMgmt())
                    {
                        ds = mgmt.GetBCompanyDetailList(ORGCD, ORGNM, SUMUPMONTH, WORKTYPE);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn(dt);
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "B협력사 월별 정산결과 상세조회-" + WORKTYPE);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }

        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}