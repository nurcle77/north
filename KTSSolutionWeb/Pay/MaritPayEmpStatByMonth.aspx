﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaritPayEmpStatByMonth.aspx.cs" Inherits="KTSSolutionWeb.MaritPayEmpStatByMonth" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        window.onload = setChartWidth;

        function setChartWidth() {
            var width = document.documentElement.clientWidth - 180;
            $("#<%= hfChartWidth.ClientID %>").val(width);
        }

        function PopupOrgTree(orgcd, empno) {

            var StMonth = $("#<%= ddlStMonth.ClientID %> option:selected").val();
            var EnMonth = $("#<%= ddlEnMonth.ClientID %> option:selected").val();

            if (StMonth.length == 0 || EnMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pORGLV: "4",
                    pCHKNODELV: "3",
                    pMDATE: EnMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SearchChk() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();
            var EmpNo = $("#<%= hfEmpNo.ClientID %>").val();

            var StMonth = $("#<%= ddlStMonth.ClientID %> option:selected").val();
            var EnMonth = $("#<%= ddlEnMonth.ClientID %> option:selected").val();
            var ValType = $("#<%= ddlValType.ClientID %> option:selected").val();

            if (StMonth.length == 0 || EnMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0 && EmpNo.length == 0) {
                alert("조회할 조직이나 사원을 선택해주세요.");
                return false;
            } else if (ValType.length == 0) {
                alert("조건을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SetOrgCd(orgcd, valtype) {

            this.focus();

            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelSearch)%>;
        }

        function SetDispEmpNo(orgcd, empno, empnm) {

            this.focus();

            document.getElementById("<%=hfEmpNo.ClientID %>").value = empno;
            document.getElementById("<%=txbEmpNm.ClientID %>").value = empnm;

            <%=Page.GetPostBackEventReference(updPanelSearch)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>
                <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox" style="margin-right:20px;">
                            <label>조회일자</label>
                            <asp:DropDownList ID="ddlStMonth" runat="server" Width="120px" OnSelectedIndexChanged="ddlStMonth_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <em>~</em>
                            <asp:DropDownList ID="ddlEnMonth" runat="server" Width="120px" OnSelectedIndexChanged="ddlEnMonth_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
			            </span>
				        <span class="inpbox" style="margin-right:10px;">
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" style="margin-right:10px;width:40px;height:40px;" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
				        </span>
				        <span class="inpbox" style="margin-right:10px;">
					        <label>이름</label>
                            <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnEmpUser" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" style="margin-right:10px;width:40px;height:40px;" />
                            <asp:HiddenField ID="hfEmpNo" runat="server" OnValueChanged="hfEmpNo_ValueChanged" />
				        </span>
                        <span class="optionbox">
                            <label>조건</label>
                            <asp:DropDownList ID="ddlValType" runat="server" Width="120px">
                                <asp:ListItem Text="합계" Value="SUMPOINT" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="매출액" Value="SUMPRICE"></asp:ListItem>
                                <asp:ListItem Text="개통건수" Value="OPENCNT"></asp:ListItem>
                                <asp:ListItem Text="AS건수" Value="ASCNT"></asp:ListItem>
                                <asp:ListItem Text="개통포인트" Value="OPENPOINT_C"></asp:ListItem>
                                <asp:ListItem Text="AS포인트" Value="ASPOINT_C"></asp:ListItem>
                                <asp:ListItem Text="수작업" Value="MANUAL"></asp:ListItem>
                                <asp:ListItem Text="기가아이즈" Value="GEYES"></asp:ListItem>
                                <asp:ListItem Text="단말" Value="TERMINAL"></asp:ListItem>
                                <asp:ListItem Text="무출동" Value="NONMOVE"></asp:ListItem>
                                <asp:ListItem Text="파견" Value="DISPATCH"></asp:ListItem>
                                <asp:ListItem Text="EVENT1" Value="EVENT1"></asp:ListItem>
                                <asp:ListItem Text="EVENT2" Value="EVENT2"></asp:ListItem>
                                <asp:ListItem Text="EVENT3" Value="EVENT3"></asp:ListItem>
                            </asp:DropDownList>
			            </span>
                        <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
                        <asp:Button id="btnReset" runat="server" OnClick="btnReset_Click" Visible="false" Enabled="false" class="btn-green" style="float:right;margin-right:10px;" Text="조건초기화" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlStMonth" EventName="SelectedIndexChanged" />         
                        <asp:AsyncPostBackTrigger ControlID="ddlEnMonth" EventName="SelectedIndexChanged" />                      
                    </Triggers>
                </asp:UpdatePanel>
		    </fieldset>
        </div>
        <br />
		<!-- //E:searchbox -->        
                
		<!-- S:datalist -->
        <div class="chartbox">
            <div style="width:100%;height:420px;overflow-x:hidden;overflow-y:hidden;"> <!-- 차트 사이즈 조정 -->
                <asp:HiddenField ID="hfChartWidth" runat="server" />
                <asp:UpdatePanel ID="updPanelChart1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:chart id="chart1" runat="server" Height="420px">
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                    <AxisX>
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineColor="#000" LineDashStyle="Solid" />
                                    </AxisX>
                                    <AxisY IsLabelAutoFit="False" IsMarginVisible="False" LabelAutoFitStyle="None">
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineDashStyle="NotSet"/>
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <Legends>
                                <asp:Legend Name="Legend1"></asp:Legend>
                            </Legends>
                        </asp:chart>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
		<!-- //E:datalist -->

		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>실적급 개인별 추이 조회결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
                </div>
		    </div>
		    <!-- //E:list-top -->
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <asp:TableHeaderRow ID="thead" runat="server">

                            </asp:TableHeaderRow>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <asp:TableRow ID="tr" runat="server" OnDataBinding="tr_DataBinding">

                                        </asp:TableRow>                                        
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
