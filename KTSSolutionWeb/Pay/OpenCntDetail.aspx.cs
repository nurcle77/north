﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Data;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Pay;

namespace KTSSolutionWeb
{
    public partial class OpenCntDetail : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }
        private string SUMUPMONTH
        {
            get
            {
                if (ViewState["SUMUPMONTH"] != null)
                    return ViewState["SUMUPMONTH"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SUMUPMONTH"] = value;
            }
        }

        private string SELECTLV
        {
            get
            {
                if (ViewState["SELECTLV"] != null)
                    return ViewState["SELECTLV"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SELECTLV"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                    SUMUPMONTH = Request.Form["pSUMUPMONTH"] == null ? "" : Request.Form["pSUMUPMONTH"].ToString().Replace("-", "");

                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (MeritPayMgmt mgmt = new MeritPayMgmt())
                {
                    ds = mgmt.GetMeritPayOpenDetailList(ORGCD, SUMUPMONTH, SELECTLV);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn(DataTable dt)
        {
            string[] ArrHeader = new string[dt.Columns.Count];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ArrHeader[i] = dt.Columns[i].ColumnName;
            }
            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {

            string[] ArrHeader = new string[31];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "일자";
            ArrHeader[2] = "시설수용조직2LV코드";
            ArrHeader[3] = "시설수용조직2LV";
            ArrHeader[4] = "시설수용조직3LV코드";
            ArrHeader[5] = "시설수용조직3LV";
            ArrHeader[6] = "시설수용조직4LV코드";
            ArrHeader[7] = "시설수용조직4LV";
            ArrHeader[8] = "시설수용조직";
            ArrHeader[9] = "국사명";
            ArrHeader[10] = "가설주소시군구";
            ArrHeader[11] = "가설주소코드";
            ArrHeader[12] = "가설주소읍면동";
            ArrHeader[13] = "가설오더유형";
            ArrHeader[14] = "가설주문코드";
            ArrHeader[15] = "가설주문유형";
            ArrHeader[16] = "명령번호";
            ArrHeader[17] = "동시처리작업번호";
            ArrHeader[18] = "분석상품레벨3";
            ArrHeader[19] = "시설서비스기술방식";
            ArrHeader[20] = "변경전인터넷서비스기술방식";
            ArrHeader[21] = "인터넷서비스기술방식";
            ArrHeader[22] = "현장작업공정코드";
            ArrHeader[23] = "현장작업공정";
            ArrHeader[24] = "모건물프로모션구분";
            ArrHeader[25] = "WM업체";
            ArrHeader[26] = "kt사번";
            ArrHeader[27] = "이름";
            ArrHeader[28] = "매트릭";
            ArrHeader[29] = "가설건수";
            ArrHeader[30] = "최종포인트 ";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    using (MeritPayMgmt mgmt = new MeritPayMgmt())
                    {
                        ds = mgmt.GetMeritPayOpenDetailList(ORGCD, SUMUPMONTH, SELECTLV);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn(dt);
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "실적급 월별 상세조회-개통");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }

        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}