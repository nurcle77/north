﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Pay;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace KTSSolutionWeb
{
    public partial class TurnkeyStatByMonth : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private DataTable DtSumupMonth
        {
            get
            {
                if (ViewState["DtSumupMonth"] != null)
                    return (DataTable)ViewState["DtSumupMonth"];
                else
                    return null;
            }
            set
            {
                ViewState["DtSumupMonth"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetSumupMonth();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;
                }

                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strEmpNo, string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperBCompanyOrgMonthList(strEmpNo, strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetSumupMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                {
                    ds = mgmt.GetSumupMonth();
                }

                ddlStMonth.Items.Clear();
                ddlStMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    DtSumupMonth = ds.Tables[0];

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlStMonth.Items.Add(new ListItem(DtSumupMonth.Rows[i]["SUMUPMONTH"].ToString(), DtSumupMonth.Rows[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hfOrgCd.Value;
                string strStMonth = this.ddlStMonth.SelectedValue.Replace("-", "");
                string strEnMonth = this.ddlEnMonth.SelectedValue.Replace("-", "");
                string strValType = this.ddlValType.SelectedValue;

                if (strStMonth.Length > 0 && strEnMonth.Length > 0 && strValType.Length > 0)
                {
                    using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                    {
                        ds = mgmt.GetTurnkeyStatByMonth(strOrgCd, strStMonth, strEnMonth, strValType);
                    }

                    paging.PageNumber = 0;
                    paging.PageSize = 30;

                    paging.Dt = null;

                    if (ds.Tables.Count > 0)
                    {
                        paging.TotalRows = ds.Tables[0].Rows.Count;
                        paging.Dt = ds.Tables[0];
                    }

                    DateTime StDate = DateTime.Parse(this.ddlStMonth.SelectedValue);
                    DateTime EnDate = DateTime.Parse(this.ddlEnMonth.SelectedValue);

                    int nDiffMonth = 12 * ((EnDate.Year - StDate.Year)) + (EnDate.Month - StDate.Month);

                    TableCell thNum = new TableCell();
                    thNum.Style.Add("font-weight", "800");
                    thNum.Text = "No.";
                    thead.Controls.Add(thNum);

                    TableCell thOrgNm = new TableCell();
                    thOrgNm.Style.Add("font-weight", "800");
                    thOrgNm.Text = "조직명";
                    thead.Controls.Add(thOrgNm);

                    for (int i = 0; i <= nDiffMonth; i++)
                    {
                        TableCell th = new TableCell();
                        th.Style.Add("font-weight", "800");
                        th.Text += StDate.AddMonths(i).ToString("yyyy년 MM월");
                        thead.Controls.Add(th);
                    }

                    TableCell thSum = new TableCell();
                    thSum.Style.Add("font-weight", "800");
                    thSum.Text = "합계";
                    thead.Controls.Add(thSum);

                    updPanel1.Update();
                    paging.SetPagingDataList();

                    SetChart(StDate, nDiffMonth);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetChart(DateTime StDate, int nDiffMonth)
        {
            try
            {
                try
                {
                    chart1.Width = Unit.Parse(hfChartWidth.Value + "px");
                }
                catch
                {
                    chart1.Width = Unit.Parse("2000px");
                }

                Color[] colors = { ColorTranslator.FromHtml("#00c0a9")
                                     , ColorTranslator.FromHtml("#029f88")
                                     , ColorTranslator.FromHtml("#d5b911")
                                     , ColorTranslator.FromHtml("#ed8a1d")
                                     , ColorTranslator.FromHtml("#985105")
                                     , ColorTranslator.FromHtml("#8ac8fc")
                                     , ColorTranslator.FromHtml("#4069fa")
                                     , ColorTranslator.FromHtml("#ab52fb")
                                     , ColorTranslator.FromHtml("#6ad7c4")};


                for (int i = 1; i < paging.Dt.Rows.Count; i++)
                {
                    Series series = new Series("series" + i.ToString());
                    series.ChartType = SeriesChartType.Line;
                    series.MarkerStyle = MarkerStyle.Circle;
                    series.MarkerSize = 6;
                    series.IsValueShownAsLabel = false;
                    series.BorderWidth = 2;
                    series.Color = colors[(i - 1) % 9];
                    series.LegendText = paging.Dt.Rows[i]["ORGNM"].ToString();
                    series.SmartLabelStyle.Enabled = true;

                    chart1.Series.Add(series);

                    for (int j = 0; j <= nDiffMonth; j++)
                    {
                            
                        chart1.Series[i - 1].Points.AddY(paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString().Replace(",", ""));

                        if (!paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString().Equals("0") 
                            && !paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString().Equals("0.00"))
                        {
                            chart1.Series[i - 1].Points[j].IsValueShownAsLabel = true;
                            chart1.Series[i - 1].Points[j].Label = paging.Dt.Rows[i]["ORGNM"].ToString() + " : " + paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString();
                            chart1.Series[i - 1].Points[j].LabelForeColor = colors[(i - 1) % 9];
                            //chart1.Series[i - 1].Points[j].LabelToolTip = paging.Dt.Rows[i]["ORGNM"].ToString() + " : " + paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString();
                            chart1.Series[i - 1].Points[j].ToolTip = paging.Dt.Rows[i]["ORGNM"].ToString() + " : " + paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString();
                        }

                        if (i - 1 == 0)
                        {
                            chart1.Series[0].Points[j].AxisLabel = StDate.AddMonths(j).ToString("yyyy년 MM월");
                        }
                    }
                }

                chart1.Legends[0].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
                chart1.ChartAreas[0].AxisX.Interval = 1;
                chart1.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;

                updPanelChart1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private string[] GetHeaderColumn(int nDiffMonth)
        {
            string[] ArrHeader = new string[nDiffMonth + 5];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGFULLNM";

            for (int i = 0; i <= nDiffMonth; i++)
            {
                ArrHeader[i + 3] = "VALDATA" + i.ToString();
            }

            ArrHeader[nDiffMonth + 4] = "SUMVALDATA";

            return ArrHeader;
        }

        private string[] GetHeaderTop(int nDiffMonth)
        {
            string[] ArrHeader = new string[nDiffMonth + 5];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "조직코드";
            ArrHeader[2] = "조직명";

            DateTime StDate = DateTime.Parse(ddlStMonth.SelectedValue);

            for (int i = 0; i <= nDiffMonth; i++)
            {
                ArrHeader[i + 3] = StDate.AddMonths(i).ToString("yyyy년 MM월");
            }

            ArrHeader[nDiffMonth + 4] = "합계";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        if (rptResult.Items.Count > 0)
                        {
                            TableRow tr = (TableRow)rptResult.Items[0].FindControl("tr");

                            if (paging.PageNumber == 0)
                            {
                                tr.Style.Add("background-color", "#e6e6e6");
                            }
                        }
                    }
                    
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }


        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {

                    string strOrgCd = this.hfOrgCd.Value;
                    string strStMonth = this.ddlStMonth.SelectedValue.Replace("-", "");
                    string strEnMonth = this.ddlEnMonth.SelectedValue.Replace("-", "");
                    string strValType = this.ddlValType.SelectedValue;

                    if (strStMonth.Length > 0 && strEnMonth.Length > 0 && strValType.Length > 0)
                    {
                        using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                        {
                            ds = mgmt.GetTurnkeyStatByMonth(strOrgCd, strStMonth, strEnMonth, strValType);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                        return;
                    }
                }

                DateTime StDate = DateTime.Parse(this.ddlStMonth.SelectedValue);
                DateTime EnDate = DateTime.Parse(this.ddlEnMonth.SelectedValue);

                int nDiffMonth = 12 * ((EnDate.Year - StDate.Year)) + (EnDate.Month - StDate.Month);

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn(nDiffMonth);
                    excel.HeaderTop = GetHeaderTop(nDiffMonth);
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "실적급 추이 조회 결과_" + this.ddlValType.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlStMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strStMonth = ddlStMonth.SelectedValue;

                ddlEnMonth.Items.Clear();
                ddlEnMonth.Items.Add(new ListItem("선택", ""));

                if (strStMonth.Length > 0)
                {
                    DataRow[] dr = DtSumupMonth.Select(string.Format("SUMUPMONTH >= '{0}'", strStMonth));

                    for (int i = 0; i < dr.Length; i++)
                    {
                        ddlEnMonth.Items.Add(new ListItem(dr[i]["SUMUPMONTH"].ToString(), dr[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void tr_DataBinding(object sender, EventArgs e)
        {
            try
            {
                TableRow tr = (TableRow)sender;

                string strStMonth = this.ddlStMonth.SelectedValue.Replace("-", "");
                string strEnMonth = this.ddlEnMonth.SelectedValue.Replace("-", "");

                DateTime StDate = DateTime.Parse(this.ddlStMonth.SelectedValue);
                DateTime EnDate = DateTime.Parse(this.ddlEnMonth.SelectedValue);

                int nDiffMonth = 12 * ((EnDate.Year - StDate.Year)) + (EnDate.Month - StDate.Month);

                TableCell tdNum = new TableCell();
                tdNum.Text = string.Format("{0}", Eval("NUM").ToString());
                tr.Controls.Add(tdNum);


                TableCell tdOrgNm = new TableCell();
                tdOrgNm.Text = string.Format("{0}", Eval("ORGFULLNM").ToString());
                tr.Controls.Add(tdOrgNm);

                for (int i = 0; i <= nDiffMonth; i++)
                {
                    TableCell td = new TableCell();
                    td.Text = string.Format("{0}", Eval("VALDATA" + i.ToString()).ToString());
                    tr.Controls.Add(td);
                }

                TableCell tdSum = new TableCell();
                tdSum.Text = string.Format("{0}", Eval("SUMVALDATA").ToString());
                tr.Controls.Add(tdSum);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlEnMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = ddlEnMonth.SelectedValue.Replace("-", "");

                if (!strMonth.Equals(""))
                {
                    if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                    {
                        SetOrgList("", strMonth);
                    }
                    else
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    using (KTSUser user = new KTSUser())
                    {
                        ds = user.GetUserViewOrgMonth(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strOrgCd = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";

                            strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        }

                        this.hfOrgCd.Value = strOrgCd;
                        this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                    }
                    else
                    {
                        this.hfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}