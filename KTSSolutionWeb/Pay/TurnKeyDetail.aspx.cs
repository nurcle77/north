﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Data;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Pay;

namespace KTSSolutionWeb
{
    public partial class TurnKeyDetail : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }
        private string ORGCHK
        {
            get
            {
                if (ViewState["ORGCHK"] != null)
                    return ViewState["ORGCHK"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCHK"] = value;
            }
        }

        private string SUMUPMONTH
        {
            get
            {
                if (ViewState["SUMUPMONTH"] != null)
                    return ViewState["SUMUPMONTH"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SUMUPMONTH"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                    string strOrgNm = Request.Form["pORGNM"] == null ? "" : Request.Form["pORGNM"].ToString();
                    SUMUPMONTH = Request.Form["pSUMUPMONTH"] == null ? "" : Request.Form["pSUMUPMONTH"].ToString().Replace("-", "");
                    string strOrgLv = Request.Form["pORGLV"] == null ? "" : Request.Form["pORGLV"].ToString();

                    if (strOrgLv.Equals("0"))
                    {
                        ORGCHK = strOrgNm.Length == 0 ? "Y" : "B";
                    }
                    else if (strOrgLv.Equals("1"))
                    {
                        ORGCHK = strOrgNm.Length == 0 ? "N" : "Y";
                    }
                    else
                    {
                        ORGCHK = strOrgNm.Length == 0 ? "Y" : "N";
                    }

                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                {
                    ds = mgmt.GetTurnkeyDetail(ORGCD, ORGCHK, SUMUPMONTH);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {

            string[] ArrHeader = new string[46];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "SUMUPMONTH";
            ArrHeader[2] = "ORGLV2NM";
            ArrHeader[3] = "COMPANYNM";
            ArrHeader[4] = "COMPANYCD";
            ArrHeader[5] = "OFFICENM";
            ArrHeader[6] = "OFFICECD";

            ArrHeader[7] = "OPENPRICE";
            ArrHeader[8] = "ASPRICE";
            ArrHeader[9] = "FM";
            ArrHeader[10] = "LAPTOP10G";
            ArrHeader[11] = "MATERIAL";
            ArrHeader[12] = "RENTAL";
            ArrHeader[13] = "DAONPLAN";
            ArrHeader[14] = "OFFICE";
            ArrHeader[15] = "MATERIALSTORED";
            ArrHeader[16] = "SHOWERROOM";
            ArrHeader[17] = "PARKING";
            ArrHeader[18] = "CUSTLINE";
            ArrHeader[19] = "SELFOPEN";
            ArrHeader[20] = "SPECIALPLACECOST";
            ArrHeader[21] = "QUALITYMGMT";
            ArrHeader[22] = "SUBTOTAL1";

            ArrHeader[23] = "CTYPE";
            ArrHeader[24] = "SELFLINE";
            ArrHeader[25] = "GEYEOPEN";
            ArrHeader[26] = "GEYEAS";
            ArrHeader[27] = "MANUALOPEN";
            ArrHeader[28] = "MANUALAS";
            ArrHeader[29] = "IMPROVE";
            ArrHeader[30] = "APP_PAIRRING";
            ArrHeader[31] = "IOT";
            ArrHeader[32] = "VIPCARESVC";
            ArrHeader[33] = "OPENCOST";
            ArrHeader[34] = "ASCOST";
            ArrHeader[35] = "OPENASCOST";
            ArrHeader[36] = "TERMINAL";
            ArrHeader[37] = "ETC";
            ArrHeader[38] = "SLAOPEN";
            ArrHeader[39] = "SLAAS";
            ArrHeader[40] = "OPENASMATERIAL";
            ArrHeader[41] = "MODEM";
            ArrHeader[42] = "EVENTSUM";
            ArrHeader[43] = "RETURNTERMINAL";
            ArrHeader[44] = "SUBTOTAL2";
            ArrHeader[45] = "TOTALPRICE";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {

            string[] ArrHeader = new string[10];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "일자";
            ArrHeader[2] = "본부";
            ArrHeader[3] = "업체명";
            ArrHeader[4] = "업체코드";
            ArrHeader[5] = "국사명";
            ArrHeader[6] = "국사코드";

            ArrHeader[7] = "턴키금액";
            ArrHeader[8] = "추가금액";
            ArrHeader[9] = "총계";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[46];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "일자";
            ArrHeader[2] = "본부";
            ArrHeader[3] = "업체명";
            ArrHeader[4] = "업체코드";
            ArrHeader[5] = "국사명";
            ArrHeader[6] = "국사코드";

            ArrHeader[7] = "개통";
            ArrHeader[8] = "AS";
            ArrHeader[9] = "FM인건비";
            ArrHeader[10] = "10G노트북";
            ArrHeader[11] = "자재인건비";
            ArrHeader[12] = "고소차렌탈비";
            ArrHeader[13] = "다온플랜";
            ArrHeader[14] = "사무실";
            ArrHeader[15] = "자재창고";
            ArrHeader[16] = "샤워장";
            ArrHeader[17] = "주차비";
            ArrHeader[18] = "고객회선 관리실";
            ArrHeader[19] = "자가개통 운영비";
            ArrHeader[20] = "특수지역정산";
            ArrHeader[21] = "품질관리평가";
            ArrHeader[22] = "소계";

            ArrHeader[23] = "C-TYPE 이너텔";
            ArrHeader[24] = "자가망";
            ArrHeader[25] = "기가아이즈(개통)";
            ArrHeader[26] = "기가아이즈(수리)";
            ArrHeader[27] = "수작업정산(개통)";
            ArrHeader[28] = "수작업정산(수리)";
            ArrHeader[29] = "초고속품질개선비";
            ArrHeader[30] = "기가지니앱페어링차감";
            ArrHeader[31] = "IOT신상품";
            ArrHeader[32] = "핵심고객care서비스";
            ArrHeader[33] = "개통";
            ArrHeader[34] = "수리";
            ArrHeader[35] = "개통/AS";
            ArrHeader[36] = "단말관리";
            ArrHeader[37] = "기타";
            ArrHeader[38] = "SLA개통";
            ArrHeader[39] = "SLA수리";
            ArrHeader[40] = "개통/AS물자비";
            ArrHeader[41] = "모뎀물자비";
            ArrHeader[42] = "이벤트";
            ArrHeader[43] = "단말회수";
            ArrHeader[44] = "소계";

            ArrHeader[45] = "총계";

            return ArrHeader;
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {

                    using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                    {
                        ds = mgmt.GetTurnkeyDetail(ORGCD, ORGCHK, SUMUPMONTH);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();
                    excel.MergeCell = "7/16,8/22";

                    excel.ExcelDownLoad(this.Page, dt, "턴키 월별 상세조회");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }

        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}