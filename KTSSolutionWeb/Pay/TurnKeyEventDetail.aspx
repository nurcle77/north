﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TurnKeyEventDetail.aspx.cs" Inherits="KTSSolutionWeb.TurnKeyEventDetail" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>턴키 월별 이벤트 상세조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        
		<!-- S: windowpop-wrap -->
        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>턴키 월별 이벤트 상세</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <div class="pull-right">
                            <div class="btnset" style="margin-right:10px;">
                                <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                            </div>'
                        </div>
                    </div>
                    <!-- E:list-top -->
			        <!-- S:scrollbox -->
			        <div class="scrollbox" style="height:500px;overflow-y:auto;">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <asp:TableHeaderRow ID="thead" runat="server">

                                    </asp:TableHeaderRow>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("NUM") %></td>
                                                    <td><%# Eval("SUMUPMONTH") %></td>
                                                    <td><%# Eval("ORGLV2NM") %></td>
                                                    <td><%# Eval("COMPANYNM") %></td>
                                                    <td><%# Eval("COMPANYCD") %></td>
                                                    <td><%# Eval("OFFICENM") %></td>
                                                    <td><%# Eval("OFFICECD") %></td>
                                                    <td><%# Eval("EVENTSUM") %></td>
			                                        <td><%# Eval("VALUE1") %></td>
                                                    <td><%# Eval("VALUE2") %></td>
                                                    <td><%# Eval("VALUE3") %></td>
                                                    <td><%# Eval("VALUE4") %></td>
                                                    <td><%# Eval("VALUE5") %></td>
                                                    <td><%# Eval("VALUE6") %></td>
                                                    <td><%# Eval("VALUE7") %></td>
                                                    <td><%# Eval("VALUE8") %></td>
                                                    <td><%# Eval("VALUE9") %></td>
                                                    <td><%# Eval("VALUE10") %></td>
                                                    <td><%# Eval("VALUE11") %></td>
                                                    <td><%# Eval("VALUE12") %></td>
                                                    <td><%# Eval("VALUE13") %></td>
                                                    <td><%# Eval("VALUE14") %></td>
                                                    <td><%# Eval("VALUE15") %></td>
                                                    <td><%# Eval("VALUE16") %></td>
                                                    <td><%# Eval("VALUE17") %></td>
                                                    <td><%# Eval("VALUE18") %></td>
                                                    <td><%# Eval("VALUE19") %></td>
                                                    <td><%# Eval("VALUE20") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="paging" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
			        <!-- E:scrollbox -->
                    <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
                </div>
                <!-- E:datalist -->
            </div>
        </div>
    </form>
</body>
</html>
