﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class NoticeList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string strAuthId = this.Page.Session["AUTHID"] == null ? "" : this.Page.Session["AUTHID"].ToString();

                if (strAuthId.Equals("AUTH0001"))
                {
                    this.btnWrite.Visible = true;
                    btnWrite.Attributes.Add("onclick", "NoticeWrite('', '')");
                }
                else
                {
                    this.btnWrite.Visible = false;
                    btnWrite.Attributes.Remove("onclick");
                }

                if (!IsPostBack)
                {
                    GetDataList();
                }
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetNotiList("", "", "", "");
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblNoticeID = (Label)rptResult.Items[i].FindControl("lblNoticeID");
                            Label lblWriterID = (Label)rptResult.Items[i].FindControl("lblWriterID");
                            HtmlAnchor aTitle = (HtmlAnchor)rptResult.Items[i].FindControl("aTitle");

                            aTitle.Attributes.Add("onclick", "NoticeView('" + lblNoticeID.Text + "', '" + lblWriterID.Text + "')");
                        }
                    }

                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}