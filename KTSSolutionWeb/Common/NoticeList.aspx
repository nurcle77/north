﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoticeList.aspx.cs" Inherits="KTSSolutionWeb.NoticeList" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>공지사항</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">
        function NoticeWrite(noticeid, writerid) {
            var nWidth = 690;
            var nHeight = 820;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pNOTICEID: noticeid,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/NoticeWrite", param);

            Popupform.target = "NoticeWrite";
            var win = window.open("", "NoticeWrite", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function NoticeView(noticeid, writerid) {
            var nWidth = 600;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pNOTICEID: noticeid,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/NoticeView", param);

            Popupform.target = "NoticeView";
            var win = window.open("", "NoticeView", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function RefreshList() {
            __doPostBack("<%=btnSelect.UniqueID %>", "OnClick");
            opener.RefreshList();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <!-- S:windowpop-wrap -->
        <div id="windowpop-wrap">
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title">
                        <strong>공지사항</strong>
                    </div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green" Visible="false" Text="조회" />
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />
            
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-notice-list">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:boardlist -->
                    <div class="boardlist">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional" style="min-height:500px">
                            <ContentTemplate>
                                <table>
                                    <colgroup>
                                        <col style="width:68px;" />
                                        <col style="width:auto;" />
                                        <col style="width:100px;" />
                                        <col style="width:110px;" />
                                    </colgroup>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="number"><%# Eval("NUM") %></td>
                                                    <td class="list-title">
                                                        <asp:Label ID="lblNoticeID" runat="server" Visible="false" Text='<%# Eval("NOTICEID") %>'></asp:Label>
                                                        <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                                        <a id="aTitle" runat="server" style="cursor:pointer"><%# Eval("TITLE") %></a>
                                                    </td>
                                                    <td  class="writer"><%# Eval("EMPNM") %></td>
                                                    <td  class="date"><%# Eval("WRITEDT") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="paging" />
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                                <asp:AsyncPostBackTrigger ControlID="btnWrite" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
                    </div>
                    <!-- E:boardlist -->
                    <!-- S:btncenter -->
                    <div style="margin-top:10px;margin-left:680px;">
                        <asp:Button id="btnWrite" runat="server" class="btn-green" Visible="false" Text="글쓰기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
