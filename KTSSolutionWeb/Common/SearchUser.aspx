﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchUser.aspx.cs" Inherits="KTSSolutionWeb.SearchUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사용자검색</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">
        function SearchChk() {
            var EmpNm = $("#<%= txbEmpNm.ClientID %>").val();

            if (EmpNm.length == 0) {
                alert("이름을 입력해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SendParentsForm(orgcd, empno, empnm) {
            opener.SetDispEmpNo(orgcd, empno, empnm);
            window.close();
        }

        function CheckPostBack(obj) {
            __doPostBack(obj.id, "");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>직원검색</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <!-- S:searchbox -->
                    <div class="searchboxPopUp">
		                <fieldset>
                            <span class="inpbox first">
                                <label>이름</label>
                                <asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" Width="120px" TabIndex="1"></asp:TextBox>
                            </span>
                            <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
                        </fieldset>
                    </div>
                    <!-- E:searchbox -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:boardlist -->
                    <div class="datalist" style="max-height:250px;overflow-y:auto;margin-bottom:10px">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>소속</th>
                                            <th>사번</th>
                                            <th>KTS사번</th>
                                            <th>이름</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <input id="cbEmpNo" runat="server" type="checkbox" style="width:20px;height:20px;" onchange="javascript:CheckPostBack(this);" onserverchange="cbEmpNo_CheckedChanged" />
                                                    </td>
                                                    <td>
                                                        <%# Eval("ORGFULLNM") %>
                                                        <asp:Label id="lblOrgCd" runat="server" Visible="false" Text='<%# Eval("ORGCD") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label id="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                                    </td>
                                                    <td><%# Eval("KTSEMPNO") %></td>
                                                    <td>
                                                        <asp:Label id="lblEmpNm" runat="server" Text='<%# Eval("EMPNM") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <!-- E:boardlist -->
                    <!-- S:btncenter -->
                    <div style="float:right;margin-right:10px;">
                        <asp:Button id="btnConfirm" runat="server" OnClick="btnConfirm_Click" class="btn-green" Text="선택" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
