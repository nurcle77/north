﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BoardView.aspx.cs" Inherits="KTSSolutionWeb.Common.BoardView" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>기술공유 게시판</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">   
        function BoardWrite(boardid, writerId) {
            var nWidth = 690;
            var nHeight = 780;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pBOARDID: boardid,
                pWRITERID: writerId
            };

            var Popupform = createForm("/Common/BoardWrite", param);

            Popupform.target = "BoardWrite";
            var win = window.open("", "BoardWrite", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            win.opener = self.opener; 
            Popupform.submit();

            win.focus();
        }

        function DelCheck() {
            if (confirm("현재 게시글을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ReRegChk() {
            if (confirm("댓글을 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ReDelChk() {
            if (confirm("댓글을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function setScroll() {
            $("#hdfScrollTop").val($("#divResult").scrollTop());
        }

        function FileDownload() {
            __doPostBack("<%=btnFileDownload.ClientID %>", "");
        }

        function RefreshList() {
            opener.RefreshList();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">

            <!-- S:pop-organization -->
            <div class="windowpop pop-mainlist-item"> <!-- 팝업별로 클래스가 달라집니다 -->

                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <div class="list-wrapper">
                            <div class="list-top">
                                <strong id="sTitle" runat="server"></strong>
                                <p id="pWriterNm" runat="server"></p>
                            </div>
                            <div class="list-item" style="margin-bottom:30px;overflow-y:auto;height:350px" id="divListItem" runat="server">
                                <asp:UpdatePanel ID="updPanelImg" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divImg" runat="server">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="item-text" id="divContent" runat="server">
                                </div>
                            </div>
                            <!-- //E:list-item -->

                            <!-- S:scrollbox -->
                            <div class="scrollbox" id="divFileList" runat="server" style="max-height:70px;overflow-y:auto;">
                                <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <colgroup>
                                                <col style="width:70px" />
                                                <col style="width:auto" />
                                            </colgroup>
                                            <tbody>
                                                <tr class="upload-field">
                                                    <th style="vertical-align:middle;">첨부파일</th>
                                                    <td>
                                                        <asp:Repeater ID="rptFile" runat="server">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                                                <p><a id="btnDownload" runat="server" onserverclick="BtnDownload_ServerClick" ><%# Eval("FILENM") %></a>&nbsp;</p>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <br />
                            <!-- E:scrollbox -->

                            <!-- S:scrollbox -->
                            <div id="divResult" class="scrollbox" style="overflow-y:auto;height:240px">
                                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <colgroup>
                                                <col style="width:403px" />
                                                <col style="width:93px" />
                                            </colgroup>
                                            <tbody>
                                                <asp:Repeater ID="rptReply" runat="server">
                                                    <ItemTemplate>
                                                        <tr style="height:80px">
                                                            <td style="text-align:left;padding-left:3px;padding-right:3px;">
                                                                <p style="font-size:14px;line-height:1;color:#888;font-weight:200;margin-bottom:5px;"><%# Eval("WRITERNM") %></p>
                                                                <asp:Label ID="lblReSeq" runat="server" Visible="false" Text='<%# Eval("REPLYSEQ") %>'></asp:Label>
                                                                <asp:Label ID="lblReEmpNo" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                                                <asp:TextBox ID ="txbRePly" runat="server" Height="78px" Width="395px" style="font-size:14px;line-height:1;color:#888;font-weight:200;margin-bottom:5px;" TextMode="MultiLine" Text='<%# Eval("CONTENTS") %>' Visible="false"></asp:TextBox>
                                                                <asp:Label ID ="lblRePly" runat="server" style="font-size:14px;line-height:1;color:#888;font-weight:200;margin-bottom:5px;" Text='<%# Eval("CONTENTS") %>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Button id="btnReReg" runat="server" OnClientClick="setScroll();return ReRegChk();" OnClick="btnReReg_ServerClick" class="btn-replysave" style="margin-left:2px;margin-right:2px;" Text="저장" />
                                                                <asp:Button id="btnReDel" runat="server" OnClientClick="setScroll();return ReDelChk();" OnClick="btnReDel_ServerClick" class="btn-replydel" style="margin-right:2px;" Text="삭제" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                        <asp:HiddenField ID="hdfScrollTop" runat="server" Value="0" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin-top:10px;">
                            <asp:UpdatePanel ID="updPanelBtn" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button id="btnRegBoard" runat="server" style="margin-right:5px" class="btn-green" Text="수정" />
                                    <asp:Button id="btnDelBoard" runat="server" OnClientClick="return DelCheck();" style="margin-right:5px" class="btn-black" Text="삭제" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button id="btnFileDownload" OnClick="BtnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                        </div>
                        <!-- //E:btncenter -->
                
                    </div>
                    <!-- //E:popcontents -->

                </div>
                <!-- //E:popupwrap -->
            </div>
        </div>
    </form>
</body>
</html>
