﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BoardWrite.aspx.cs" Inherits="KTSSolutionWeb.Common.BoardWrite" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>기술공유 게시판</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">        
        function RegCheck() {
            if (confirm("게시글을 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function DelCheck() {
            if (confirm("현재 게시글을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function DelFileCheck() {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function FileDownload() {
            __doPostBack("<%=btnFileDownload.ClientID %>", "");
        }

        function AddFileUpload() {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("divFileUpload");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:200px;");

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "document.getElementById('fu" + AddFileUpload.lastAssignedId + "-value').value=this.value;");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.innerText = "찾아보기";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        }

        function RefreshList() {
            opener.RefreshList();
        }
    </script>
</head>
<body>
    <form runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:windowpop -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title">
                        <strong>기술공유</strong>
                    </div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>

                    <!-- S:popcontents -->
                    <div class="popcontents">                
                        <!-- S:datalist -->
                        <div class="datalist">                    
                            <!-- S:list-top -->
                            <div class="list-top">
                                <strong>등록</strong>
                            </div>
                            <!-- E:list-top -->

                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <colgroup>
                                                <col style="width:60px" />
                                                <col style="width:auto" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th>제목</th>
                                                    <td>
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbTitle" runat="server" Width="480px" MaxLength="50" ></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>내용</th>
                                                    <td>
                                                        <asp:TextBox ID="txbContents" runat="server" Width="480px" Height="240px" MaxLength="8000" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>파일추가</th>
                                                    <td>
                                                        <div id="divFileUpload" style="height:140px;overflow-y:auto;">
                                                            <p class="filebox">
                                                                <input type="text" id="fu1-value" style="width:200px;" />
                                                                <span class="file">
                                                                    <input type="file" id="fu1" name="fu1" onchange="document.getElementById('fu1-value').value=this.value;" /> 
                                                                    <label for="fu1">찾아보기</label>
                                                                </span>
                                                                <button type="button" onclick="AddFileUpload();" class="btn-green">추가</button>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="upload-field">
                                                    <th style="vertical-align:middle;">첨부파일</th>
                                                    <td>
                                                        <div class="scrollbox" id="divFileList" runat="server" style="max-height:90px;overflow-y:auto;">
                                                            <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Repeater ID="rptFile" runat="server">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                                                            <p><a id="btnDownload" runat="server" onserverclick="BtnDownload_ServerClick" ><%# Eval("FILENM") %></a>&nbsp;<button type="button" id="btnDelete" runat="server" onclick="DelFileCheck();" onserverclick="BtnDelete_ServerClick" style="color:black;">삭제</button></p>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnRegBoard" />
                                        <asp:PostBackTrigger ControlID="btnDelBoard" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                        <!-- S:btncenter -->
                        <div style="margin-top:10px;text-align:center;">
                            <asp:UpdatePanel ID="updPanelBtn" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button id="btnRegBoard" runat="server" OnClientClick="return RegCheck();" style="margin-right:5px" class="btn-green" Text="등록" />
                                    <asp:Button id="btnDelBoard" runat="server" OnClientClick="return DelCheck();" style="margin-right:5px" class="btn-black" Text="삭제" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button id="btnFileDownload" runat="server" OnClick="BtnFileDownload_ServerClick" Visible="false" Width="0px" class="btn-black" Text="" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:windowpop -->
        </div>
    </form>
</body>
</html>
