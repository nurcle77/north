﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using Microsoft.Ajax.Utilities;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class SearchOrgCd : PageBase
    {
        private string ORGLV
        {
            get
            {
                if (ViewState["ORGLV"] != null)
                    return ViewState["ORGLV"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGLV"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    ORGLV = Request.Form["pORGLV"] == null ? "0" : Request.Form["pORGLV"].ToString();

                    if (ORGLV.Equals("F"))
                    {
                        ORGLV = "";
                    }
                }
            }
        }

        private void GetOrgInfo()
        {
            DataSet ds = new DataSet();

            try
            {
                String strOrgCd = "";

                if (ORGLV.Equals(""))
                {
                    strOrgCd = this.Session["ORGCD"].ToString();
                }
                
                string strOrgNm = this.txbOrgNm.Text;

                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOrgNmList(strOrgCd, strOrgNm, ORGLV);
                }

                rptResult.DataSource = ds.Tables[0];
                rptResult.DataBind();

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    HtmlInputCheckBox cbOrgCd = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbOrgCd");

                    AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
                    trigger.ControlID = cbOrgCd.UniqueID;
                    trigger.EventName = "serverchange";
                    updPanel1.Triggers.Add(trigger);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetOrgInfo();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    HtmlInputCheckBox cbOrgCd = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbOrgCd");
                    
                    Label lblOrgCd = (Label)rptResult.Items[i].FindControl("lblOrgCd");
                    Label lblOrgNm = (Label)rptResult.Items[i].FindControl("lblOrgNm");

                    if (cbOrgCd.Checked)
                    {
                        string strOrgCd = lblOrgCd.Text;
                        string strOrgNm = lblOrgNm.Text;

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "SendParentsForm('" + strOrgCd + "', '" + strOrgNm + "');", true);

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void cbOrgCd_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                HtmlInputCheckBox cbOrgCd = (HtmlInputCheckBox)sender;

                if (cbOrgCd.Checked)
                {
                    for (int i = 0; i < rptResult.Items.Count; i++)
                    {
                        HtmlInputCheckBox chb = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbOrgCd");

                        if (!cbOrgCd.UniqueID.Equals(chb.UniqueID))
                        {
                            chb.Checked = false;
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}