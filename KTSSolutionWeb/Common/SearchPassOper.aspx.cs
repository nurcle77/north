﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using Microsoft.Ajax.Utilities;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class SearchPassOper : PageBase
    {
        private string PTYPE
        {
            get
            {
                if (ViewState["PTYPE"] != null)
                    return ViewState["PTYPE"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["PTYPE"] = value;
            }
        }

        private string EMPNO
        {
            get
            {
                if (ViewState["EMPNO"] != null)
                    return ViewState["EMPNO"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["EMPNO"] = value;
            }
        }

        private string PASSTYPE
        {
            get
            {
                if (ViewState["PASSTYPE"] != null)
                    return ViewState["PASSTYPE"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["PASSTYPE"] = value;
            }
        }

        private string PROCID
        {
            get
            {
                if (ViewState["PROCID"] != null)
                    return ViewState["PROCID"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["PROCID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    PTYPE = Request.Form["pPTYPE"] == null ? "ORGCD" : Request.Form["pPTYPE"].ToString();
                    EMPNO = Request.Form["pEMPNO"] == null ? "" : Request.Form["pEMPNO"].ToString();
                    PASSTYPE = Request.Form["pPASSTYPE"] == null ? "" : Request.Form["pPASSTYPE"].ToString();
                    PROCID = Request.Form["pPROCID"] == null ? "" : Request.Form["pPROCID"].ToString();
                }
            }
        }

        private void GetOperInfo()
        {
            DataSet ds = new DataSet();

            try
            {
                            
                string strOperNm = this.txbOperNm.Text;

                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetPassOperNmList(strOperNm, PTYPE, EMPNO, PASSTYPE);
                }

                rptResult.DataSource = ds.Tables[0];
                rptResult.DataBind();

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    HtmlInputCheckBox cbOperCd = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbOperCd");

                    AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
                    trigger.ControlID = cbOperCd.UniqueID;
                    trigger.EventName = "serverchange";
                    updPanel1.Triggers.Add(trigger);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetOperInfo();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    HtmlInputCheckBox cbOperCd = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbOperCd");

                    HiddenField hfOperCd = (HiddenField)rptResult.Items[i].FindControl("hfOperCd");
                    Label lblOperNm = (Label)rptResult.Items[i].FindControl("lblOperNm");

                    if (cbOperCd.Checked)
                    {
                        string strOperCd = hfOperCd.Value;
                        string strOperNm = lblOperNm.Text;

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "SendParentsForm('" + strOperCd + "', '" + strOperNm + "', '" + PROCID + "');", true);

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void cbOperCd_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                HtmlInputCheckBox cbOperCd = (HtmlInputCheckBox)sender;

                if (cbOperCd.Checked)
                {
                    for (int i = 0; i < rptResult.Items.Count; i++)
                    {
                        HtmlInputCheckBox chb = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbOperCd");

                        if (!cbOperCd.UniqueID.Equals(chb.UniqueID))
                        {
                            chb.Checked = false;
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}