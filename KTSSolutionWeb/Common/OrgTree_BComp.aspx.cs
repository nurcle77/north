﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class OrgTree_BComp : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }

        private string EMPNO
        {
            get
            {
                if (ViewState["EMPNO"] != null)
                    return ViewState["EMPNO"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["EMPNO"] = value;
            }
        }

        private int CHKNODELV
        {
            get
            {
                if (ViewState["CHKNODELV"] != null)
                    return int.Parse(ViewState["CHKNODELV"].ToString());
                else
                    return 9;
            }
            set
            {
                ViewState["CHKNODELV"] = value;
            }
        }

        private DataSet Ds
        {
            get
            {
                if (ViewState["Ds"] != null)
                    return (DataSet)ViewState["Ds"];
                else
                    return null;
            }
            set
            {
                ViewState["Ds"] = value;
            }
        }

        private string KTSYN
        {
            get
            {
                if (ViewState["KTSYN"] != null)
                    return ViewState["KTSYN"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["KTSYN"] = value;
            }
        }

        private string MULTICHK
        {
            get
            {
                if (ViewState["MULTICHK"] != null)
                    return ViewState["MULTICHK"].ToString();
                else
                    return "N";
            }
            set
            {
                ViewState["MULTICHK"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    SetTreeView();

                    OrgTree.Attributes.Add("onclick", "postBackObject();");
                }
            }
        }

        private void SetTreeView()
        {
            try
            {
                ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                EMPNO = Request.Form["pEMPNO"] == null ? "" : Request.Form["pEMPNO"].ToString();
                KTSYN = Request.Form["pKTSYN"] == null ? "N" : Request.Form["pKTSYN"].ToString();
                MULTICHK = Request.Form["pMULTICHK"] == null ? "N" : Request.Form["pMULTICHK"].ToString(); 

                string strChkNodeLv = Request.Form["pCHKNODELV"] == null ? "9" : Request.Form["pCHKNODELV"].ToString();
                string strOperYn = Request.Form["pOPER"] == null ? "" : Request.Form["pOPER"].ToString();

                string strMdate = Request.Form["pMdate"] == null ? "" : Request.Form["pMdate"].ToString().Replace("-", "");
                string strType = Request.Form["pTYPE"] == null ? "" : Request.Form["pTYPE"].ToString().Replace("-", "");

                if (strOperYn.Equals("N"))
                {
                    EMPNO = Page.Session["EMPNO"].ToString();
                }

                int nChkNodeLv = 9;
                int.TryParse(strChkNodeLv, out nChkNodeLv);

                CHKNODELV = nChkNodeLv;

                using (OrgCodeTree org = new OrgCodeTree())
                {
                    if ((strType.ToLower().Equals("pay") || strType.ToLower().Equals("cloth")) && !strMdate.Equals(DateTime.Now.ToString("yyyyMM")))
                    {
                        Ds = org.GetOperBCompanyOrgMonthList(EMPNO, strMdate);
                    }
                    else
                    {
                        Ds = org.GetOperBCompanyOrgList(EMPNO);
                    }
                }

                if (Ds.Tables.Count > 0)
                {
                    Dictionary<string, string> nodeValPath = new Dictionary<string, string>();

                    foreach (DataRow dr in Ds.Tables[0].Rows)
                    {
                        TreeNode node = new TreeNode(dr["ORGNM"].ToString());
                        node.Value = dr["ORGCD"].ToString();

                        node.ShowCheckBox = false;

                        if (nodeValPath.ContainsKey(dr["UPPERORGCD"].ToString()))
                        {
                            string strValPath = nodeValPath[dr["UPPERORGCD"].ToString()];

                            TreeNode findNode = OrgTree.FindNode(strValPath);
                            findNode.ChildNodes.Add(node);
                            nodeValPath.Add(node.Value, node.ValuePath);
                        }
                        else
                        {
                            OrgTree.Nodes.Add(node);
                            nodeValPath.Add(node.Value, node.ValuePath);
                        }

                        if (nChkNodeLv == 9)
                        {
                            if (KTSYN.Equals("Y"))
                            {
                                node.ShowCheckBox = true;
                            }
                            else
                            {
                                if (dr["ORGLEVEL"].ToString().Equals("5"))
                                {
                                    node.ShowCheckBox = true;
                                }
                            }
                        }
                        else
                        {
                            if (CHKNODELV <= int.Parse(dr["ORGLEVEL"].ToString()))
                            {
                                node.ShowCheckBox = true;
                            }
                            else
                            {
                                node.ShowCheckBox = false;
                            }
                        }
                    }

                    OrgTree.CollapseAll();

                    if (OrgTree.Nodes.Count > 0)
                    {
                        OrgTree.Nodes[0].Expand();

                        if (ORGCD.Length > 0)
                        {
                            string[] arrOrgCd = ORGCD.Split(',');

                            CheckNode(OrgTree.Nodes[0], arrOrgCd);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void CheckNode(TreeNode node, string[] arrOrgCd)
        {
            bool bNodeChk = false;


            foreach (string strOrg in arrOrgCd)
            {
                if (node.Value.Equals(strOrg))
                {
                    bNodeChk = true;
                    break;
                }
                else
                {
                    if (node.Parent != null)
                    {
                        bNodeChk = node.Parent.Checked;
                    }
                }
            }

            node.Checked = bNodeChk;

            if (bNodeChk)
            {
                if (node.Parent != null)
                {
                    if (!(bool)node.Parent.Expanded)
                    {
                        node.Parent.Expand();

                        if (node.Parent.Parent != null)
                        {
                            if (!(bool)node.Parent.Parent.Expanded)
                            {
                                node.Parent.Parent.Expand();
                            }
                        }
                    }
                }
            }
            if (node.ChildNodes.Count > 0)
            {
                foreach (TreeNode item in node.ChildNodes)
                {
                    CheckNode(item, arrOrgCd);
                }
            }
        }

        private void UnCheckNode(TreeNode node, TreeNode chkNode)
        {
            if (node != null)
            {
                foreach (TreeNode item in node.ChildNodes)
                {
                    if ((bool)item.ShowCheckBox)
                    {
                        if (item != chkNode)
                            item.Checked = false;
                    }

                    if (item.ChildNodes.Count > 0)
                    {
                        UnCheckNode(item, chkNode);
                    }
                }
            }
        }

        private void CheckUnCheckNode(TreeNode node, bool bCheck)
        {
            try
            {
                if (node != null)
                {
                    foreach (TreeNode item in node.ChildNodes)
                    {
                        if ((bool)item.ShowCheckBox)
                        {
                            item.Checked = bCheck;
                        }

                        if (item.ChildNodes.Count > 0)
                        {
                            CheckUnCheckNode(item, bCheck);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CheckUnCheckParentNode(TreeNode node)
        {
            try
            {
                TreeNode pNode = node.Parent;

                bool bCheck = true;

                foreach (TreeNode child in pNode.ChildNodes)
                {
                    if (!child.Checked)
                        bCheck = false;
                }

                pNode.Checked = bCheck;

                if (pNode.Parent != null)
                {
                    CheckUnCheckParentNode(pNode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void OrgTree_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            bool bNodeChk = e.Node.Checked;

            if (MULTICHK.Equals("Y"))
            {
                CheckUnCheckNode(e.Node, bNodeChk);

                if (e.Node.Parent != null)
                {
                    CheckUnCheckParentNode(e.Node);
                }
            }
            else
            {
                if (e.Node.Value.Split('|').Length > 1)
                {
                    this.hdfOrgCd.Value = e.Node.Value.Split('|')[1];
                }
                else
                {
                    this.hdfOrgCd.Value = e.Node.Value;
                }

                if (bNodeChk)
                {
                    UnCheckNode(OrgTree.Nodes[0], e.Node);
                }
            }

            updPanel1.Update();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNodeCollection checkNodes = OrgTree.CheckedNodes;

                string strOrgCd = "";

                for (int i = 0; i < checkNodes.Count; i++)
                {
                    DataRow[] dr = Ds.Tables[0].Select(string.Format("ORGCD = '{0}'", checkNodes[i].Value));

                    string strNodeLv = "1";

                    if (dr.Length > 0)
                        strNodeLv = dr[0]["ORGLEVEL"].ToString();

                    if (strNodeLv.Equals("0"))
                    {
                        strOrgCd = checkNodes[i].Value;
                        break;
                    }
                    else if (strNodeLv.Equals("1"))
                    {
                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                    else if (strNodeLv.Equals("2"))
                    {
                        string[] strOrgLvList = strOrgCd.Split(',');
                        bool bUpperChk = false;

                        if (checkNodes[i].Parent != null)
                        {
                            for (int j = 0; j < strOrgLvList.Length; j++)
                            {
                                if (strOrgLvList[j].Equals(checkNodes[i].Parent.Value))
                                {
                                    bUpperChk = true;
                                    break;
                                }
                            }
                        }

                        if (bUpperChk)
                        {
                            continue;
                        }

                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                    else if (strNodeLv.Equals("3"))
                    {
                        string[] strOrgLvList = strOrgCd.Split(',');
                        bool bUpperChk = false;

                        if (checkNodes[i].Parent != null)
                        {
                            for (int j = 0; j < strOrgLvList.Length; j++)
                            {
                                if (strOrgLvList[j].Equals(checkNodes[i].Parent.Value))
                                {
                                    bUpperChk = true;
                                    break;
                                }
                            }

                            if (checkNodes[i].Parent.Parent != null)
                            {
                                for (int j = 0; j < strOrgLvList.Length; j++)
                                {
                                    if (strOrgLvList[j].Equals(checkNodes[i].Parent.Parent.Value))
                                    {
                                        bUpperChk = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (bUpperChk)
                        {
                            continue;
                        }

                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                    else
                    {
                        string[] strOrgLvList = strOrgCd.Split(',');
                        bool bUpperChk = false;

                        if (checkNodes[i].Parent != null)
                        {
                            for (int j = 0; j < strOrgLvList.Length; j++)
                            {
                                if (strOrgLvList[j].Equals(checkNodes[i].Parent.Value))
                                {
                                    bUpperChk = true;
                                    break;
                                }
                            }

                            if (checkNodes[i].Parent.Parent != null)
                            {
                                for (int j = 0; j < strOrgLvList.Length; j++)
                                {
                                    if (strOrgLvList[j].Equals(checkNodes[i].Parent.Parent.Value))
                                    {
                                        bUpperChk = true;
                                        break;
                                    }
                                }

                                if (checkNodes[i].Parent.Parent.Parent != null)
                                {
                                    for (int j = 0; j < strOrgLvList.Length; j++)
                                    {
                                        if (strOrgLvList[j].Equals(checkNodes[i].Parent.Parent.Parent.Value))
                                        {
                                            bUpperChk = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (bUpperChk)
                        {
                            continue;
                        }

                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                }

                if (strOrgCd.Length == 0)
                {
                    strOrgCd = Session["ORGCD"].ToString();
                }

                this.hdfOrgCd.Value = strOrgCd;

                updPanel1.Update();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "SendParentsForm();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}