﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class BoardWriteM : PageBase
    {
        private string BoardId
        {
            get
            {
                if (ViewState["BoardId"] != null)
                    return ViewState["BoardId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["BoardId"] = value;
            }
        }

        private string WriterId
        {
            get
            {
                if (ViewState["WriterId"] != null)
                    return ViewState["WriterId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["WriterId"] = value;
            }
        }

        private string FileSeq
        {
            get
            {
                if (ViewState["FileSeq"] != null)
                    return ViewState["FileSeq"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["FileSeq"] = value;
            }
        }

        private string EMPNO
        {
            get
            {
                if (ViewState["EMPNO"] != null)
                {
                    return ViewState["EMPNO"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["EMPNO"] = value;
            }
        }

        private string AUTHID
        {
            get
            {
                if (ViewState["AUTHID"] != null)
                {
                    return ViewState["AUTHID"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["AUTHID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                initializeControls();

                if (!IsPostBack)
                {
                    GetDataList();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    BoardId = Request.Form["pBOARDID"] == null ? "" : Request.Form["pBOARDID"].ToString();
                    WriterId = Request.Form["pWRITERID"] == null ? "" : Request.Form["pWRITERID"].ToString();
                    EMPNO = Request.Form["pEMPNO"] == null ? "" : Request.Form["pEMPNO"].ToString();
                    AUTHID = Request.Form["pAUTHID"] == null ? "" : Request.Form["pAUTHID"].ToString();
                }

                if (!BoardId.Equals(""))
                {
                    if (AUTHID.Equals("AUTH0001") || EMPNO.Equals(WriterId))
                    {
                        this.btnRegBoard.Click += new EventHandler(BtnRegBoard_ServerClick);

                        if (!BoardId.Equals(""))
                        {
                            this.btnRegBoard.Text = "수정";

                            this.btnDelBoard.Click += new EventHandler(BtnDelBoard_ServerClick);
                            this.btnDelBoard.Visible = true;
                        }
                        else
                        {
                            this.btnRegBoard.Text = "등록";
                            this.btnDelBoard.Visible = false;
                        }
                        //this.txbContent.ReadOnly = false;

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');window.close();", true);
                    }
                }
                else
                {
                    this.btnRegBoard.Click += new EventHandler(BtnRegBoard_ServerClick);

                    this.btnRegBoard.Text = "등록";
                    this.btnDelBoard.Visible = false;
                }

                this.btnRegBoard.Visible = true;

                this.txbTitle.ReadOnly = false;

                updPanelBtn.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();
            DataSet dsFile = new DataSet();

            try
            {
                if (BoardId != "")
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardList(BoardId, "", "");
                        dsFile = Noti.GetBoardFile(BoardId, "");
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.txbTitle.Text = ds.Tables[0].Rows[0]["TITLE"].ToString();
                            //this.txbContents.Text = ds.Tables[0].Rows[0]["CONTENTS"].ToString();
                            this.txaContent.InnerText = ds.Tables[0].Rows[0]["CONTENTS"].ToString();
                        }
                    }

                    if (dsFile.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            rptFile.DataSource = dsFile.Tables[0];
                            rptFile.DataBind();
                        }
                    }
                }

                updPanel1.Update();
                updPanelFile.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (ds != null)
                    dsFile.Dispose();
            }
        }

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strTitle"></param>
        /// <param name="strContents"></param>
        /// <param name="strMsg"></param>
        /// <returns></returns>
        private bool ValidationCheck(string strTitle, string strContents, ref string strMsg)
        {
            bool bResult = true;

            if (strTitle.Length == 0)
            {
                strMsg = "제목을 입력해주세요.";
                bResult = false;
            }
            else if (strContents.Length == 0)
            {
                strMsg = "내용을 입력해주세요.";
                bResult = false;
            }

            return bResult;
        }
        #endregion

        #region SetFileUpload
        /// <summary>
        /// SetFileUpload
        /// </summary>
        private void SetFileUpload()
        {
            Stream stream = null;
            try
            {
                HttpFileCollection uploadFiles = Request.Files;

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];

                    stream = postedFIle.InputStream;

                    if (stream.Length == 0)
                        continue;

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    //type체크 필요 

                    int nFileLen = Convert.ToInt32(stream.Length);

                    byte[] byteFile = new byte[nFileLen];

                    stream.Read(byteFile, 0, nFileLen);

                    using (Notice Noti = new Notice())
                    {
                        Noti.InsBoardFile(BoardId, strFileNm, byteFile, strFileType, nFileLen);
                    }

                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('파일 업로드 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }
        }
        #endregion

        #region BtnRegBoard_ServerClick
        /// <summary>
        /// BtnRegBoard_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnRegBoard_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMsg = "";

                string strTitle = Request.Form[txbTitle.UniqueID] == null ? "" : Utility.ReplaceChar(Request.Form[txbTitle.UniqueID].ToString());
                //StringBuilder sbContents = new StringBuilder(Utility.ReplaceChar(txbContents.Text));

                if (!ValidationCheck(strTitle, Utility.ReplaceChar(this.txaContent.InnerText), ref strMsg))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationError", "alert('" + strMsg + "');", true);
                }
                else
                {
                    if (BoardId != "")
                    {
                        using (Notice Noti = new Notice())
                        {
                            Noti.UpdBoardList(BoardId, strTitle, Utility.ReplaceChar(this.txaContent.InnerText));
                        }
                    }
                    else
                    {
                        using (Notice Noti = new Notice())
                        {
                            ds = Noti.InsBoardList(strTitle, Utility.ReplaceChar(this.txaContent.InnerText), Session["EMPNO"].ToString());
                        }

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                BoardId = ds.Tables[0].Rows[0]["BOARDID"].ToString();
                            }
                        }
                    }

                    if (BoardId.Length > 0)
                    {
                        SetFileUpload();
                    }

                    //GetDataList();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetNotice", "alert('게시글 저장을 완료했습니다.');BoardList();", true);

                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region BtnDelBoard_ServerClick
        /// <summary>
        /// BtnDelBoard_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnDelBoard_ServerClick(object sender, EventArgs e)
        {
            try
            {
                using (Notice Noti = new Notice())
                {
                    Noti.DelBoardList(BoardId);
                    Noti.DelBoardFile(BoardId, "");
                    Noti.DelBoardReply(BoardId, "");
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "BoardDelete", "alert('게시글 삭제를 완료 했습니다.');BoardList();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnDownload_ServerClick
        /// <summary>
        /// BtnDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnDownload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlAnchor btnDownload = (HtmlAnchor)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                FileSeq = lblSeq.Text;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDownLoad", "FileDownload();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnFileDownload_ServerClick
        /// <summary>
        /// BtnFileDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnFileDownload_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                if (FileSeq.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다..');", true);
                }
                else
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardFile(BoardId, FileSeq);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                            byte[] bFile = new byte[nFileLen];
                            bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                            string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                            using (FileUtil fileUtil = new FileUtil())
                            {
                                fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                            }
                        }
                    }

                    FileSeq = "";
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region BtnDelete_ServerClick
        /// <summary>
        /// BtnDelete_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlButton btnDownload = (HtmlButton)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                using (Notice Noti = new Notice())
                {
                    Noti.DelBoardFile(BoardId, lblSeq.Text);
                }

                //GetDataList();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "alert('파일 삭제를 완료 했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion
    }
}