﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Mobile;
using KTS.KTSSolution.BSL.Car;

namespace KTSSolutionWeb
{
    public partial class UserCarMoveInfo : PageBase
    {
        #region Properties

        #region DtCarList
        private DataTable DtCarList
        {
            get
            {
                if (ViewState["DtCarList"] != null)
                    return (DataTable)ViewState["DtCarList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtCarList"] = value;
            }
        }
        #endregion

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetDDLCarInfo();

                    SetPageInit();
                }
            }
        }

        private void SetDDLCarInfo()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = this.Session["EMPNO"].ToString();

                using (CarMgmt Mgmt = new CarMgmt())
                {
                    ds = Mgmt.GetUserCarNum(strEmpNo);
                }

                ddlCarNum.Items.Clear();
                ddlCarNum.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    DtCarList = ds.Tables[0];

                    for (int i = 0; i < DtCarList.Rows.Count; i++)
                    {
                        ddlCarNum.Items.Add(new ListItem(DtCarList.Rows[i]["CARNUM"].ToString(), DtCarList.Rows[i]["CARNUM"].ToString()));
                    }
                }

                updPanelCarNum.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetPageInit()
        {
            try
            {
                this.txbStDt.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");

                if (this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                    || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                {
                    this.spanKtsEmpNo.Visible = true;
                    this.spanKtsEmpNm.Visible = true;

                    this.txbKtsEmpNo.Text = "";
                    this.txbEmpNm.Text = "";
                }
                else
                {
                    this.spanKtsEmpNo.Visible = false;
                    this.spanKtsEmpNm.Visible = false;

                    this.txbKtsEmpNo.Text = Session["KTSEMPNO"].ToString();
                    this.txbEmpNm.Text = Session["EMPNM"].ToString();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.Session["ORGCD"].ToString();
                string strEmpNo = this.Session["EMPNO"].ToString();
                string strKtsEmpNo = this.txbKtsEmpNo.Text.ToString();
                string strEmpNm = this.txbEmpNm.Text.ToString();
                string strCarNum = this.txbCarNum.Text.ToString();
                string strStDt = Request.Form[this.txbStDt.UniqueID].ToString().Replace("-", "");
                string strEnDt = Request.Form[this.txbEnDt.UniqueID].ToString().Replace("-", "");

                using (CarMgmt mgmt = new CarMgmt())
                {
                    if (this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                        || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                    {
                        ds = mgmt.GetUserCarInfoList_ByEmpUser(strOrgCd, strKtsEmpNo, strEmpNm, strCarNum, strStDt, strEnDt);
                    }
                    else
                    {
                        ds = mgmt.GetUserCarInfoList(strOrgCd, strEmpNo, strCarNum, strStDt, strEnDt);
                    }
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnRegCar_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = this.Session["EMPNO"].ToString();
                string strCarNum = this.ddlCarNum.SelectedValue;
                string strWorkDt = Request.Form[this.txbWorkDt.UniqueID].Replace("-", "");
                string strFuels = this.txbFuels.Text.Length == 0 ? "0" : this.txbFuels.Text;
                string strEnkm = this.txbEnkm.Text;
                string strLeaveYn = this.chkReqLeave.Checked ? "R" : "N";
                string strChkKm = this.hfStKm.Value.Length == 0 ? "0" : this.hfStKm.Value;
                string strRegYn = this.hfRegYn.Value;

                if (strCarNum.Length == 0 || strWorkDt.Length == 0 || strEnkm.Length == 0 || strRegYn.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    bool bSave = true;

                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        ds = mgmt.GetUserCarInfoCheck(strCarNum);
                    }
                    string strMsg = "";

                    if (strRegYn.Equals("N"))
                    {
                        bSave = false;
                        strMsg = "해당 차량은 직퇴승인 대기중이거나 직퇴결과가 입력되지 않았습니다.";
                    }

                    DateTime ReqDate = DateTime.Parse(Request.Form[this.txbWorkDt.UniqueID].ToString());
                    DateTime NowDate = DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"));

                    if (bSave)
                    {
                        if (ReqDate >= NowDate)
                        {
                            bSave = false;
                            strMsg = "운행일자는 미래가 될 수 없습니다.";
                        }
                    }

                    if (bSave && ds.Tables[0].Rows.Count > 0)
                    {
                        DateTime ChkDate = DateTime.Parse(ds.Tables[0].Rows[0]["WORKDATE"].ToString());

                        if (ReqDate == ChkDate)
                        {
                            bSave = false;
                            strMsg = "해당 차량은 운행일자가 같은 운행정보가 이미 등록 되어있습니다.";
                        }
                        else if (ReqDate < ChkDate)
                        {
                            bSave = false;
                            strMsg = "마지막 운행일지 등록일보다 이전 날짜는 등록할 수 없습니다.";
                        }
                        else
                        {
                            bSave = true;
                        }
                    }

                    if (bSave && int.Parse(strChkKm) >= int.Parse(strEnkm))
                    {
                        bSave = false;
                        strMsg = "마지막 운행일지에 등록된 최종km 보다 종료km 가 작으면 등록할 수 없습니다.";
                    }

                    if (!bSave)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strMsg + "');", true);
                    }
                    else
                    {

                        using (CarMgmt mgmt = new CarMgmt())
                        {
                            mgmt.SetUserCarInfo(strEmpNo, strCarNum, strWorkDt, strFuels, strEnkm, strLeaveYn);
                        }

                        this.ddlCarNum.SelectedIndex = 0;
                        this.txbWorkDt.Text = "";
                        this.txbFuels.Text = "";
                        this.txbEnkm.Text = "";
                        this.chkReqLeave.Checked = false;

                        updPanelCarNum.Update();
                        updPanelFuels.Update();
                        updPanelEnKm.Update();
                        updPanelLeave.Update();

                        SetDDLCarInfo();
                        GetDataList();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('운행일지가 등록 되었습니다.');InitValue();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('차량정보 저장 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            HtmlTableCell td = (HtmlTableCell)rptResult.Items[i].FindControl("td");
                            HtmlTableRow trStatus = (HtmlTableRow)rptResult.Items[i].FindControl("trStatus");
                            Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");
                            Label lblLeaveYn = (Label)rptResult.Items[i].FindControl("lblLeaveYn");
                            Label lblLeaveKm = (Label)rptResult.Items[i].FindControl("lblLeaveKm");
                            TextBox txbLeaveKm = (TextBox)rptResult.Items[i].FindControl("txbLeaveKm");
                            Label lblModifyYn = (Label)rptResult.Items[i].FindControl("lblModifyYn");
                            Label lblEnKm = (Label)rptResult.Items[i].FindControl("lblEnKm");
                            TextBox txbEnKm2 = (TextBox)rptResult.Items[i].FindControl("txbEnKm2");
                            Label lblFuels = (Label)rptResult.Items[i].FindControl("lblFuels");
                            TextBox txbFuels2 = (TextBox)rptResult.Items[i].FindControl("txbFuels2");
                            Button btnSave = (Button)rptResult.Items[i].FindControl("btnSave");
                            Button btnConfirm = (Button)rptResult.Items[i].FindControl("btnConfirm");
                            Button btnReturn = (Button)rptResult.Items[i].FindControl("btnReturn");
                            HtmlTableRow trOrg = (HtmlTableRow)rptResult.Items[i].FindControl("trOrg");
                            HtmlTableRow trEmpInfo = (HtmlTableRow)rptResult.Items[i].FindControl("trEmpInfo");
                            Button btnDelete = (Button)rptResult.Items[i].FindControl("btnDelete");

                            trStatus.Visible = false;

                            //"E" 직퇴 저장 완료
                            //"Y" 직퇴 승인 완료
                            //"R" 직퇴 승인 요청
                            //"C" 직퇴 요청 반려
                            //"N" 직퇴X

                            if (this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                                || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                            {
                                trOrg.Visible = true;
                                trEmpInfo.Visible = true;

                                if (i == 0 && !this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                                {
                                    btnDelete.Visible = true;
                                    btnDelete.Enabled = true;

                                    AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                                    asyncBtnDelete.ControlID = btnDelete.UniqueID;
                                    asyncBtnDelete.EventName = "Click";

                                    updPanel1.Triggers.Add(asyncBtnDelete);
                                }
                                else
                                {
                                    btnDelete.Visible = false;
                                    btnDelete.Enabled = false;
                                }
                            }
                            else
                            {
                                trOrg.Visible = false;
                                trEmpInfo.Visible = false;

                                btnDelete.Visible = false;
                                btnDelete.Enabled = false;
                            }

                            if (lblLeaveYn.Text.Equals("Y"))
                            {
                                if (Session["EMPNO"].ToString().Equals(lblEmpNo.Text)
                                    || this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                                    || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                                {
                                    lblLeaveKm.Visible = false;
                                    txbLeaveKm.Visible = true;

                                    lblEnKm.Visible = false;
                                    txbEnKm2.Visible = true;

                                    lblFuels.Visible = false;
                                    txbFuels2.Visible = true;

                                    AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();

                                    btnSave.Visible = true;
                                    btnSave.Enabled = true;

                                    asyncBtnSave.ControlID = btnSave.UniqueID;
                                    asyncBtnSave.EventName = "Click";

                                    updPanel1.Triggers.Add(asyncBtnSave);
                                }
                                else
                                {
                                    lblLeaveKm.Visible = true;
                                    txbLeaveKm.Visible = false;

                                    lblEnKm.Visible = true;
                                    txbEnKm2.Visible = false;

                                    lblFuels.Visible = true;
                                    txbFuels2.Visible = false;
                                }

                                trStatus.Visible = true;
                                td.InnerText = "승인되었습니다.";
                            }
                            else if (lblLeaveYn.Text.Equals("R"))
                            {
                                lblLeaveKm.Visible = true;
                                txbLeaveKm.Visible = false;

                                lblEnKm.Visible = true;
                                txbEnKm2.Visible = false;

                                lblFuels.Visible = true;
                                txbFuels2.Visible = false;

                                if (Session["EMPNO"].ToString().Equals(lblEmpNo.Text) && i == 0)
                                {
                                    lblEnKm.Visible = false;
                                    txbEnKm2.Visible = true;

                                    lblFuels.Visible = false;
                                    txbFuels2.Visible = true;

                                    AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();

                                    btnSave.Visible = true;
                                    btnSave.Enabled = true;

                                    asyncBtnSave.ControlID = btnSave.UniqueID;
                                    asyncBtnSave.EventName = "Click";

                                    updPanel1.Triggers.Add(asyncBtnSave);
                                }

                                if (this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                                    || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                                {
                                    AsyncPostBackTrigger asyncBtnConfirm = new AsyncPostBackTrigger();

                                    btnConfirm.Visible = true;
                                    btnConfirm.Enabled = true;

                                    asyncBtnConfirm.ControlID = btnConfirm.UniqueID;
                                    asyncBtnConfirm.EventName = "Click";

                                    updPanel1.Triggers.Add(asyncBtnConfirm);

                                    AsyncPostBackTrigger asyncBtnReturn = new AsyncPostBackTrigger();

                                    btnReturn.Visible = true;
                                    btnReturn.Enabled = true;

                                    asyncBtnReturn.ControlID = btnReturn.UniqueID;
                                    asyncBtnReturn.EventName = "Click";

                                    updPanel1.Triggers.Add(asyncBtnReturn);
                                }

                                trStatus.Visible = true;
                                td.InnerText = "승인대기중입니다.";
                            }
                            else if (lblLeaveYn.Text.Equals("C"))
                            {
                                lblLeaveKm.Visible = true;
                                txbLeaveKm.Visible = false;

                                lblEnKm.Visible = true;
                                txbEnKm2.Visible = false;

                                lblFuels.Visible = true;
                                txbFuels2.Visible = false;

                                if (Session["EMPNO"].ToString().Equals(lblEmpNo.Text))
                                {
                                    lblEnKm.Visible = false;
                                    txbEnKm2.Visible = true;

                                    lblFuels.Visible = false;
                                    txbFuels2.Visible = true;

                                    AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();

                                    btnSave.Visible = true;
                                    btnSave.Enabled = true;

                                    asyncBtnSave.ControlID = btnSave.UniqueID;
                                    asyncBtnSave.EventName = "Click";

                                    updPanel1.Triggers.Add(asyncBtnSave);
                                }

                                trStatus.Visible = true;
                                td.InnerText = "반려되었습니다.";
                            }
                            else if (lblModifyYn.Text.Equals("Y"))
                            {
                                if (Session["EMPNO"].ToString().Equals(lblEmpNo.Text)
                                    || this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                                    || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                                {
                                    if (lblLeaveYn.Text.Equals("E"))
                                    {
                                        lblLeaveKm.Visible = false;
                                        txbLeaveKm.Visible = true;
                                    }
                                    else
                                    {
                                        lblLeaveKm.Visible = true;
                                        txbLeaveKm.Visible = false;
                                    }

                                    lblEnKm.Visible = false;
                                    txbEnKm2.Visible = true;

                                    lblFuels.Visible = false;
                                    txbFuels2.Visible = true;

                                    AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();

                                    btnSave.Visible = true;
                                    btnSave.Enabled = true;

                                    btnSave.Text = "수정";

                                    asyncBtnSave.ControlID = btnSave.UniqueID;
                                    asyncBtnSave.EventName = "Click";

                                    updPanel1.Triggers.Add(asyncBtnSave);
                                }
                                else
                                {
                                    lblLeaveKm.Visible = true;
                                    txbLeaveKm.Visible = false;

                                    lblEnKm.Visible = true;
                                    txbEnKm2.Visible = false;

                                    lblFuels.Visible = true;
                                    txbFuels2.Visible = false;
                                }
                            }
                        }
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblEmpNo = (Label)btnSave.Parent.FindControl("lblEmpNo");
                Label lblCarNum = (Label)btnSave.Parent.FindControl("lblCarNum");
                Label lblWorkDt = (Label)btnSave.Parent.FindControl("lblWorkDt");
                Label lblLeaveYn = (Label)btnSave.Parent.FindControl("lblLeaveYn");
                TextBox txbLeaveKm = (TextBox)btnSave.Parent.FindControl("txbLeaveKm");

                Label lblModifyYn = (Label)btnSave.Parent.FindControl("lblModifyYn");
                Label lblStKm = (Label)btnSave.Parent.FindControl("lblStKm");
                TextBox txbEnKm2 = (TextBox)btnSave.Parent.FindControl("txbEnKm2");
                TextBox txbFuels2 = (TextBox)btnSave.Parent.FindControl("txbFuels2");


                string strEmpNo = lblEmpNo.Text;
                string strCarNum = lblCarNum.Text;
                string strWorkDt = lblWorkDt.Text.Replace("-", "");
                string strLeaveKm = txbLeaveKm.Text;
                string strEnKm = txbEnKm2.Text;
                string strFuels = txbFuels2.Text;

                if (strEmpNo.Length == 0 || strCarNum.Length == 0 || (!lblLeaveYn.Text.Equals("Y") && !lblModifyYn.Text.Equals("Y")) || strWorkDt.Length == 0 || lblStKm.Text.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else if (lblLeaveYn.Text.Equals("Y") && (strLeaveKm.Equals("") || strLeaveKm.Equals("0")))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('직퇴km를 입력해주세요.');", true);
                }
                else if (lblLeaveYn.Text.Equals("Y") && (int.Parse(strLeaveKm) <= int.Parse(strEnKm)))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('직퇴km는 종료km 보다 커야합니다.');", true);
                }
                else if (lblModifyYn.Text.Equals("Y") && (strEnKm.Equals("") || strEnKm.Equals("0")))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('종료km를 입력해주세요.');", true);
                }
                else if (lblModifyYn.Text.Equals("Y") && (int.Parse(strEnKm) <= int.Parse(lblStKm.Text)))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('종료km는 시작km 보다 커야합니다.');", true);
                }
                else if (strFuels.Equals(""))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('주유(ℓ)를 입력해주세요.');", true);
                }
                else
                {
                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        if (lblModifyYn.Text.Equals("Y"))
                        {
                            mgmt.SetUserCarEnKm(strEmpNo, strCarNum, strWorkDt, strEnKm, strFuels);

                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('종료km가 수정되었습니다.');", true);
                        }

                        if (lblLeaveYn.Text.Equals("Y"))
                        {
                            mgmt.SetUserCarLeaveKm(strEmpNo, strCarNum, strWorkDt, strLeaveKm, strFuels);

                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('직퇴km가 저장되었습니다.');", true);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('저장되었습니다.');", true);
                    }

                    SetDDLCarInfo();
                    GetDataList();
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('직퇴km 저장 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                Label lblEmpNo = (Label)btn.Parent.FindControl("lblEmpNo");
                Label lblCarNum = (Label)btn.Parent.FindControl("lblCarNum");
                Label lblWorkDt = (Label)btn.Parent.FindControl("lblWorkDt");
                Label lblLeaveYn = (Label)btn.Parent.FindControl("lblLeaveYn");

                string strEmpNo = lblEmpNo.Text;
                string strCarNum = lblCarNum.Text;
                string strWorkDt = lblWorkDt.Text.Replace("-", "");

                string strLeaveYn = btn.ID == "btnConfirm" ? "Y" : "C";

                string strConfirmUser = this.Session["EMPNO"].ToString();

                if (strEmpNo.Length == 0 || strCarNum.Length == 0 || !lblLeaveYn.Text.Equals("R") || strWorkDt.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        mgmt.SetUserCarLeaveConfirm(strEmpNo, strCarNum, strWorkDt, strLeaveYn, strConfirmUser);
                    }

                    SetDDLCarInfo();
                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('직퇴요청이 승인되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('직퇴요청 승인 중 오류가 발생했습니다.');", true);
            }
        }

        protected void ddlCarNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strCarNum = ddlCarNum.SelectedValue;

                if (strCarNum.Length > 0)
                {
                    string strWhere = string.Format("CARNUM = '{0}'", strCarNum);
                    DataRow[] dr = DtCarList.Select(strWhere);

                    if (dr.Length > 0)
                    {
                        this.hfStKm.Value = dr[0]["STKM"].ToString();
                        this.hfRegYn.Value = dr[0]["REGYN"].ToString();
                        this.txbNoti.Text = dr[0]["NOTICE"].ToString();
                    }
                    else
                    {
                        this.hfStKm.Value = "";
                        this.hfRegYn.Value = "";
                        this.txbNoti.Text = "";
                    }
                }
                else
                {
                    this.hfStKm.Value = "";
                    this.hfRegYn.Value = "";
                    this.txbNoti.Text = "";
                }

                updPanelCarNum.Update();
                updPanelNoti.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                Label lblEmpNo = (Label)btn.Parent.FindControl("lblEmpNo");
                Label lblCarNum = (Label)btn.Parent.FindControl("lblCarNum");
                Label lblWorkDt = (Label)btn.Parent.FindControl("lblWorkDt");

                string strEmpNo = lblEmpNo.Text;
                string strCarNum = lblCarNum.Text;
                string strWorkDt = lblWorkDt.Text.Replace("-", "");

                if (strEmpNo.Length == 0 || strCarNum.Length == 0 || strWorkDt.Length == 0 || !btn.Enabled)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        mgmt.DelUserCarMoveInfo(strEmpNo, strCarNum, strWorkDt);
                    }

                    SetDDLCarInfo();
                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('삭제 되었습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}