﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_HTC.Master" AutoEventWireup="true" CodeBehind="HTC.aspx.cs" Inherits="KTSSolutionWeb.HTC" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function ChkSendMsg() {
            if (confirm("선택한 업체정보로 메세지를 전송 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
		}
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">

		<div class="page">
			<h2>고객편의서비스</h2>

			<asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
				<ContentTemplate>					
					<!-- S:fieldset -->
					<fieldset>
						<legend>HTC</legend>
						<span class="inpbox" style="width:49%">
							<label>사번</label>
							<asp:TextBox ID="txbEmpNo" runat="server" ReadOnly="true"></asp:TextBox>
						</span>
						<span class="inpbox" style="width:49%">
							<label>이름</label>
							<asp:TextBox ID="txbEmpNm" runat="server" ReadOnly="true"></asp:TextBox>
						</span>
					</fieldset>
					<fieldset>
						<span class="inpbox" style="width:100%">
							<label style="width:15%">소속</label>
							<asp:TextBox ID="txbOrgNm" runat="server" Width="84.5%" ReadOnly="true"></asp:TextBox>
						</span>
					</fieldset>
					<!-- E:fieldset -->

					<h2></h2>

					<!-- S:fieldset -->
					<fieldset>
						<span class="optionbox" style="width:100%">
							<label style="width:15%">업체</label>
							<asp:DropDownList ID="ddlCompanyUrl" runat="server" Width="83%"></asp:DropDownList>
						</span>
					</fieldset>
					<!-- E:feileset -->
					<!-- S:fieldset -->
					<fieldset>
						<span class="inpbox" style="width:100%">
							<label style="width:15%">내용</label>
							<asp:TextBox ID="txbContent" runat="server" Width="83%"></asp:TextBox>
						</span>
					</fieldset>
					<!-- E:feileset -->
					<div>
						<span class="inpbox" style="width:100%">
							<label style="width:15%;margin-left:-8px;">전화번호</label>
							<asp:TextBox ID="txbPhone1" MaxLength="3" runat="server" Width="23%"></asp:TextBox>
							<label style="margin-left:8px;">-</label>
							<asp:TextBox ID="txbPhone2" MaxLength="4" runat="server" Width="23%"></asp:TextBox>
							<label style="margin-left:8px;">-</label>
							<asp:TextBox ID="txbPhone3" MaxLength="4" runat="server" Width="23%"></asp:TextBox>
						</span>
					</div>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnSendMsg" EventName="click" />
				</Triggers>
			</asp:UpdatePanel>

			<div style="text-align:center;margin-top:40px;">
				<asp:Button ID="btnSendMsg" runat="server"  class="btn-green" style="width:150px" OnClientClick="return ChkSendMsg();" OnClick="btnSendMsg_Click" BorderStyle="None" Text="전송" />
			</div>
		</div>		
	</div>
	<!--//E: contentsarea -->
</asp:Content>