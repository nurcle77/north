﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.WorkingClothes;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class WorkingClothInfo : PageBase
    {
        private string REQYN
        {
            get
            {
                if (ViewState["REQYN"] != null)
                {
                    return ViewState["REQYN"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["REQYN"] = value;
            }
        }

        private string NEWREQYN
        {
            get
            {
                if (ViewState["NEWREQYN"] != null)
                {
                    return ViewState["NEWREQYN"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["NEWREQYN"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetDDLYears();
                }
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = Session["EMPNO"].ToString();
                string strYears = ddlYears.SelectedValue;
                string strPointSeq = ddlPointSeq.SelectedValue;
                string strReqType = "N";
                REQYN = "N";
                NEWREQYN = "N";

                bool bViewPoint = false;
                bool bReq = false;

                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetRequestUserClothesList(strEmpNo, strYears, strPointSeq);
                }

                if (ds.Tables.Count > 0)
                {
                    rptResult.DataSource = ds.Tables[0];
                    rptResult.DataBind();

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        strReqType = ds.Tables[1].Rows[0]["REQTYPE"].ToString();
                    }

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        REQYN = ds.Tables[2].Rows[0]["REQYN"].ToString();
                        NEWREQYN = ds.Tables[2].Rows[0]["NEWREQYN"].ToString();
                    }
                }

                if (this.Session["JOBNM2"].ToString().ToLower().Equals("kte") && !strReqType.Equals("N"))
                {
                    bViewPoint = true;
                }
                else
                {
                    bViewPoint = false;
                }

                if (!strYears.Equals("") && !strPointSeq.Equals("") && REQYN.Equals("Y") && NEWREQYN.Equals("Y"))
                {
                    if (strReqType.Equals("N"))
                    {
                        bReq = true;
                    }
                    if (strReqType.Equals("A"))
                    {
                        if (this.Session["JOBNM2"].ToString().ToLower().Equals("kte"))
                        {
                            bReq = true;
                        }
                        else
                        {
                            bReq = false;
                        }
                    }
                    else
                    {
                        bReq = true;
                    }
                }
                else
                {
                    bReq = false;
                }

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    HtmlTableRow tr1 = (HtmlTableRow)rptResult.Items[i].FindControl("tr1");
                    HtmlTableRow tr2 = (HtmlTableRow)rptResult.Items[i].FindControl("tr2");
                    
                    HtmlTableCell tdReqStat = (HtmlTableCell)rptResult.Items[i].FindControl("tdReqStat");
                    HtmlTableCell thPoint = (HtmlTableCell)rptResult.Items[i].FindControl("thPoint");
                    HtmlTableCell tdPoint = (HtmlTableCell)rptResult.Items[i].FindControl("tdPoint");
                    
                    Label lblYears = (Label)rptResult.Items[i].FindControl("lblYears");
                    Label lblPointSeq = (Label)rptResult.Items[i].FindControl("lblPointSeq");
                    Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");
                    Label lblReqType = (Label)rptResult.Items[i].FindControl("lblReqType");

                    tr1.Attributes.Add("onclick", "RegRequestCloth('" + lblYears.Text + "', '" + lblPointSeq.Text + "', '" + lblEmpNo.Text + "', '" + lblReqType.Text + "')");
                    tr2.Attributes.Add("onclick", "RegRequestCloth('" + lblYears.Text + "', '" + lblPointSeq.Text + "', '" + lblEmpNo.Text + "', '" + lblReqType.Text + "')");

                    if (bViewPoint)
                    {
                        tdReqStat.ColSpan = 1;
                        thPoint.Visible = true;
                        tdPoint.Visible = true;
                    }
                    else
                    {
                        tdReqStat.ColSpan = 3;
                        thPoint.Visible = false;
                        tdPoint.Visible = false;
                    }
                }

                if (bReq)
                {
                    btnReq.Visible = true;
                    btnReq.Attributes.Remove("onclick");
                    btnReq.Attributes.Add("onclick", "RegRequestCloth('" + strYears + "', '" + strPointSeq + "', '" + strEmpNo + "', '" + strReqType + "')");
                }
                else
                {
                    btnReq.Visible = false;
                    btnReq.Attributes.Remove("onclick");
                }

                updPanelBtn.Update();
                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSearh_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                ddlPointSeq.Items.Clear();

                string strYears = ddlYears.SelectedValue;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointInfo(strYears);
                }

                ddlPointSeq.Items.Add(new ListItem("선택하세요", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlPointSeq.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TITLE"].ToString(), ds.Tables[0].Rows[i]["POINTSEQ"].ToString()));
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

        }
    }
}