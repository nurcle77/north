﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Pass;

namespace KTSSolutionWeb
{
    public partial class PassRegM : PageBase
    {
        #region fields

        #region PROCID
        /// <summary>
        /// PROCID
        /// </summary>
        private string PROCID
        {
            get
            {
                if (ViewState["PROCID"] != null)
                    return ViewState["PROCID"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["PROCID"] = value;
            }
        }
        #endregion

        #region CUSTSIGNYN
        /// <summary>
        /// PROCID
        /// </summary>
        private string CUSTSIGNYN
        {
            get
            {
                if (ViewState["CUSTSIGNYN"] != null)
                    return ViewState["CUSTSIGNYN"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["CUSTSIGNYN"] = value;
            }
        }
        #endregion

        #region DtSvc
        /// <summary>
        /// DtSvc
        /// </summary>
        private DataTable DtSvc
        {
            get
            {
                if (ViewState["DtSvc"] != null)
                {
                    return (DataTable)ViewState["DtSvc"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["DtSvc"] = value;
            }
        }
        #endregion

        #region DtConsult
        /// <summary>
        /// DtConsult
        /// </summary>
        private DataTable DtConsult
        {
            get
            {
                if (ViewState["DtConsult"] != null)
                {
                    return (DataTable)ViewState["DtConsult"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["DtConsult"] = value;
            }
        }
        #endregion

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    initializeControls();

                    this.txbHopeDt.Text = DateTime.Now.ToString("yyyy-MM-dd");

                    if (!PROCID.Equals("-1"))
                    {
                        GetDataList();
                    }
                }
            }
        }
        #endregion

        #region ddlPassType_SelectedIndexChanged
        /// <summary>
        /// ddlPassType_SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPassType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetDropDownList();

                this.txbCompEmpNm.Text = "";
                this.hfCompEmpNo.Value = "";

                this.updPanelRel.Update();
                this.updPanelSvc.Update();
                this.updPanelConsult.Update();
                this.updPanelAgree.Update();
                this.updPanelComp.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region ddlSvcType_SelectedIndexChanged
        /// <summary>
        /// ddlSvcType_SelectedIndexChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void ddlSvcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strPassType = this.ddlPassType.SelectedValue;
                this.txbCompEmpNm.Attributes.Remove("onClick");

                if (strPassType.Equals("COMP"))
                {
                    string strSvcType = this.ddlSvcType.SelectedValue;
                    string strScript = "";

                    if (strSvcType.Equals("직원"))
                    {                
                        this.txbCompEmpNm.Attributes.Add("onClick", "ModalShow();");
                        strScript = "$('#spanCustNm').hide();$('#spanCustTel').hide();$('#spanComp').show();";			
                    }
                    else
                    {
                        strScript = "$('#spanCustNm').show();$('#spanCustTel').show();$('#spanComp').hide();";
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "controlResize", strScript, true);

                    updPanelComp.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnRegPass_Click
        /// <summary>
        /// btnRegPass_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegPass_Click(object sender, EventArgs e)
        {
            try
            {
                string strEmpNo = this.Session["EMPNO"].ToString();
                string strCustNm = this.txbCustNm.Text;
                string strCustTelNo = this.txbTelNo.Text.Replace("-", "");
                string strHopeDt = Request.Form[this.txbHopeDt.UniqueID].Replace("-", "");
                string strRelType = this.txbRelType.Text;
                string strPassType = this.ddlPassType.SelectedValue;
                string strSvcType = this.ddlSvcType.SelectedValue;
                string strMotType = this.ddlMotType.SelectedValue;
                string strConsult = this.ddlConsult.SelectedValue;
                string strCompEmpNo = this.hfCompEmpNo.Value;
                string strNoti = this.txaNoti.Value.Replace("<", "(").Replace(">", ")");

                string strSignYn = "";
                string strImgData = "";
                string strType = "U";

                string strMsg = "";

                if (strPassType.Equals("COMP"))
                {
                    if (strSvcType.Equals("직원"))
                    {
                        strCustNm = "칭찬";
                    }

                    strRelType = strSvcType;
                }
                if (PROCID.Equals("-1"))
                {
                    strSignYn = Request.Form[this.hfSignYn.UniqueID];
                    strImgData = Request.Form[this.hfImgSrc.UniqueID];
                    strType = "I";
                }

                if (ValidationCheck(strCustNm, strCustTelNo, strHopeDt, strRelType, strPassType, strSvcType, strCompEmpNo, strConsult, strMotType, strSignYn, strImgData, ref strMsg))
                {
                    if (!strPassType.Equals("MOT"))
                    {
                        strMotType = "";
                    }

                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.SetPassProcess(strEmpNo, strCustNm, strCustTelNo, strHopeDt, strRelType, strPassType, strSvcType, strMotType, strConsult, strNoti, strCompEmpNo, strImgData, PROCID);
                    }

                    string strScript = "";

                    if (PROCID.Equals("-1"))
                    {
                        strScript = "alert('PASS가 저장되었습니다.');PassInfoList('I');";
                    }
                    else
                    {
                        strScript = "alert('PASS가 저장되었습니다.');PassInfoList('U');";
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", strScript, true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strMsg + "');controlResize('" + strPassType + "', '" + strType + "');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('저장 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnEmpNo_Click
        /// <summary>
        /// btnEmpNo_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnEmpNo_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNm = this.txbEmpNm.Text;

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetUserList(strEmpNm);
                }

                rptResult3.DataSource = ds.Tables[0];
                rptResult3.DataBind();

                for (int i = 0; i < rptResult3.Items.Count; i++)
                {
                    Button btnConfirm = (Button)rptResult3.Items[i].FindControl("btnConfirm");

                    AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
                    trigger.ControlID = btnConfirm.UniqueID;
                    trigger.EventName = "Click";
                    updPanelModal.Triggers.Add(trigger);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region btnConfirm_Click
        /// <summary>
        /// btnConfirm_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnConfirm = (Button)sender;
                Label lblOrgFullNm = (Label)btnConfirm.Parent.FindControl("lblOrgFullNm");
                Label lblEmpNm = (Label)btnConfirm.Parent.FindControl("lblEmpNm");
                Label lblEmpNo = (Label)btnConfirm.Parent.FindControl("lblEmpNo");

                string strOrgFullNm = lblOrgFullNm.Text;
                string strEmpNm = lblEmpNm.Text;
                string strEmpNo = lblEmpNo.Text;

                this.txbCompEmpNm.Text = strOrgFullNm + " " + strEmpNm;
                this.hfCompEmpNo.Value = strEmpNo;

                this.updPanelComp.Update();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalClose", "ModalClose();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region initializeControls
        /// <summary>
        /// initializeControls
        /// </summary>
        private void initializeControls()
        {
            DataSet dsSvc = null;
            DataSet dsConsult = null;

            try
            {
                if (!IsPostBack)
                {
                    PROCID = Request.Form["pPROCID"] == null ? "-1" : Request.Form["pPROCID"].ToString();
                    CUSTSIGNYN = Request.Form["pCUSTSIGNYN"] == null ? "-1" : Request.Form["pCUSTSIGNYN"].ToString();
                    string strEmpNo = this.Session["EMPNO"].ToString();

                    using (PassMgmt pass = new PassMgmt())
                    {
                        dsSvc = pass.GetPassSvcList();
                        dsConsult = pass.GetPassBizAuthist(strEmpNo);
                    }

                    if (dsSvc.Tables.Count > 0)
                    {
                        DtSvc = dsSvc.Tables[0];
                    }

                    if (dsConsult.Tables.Count > 0)
                    {
                        DtConsult = dsConsult.Tables[0];
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (dsSvc != null)
                    dsSvc.Dispose();

                if (dsConsult != null)
                    dsConsult.Dispose();
            }
        }
        #endregion

        #region GetDataList
        /// <summary>
        /// GetDataList
        /// </summary>
        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetPassProcessForProcId(PROCID);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.txbCustNm.Text = ds.Tables[0].Rows[0]["CUSTNM"].ToString();   
                        this.txbTelNo.Text = ds.Tables[0].Rows[0]["CUSTTELNO"].ToString();
                        this.txbHopeDt.Text = ds.Tables[0].Rows[0]["HOPEDT"].ToString();
                        this.ddlPassType.SelectedValue = ds.Tables[0].Rows[0]["PASSTYPE"].ToString();
                        this.txaNoti.Value = ds.Tables[0].Rows[0]["NOTI1"].ToString();

                        SetDropDownList();

                        string strPassType = this.ddlPassType.SelectedValue;

                        this.ddlSvcType.SelectedValue = ds.Tables[0].Rows[0]["SVCTYPE"].ToString();

                        if (!strPassType.Equals("COMP"))
                        {
                            this.txbRelType.Text = ds.Tables[0].Rows[0]["RELATIONTYPE"].ToString();

                            if (strPassType.Equals("MOT"))
                            {
                                this.ddlMotType.SelectedValue = ds.Tables[0].Rows[0]["MOTTYPE"].ToString();
                            }
                            else if (strPassType.Equals("BIZ"))
                            {
                                this.ddlConsult.SelectedValue = ds.Tables[0].Rows[0]["TRTEMPNO"].ToString();
                            }

                            this.imgSign.Src = ds.Tables[0].Rows[0]["IMGDATA"].ToString();
                            chkAgree.Checked = true;
                        }
                        else
                        {
                            this.txbCompEmpNm.Text = ds.Tables[0].Rows[0]["COMPEMPNM"].ToString();
                            this.hfCompEmpNo.Value = ds.Tables[0].Rows[0]["COMPEMPNO"].ToString();
                        }

                        this.updPanelRel.Update();
                        this.updPanelSvc.Update();
                        this.updPanelConsult.Update();
                        this.updPanelAgree.Update();
                        this.updPanelComp.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');PassInfoList('I');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');PassInfoList('I');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region SetDropDownList
        /// <summary>
        /// SetDropDownList
        /// </summary>
        private void SetDropDownList()
        {
            try
            {
                this.ddlSvcType.Items.Clear();
                this.ddlConsult.Items.Clear();

                string strPassType = this.ddlPassType.SelectedValue;
                string strWhere = string.Format("PASSTYPE = '{0}'", strPassType);

                string strType = "U";

                if (PROCID.Equals("-1") || CUSTSIGNYN.Equals("Y"))
                {
                    strType = "I";
                }

                DataRow[] drSvc = DtSvc.Select(strWhere);

                this.ddlSvcType.Items.Add(new ListItem("선택하세요", ""));

                if (!strPassType.Equals("COMP"))
                {
                    this.lblDt.InnerText = "희망일자";
                    this.lblSvcType.InnerText = "희망상품";
                }
                else
                {
                    this.txbRelType.Text = "";

                    this.lblDt.InnerText = "일자";
                    this.lblSvcType.InnerText = "관계";
                }

                foreach (DataRow dr in drSvc)
                {
                    this.ddlSvcType.Items.Add(new ListItem(dr["SVCTYPE"].ToString(), dr["SVCTYPE"].ToString()));
                }

                this.ddlSvcType.SelectedIndex = 0;

                if (strPassType.Equals("MOT"))
                {
                    this.ddlConsult.Visible = false;
                    this.ddlMotType.Visible = true;

                    this.lblConsult.InnerText = "타입";
                }
                else if (strPassType.Equals("BIZ"))
                {
                    this.ddlConsult.Visible = true;
                    this.ddlMotType.Visible = false;

                    this.ddlConsult.Items.Add(new ListItem("선택하세요", ""));

                    foreach (DataRow dr in DtConsult.Rows)
                    {
                        this.ddlConsult.Items.Add(new ListItem(dr["AUTHOPERNM"].ToString(), dr["AUTHOPERATOR"].ToString()));
                    }

                    this.lblConsult.InnerText = "컨설팅";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "controlResize", "javascript:controlResize('" + strPassType + "', '" + strType +"');", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strCustNm">string</param>
        /// <param name="strCustTelNo">string</param>
        /// <param name="strHopeDt">string</param>
        /// <param name="strRelType">string</param>
        /// <param name="strPassType">string</param>
        /// <param name="strSvcType">string</param>
        /// <param name="strCompEmpNo">string</param>
        /// <param name="strConsult">string</param>
        /// <param name="strMotType">string</param>
        /// <param name="strSignYn">string</param>
        /// <param name="strImgData">string</param>
        /// <param name="strMsg">ref string</param>
        /// <returns></returns>
        private bool ValidationCheck(string strCustNm, string strCustTelNo, string strHopeDt, string strRelType, string strPassType, string strSvcType, string strCompEmpNo, string strConsult, string strMotType, string strSignYn, string strImgData, ref string strMsg)
        {
            bool bResult = true;

            try
            {
                if (strPassType.Length == 0)
                {
                    strMsg = "잘못된 접근입니다.";
                    bResult = false;
                }
                else if (strCustNm.Length == 0 && !(strPassType.Equals("COMP") && strSvcType.Equals("직원")))
                {
                    strMsg = "성명을 입력해주세요.";
                    bResult = false;
                }
                else if (strCustTelNo.Length == 0 && !(strPassType.Equals("COMP") && strSvcType.Equals("직원")))
                {
                    strMsg = "TEL을 입력해주세요.";
                    bResult = false;
                }
                else if (strRelType.Length == 0)
                {
                    strMsg = "관계를 입력해주세요.";
                    bResult = false;
                }
                else if (strHopeDt.Length == 0)
                {
                    strMsg = "일자를 선택해주세요.";
                    bResult = false;
                }
                else if (strSvcType.Length == 0)
                {
                    if (strPassType.Equals("COMP"))
                    {
                        strMsg = "관계를 선택해주세요.";
                    }
                    else
                    {
                        strMsg = "희망상품을 선택해주세요.";
                    }
                    bResult = false;
                }
                else if (strPassType.Equals("COMP") && strSvcType.Equals("직원") && strCompEmpNo.Length == 0)
                {
                    strMsg = "칭찬 대상자를 선택해주세요.";
                    bResult = false;
                }
                else if (strPassType.Equals("BIZ") && strConsult.Length == 0)
                {
                    strMsg = "컨설팅을 선택해주세요.";
                    bResult = false;
                }
                else if (strPassType.Equals("MOT") && strMotType.Length == 0)
                {
                    strMsg = "타입를 선택해주세요.";
                    bResult = false;
                }
                else if (strSignYn.Equals("N") && PROCID.Equals("-1") && !strPassType.Equals("COMP"))
                {
                    strMsg = "서명을 해주세요.";
                    bResult = false;
                }
                else if (strImgData.Length == 0 && PROCID.Equals("-1") && !strPassType.Equals("COMP"))
                {
                    strMsg = "잘못된 접근입니다.";
                    bResult = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bResult;
        }
        #endregion

        #endregion
    }
}