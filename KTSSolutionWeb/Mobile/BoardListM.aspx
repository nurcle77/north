﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Board.Master" AutoEventWireup="true" CodeBehind="BoardListM.aspx.cs" Inherits="KTSSolutionWeb.BoardListM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function BoardWrite(boardid, writerid, empno, authid) {
            var param = {
                pBOARDID: boardid,
                pWRITERID: writerid,
                pEMPNO: empno,
                pAUTHID: authid
            };

            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/BoardWriteM");

            if (param != null) {
                for (var name in param) {
                    var valObj = param[name];
                    addInput(form, name, valObj);
                }
            }

            form.submit();
        }

        function BoardView(boardid, writerid, empno, authid) {
            var param = {
                pBOARDID: boardid,
                pWRITERID: writerid,
                pEMPNO: empno,
                pAUTHID: authid
            };

            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/BoardViewM");

            if (param != null) {
                for (var name in param) {
                    var valObj = param[name];
                    addInput(form, name, valObj);
                }
            }

            form.submit();
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<h2>기술공유 게시판</h2>
        <!-- S:searchbox -->
        <div class="searchboxPopUp">
		    <fieldset>
                <span class="inpbox" style="width:75%; margin:3px 0 3px 0; font-size:0; text-align:left;">
                    <label style="width:17%;padding:0">제목</label>
                    <asp:TextBox ID="txbTitle" runat="server" MaxLength="150" style="width:81% !important;padding:0"></asp:TextBox>
                </span>
                <span style="width:25%; margin:3px 0 3px 0; font-size:0; text-align:right">
                    <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green" BorderStyle="None" style="width:50px !important;" Text="조회" />
                </span>
                <span class="inpbox" style="width:75%; margin:3px 0 3px 0; font-size:0; text-align:left;">
                    <label style="width:17%">작성자</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:45% !important"></asp:TextBox>
                </span>
                <span style="width:25%; margin:3px 0 3px 0; font-size:0; text-align:right">
                    <asp:Button id="btnWrite" runat="server" class="btn-green last" BorderStyle="None" style="width:50px !important" Text="글쓰기" />
                </span>
            </fieldset>
        </div>
        <!-- E:searchbox -->
			
        <!-- S:boardlist -->
        <div class="boardlist">
		    <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
			    <ContentTemplate>	
                    <ul>
                        <asp:Repeater ID="rptResult" runat="server">
                            <ItemTemplate>
                                <li id="li" runat="server">
                                    <span class="num"><%# Eval("NUM") %></span>
                                    <strong class="title">
                                        <asp:Label ID="lblBoardID" runat="server" Visible="false" Text='<%# Eval("BOARDID") %>'></asp:Label>
                                        <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                        <%# Eval("TITLE") %><%--<a id="aTitle" runat="server" style="cursor:pointer"></a>--%>
                                    </strong>
                                    <em class="writer"><%# Eval("EMPNM") %></em>
                                    <span class="date"><%# Eval("WRITEDT") %></span>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
					<asp:AsyncPostBackTrigger ControlID="paging" />
				</Triggers>
			</asp:UpdatePanel>
        </div>
		<uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        <!-- //E:boardlist -->
	</div>
	<!--//E: contentsarea -->
</asp:Content>