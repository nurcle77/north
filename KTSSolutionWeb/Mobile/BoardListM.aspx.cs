﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Configuration;
using System.Text;
using System.IO;

namespace KTSSolutionWeb
{
    public partial class BoardListM : PageBase
    {
        private string EMPNO
        {
            get
            {
                if (ViewState["EMPNO"] != null)
                {
                    return ViewState["EMPNO"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["EMPNO"] = value;
            }
        }
        private string AUTHID
        {
            get
            {
                if (ViewState["AUTHID"] != null)
                {
                    return ViewState["AUTHID"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["AUTHID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                EMPNO = this.Session["EMPNO"].ToString();
                AUTHID = this.Session["AUTHID"].ToString();

                btnWrite.Attributes.Add("onclick", "BoardWrite('', '', '" + EMPNO + "', '" + AUTHID + "');");
                if (!IsPostBack)
                {
                    SetDownloadDir();
                    GetDataList();
                }
            }
        }

        private void SetDownloadDir()
        {
            string strEmpNo = Page.Session["EMPNO"].ToString();
            
            string strSource = ConfigurationSettings.AppSettings["mKateDownloadPath"];
            byte[] str2byte = Encoding.UTF8.GetBytes(ConfigurationSettings.AppSettings["ServerNum"].ToString());

            string strPath = DeCryption.Decryption.DecryptString(strSource, ConfigurationSettings.AppSettings["ServerNum"].ToString(), str2byte);

            strPath = strPath + strEmpNo;

            DirectoryInfo di = new DirectoryInfo(strPath);

            if (di.Exists)
            {
                FileInfo[] fis = di.GetFiles();

                for (int i = 0; i < fis.Length; i++)
                {
                    fis[i].Delete();
                }
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strTitle = txbTitle.Text.ToString();
                string strEmpnm = txbEmpNm.Text.ToString();

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetBoardList("", strTitle, strEmpnm);
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblBoardID = (Label)rptResult.Items[i].FindControl("lblBoardID");
                            Label lblWriterID = (Label)rptResult.Items[i].FindControl("lblWriterID");
                            HtmlGenericControl li = (HtmlGenericControl)rptResult.Items[i].FindControl("li");                            

                            li.Attributes.Add("onclick", "BoardView('" + lblBoardID.Text + "', '" + lblWriterID.Text + "', '" + EMPNO + "', '" + AUTHID + "');");
                        }
                    }

                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}