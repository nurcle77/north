﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Pass;

namespace KTSSolutionWeb
{
    public partial class PassInfoM : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    this.txbStDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    this.txbEnDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    this.txbStMonth.Value = DateTime.Now.ToString("yyyy-MM");
                    this.txbEnMonth.Value = DateTime.Now.ToString("yyyy-MM");
                }
            }
        }
        #endregion

        #region btnSelect_Click
        /// <summary>
        /// btnSelect_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnDelete_Click
        /// <summary>
        /// btnDelete_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnDelete = (Button)sender;

                Label lblProcId = (Label)btnDelete.Parent.FindControl("lblProcId");
                Label lblPassStat = (Label)btnDelete.Parent.FindControl("lblPassStat");

                string strProcId = lblProcId.Text;
                string strPasssStat = lblPassStat.Text;

                if (strPasssStat.Equals("P"))
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.DelPassProcess(strProcId);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('PASS 삭제가 완료되었습니다.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다..');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('PASS 삭제 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region paging_PreRender
        /// <summary>
        /// paging_PreRender
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult2);

                        string strPassType = this.ddlPassType.SelectedValue;

                        for (int i = 0; i < rptResult2.Items.Count; i++)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult2.Items[i].FindControl("tr");
                            HtmlTableCell tdSvcType = (HtmlTableCell)rptResult2.Items[i].FindControl("tdSvcType");
                            HtmlTableCell tdMotType = (HtmlTableCell)rptResult2.Items[i].FindControl("tdMotType");
                            Label lblPassStatNm = (Label)rptResult2.Items[i].FindControl("lblPassStatNm");
                            Label lblTrtEmpNo = (Label)rptResult2.Items[i].FindControl("lblTrtEmpNo");
                            Label lblPassStat = (Label)rptResult2.Items[i].FindControl("lblPassStat");
                            Label lblProcId = (Label)rptResult2.Items[i].FindControl("lblProcId");
                            Label lblCustSignYn = (Label)rptResult2.Items[i].FindControl("lblCustSignYn");
                            Button btnSave = (Button)rptResult2.Items[i].FindControl("btnSave");
                            Button btnDelete = (Button)rptResult2.Items[i].FindControl("btnDelete"); 

                            if (strPassType.Equals("MOT") || strPassType.Equals(""))
                            {
                                tdSvcType.Visible = true;
                                tdMotType.Visible = true;
                            }
                            else if (strPassType.Equals("COMP"))
                            {
                                tdSvcType.Visible = false;
                                tdMotType.Visible = false;
                            }
                            else
                            {
                                tdSvcType.Visible = true;
                                tdMotType.Visible = false;
                            }

                            string strPassStat = lblPassStat.Text;

                            if (strPassStat.Equals("P"))
                            {
                                btnSave.Visible = true;
                                btnSave.Enabled = true;
                                btnDelete.Visible = true;
                                btnDelete.Enabled = true;

                                string strProcId = lblProcId.Text;
                                string strCustSignYn = lblCustSignYn.Text;
                                btnSave.Attributes.Remove("onClick");
                                btnSave.Attributes.Add("onClick", "PagePassRegM('" + strProcId + "', '" + strCustSignYn + "');");

                                AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                                asyncBtnDelete.ControlID = btnDelete.UniqueID;
                                asyncBtnDelete.EventName = "Click";

                                updPanel2.Triggers.Add(asyncBtnDelete);
                            }
                            else
                            {
                                btnSave.Visible = false;
                                btnSave.Enabled = false;
                                btnDelete.Visible = false;
                                btnDelete.Enabled = false;

                                if (strPassStat.Equals("S") || strPassStat.Equals("F"))                                
                                {
                                    string strEmpNo = lblTrtEmpNo.Text;
                                    tr.Attributes.Remove("onClick");
                                    tr.Attributes.Add("onClick", "ModalEmpInfo('" + strEmpNo + "');");
                                }                            
                            }
                        }
                    }

                    updPanel2.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region hfEmpNo_ValueChanged
        /// <summary>
        /// hfEmpNo_ValueChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void hfEmpNo_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            try
            {
                string strEmpNo = this.hfEmpNo.Value;

                if (!strEmpNo.Equals(""))
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        ds = pass.GetPassEmpInfo(strEmpNo);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];

                        rptResult3.DataSource = dt;
                        rptResult3.DataBind();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다..');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #endregion

        #region Method

        #region GetDataList
        /// <summary>
        /// GetDataList
        /// </summary>
        private void GetDataList()
        {
            DataSet dsSts = new DataSet();
            DataSet dsDetail = new DataSet();

            try
            {
                string strEmpNo = Session["EMPNO"].ToString();
                string strCustNm = this.txbCustNm.Text;
                string strCustTelNo = this.txbCustTelNo.Text;
                string strPassType = this.ddlPassType.SelectedValue;
                string strStDate = Request.Form[this.txbStDate.UniqueID].ToString();
                string strEnDate = Request.Form[this.txbEnDate.UniqueID].ToString();

                string strStMonth = Request.Form[this.txbStMonth.UniqueID].ToString();
                string strEnMonth = Request.Form[this.txbEnMonth.UniqueID].ToString();

                string strStDt = "";
                string strEnDt = "";

                if (strPassType.Equals("MOT") || strPassType.Equals(""))
                {
                    thSvcType.Visible = true;
                    thMotType.Visible = true;
                    thSvcType2.Visible = true;
                }
                else if (strPassType.Equals("COMP"))
                {
                    thSvcType.Visible = false;
                    thMotType.Visible = false;
                    thSvcType2.Visible = false;
                }
                else
                {
                    thSvcType.Visible = true;
                    thMotType.Visible = false;
                    thSvcType2.Visible = true;
                }

                if (this.rbMonth.Checked)
                {
                    strStDt = strStMonth + "-01 00:00:00";
                    DateTime date = DateTime.Parse(strEnMonth + "-01 23:59:59").AddMonths(1).AddDays(-1);
                    strEnDt = date.ToString("yyyy-MM-dd HH:mm:ss");

                    using (PassMgmt pass = new PassMgmt())
                    {
                        dsSts = pass.GetPassProcessSts(strEmpNo, strCustNm, strCustTelNo, strPassType, strStDt, strEnDt);
                    }

                    if (dsSts.Tables.Count > 0)
                    { 
                        DataTable dt = dsSts.Tables[0];

                        rptResult1.DataSource = dt;
                        rptResult1.DataBind();

                        for (int i = 0; i < rptResult1.Items.Count; i++)
                        {
                            HtmlTableCell tdNo = (HtmlTableCell)rptResult1.Items[i].FindControl("tdNo");
                            HtmlTableCell tdDt = (HtmlTableCell)rptResult1.Items[i].FindControl("tdDt");
                            HtmlTableCell tdPassType = (HtmlTableCell)rptResult1.Items[i].FindControl("tdPassType");
                            HtmlTableCell tdSvcType = (HtmlTableCell)rptResult1.Items[i].FindControl("tdSvcType");

                            string strDt = tdDt.InnerText;

                            if (strPassType.Equals("COMP"))
                            {
                                tdSvcType.Visible = false;
                            }
                            else
                            {
                                tdSvcType.Visible = true;
                            }

                            if (strDt.Equals("9999-12"))
                            {
                                if (strPassType.Equals("COMP"))
                                {
                                    tdNo.ColSpan = 3;
                                }
                                else
                                {
                                    tdNo.ColSpan = 4;
                                }

                                tdDt.Visible = false;
                                tdPassType.Visible = false;
                                tdSvcType.Visible = false;

                                tdNo.InnerText = "계";
                            }
                        }
                    }
                }
                else
                {
                    strStDt = strStDate + " 00:00:00";
                    strEnDt = strEnDate + " 23:59:59";
                }

                paging.bPreRender = true;

                using (PassMgmt pass = new PassMgmt())
                {
                    dsDetail = pass.GetPassProcessDetail(strEmpNo, strCustNm, strCustTelNo, strPassType, strStDt, strEnDt);
                }

                paging.PageNumber = 0;
                paging.PageSize = 5;

                paging.Dt = null;

                if (dsDetail.Tables.Count > 0)
                {
                    paging.TotalRows = dsDetail.Tables[0].Rows.Count;
                    paging.Dt = dsDetail.Tables[0];
                }

                paging.SetPagingDataList();

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dsSts != null)
                    dsSts.Dispose();

                if (dsDetail != null)
                    dsDetail.Dispose();
            }
        }
        #endregion

        #endregion
    }
}