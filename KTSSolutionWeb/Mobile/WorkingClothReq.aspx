﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="WorkingClothReq.aspx.cs" Inherits="KTSSolutionWeb.WorkingClothReq" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function RegChk() {
            if (confirm("신청(수정)하시겠습니까?")) {
                return true;
            } else {
                return false;
            }
        }

        function DelChk() {
            if (confirm("삭제 하시겠습니까?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		
        <div class="page-clothes-request">
			<h2>피복신청</h2>
			
			<div class="pointlist">
				<asp:UpdatePanel ID="updPanelInfo" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<fieldset>
							<legend>포인트내역 입력폼</legend>
							<span class="inpbox" style="display:block; width:100%; margin:11px 0 0 0; font-size:0;">
								<label style="width:15%; margin:0 !important;margin-left:0;">소속</label>
								<asp:TextBox ID="txbOrgFullNm" runat="server" ReadOnly="true" style="width:84% !important"></asp:TextBox>
							</span>
							<span class="inpbox" style="display:block; width:100%; margin:11px 0 11px 0; font-size:0;">
								<label style="width:15%; margin:0 !important;margin-left:0;">지급기준</label>
								<asp:TextBox ID="txbTitle" runat="server" ReadOnly="true" style="width:84% !important"></asp:TextBox>
							</span>
						</fieldset>
						<!-- S:fieldset -->
						<fieldset id="fieldset" runat="server" visible="false">
							<legend>포인트내역 입력폼</legend>
							<span class="inpbox">
								<label>총 포인트</label>
								<asp:TextBox ID="txbTotPoint" runat="server" ReadOnly="true"></asp:TextBox>
							</span>
							<span class="inpbox">
								<label>사용</label>
								<asp:TextBox ID="txbUsePoint" runat="server" ReadOnly="true"></asp:TextBox>
							</span>
							<span class="inpbox">
								<label>잔여</label>
                                <asp:TextBox ID="txbRestPoint" runat="server" ReadOnly="true"></asp:TextBox>
							</span>
						</fieldset>
						<!-- //E:fieldset -->
					</ContentTemplate>
					<Triggers>
					</Triggers>
				</asp:UpdatePanel>
			</div>
			<br />
            <!-- S: scrollBox -->
            <div class="scrollbox">
				<div class="requestlist">
					<strong id="ReqTitle" runat="server" style="margin-top:2px;margin-right:10px;float:left">추가 신청</strong>
					<asp:Button id="btnAddList" runat="server" onClick="btnAddList_Click" class="btn-plus" BorderStyle="None" Text="+" />
					
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
							<ContentTemplate>	
								<table>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">동/하복</th>
                                                    <td style="min-width:15px; padding:10px 12px 10px 0px;">
														<p class="inpbox" style="text-align:left;">
															<asp:TextBox ID="txbClothGbn" Width="80%" runat="server" ReadOnly="true" Text='<%# Eval("CLOTHGBN") %>' style="padding:0;text-align:center;"></asp:TextBox>
														</p>
                                                    </td>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">종류</th>
                                                    <td style="min-width:15px; padding:10px 15px 10px 0px;text-align:left;">
                                                        <p class="optionbox" style="text-align:left;">
                                                            <asp:Label ID="lblRequestID" runat="server" Visible="false" Text='<%# Eval("REQUESTID") %>'></asp:Label>
															<asp:Label ID="lblDetailSeq" runat="server" Visible="false" Text='<%# Eval("DETAILSEQ") %>'></asp:Label>
															<asp:TextBox ID="txbClothType" runat="server" Width="90%" ReadOnly="true" Text='<%# Eval("CLOTHTYPE") %>' style="padding:0;text-align:center;"></asp:TextBox>
															<asp:DropDownList ID="ddlAddClothType" Width="90%" runat="server" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlAddClothType_SelectedIndexChanged" style="background:url('/Resource/Mobile_images/icon_select_arr.png') #fff no-repeat 100% 50% / 14px auto;"></asp:DropDownList>
														</p>
                                                    </td>
                                                </tr>
                                                <tr>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">사이즈</th>
                                                    <td style="min-width:15px; padding:10px 12px 10px 0px;">
														<p class="optionbox" style="text-align:left;">
															<asp:TextBox ID="txbSize" runat="server" Width="55px" Visible="false" ReadOnly="true" Text='<%# Eval("CLOTHSIZE") %>' style="padding:0;text-align:center;"></asp:TextBox>
                                                            <asp:DropDownList ID="ddlSize1" Width="55px" runat="server" style="background:url('/Resource/Mobile_images/icon_select_arr.png') #fff no-repeat 100% 50% / 14px auto;"></asp:DropDownList>
                                                            <asp:DropDownList ID="ddlSize2" Width="93px" runat="server" Visible="false" style="padding:0 5px;background:url('/Resource/Mobile_images/icon_select_arr.png') #fff no-repeat 100% 50% / 14px auto;"></asp:DropDownList>
														</p>
                                                    </td>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">수량</th>
                                                    <td style="min-width:15px; padding:10px 12px 10px 0px;">
														<p class="inpbox" style="text-align:left;">
															<asp:TextBox ID="txbReqCnt" AutoPostBack="true" Width="90%" MaxLength="3" runat="server" Text='<%# Eval("REQCNT") %>' onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" OnTextChanged="txbReqCnt_TextChanged" style="padding:0;text-align:center;"></asp:TextBox>
														</p>
													</td>
                                                </tr>
                                                <tr id="trPoint" runat="server">
													<th style="min-width:15px; padding:10px 12px 10px 0px; border-bottom:1px solid #000;">포인트</th>
                                                    <td style="min-width:15px; padding:10px 12px 10px 0px; border-bottom:1px solid #000;" colspan="3">
														<p class="inpbox" style="text-align:left;">
															<asp:TextBox ID="txbPoint" runat="server" Width="100px" ReadOnly="true" Text='<%# Eval("USEPOINT") %>' style="padding:0;text-align:center;"></asp:TextBox>
														</p>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
								</table>
							</ContentTemplate>
							<Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnAddList" EventName="Click"/>
                                <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click"/>
                                <asp:AsyncPostBackTrigger ControlID="btnDel" EventName="Click"/>
							</Triggers>
						</asp:updatepanel>
					</div>
				</div>
			</div>
		</div>	
        <!-- S:btncenter -->
		<div class="list-top" style="margin-top:20px;">
			<div style="text-align:center;">
				<asp:Button id="btnReg" runat="server" OnClientClick="return RegChk();" OnClick="btnReg_Click" class="btn-green" Width="80px" BorderStyle="None" Text="신청" />
				<asp:Button id="btnDel" runat="server" OnClientClick="return DelChk();" OnClick="btnDel_Click" class="btn-green" Width="80px" BorderStyle="None" Text="삭제" />
			</div>
        </div>
        <!-- //E:btncenter -->	
	</div>
	<!--//E: contentsarea -->
</asp:Content>