﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Pass;

namespace KTSSolutionWeb
{
    public partial class PassCompInfoM : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    this.txbStDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    this.txbEnDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    this.txbStMonth.Value = DateTime.Now.ToString("yyyy-MM");
                    this.txbEnMonth.Value = DateTime.Now.ToString("yyyy-MM");
                }
            }
        }
        #endregion

        #region btnSelect_Click
        /// <summary>
        /// btnSelect_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region paging_PreRender
        /// <summary>
        /// paging_PreRender
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region GetDataList
        /// <summary>
        /// GetDataList
        /// </summary>
        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = Session["EMPNO"].ToString();
                string strStDate = Request.Form[this.txbStDate.UniqueID].ToString();
                string strEnDate = Request.Form[this.txbEnDate.UniqueID].ToString();

                string strStMonth = Request.Form[this.txbStMonth.UniqueID].ToString();
                string strEnMonth = Request.Form[this.txbEnMonth.UniqueID].ToString();

                string strStDt = "";
                string strEnDt = "";

                if (this.rbMonth.Checked)
                {
                    strStDt = strStMonth + "-01 00:00:00";
                    DateTime date = DateTime.Parse(strEnMonth + "-01 23:59:59").AddMonths(1).AddDays(-1);
                    strEnDt = date.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    strStDt = strStDate + " 00:00:00";
                    strEnDt = strEnDate + " 23:59:59";
                }

                paging.bPreRender = true;

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetPassProcessCompList(strEmpNo, strStDt, strEnDt);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #endregion
    }
}