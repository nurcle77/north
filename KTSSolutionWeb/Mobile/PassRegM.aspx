﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PassRegM.aspx.cs" Inherits="KTSSolutionWeb.PassRegM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
		function SetCanvas() {
			var drawCanvas = document.getElementById("cvSign");
			var rect = drawCanvas.getBoundingClientRect();
			var drawBackup = new Array();

			if (typeof drawCanvas.getContext == 'function') {
				var ctx = drawCanvas.getContext("2d");

				ctx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
				ctx.beginPath();
                localStorage.removeItem("ImgCanvas");
                $("#<%= hfSignYn.ClientID %>").val("N");

				var isDraw = false;
				var pDraw = $("#cvSign").offset();

				var currP = null;
				ctx.clearRect(0, 0, drawCanvas.offsetwidth, drawCanvas.offsetHeight);

				$("#cvSign").bind('mousedown', function (e) {
					if (e.button == 0) {
						saveCanvas();
						e.preventDefault();
						ctx.beginPath();

						isDraw = true;
					}
				});

				$("#cvSign").bind('mousemove', function (e) {
					var event = e.originalEvent;
					e.preventDefault();
					currP = { X: event.ClientX - pDraw.Left, Y: event.ClientY - pDraw.Top };

                    if (isDraw) {
                        $("#<%= hfSignYn.ClientID %>").val("Y");
						draw_line(currP);
					}
				});

				$("#cvSign").bind('mouseup', function (e) {
					e.preventDefault();

					isDraw = false;
				});

				$("#cvSign").bind('mouseleave', function (e) {
					isDraw = false;
				});

				$("#cvSign").bind('touchstart', function (e) {
					saveCanvas();
					e.preventDefault();
					ctx.beginPath();

				});

				$("#cvSign").bind('touchmove', function (e) {
					var event = e.originalEvent;
					e.preventDefault();

					currP = { X: event.touches[0].pageX - pDraw.left, Y: event.touches[0].pageY - pDraw.top };

                    $("#<%= hfSignYn.ClientID %>").val("Y");
					draw_line(currP);
				});

				$("#cvSign").bind('touchend', function (e) {
					e.preventDefault();
				});

				function draw_line(p) {
					ctx.lineWidth = "3px";
					ctx.lineCap = "round";
					ctx.lineTo(p.X, p.Y);
					ctx.moveTo(p.X, p.Y);
					ctx.strokeStyle = "#000000";
					ctx.stroke();
				}

				function saveCanvas() {
					drawBackup.push(ctx.getImageData(0, 0, drawCanvas.width, drawCanvas.height));
				}
			}
        }

		function saveImage() {
			var canvas = document.getElementById("cvSign");
			localStorage.setItem("ImgCanvas", canvas.toDataURL("image/png"));

			var img = document.getElementById("<%= imgSign.ClientID %>");

			img.src = canvas.toDataURL("image/png");
            $("#<%= hfImgSrc.ClientID %>").val(canvas.toDataURL("image/png"));
		}

		function RegCheck() {
			var strPasstype = $("#<%= ddlPassType.ClientID %> option:selected").val();
			var bChk = $("#<%= chkAgree.ClientID %>").is(":checked");

			if (strPasstype == "") {
				alert("구분을 선택해주세요.");
				return false;
			}
			else if (strPasstype == "COMP") {
				if (confirm("PASS를 저장 하시겠습니까?")) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				if (bChk) {
					if (confirm("PASS를 저장 하시겠습니까?")) {
						saveImage();
						return true;
					}
					else {
						return false;
					}
				}
				else {
					alert("성명 및 전화번호 제공에 동의 하셔야 등록 가능합니다.");
					return false;
				}
			}
		}

		function controlResize(obj, type) {
			if (obj == "COMP") {
                $("#spanCustNm").hide();
                $("#spanCustTel").hide();
                $("#spanDt").width("100%");
                $("#<%=txbHopeDt.ClientID%>").width("38%");
				$("#spanRel").hide();
				$("#spanConsult").hide();
                $("#spanAgree").hide();
                $("#divCustSign").css({ width: 0, height: 0 });
                $("#divCustSign").hide();

			}
            else {
                $("#spanCustNm").show();
                $("#spanCustTel").show();
                $("#spanDt").width("50%");
                $("#<%=txbHopeDt.ClientID%>").width("76%");
                $("#spanRel").show();
                $("#spanAgree").show();
                $("#spanComp").hide();
                $("#divCustSign").css({ width: 220, height: 150 });
                $("#divCustSign").show();

				if (obj == "MOT") {
					$("#spanConsult").show();
					$("#spanConsult").width("50%");
				}
				else if (obj == "BIZ") {
					$("#spanConsult").show();
					$("#spanConsult").width("100%");
				}
				else {
					$("#spanConsult").hide();
				}

                if (type == "U") {
                    $("#cvSign").hide();
                    $("#<%=imgSign.ClientID%>").show();
				}
                else {                    
                    $("#cvSign").show();
                    $("#<%=imgSign.ClientID%>").hide();
                    SetCanvas();
				}
			}
		}

        function PassInfoList(val) {
            var form = document.forms["form1"];

			form.setAttribute("method", "post");

            if (val == "I") {
                form.setAttribute("action", "/MobileMain");
			}
			else {
                form.setAttribute("action", "/Mobile/PassInfoM");
            }

            form.submit();
        }

        function ModalShow() {
            $("#divModal").show();
            return false;
		}

        function ModalClose() {
            $("#divModal").hide();
            return false;
		}

		function EmpNmCheck() {

			var CompEmpNm = $("#<%= txbEmpNm.ClientID %>").val();

            if (CompEmpNm == "") {
				alert("사원명을 입력해주세요.");
				return false;
			}
            else {
                return true;
			}
        }

		
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div class="page-clothes-request">
		    <h2>PASS 등록</h2>
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<span class="optionbox" style="width:50%; margin:3px 0 0 0; font-size:0;">
						<asp:UpdatePanel ID="updPanelPass" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label style="margin:0 !important;margin-left:0;">구분</label><br />
								<asp:DropDownList ID="ddlPassType" runat="server" style="width:92% !important" AutoPostBack="true" OnSelectedIndexChanged="ddlPassType_SelectedIndexChanged">
									<asp:ListItem Text="선택하세요" Value="" Selected="True"></asp:ListItem>
									<asp:ListItem Text="MOT" Value="MOT"></asp:ListItem>
									<asp:ListItem Text="신사업" Value="BIZ"></asp:ListItem>
									<asp:ListItem Text="공간" Value="SPACE"></asp:ListItem>
									<asp:ListItem Text="칭찬" Value="COMP"></asp:ListItem>
								</asp:DropDownList>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
							</Triggers>
						</asp:UpdatePanel>
					</span>
					<span class="optionbox" style="width:50%; margin:3px 0 0 0; font-size:0;">
						<asp:UpdatePanel ID="updPanelSvc" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label id="lblSvcType" runat="server" style="margin:0 !important;margin-left:0;">희망상품</label><br />
								<asp:DropDownList ID="ddlSvcType" runat="server" style="width:92% !important" AutoPostBack="true" OnSelectedIndexChanged="ddlSvcType_SelectedIndexChanged">
								</asp:DropDownList>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
								<asp:AsyncPostBackTrigger ControlID="ddlSvcType" EventName="SelectedIndexChanged" />
							</Triggers>
						</asp:UpdatePanel>
					</span>
					<span id="spanCustNm" class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">성명</label><br />
						<asp:TextBox ID="txbCustNm" runat="server" MaxLength="50" style="width:92% !important"></asp:TextBox>
					</span>
					<span id="spanCustTel" class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">TEL</label><br />
						<asp:TextBox ID="txbTelNo" runat="server" MaxLength="20" style="width:92% !important" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
					</span>
					<span id="spanDt" class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label id="lblDt" runat="server" style="margin:0 !important;margin-left:0;">희망일자</label><br />
						<asp:TextBox ID="txbHopeDt" runat="server" class="date" ReadOnly="true" style="width:92% !important"></asp:TextBox>
					</span>
					<span id="spanRel" class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<asp:UpdatePanel ID="updPanelRel" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label id="lblRelType" runat="server" style="margin:0 !important;margin-left:0;">관계</label><br />
								<asp:TextBox ID="txbRelType" runat="server" MaxLength="50" style="width:92% !important"></asp:TextBox>
							</ContentTemplate>
						</asp:UpdatePanel>
					</span>
					<span id="spanConsult" class="optionbox" style="width:100%; margin:3px 0 0 0; font-size:0; display:none;">
						<asp:UpdatePanel ID="updPanelConsult" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label id="lblConsult" runat="server" style="margin:0 !important;margin-left:0;">컨설팅</label><br />
								<asp:DropDownList ID="ddlConsult" runat="server" Visible="false" style="width:96% !important">
								</asp:DropDownList>
								<asp:DropDownList ID="ddlMotType" runat="server" Visible="false" style="width:92% !important">
									<asp:ListItem Text="B" Value="B"></asp:ListItem>
									<asp:ListItem Text="P" Value="P"></asp:ListItem>
								</asp:DropDownList>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
							</Triggers>
						</asp:UpdatePanel>
					</span>
					<span id="spanComp" class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0; display:none;">
						<asp:UpdatePanel ID="updPanelComp" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label style="margin:0 !important;margin-left:0;">칭찬대상자</label><br />
								<asp:TextBox ID="txbCompEmpNm" runat="server" ReadOnly="true" style="width:96% !important"></asp:TextBox>
								<asp:HiddenField ID="hfCompEmpNo" runat="server" />
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
								<asp:AsyncPostBackTrigger ControlID="ddlSvcType" EventName="SelectedIndexChanged" />
							</Triggers>
						</asp:UpdatePanel>
					</span>
					<span class="inpbox" style="width:100%; margin:6px 0 0px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">참고사항</label><br />
						<textarea id="txaNoti" runat="server" maxlength="8000" style="padding-top:5px;padding-bottom:5px;width:96%; "></textarea>
					</span>
					<span id="spanAgree" class="ickbox" style="width:100%; margin:3px 0 0 0;">
						<asp:UpdatePanel ID="updPanelAgree" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<span style="display:inline-block;width:84%; padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:800;float:left;">
									※성명 및 전화번호 제공에 대하여 동의하시겠습니까?
								</span>
								<input id="chkAgree" runat="server" style="margin-top:8px;margin-left:5px;width:18px;height:18px; border:1px solid #dfdfdf;"  type="checkbox" />
								<div id="divCustSign" style="position:relative;float:right; text-align:center;width:0px;height:0px;right:20px;">
									<canvas id="cvSign" style="border:1px solid #000000;background-image:url('/Resource/Mobile_images/signBackground.png');background-size:100% 100%;width:100%;height:100%;display:none;"></canvas>
									<img id="imgSign" runat="server" src="" style="border:1px solid black; width:100%; height:100%;display:none; background-color:white;" />
									<asp:HiddenField ID="hfImgSrc" runat="server" />
									<asp:HiddenField ID="hfSignYn" runat="server" />
								</div>
							</ContentTemplate>
						</asp:UpdatePanel>
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
						<asp:Button id="btnRegPass" runat="server" OnClientClick="return RegCheck();" OnClick="btnRegPass_Click" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;" Text="등록" />
					</span>
				</fieldset>
			</div>
			
            <div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:80%;top:30%;left:0;background:rgb(255, 255, 255); border-radius:10px;overflow-y:scroll;">
				<div class="requestlist" style="width:90%;height:90%;text-align:center;margin-left:5%">
					<strong style="margin-top:2px;margin-right:10px;margin-bottom:10px;float:left";">사원 조회</strong>

					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">사원명</label>
						<asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
						<asp:Button id="btnEmpNm" runat="server" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;color:white" OnClientClick="return EmpNmCheck();" OnClick="btnEmpNo_Click" Text="조회" />
					</span>

                    <div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<asp:updatepanel id="updPanelModal" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<table>
									<thead>
										<th></th>
										<th>성명</th>
										<th>부서</th>
										<th>직책</th>
										<th>전화번호</th>
									</thead>
									<tbody>
										<asp:Repeater ID="rptResult3" runat="server">
											<ItemTemplate>
												<tr>
													<td>
														<asp:Button id="btnConfirm" runat="server" class="btn-save" Text="선택" OnClick="btnConfirm_Click"/>
													</td>
													<td>
														<asp:Label ID="lblEmpNm" runat="server" Text='<%# Eval("EMPNM") %>'></asp:Label>
														<asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>' Visible="false"></asp:Label>
													</td>
													<td>
														<asp:Label ID="lblOrgFullNm" runat="server" Text='<%# Eval("ORGFULLNM") %>'></asp:Label>
													</td>
													<td><%# Eval("TITLENM") %></td>
													<td><a href='tel:<%# Eval("MOBILE") %>'><%# Eval("MOBILE") %></a></td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnEmpNm" EventName="Click" />
							</Triggers>
						</asp:updatepanel>
					</div>
					<asp:Button id="btnModalClose" runat="server" class="btn-black" BorderStyle="None" style="width:33px;margin:0 !important;" OnClientClick="return ModalClose();" Text="닫기" />
				</div>
			</div>
        </div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>