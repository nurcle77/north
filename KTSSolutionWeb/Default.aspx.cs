﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using Microsoft.AspNet.FriendlyUrls;

namespace KTSSolutionWeb
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                GetNotiPopup();

                GetDataList_Noti();
                GetDataList_Board();
            }
        }

        #region GetNotiPopup
        /// <summary>
        /// GetNotiPopup
        /// </summary>
        private void GetNotiPopup()
        {
            DataSet ds = new DataSet();

            try
            {
                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetNotiPopUpList();
                }

                string strScript = "";

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string pid = ds.Tables[0].Rows[i]["NOTICEID"].ToString();
                    string strWriterId = ds.Tables[0].Rows[i]["WRITERID"].ToString();
                    string strPopupNm = "Notice_" + pid;

                    bool bPop = true;

                    HttpCookie Cookie = Request.Cookies[strPopupNm];

                    if (Cookie != null)
                    {
                        if (Cookie["POPUPYN"] == "Y" && Cookie["DATE"].ToString().Equals(DateTime.Now.ToString("yyyyMMdd")))
                        {
                            bPop = false;
                        }
                    }

                    if (bPop)
                    {
                        strScript += "NoticeView('" + pid + "', '" + strWriterId + "');";
                    }
                }

                if (strScript.Length > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "NoticePopup", strScript, true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        private void GetDataList_Noti()
        {
            DataSet ds = new DataSet();

            try
            {
                pagingNoti.bPreRender = true;

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetNotiList("", "", "", "");
                }

                pagingNoti.PageNumber = 0;
                pagingNoti.PageSize = 6;

                pagingNoti.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    pagingNoti.TotalRows = ds.Tables[0].Rows.Count;
                    pagingNoti.Dt = ds.Tables[0];
                }

                pagingNoti.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList_Board()
        {
            DataSet ds = new DataSet();

            try
            {
                pagingBoard.bPreRender = true;

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetBoardList("", "", "");
                }

                pagingBoard.PageNumber = 0;
                pagingBoard.PageSize = 6;

                pagingBoard.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    pagingBoard.TotalRows = ds.Tables[0].Rows.Count;
                    pagingBoard.Dt = ds.Tables[0];
                }

                pagingBoard.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void pagingNoti_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (pagingNoti.bPreRender)
                {
                    if (pagingNoti.Dt != null)
                    {
                        pagingNoti.RepeaterDataBind(rptResultNoti);

                        for (int i = 0; i < rptResultNoti.Items.Count; i++)
                        {
                            Label lblNoticeID = (Label)rptResultNoti.Items[i].FindControl("lblNoticeID");
                            Label lblWriterID = (Label)rptResultNoti.Items[i].FindControl("lblWriterID");
                            HtmlAnchor aTitle = (HtmlAnchor)rptResultNoti.Items[i].FindControl("aTitle");

                            aTitle.Attributes.Add("onclick", "NoticeView('" + lblNoticeID.Text + "', '" + lblWriterID.Text + "')");
                        }
                    }

                    pagingNoti.bPreRender = false;
                }

                updPanelNoti.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void pagingBoard_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (pagingBoard.bPreRender)
                {
                    if (pagingBoard.Dt != null)
                    {
                        pagingBoard.RepeaterDataBind(rptResultBoard);

                        for (int i = 0; i < rptResultBoard.Items.Count; i++)
                        {
                            Label lblBoardID = (Label)rptResultBoard.Items[i].FindControl("lblBoardID");
                            Label lblWriterID = (Label)rptResultBoard.Items[i].FindControl("lblWriterID");
                            HtmlAnchor aTitle = (HtmlAnchor)rptResultBoard.Items[i].FindControl("aTitle");

                            aTitle.Attributes.Add("onclick", "BoardView('" + lblBoardID.Text + "', '" + lblWriterID.Text + "')");
                        }
                    }
                    pagingBoard.bPreRender = false;
                }

                updPanelBoard.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList_Noti();

                GetDataList_Board();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}