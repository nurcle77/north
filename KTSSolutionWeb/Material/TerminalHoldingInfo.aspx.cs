﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Pay;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace KTSSolutionWeb
{
    public partial class TerminalHoldingInfo : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");

                updPanelOrg.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hdfOrgCd.Value;

                using (MeritPayMgmt mgmt = new MeritPayMgmt())
                {
                    ds = mgmt.GetTerminalHoldingInfo(strOrgCd);
                }

                paging.PageNumber = 0;
                paging.PageSize = 30;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[39];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "BASEDATE";
            ArrHeader[2] = "ORGLV2NM";
            ArrHeader[3] = "ORGLV3NM";
            ArrHeader[4] = "ORGLV4NM";
            ArrHeader[5] = "OFFICENAMESCODE";
            ArrHeader[6] = "GROUPLV1NM";
            ArrHeader[7] = "GROUPLV2NM";
            ArrHeader[8] = "GROUPLV3NM";
            ArrHeader[9] = "MAKERNM";
            ArrHeader[10] = "MODELNM";
            ArrHeader[11] = "K9";
            ArrHeader[12] = "BARCODE";
            ArrHeader[13] = "CATEGORY";
            ArrHeader[14] = "USECNT";
            ArrHeader[15] = "PRODDATE";
            ArrHeader[16] = "TERMINALSTATE";
            ArrHeader[17] = "WORKCAUSE";
            ArrHeader[18] = "MACADDR";
            ArrHeader[19] = "ASSETNUM";
            ArrHeader[20] = "FIRSTSTOREDATE";
            ArrHeader[21] = "STORECLASS";
            ArrHeader[22] = "STOREDATE";
            ArrHeader[23] = "POSSESSIONDAY";
            ArrHeader[24] = "ENGINEER_POSSESSIONDAY";
            ArrHeader[25] = "STORE_POSSESSIONDAY";
            ArrHeader[26] = "LOCATION";
            ArrHeader[27] = "LOCATIONDATE";
            ArrHeader[28] = "LOCATIONDAY";
            ArrHeader[29] = "BEFORESTATE";
            ArrHeader[30] = "CARRYOUTCNT";
            ArrHeader[31] = "LASTORGNM";
            ArrHeader[32] = "LASTWORKERID";
            ArrHeader[33] = "LASTWORKERNM";
            ArrHeader[34] = "PURCHASEYN";
            ArrHeader[35] = "RETRIEVEYN";
            ArrHeader[36] = "RECYCLEYN";
            ArrHeader[37] = "SMSTARGETYN";
            ArrHeader[38] = "SMSSENDYN";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[39];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "기준일자";
            ArrHeader[2] = "본부명";
            ArrHeader[3] = "지사명";
            ArrHeader[4] = "지점명";
            ArrHeader[5] = "국사명";
            ArrHeader[6] = "대분류";
            ArrHeader[7] = "중분류";
            ArrHeader[8] = "소분류";
            ArrHeader[9] = "제조사";
            ArrHeader[10] = "모델명";
            ArrHeader[11] = "K9";
            ArrHeader[12] = "바코드";
            ArrHeader[13] = "유형";
            ArrHeader[14] = "사용횟수";
            ArrHeader[15] = "제조일";
            ArrHeader[16] = "단말상태";
            ArrHeader[17] = "단말작업사유";
            ArrHeader[18] = "MAC주소";
            ArrHeader[19] = "자산번호";
            ArrHeader[20] = "최초지사입고일";
            ArrHeader[21] = "구분";
            ArrHeader[22] = "지사입고일";
            ArrHeader[23] = "지사보유기간(일)";
            ArrHeader[24] = "Engineer보유기간(일)";
            ArrHeader[25] = "지사창고보유기간(일)";
            ArrHeader[26] = "현위치";
            ArrHeader[27] = "현위치입고일";
            ArrHeader[28] = "현위치보유기간(일)";
            ArrHeader[29] = "지사입고전상태";
            ArrHeader[30] = "Engineer보유기간분출횟수";
            ArrHeader[31] = "최종보유작업자소속";
            ArrHeader[32] = "최종보유작업자ID";
            ArrHeader[33] = "보유작업자명";
            ArrHeader[34] = "단말구매여부";
            ArrHeader[35] = "회수여부";
            ArrHeader[36] = "재활용여부";
            ArrHeader[37] = "SMS대상여부";
            ArrHeader[38] = "SMS발송여부";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        if (rptResult.Items.Count > 0)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult.Items[0].FindControl("tr");

                            if (paging.PageNumber == 0)
                            {
                                tr.Style.Add("background-color", "#e6e6e6");
                            }
                        }
                    }
                    
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }


        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {

                    string strOrgCd = this.hdfOrgCd.Value;

                    using (MeritPayMgmt mgmt = new MeritPayMgmt())
                    {
                        ds = mgmt.GetTerminalHoldingInfo(strOrgCd);
                    }
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "실적급 월별 조회 결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}