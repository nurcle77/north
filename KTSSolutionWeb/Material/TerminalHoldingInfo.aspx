﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TerminalHoldingInfo.aspx.cs" Inherits="KTSSolutionWeb.TerminalHoldingInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function SearchChk() {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
            } else {
                return true;
            }
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno,
                pOPER: "N"
            };

            var Popupform = createForm("/Common/OrgTree_Oper", param);

            Popupform.target = "OrgTree_Oper";
            var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetOrgCd(orgcd, valtype) {

            this.focus();

            document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelOrg)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
                            <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
		    </fieldset>
        </div>
        <br />
		<!-- //E:searchbox -->      

		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>단말보유 정보 조회결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
                </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>기준일자</th>
                                    <th>본부명</th>
                                    <th>지사명</th>
                                    <th>지점명</th>
                                    <th>국사명</th>
                                    <th>대분류</th>
                                    <th>중분류</th>
                                    <th>소분류</th>
                                    <th>제조사</th>
                                    <th>모델명</th>
                                    <th>K9</th>
                                    <th>바코드</th>
                                    <th>유형</th>
                                    <th>사용횟수</th>
                                    <th>제조일</th>
                                    <th>단말상태</th>
                                    <th>단말작업사유</th>
                                    <th>MAC주소</th>
                                    <th>자산번호</th>
                                    <th>최초지사입고일</th>
                                    <th>구분</th>
                                    <th>지사입고일</th>
                                    <th>지사보유기간(일)</th>
                                    <th>Engineer보유기간(일)</th>
                                    <th>지사창고보유기간(일)</th>
                                    <th>현위치</th>
                                    <th>현위치입고일</th>
                                    <th>현위치보유기간(일)</th>
                                    <th>지사입고전상태</th>
                                    <th>Engineer보유기간분출횟수</th>
                                    <th>최종보유작업자소속</th>
                                    <th>최종보유작업자ID</th>
                                    <th>보유작업자명</th>
                                    <th>단말구매여부</th>
                                    <th>회수여부</th>
                                    <th>재활용여부</th>
                                    <th>SMS대상여부</th>
                                    <th>SMS발송여부</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr id="tr" runat="server">
                                            <td><%# Eval("NUM") %>") %></td>
                                            <td><%# Eval("BASEDATE") %></td>
	                                        <td><%# Eval("ORGLV2NM") %></td>
	                                        <td><%# Eval("ORGLV3NM") %></td>
	                                        <td><%# Eval("ORGLV4NM") %></td>
	                                        <td><%# Eval("OFFICENAMESCODE") %></td>
	                                        <td><%# Eval("GROUPLV1NM") %></td>
	                                        <td><%# Eval("GROUPLV2NM") %></td>
	                                        <td><%# Eval("GROUPLV3NM") %></td>
	                                        <td><%# Eval("MAKERNM") %></td>
	                                        <td><%# Eval("MODELNM") %></td>
	                                        <td><%# Eval("K9") %></td>
	                                        <td><%# Eval("BARCODE") %></td>
	                                        <td><%# Eval("CATEGORY") %></td>
	                                        <td><%# Eval("USECNT") %></td>
	                                        <td><%# Eval("PRODDATE") %></td>
	                                        <td><%# Eval("TERMINALSTATE") %></td>
	                                        <td><%# Eval("WORKCAUSE") %></td>
	                                        <td><%# Eval("MACADDR") %></td>
	                                        <td><%# Eval("ASSETNUM") %></td>
	                                        <td><%# Eval("FIRSTSTOREDATE") %></td>
	                                        <td><%# Eval("STORECLASS") %></td>
	                                        <td><%# Eval("STOREDATE") %></td>
	                                        <td><%# Eval("POSSESSIONDAY") %></td>
	                                        <td><%# Eval("ENGINEER_POSSESSIONDAY") %></td>
	                                        <td><%# Eval("STORE_POSSESSIONDAY") %></td>
	                                        <td><%# Eval("LOCATION") %></td>
	                                        <td><%# Eval("LOCATIONDATE") %></td>
	                                        <td><%# Eval("LOCATIONDAY") %></td>
	                                        <td><%# Eval("BEFORESTATE") %></td>
	                                        <td><%# Eval("CARRYOUTCNT") %></td>
	                                        <td><%# Eval("LASTORGNM") %></td>
	                                        <td><%# Eval("LASTWORKERID") %></td>
	                                        <td><%# Eval("LASTWORKERNM") %></td>
	                                        <td><%# Eval("PURCHASEYN") %></td>
	                                        <td><%# Eval("RETRIEVEYN") %></td>
	                                        <td><%# Eval("RECYCLEYN") %></td>
	                                        <td><%# Eval("SMSTARGETYN") %></td>
	                                        <td><%# Eval("SMSSENDYN") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
