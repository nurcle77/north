﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuControl.ascx.cs" Inherits="KTSSolutionWeb.Controls.MenuControl" %>
<!-- S: header -->
<div id="header">
	<!-- S: header-inner -->
	<div class="header-inner">
		<h1><a href="/Default">kt service 북부 업무지원시스템</a></h1>
		<div class="utilmenu">
            <div class="profile">
                <div class="pic"><img src="/Resource/images/icon_profile_default.png" alt="" /></div>
                <p><strong id="strUserNm" runat="server">케이티</strong>님 안녕하세요.</p>
            </div>
			<ul>
				<li><a onclick="UserInfoPopup();">사용자정보</a></li>
				<li><a id="aLogout" runat="server" onclick="return LogoutChk();" onserverclick="aLogout_ServerClick">로그아웃</a></li>
				<li><a onclick="NoticeList();">공지사항</a></li>
				<li><a onclick="BoardList();">기술공유</a></li>
			</ul>
		</div>
	</div>
	<!-- //E: header-inner -->

	<!-- S:topmenu -->
	<div class="topmenu">
		<ul id="ulMenuList" runat="server">
		</ul>
	</div>
	<!-- E:topmenu -->
</div>
<!-- E: header -->