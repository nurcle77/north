﻿<%@ Page Language="C#" MasterPageFile="~/SiteMain.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="KTSSolutionWeb.Default" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function NoticeView(noticeid, writerid) {
            var nWidth = 600;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pNOTICEID: noticeid,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/NoticeView", param);

            Popupform.target = "NoticeView";
            var win = window.open("", "NoticeView", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function BoardView(boardid, writerid) {
            var nWidth = 600;
            var nHeight = 900;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pBOARDID: boardid,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/BoardView", param);

            Popupform.target = "BoardView";
            var win = window.open("", "BoardView", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PageRefresh() {
            __doPostBack("<%= btnSelect.UniqueID %>", "OnClick");
        }
    </script>
    <!-- S: contentsarea -->
    <div class="contentsarea">
        <div class="main-banner"></div>
        <a href="#" onclick="window.location.href='/MobileMain';">TEST PAGE MOVE</a>
        <div class="table-wrapper">
            <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green" Visible="false" Text="조회" />
            <div class="table-notice">
                <h2>공지사항</h2>
                <asp:UpdatePanel ID="updPanelNoti" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="boardlist" style="min-height:300px">
                            <table>
                                <colgroup>
                                    <col style="width:11.5294%">
                                    <col style="width:auto">
                                    <col style="width:16%">
                                </colgroup>
                                <tbody>
                                    <asp:Repeater ID="rptResultNoti" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="number"><%# Eval("NUM") %></td>
                                                <td class="list-title">
                                                    <asp:Label ID="lblNoticeID" runat="server" Visible="false" Text='<%# Eval("NOTICEID") %>'></asp:Label>
                                                    <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                                    <a id="aTitle" runat="server" style="cursor:pointer"><%# Eval("TITLE") %></a>
                                                </td>
                                                <td class="date"><%# Eval("WRITEDT") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>                
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="pagingNoti" />
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                    </Triggers>
                </asp:UpdatePanel>
                <uc:paging ID="pagingNoti" runat="server" OnPreRender="pagingNoti_PreRender" />
            </div>
            <div class="table-tech">
                <h2>기술공유</h2>
                <asp:UpdatePanel ID="updPanelBoard" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="boardlist" style="min-height:300px">
                            <table>
                                <colgroup>
                                    <col style="width:11.5294%">
                                    <col style="width:auto">
                                    <col style="width:16%">
                                </colgroup>
                                <tbody>
                                    <asp:Repeater ID="rptResultBoard" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="number"><%# Eval("NUM") %></td>
                                                <td class="list-title">
                                                    <asp:Label ID="lblBoardID" runat="server" Visible="false" Text='<%# Eval("BOARDID") %>'></asp:Label>
                                                    <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                                    <a id="aTitle" runat="server" style="cursor:pointer"><%# Eval("TITLE") %></a>
                                                </td>
                                                <td class="date"><%# Eval("WRITEDT") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="pagingBoard" />
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                    </Triggers>
                </asp:UpdatePanel>
                <uc:paging ID="pagingBoard" runat="server" OnPreRender="pagingBoard_PreRender" />
            </div>    
        </div>
    </div>
</asp:Content>
