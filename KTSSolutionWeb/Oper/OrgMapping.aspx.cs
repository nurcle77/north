﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Oper;
using System.Linq;

namespace KTSSolutionWeb
{
    public partial class OrgMapping : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private string BasicChkNode
        {
            get
            {
                if (ViewState["BasicChkNode"] != null)
                    return ViewState["BasicChkNode"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["BasicChkNode"] = value;
            }
        }

        private string TargetChkNode
        {
            get
            {
                if (ViewState["TargetChkNode"] != null)
                    return ViewState["TargetChkNode"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["TargetChkNode"] = value;
            }
        }

        private Dictionary<string, string> addNodes
        {
            get
            {
                if (ViewState["addNodes"] != null)
                    return (Dictionary<string, string>)ViewState["addNodes"];
                else
                    return null;
            }
            set
            {
                ViewState["addNodes"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetTreeView();

                tvBasicOrg.Attributes.Add("onclick", "postBackObject1();");
                tvTargetOrg.Attributes.Add("onclick", "postBackObject2();");
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetTreeView()
        {
            DataSet ds = new DataSet();

            try
            {
                if (addNodes == null)
                    addNodes = new Dictionary<string, string>();

                tvBasicOrg.Nodes.Clear();

                using (OrgMappingMgmt org = new OrgMappingMgmt())
                {
                    ds = org.GetOrgMappingList();
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];

                    Dictionary<string, string> nodeValPath = new Dictionary<string, string>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        TreeNode node = new TreeNode(dr["ORGNM"].ToString());
                        node.Value = dr["ORGCD"].ToString();

                        if (dr["ORGLEVEL"].ToString().Equals("1"))
                        {
                            node.ShowCheckBox = false;
                        }
                        else
                        {
                            node.ShowCheckBox = true;
                        }

                        if (nodeValPath.ContainsKey(dr["UPPERORGCD"].ToString()))
                        {
                            string strValPath = nodeValPath[dr["UPPERORGCD"].ToString()];

                            TreeNode findNode = tvBasicOrg.FindNode(strValPath);
                            findNode.ChildNodes.Add(node);
                            nodeValPath.Add(node.Value, node.ValuePath);
                        }
                        else
                        {

                            tvBasicOrg.Nodes.Add(node);
                            nodeValPath.Add(node.Value, node.ValuePath);
                        }
                    }

                    tvBasicOrg.CollapseAll();

                    foreach (TreeNode node in tvBasicOrg.Nodes)
                    {
                        node.Expand();
                    }
                }

                SetTargetTreeView(99);
                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        private void SetTargetTreeView(int nDepth)
        {
            try
            {
                bool bBaseNodeChk = false;
                List<TreeNode> BaseNode = new List<TreeNode>();
                
                foreach(TreeNode node in tvBasicOrg.CheckedNodes)
                {
                    BaseNode.Add(node);
                }

                if (BaseNode.Count > 0)
                {
                    bBaseNodeChk = true;
                }

                tvTargetOrg.Nodes.Clear();

                Dictionary<string, string> nodeValPath = new Dictionary<string, string>();

                foreach (DataRow dr in DtOrgList.Rows)
                {
                    TreeNode node = new TreeNode(dr["ORGNM"].ToString());
                    node.Value = dr["ORGCD"].ToString();

                    node.ShowCheckBox = false;
                    bool bView = true;

                    foreach (TreeNode bNode in BaseNode)
                    {
                        if (bNode.Value.Contains(node.Value))
                            bView = false;
                    }

                    if (dr["ORGLEVEL"].ToString().Equals("1") && bView)
                    {
                        tvTargetOrg.Nodes.Add(node);
                        nodeValPath.Add(node.Value, node.ValuePath);
                    }
                    else if (nodeValPath.ContainsKey(dr["UPPERORGCD"].ToString()) && bView)
                    {
                        string strValPath = nodeValPath[dr["UPPERORGCD"].ToString()];

                        TreeNode findNode = tvTargetOrg.FindNode(strValPath);
                        findNode.ChildNodes.Add(node);
                        nodeValPath.Add(node.Value, node.ValuePath);
                    }
                    else
                    {
                        continue;
                    }

                    if (!dr["ORGLEVEL"].ToString().Equals("1") && !dr["ORGLEVEL"].ToString().Equals("5") && (node.Depth == nDepth || node.Depth == nDepth + 1))
                    {
                        node.ShowCheckBox = true;
                    }
                    else
                    {
                        node.ShowCheckBox = false;
                    }
                }

                if (addNodes != null)
                {
                    foreach (string additem in addNodes.Keys)
                    {
                        TreeNode addNode = new TreeNode(addNodes[additem]);

                        string[] strNodePath = additem.Split('/');
                        string strParentNode = "";

                        for (int i = 0; i < strNodePath.Length; i++)
                        {
                            if (i < strNodePath.Length - 1)
                            {
                                if (strParentNode.Length > 0)
                                    strParentNode += "/";

                                strParentNode += strNodePath[i];
                            }
                            else
                                addNode.Value = strNodePath[i];
                        }

                        if (nodeValPath.ContainsValue(strParentNode) && !nodeValPath.ContainsValue(additem))
                        {
                            TreeNode findNode = tvTargetOrg.FindNode(strParentNode);

                            if (addNode.Value.Split('|').Length > 0)
                            {
                                addNode.ShowCheckBox = false;
                            }
                            else
                            {
                                addNode.ShowCheckBox = true;
                            }

                            findNode.ChildNodes.Add(addNode);
                        }
                    }
                }

                tvTargetOrg.CollapseAll();

                foreach (TreeNode node in tvTargetOrg.Nodes)
                {
                    NodeExpand(node, nDepth, bBaseNodeChk);
                }

                updPanel2.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void NodeExpand(TreeNode node, int nDepth, bool bBaseNodeChk)
        {
            if (node.Depth == 0 || (node.Depth <= nDepth && bBaseNodeChk))
            {
                node.Expand();
            }

            if (node.ChildNodes.Count > 0)
            {
                foreach (TreeNode item in node.ChildNodes)
                {
                    NodeExpand(item, nDepth, bBaseNodeChk);
                }
            }
        }

        private void CheckUnCheckNode(TreeNode node, bool bCheck)
        {
            try
            {
                if (node != null)
                {
                    foreach (TreeNode item in node.ChildNodes)
                    {
                        if ((bool)item.ShowCheckBox)
                        {
                            item.Checked = bCheck;

                            if (item.ChildNodes.Count > 0)
                            {
                                CheckUnCheckNode(item, bCheck);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CheckUnCheckParentNode(TreeNode node)
        {
            try
            {
                TreeNode pNode = node.Parent;

                bool bCheck = true;

                foreach (TreeNode child in pNode.ChildNodes)
                {
                    if (!child.Checked)
                        bCheck = false;
                }

                pNode.Checked = bCheck;

                if (pNode.Parent != null)
                {
                    CheckUnCheckParentNode(pNode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetCheckNodeMinDepth(TreeNode node, ref int nDepth)
        {
            if (node.Checked)
            {
                if (node.Depth <= nDepth)
                {
                    nDepth = node.Depth;
                }
            }

            foreach (TreeNode child in node.ChildNodes)
            {
                GetCheckNodeMinDepth(child, ref nDepth);
            }
        }


        protected void tvBasicOrg_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            try
            {
                bool bCheck = e.Node.Checked;

                //if (BasicChkNode.Length > 0 && e.Node.ValuePath != BasicChkNode)
                //{
                //    TreeNode findNode = tvBasicOrg.FindNode(BasicChkNode);
                //    findNode.Checked = false;
                //    CheckUnCheckNode(findNode, false);

                //    e.Node.Checked = bCheck;
                //}

                CheckUnCheckNode(e.Node, bCheck);
                CheckUnCheckParentNode(e.Node);
                BasicChkNode = e.Node.ValuePath;

                updPanel1.Update();

                int nDepth = e.Node.Depth;

                if (nDepth > 0)
                {
                    nDepth = nDepth - 1;
                }

                SetTargetTreeView(nDepth);

                updPanel2.Update();

            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void tvTargetOrg_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            bool bCheck = e.Node.Checked;

            //if (TargetChkNode.Length > 0 && e.Node.ValuePath != TargetChkNode)
            //{
            //    TreeNode findNode = tvTargetOrg.FindNode(TargetChkNode);
            //    findNode.Checked = false;
            //    CheckUnCheckNode(findNode, false);

            //    e.Node.Checked = bCheck;
            //}

            CheckUnCheckNode(e.Node, bCheck);

            TargetChkNode = e.Node.ValuePath;

            updPanel2.Update();
        }

        protected void btnAddNode_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (tvBasicOrg.CheckedNodes.Count == 0 || tvTargetOrg.CheckedNodes.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('변경할 조직과 이동 위치을 선택해 주세요.');", true);
                }
                else
                {
                    List<TreeNode> nodes = new List<TreeNode>();
                    TreeNode TargetNode = tvTargetOrg.FindNode(TargetChkNode);
                    int nTargetOrgDepth = TargetNode.Depth;

                    List<TreeNode> BasicNodes = new List<TreeNode>();

                    int nBasicOrgDepth = 0;

                    foreach (TreeNode BasicNode in tvBasicOrg.CheckedNodes)
                    {
                        if (nBasicOrgDepth < BasicNode.Depth)
                        {
                            if (BasicNode.Depth > nTargetOrgDepth)
                            {
                                nBasicOrgDepth = nTargetOrgDepth;
                            }
                            else
                            {
                                nBasicOrgDepth = BasicNode.Depth;
                            }
                        }
                    }

                    //하위조직 이동
                    foreach (TreeNode node in tvBasicOrg.CheckedNodes)
                    {
                        if (node.Depth == nBasicOrgDepth)
                        {
                            continue;
                        }
                        nodes.Add(node);
                    }

                    foreach (TreeNode node in nodes)
                    {
                        TreeNode addNode = new TreeNode(node.Text, node.Value);

                        string strTargetPath = TargetNode.ValuePath;
                        string[] strPathLIst = node.ValuePath.Split('/');

                        for (int i = 0; i < strPathLIst.Length - 1; i++)
                        {
                            if (TargetNode.ValuePath.Split('/').Length > i)
                            {
                                continue;
                            }
                            strTargetPath += "/" + strPathLIst[i];
                        }

                        if (node.Depth == tvTargetOrg.FindNode(strTargetPath).Depth)
                            addNode.ShowCheckBox = true;
                        else
                            addNode.ShowCheckBox = false;


                        if (addNode.Value.Split('|').Length > 1)
                        {
                            TreeNode TempNode = new TreeNode(node.Text);
                            TempNode.Value = strTargetPath.Split('/')[strTargetPath.Split('/').Length - 1] + "|" + addNode.Value.Split('|')[1];
                            bool addChk = true;

                            foreach (TreeNode officeNode in tvTargetOrg.FindNode(strTargetPath).ChildNodes)
                            {
                                if (officeNode.Value.Split('|')[1].Equals(addNode.Value.Split('|')[1]))
                                {
                                    addChk = false;
                                    break;
                                }
                            }

                            if(addChk)
                            {
                                tvTargetOrg.FindNode(strTargetPath).ChildNodes.Add(addNode);

                                if (!addNodes.ContainsKey(node.ValuePath))
                                {
                                    addNodes.Add(addNode.ValuePath, addNode.Text);
                                }
                            }
                        }
                        else
                        {
                            tvTargetOrg.FindNode(strTargetPath).ChildNodes.Add(addNode);

                            if (!addNodes.ContainsKey(node.ValuePath))
                            {
                                addNodes.Add(addNode.ValuePath, addNode.Text);
                            }
                        }
                    }
                                        
                    foreach (TreeNode node in nodes)
                    {
                        node.Parent.Checked = false;
                        node.Checked = false;

                        if (node.Depth == nBasicOrgDepth + 1)
                        {
                            tvBasicOrg.FindNode(node.ValuePath).Parent.ChildNodes.Remove(node);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnRegedit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (addNodes.Count > 0)
                {
                    using (OrgMappingMgmt org = new OrgMappingMgmt())
                    {
                        org.SetOrgMappingInfo(addNodes);
                    }

                    addNodes.Clear();
                    SetTreeView();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOk", "alert('변경된 조직 정보 저장을 완료했습니다.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('변경된 정보가 없습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnReset_ServerClick(object sender, EventArgs e)
        {
            try
            {
                SetTreeView();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}