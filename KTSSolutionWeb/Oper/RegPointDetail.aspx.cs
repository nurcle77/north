﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.ExcelImport;
using OfficeOpenXml;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class RegPointDetail : PageBase
    {
        private string Years
        {
            get
            {
                if (ViewState["Years"] != null)
                    return ViewState["Years"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["Years"] = value;
            }
        }

        private string PointSeq
        {
            get
            {
                if (ViewState["PointSeq"] != null)
                    return ViewState["PointSeq"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["PointSeq"] = value;
            }
        }

        private string sTitle
        {
            get
            {
                if (ViewState["Title"] != null)
                    return ViewState["Title"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["Title"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Years = Request.Form["YEARS"] == null ? "" : Request.Form["YEARS"].ToString();
                    PointSeq = Request.Form["POINTSEQ"] == null ? "" : Request.Form["POINTSEQ"].ToString();
                    Title = Request.Form["TITLE"] == null ? "" : Request.Form["TITLE"].ToString();

                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointList_Detail(Years, PointSeq);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn(DataTable dt)
        {

            string[] ArrHeader = new string[dt.Columns.Count];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ArrHeader[i] = dt.Columns[i].ColumnName;
            }
            return ArrHeader;
        }

        private string[] GetHeaderTop(DataTable dt)
        {
            //string[] ArrHeader = new string[7];

            //return ArrHeader;
            string[] ArrHeader = new string[9];

            ArrHeader[0] = "연도";
            ArrHeader[1] = "연도SEQ";
            ArrHeader[2] = "상세SEQ";
            ArrHeader[3] = "신규구분";
            ArrHeader[4] = "직무구분";
            ArrHeader[5] = "동하복구분";
            ArrHeader[6] = "피복상세";
            ArrHeader[7] = "수량";
            ArrHeader[8] = "단가";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnUpload_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            try
            {

                HttpPostedFile postedFIle = Request.Files["fu"];

                if (postedFIle.ContentLength > 0)
                {
                    string strtype = postedFIle.ContentType;

                    if (strtype.Equals("application/vnd.ms-excel") || strtype.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    {
                        Stream stream = postedFIle.InputStream;

                        using (ExcelImport excel = new ExcelImport())
                        {
                            using (ExcelPackage excelPackage = new ExcelPackage(stream))
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                if (worksheet.Dimension.End.Column != 9)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidatioChk", "alert('업로드 파일 양식이 잘못되었습니다.');", true);
                                    return;
                                }
                                else
                                {
                                    excel.ExcelUpLoad("피복지급기준", worksheet, "tb_providepointdetail_temp");
                                }
                            }
                        }

                        using (PointMgmt point = new PointMgmt())
                        {
                            ds = point.GetProvidePointCheck(Years, PointSeq);

                            if (ds.Tables.Count > 0)
                            {
                                string strChkResult = ds.Tables[0].Rows[0]["RESULT"].ToString();

                                if (strChkResult.Equals("OK"))
                                {
                                    point.InsProvidePoint(Years, PointSeq);

                                    GetDataList();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UploadOk", "alert('업로드가 완료되었습니다.');", true);
                                }
                                else if (strChkResult.Equals("DATANULL"))
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업로드 데이터가 없습니다.');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('연도/재직구분/직무 별 사용포인트의 합은 연단위 포인트보다 클 수 없습니다.');", true);
                                }
                            }
                            else
                            {
                                throw new Exception();
                            }
                        }

                        updPanel1.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('엑셀파일만 업로드가 가능합니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('엑셀 업로드 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    using (PointMgmt point = new PointMgmt())
                    {
                        ds = point.GetProvidePointList_Detail(Years, PointSeq);
                    }

                    dt = ds.Tables[0];
                }

                dt.Columns.Remove("NUM");

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn(dt);
                    excel.HeaderTop = GetHeaderTop(dt);
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "피복기준상세_" + sTitle);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }

        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        int nRowSpan1 = 1;
                        int nRowSpan2 = 1;

                        for (int i = rptResult.Items.Count - 1; i >= 0; i--)
                        {
                            HtmlTableCell tdProvType = (HtmlTableCell)rptResult.Items[i].FindControl("tdProvType");
                            HtmlTableCell tdJobNm = (HtmlTableCell)rptResult.Items[i].FindControl("tdJobNm");

                            if (i > 0)
                            {
                                Label lblProvType = (Label)rptResult.Items[i].FindControl("lblProvType");
                                Label lblProvTypePrev = (Label)rptResult.Items[i - 1].FindControl("lblProvType");

                                Label lblJobNm = (Label)rptResult.Items[i].FindControl("lblJobNm");
                                Label lblJobNmPrev = (Label)rptResult.Items[i - 1].FindControl("lblJobNm");


                                HtmlTableCell tdProvTypePrev = (HtmlTableCell)rptResult.Items[i - 1].FindControl("tdProvType");
                                HtmlTableCell tdJobNmPrev = (HtmlTableCell)rptResult.Items[i - 1].FindControl("tdJobNm");

                                if (lblProvType != null && lblProvTypePrev != null)
                                {
                                    if (lblProvType.Text.Equals(lblProvTypePrev.Text))
                                    {
                                        nRowSpan1++;

                                        tdProvTypePrev.RowSpan = nRowSpan1;
                                        tdProvType.Visible = false;
                                    }
                                    else
                                    {
                                        nRowSpan1 = 1;
                                    }
                                }

                                if (lblJobNm != null && lblJobNmPrev != null)
                                {
                                    if (lblProvType.Text.Equals(lblProvTypePrev.Text) && lblJobNm.Text.Equals(lblJobNmPrev.Text))
                                    {
                                        nRowSpan2++;

                                        tdJobNmPrev.RowSpan = nRowSpan2;
                                        tdJobNm.Visible = false;
                                    }
                                    else
                                    {
                                        nRowSpan2 = 1;
                                    }
                                }
                            }

                        }
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}