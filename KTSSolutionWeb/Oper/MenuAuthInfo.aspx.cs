﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;

namespace KTSSolutionWeb
{
    public partial class MenuAuthInfo : PageBase
    {
        #region fields
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }
            if (!IsPostBack)
            {
                MenuTree_Load();

                GetDataList(false);
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList(bool bAdd)
        {
            List<MenuInfoItem> menuItems = new List<MenuInfoItem>();

            DataSet dsAuthList = new DataSet();
            DataSet dsMenuList = new DataSet();
            DataSet dsAuthMenu = new DataSet();

            try
            {
                using (UserMgmt userReg = new UserMgmt())
                {
                    dsAuthList = userReg.GetAuthGroup();
                }

                if (dsAuthList.Tables.Count > 0)
                {
                    using (MenuMgmt menu = new MenuMgmt())
                    {
                        dsMenuList = menu.GetMenuList();
                        dsAuthMenu = menu.GetAuthGroupMenu();
                    }

                    DataTable dtAuthList = new DataTable();

                    dtAuthList.Columns.Add("AUTHID");
                    dtAuthList.Columns.Add("AUTHNM");

                    foreach (DataRow dr in dsAuthList.Tables[0].Rows)
                    {
                        DataRow row = dtAuthList.NewRow();

                        row["AUTHID"] = dr["AUTHID"];
                        row["AUTHNM"] = dr["AUTHNM"];

                        dtAuthList.Rows.Add(row);
                    }

                    if (bAdd)
                    {
                        DataRow row = dtAuthList.NewRow();
                        row["AUTHID"] = "";
                        row["AUTHNM"] = "";
                        dtAuthList.Rows.Add(row);
                    }

                    rptHead.DataSource = dtAuthList;
                    rptHead.DataBind();

                    for (int i = 0; i < rptHead.Items.Count; i++)
                    {
                        Label lbluthId = (Label)rptHead.Items[i].FindControl("lblAuthId");
                        HtmlInputCheckBox chkDel = (HtmlInputCheckBox)rptHead.Items[i].FindControl("chkDel");

                        if (lbluthId.Text.Length > 0)
                        {
                            chkDel.Visible = true;
                        }
                        else
                        {
                            chkDel.Visible = false;
                        }
                    }

                    if (dsMenuList.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsMenuList.Tables[0].Rows)
                        {
                            MenuInfoItem menuinfo = new MenuInfoItem(dr);

                            int Upperidx = menuItems.FindLastIndex(t => t.MENUID == dr["UPPERMENUID"].ToString());
                            if (Upperidx == -1)
                            {
                                menuItems.Add(menuinfo);
                            }
                            else
                            {
                                menuItems.Insert(Upperidx + menuinfo.MENUORDER, menuinfo);
                            }
                        }

                        HtmlTable menuTable = new HtmlTable();

                        foreach (MenuInfoItem menuinfo in menuItems)
                        {
                            HtmlTableRow tableRow = new HtmlTableRow();
                            HtmlTableCell tableCell = new HtmlTableCell();

                            Label lblMenu = new Label();

                            lblMenu.Width = Unit.Parse("160px");
                            string strMenuNm = menuinfo.MENUNM;

                            if (menuinfo.MENULEVEL >= 2)
                            {
                                strMenuNm = "└ " + menuinfo.MENUNM;

                                for (int i = 1; i < menuinfo.MENULEVEL; i++)
                                {
                                    strMenuNm = "　" + strMenuNm;
                                }
                            }

                            lblMenu.Text = strMenuNm;
                            tableCell.Controls.Add(lblMenu);
                            tableCell.Style.Add("padding-left", "10px");
                            tableCell.Width = "160px";
                            tableCell.Height = "25px";

                            tableRow.Controls.Add(tableCell);

                            menuTable.Controls.Add(tableRow);
                        }

                        tdMenu.Controls.Add(menuTable);

                        rptBody.DataSource = dtAuthList;
                        rptBody.DataBind();

                        if (dsAuthMenu.Tables.Count > 0)
                        {
                            for (int i = 0; i < rptBody.Items.Count; i++)
                            {
                                string strAuthId = dtAuthList.Rows[i]["AUTHID"].ToString();

                                List<MenuAuthItem> menuAuths = new List<MenuAuthItem>();
                                DataRow[] rows = null;

                                if (strAuthId.Length > 0)
                                {
                                    rows = dsAuthMenu.Tables[0].Select(string.Format("AUTHID = '{0}'", dtAuthList.Rows[i]["AUTHID"].ToString()));
                                }
                                else
                                {
                                    if (i == 0)
                                        continue;

                                    rows = dsAuthMenu.Tables[0].Select(string.Format("AUTHID = '{0}'", dtAuthList.Rows[i - 1]["AUTHID"].ToString()));
                                }

                                foreach (DataRow dr in rows)
                                {
                                    MenuAuthItem menuauth = new MenuAuthItem(dr);

                                    int Upperidx = menuAuths.FindLastIndex(t => t.MENUID == dr["UPPERMENUID"].ToString());
                                    if (Upperidx == -1)
                                    {
                                        menuAuths.Add(menuauth);
                                    }
                                    else
                                    {
                                        menuAuths.Insert(Upperidx + menuauth.MENUORDER, menuauth);
                                    }
                                }

                                Repeater rptInTable = (Repeater)rptBody.Items[i].FindControl("rptInTable");
                                rptInTable.DataSource = menuAuths;
                                rptInTable.DataBind();

                                for (int j = 0; j < rptInTable.Items.Count; j++)
                                {
                                    Label lblMenuId = (Label)rptInTable.Items[j].FindControl("lblMenuId");

                                    RadioButton rdY =(RadioButton)rptInTable.Items[j].FindControl("rdY");
                                    RadioButton rdN = (RadioButton)rptInTable.Items[j].FindControl("rdN");

                                    lblMenuId.Text = menuAuths[j].MENUID;

                                    if (menuAuths[j].VIEWYN.Equals("Y"))
                                    {
                                        rdY.Checked = true;
                                        rdN.Checked = false;
                                    }
                                    else
                                    {
                                        rdY.Checked = false;
                                        rdN.Checked = true;
                                    }
                                }                                    
                            }
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList(false);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnAdd_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList(true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < rptHead.Items.Count; i++)
                {
                    Label lbluthId = (Label)rptHead.Items[i].FindControl("lblAuthId");
                    TextBox txbAuthNm = (TextBox)rptHead.Items[i].FindControl("txbAuthNm");

                    string strAuthId = lbluthId.Text;
                    string strAuthNm = txbAuthNm.Text;

                    using (MenuMgmt menu = new MenuMgmt())
                    {
                        if (strAuthNm.Length == 0)
                        {
                            continue;
                        }
                        else
                        {
                            if (strAuthId.Length == 0)
                            {
                                strAuthId = "AUTH0001";

                                using (UserMgmt userReg = new UserMgmt())
                                {
                                    DataSet dsAuthList = userReg.GetAuthGroup();

                                    if (dsAuthList.Tables.Count > 0)
                                    {
                                        if (dsAuthList.Tables[0].Rows.Count > 0)
                                        {
                                            strAuthId = dsAuthList.Tables[0].Rows[dsAuthList.Tables[0].Rows.Count - 1]["AUTHID"].ToString();

                                            strAuthId = "AUTH" + (int.Parse(strAuthId.Substring(4, 4)) + 1).ToString().PadLeft(4, '0');
                                        }
                                    }
                                }
                                menu.InsertAuthGroup(strAuthId, strAuthNm);
                            }
                            else
                            {
                                menu.UpdateAuthGroup(strAuthId, strAuthNm);
                            }
                        }
                    }
                }

                for (int i = 0; i < rptBody.Items.Count; i++)
                {
                    Label lbluthId = (Label)rptBody.Items[i].FindControl("lblAuthId");
                    Repeater rptInTable = (Repeater)rptBody.Items[i].FindControl("rptInTable");

                    string strAuthId = lbluthId.Text;

                    if (strAuthId.Length == 0)
                    {
                        strAuthId = "AUTH0001";

                        using (UserMgmt userReg = new UserMgmt())
                        {
                            DataSet dsAuthList = userReg.GetAuthGroup();

                            if (dsAuthList.Tables.Count > 0)
                            {
                                if (dsAuthList.Tables[0].Rows.Count > 0)
                                {
                                    strAuthId = dsAuthList.Tables[0].Rows[dsAuthList.Tables[0].Rows.Count - 1]["AUTHID"].ToString();
                                }
                            }
                        }
                    }

                    List<string> lstData = new List<string>();

                    for (int j = 0; j < rptInTable.Items.Count; j++)
                    {
                        Label lblMenuId = (Label)rptInTable.Items[j].FindControl("lblMenuId");

                        RadioButton rdY = (RadioButton)rptInTable.Items[j].FindControl("rdY");

                        string strMenuId = lblMenuId.Text;
                        string strViewYn = rdY.Checked ? "Y" : "N";

                        string strData = strAuthId + "|" + strMenuId + "|" + strViewYn;

                        lstData.Add(strData);
                    }

                    using (MenuMgmt menu = new MenuMgmt())
                    {
                        menu.UpdateAuthMenu(lstData);
                    }
                }

                GetDataList(false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOk", "alert('권한 정보 저장을 완료했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            DataSet dsAuth = new DataSet();
            try
            {
                string strDelAuthNm = "";

                for (int i = 0; i < rptHead.Items.Count; i++)
                {
                    Label lbluthId = (Label)rptHead.Items[i].FindControl("lblAuthId");
                    TextBox txbAuthNm = (TextBox)rptHead.Items[i].FindControl("txbAuthNm");
                    HtmlInputCheckBox chkDel = (HtmlInputCheckBox)rptHead.Items[i].FindControl("chkDel");

                    if (chkDel.Checked)
                    {
                        if (lbluthId.Text.Length > 0 && txbAuthNm.Text.Length > 0)
                        {
                            using (UserMgmt userReg = new UserMgmt())
                            {
                                dsAuth = userReg.GetDelAuthCheck(lbluthId.Text);
                            }

                            bool DelCheck = false;

                            if (dsAuth.Tables.Count > 0)
                            {
                                if (dsAuth.Tables[0].Rows.Count > 0)
                                {
                                    if (dsAuth.Tables[0].Rows[0]["DELYN"].ToString().Equals("Y"))
                                    {
                                        DelCheck = true;
                                    }
                                }
                            }

                            if (DelCheck)
                            {
                                using (UserMgmt userReg = new UserMgmt())
                                {
                                    userReg.DelAuthGroup(lbluthId.Text);
                                }

                                if (strDelAuthNm.Length > 0)
                                    strDelAuthNm += ", ";

                                strDelAuthNm += txbAuthNm.Text;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                GetDataList(false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DelOk", "alert('삭제가 완료되었습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (dsAuth != null)
                    dsAuth.Dispose();
            }
        }
    }
}