﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PointInfo.aspx.cs" Inherits="KTSSolutionWeb.PointInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function RegPointDetailPopup(strYears, strPointSeq, strtitle) {
            var nWidth = 770;
            var nHeight = 750;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                YEARS: strYears,
                POINTSEQ: strPointSeq,
                TITLE: strtitle
            };

            var Popupform = createForm("/Oper/RegPointDetail", param);

            Popupform.target = "RegPointDetail";
            var win = window.open("", "RegPointDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function ChkInsYearPoint() {
            if (confirm("연단위 포인트를 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ChkDelYearPoint() {
            if (confirm("지급별포인트가 있으면 삭제가 불가능합니다. 선책한 연단위 포인트를 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ChkInsUsePoint() {
            if (confirm("지급별 포인트 내역을 추가 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ChkDelUsePoint() {
            if (confirm("선택한 지급별 포인트 내역을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ChkSaveUsePoint() {
            if (confirm("지급별 포인트 내역을 수정하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function InitValue() {
            $("#<%= txbStDt.ClientID %>").val('');
            $("#<%= txbEnDt.ClientID %>").val('');
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <!-- S:leftbox -->
            <div class="leftbox">
                <!-- S:searchbox -->
                <div class="searchbox">
                    <asp:UpdatePanel ID="updPanelDDL1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset>
                                <span class="optionbox first">
                                    <label>연도</label>
                                    <asp:DropDownList ID="ddlYears" runat="server" Width="80px" TabIndex="1"></asp:DropDownList>
                                </span>
                                <span class="inpbox">
                                    <label>포인트</label>
                                    <asp:TextBox ID="txbPoint" name="txbPoint" runat="server" MaxLength="10" TabIndex="2" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" ></asp:TextBox>
                                </span>
                                <asp:Button ID="btnInsYearPoint" class="btn-green last" runat="server" OnClientClick="return ChkInsYearPoint();" OnClick="btnInsYearPoint_ServerClick" Text="추가" />
                            </fieldset>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <!-- E:searchbox -->
    
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>연단위 포인트 조회 결과</strong>
                        <div class="pull-right">
                            <div class="btnset">
                                <asp:Button ID="btnDelYearPoint" runat="server" OnClientClick="return ChkDelYearPoint();" OnClick="btnDelYearPoint_ServerClick" class="btn-black" Text="삭제" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->
    
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>No.</th>
                                            <th>연도</th>
                                            <th>기준포인트</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult1" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <input id="cbPoint" runat="server" type="checkbox" style="width:20px;height:20px;" />
                                                    </td>
                                                    <td><%# Eval("NUM") %></td>
                                                    <td>
                                                        <asp:Label ID="lblYears" runat="server" Text='<%# Eval("YEARS") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDefaultPoint" runat="server" Text='<%# Eval("DEFAULTPOINT") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnInsYearPoint" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnDelYearPoint" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="paging1" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>    
                    <!-- E:scrollbox -->
                    <uc:paging ID="paging1" runat="server" OnPreRender="paging1_PreRender" />
                </div>
            </div>
            <!-- E:leftbox -->
            <!-- S:rightbox -->
            <div class="rightbox">
                <!-- S:searchbox -->
                <div class="searchbox">
                    <fieldset>
                        <span class="optionbox first">
                            <asp:UpdatePanel ID="updPanelDDL2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <label>연도</label>
                                    <asp:DropDownList ID="ddlPointYears" runat="server" Width="80px" TabIndex="3"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </span>
                        <span class="inpbox">
                            <asp:UpdatePanel ID="updPanelTitle" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <label>제목</label>
                                    <asp:TextBox ID="txbTitle" runat="server" MaxLength="150" TabIndex="4" ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </span>
                        <span class="inpbox">
                            <label>일자</label>
                            <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" MaxLength="10" TabIndex="5" ReadOnly="true"></asp:TextBox>
                            <em>~</em>
                            <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" MaxLength="10" TabIndex="6" ReadOnly="true"></asp:TextBox>
                        </span>
                        <asp:Button ID="btnInsUsePoint" class="btn-green last" runat="server" OnClientClick="return ChkInsUsePoint();" OnClick="btnInsUsePoint_ServerClick" Text="추가" />
                    </fieldset>
                </div>
                <!-- E:searchbox -->
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>지급별 조회 결과</strong>
                        <div class="pull-right">
                            <div class="btnset">
                                <asp:Button ID="btnDelUsePoint" class="btn-black" runat="server" OnClientClick="return ChkDelUsePoint();" OnClick="btnDelUsePoint_ServerClick" Text="삭제" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->                    
                        
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <asp:UpdatePanel ID="updPanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>No.</th>
                                            <th>제목</th>
                                            <th>시작일</th>
                                            <th>마감일</th>
                                            <th>사용포인트</th>
                                            <th>저장</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult2" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <input id="cbPoint" runat="server" type="checkbox" style="width:20px;height:20px;" />
                                                    </td>
                                                    <td><%# Eval("NUM") %></td>
                                                    <td>
                                                        <asp:Label ID="lblYears" runat="server" Visible="false" Text='<%# Eval("YEARS") %>'></asp:Label>
                                                        <asp:Label ID="lblPointSeq" runat="server" Visible="false" Text='<%# Eval("POINTSEQ") %>'></asp:Label>
                                                        <asp:Label ID="lblTitle" runat="server" style="cursor:pointer" Text='<%# Eval("TITLE") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div class="txtbox">
                                                            <asp:TextBox id="txbPointStdt" runat="server" Width="100%" Height="40px" MaxLength="8" style="text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Text='<%# Eval("STDT") %>'></asp:TextBox>
                                                        </div>
                                                    <td>
                                                        <div class="txtbox">
                                                            <asp:TextBox id="txbPointEndt" runat="server" Width="100%" Height="40px" MaxLength="8" style="text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Text='<%# Eval("ENDT") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td><%# Eval("USEPOINT") %></td>
                                                    <td>
                                                        <asp:Button id="btnSaveUsePoint" runat="server" OnClientClick="return ChkSaveUsePoint();" OnClick="btnSaveUsePoint_Click" class="btn-save" Text="수정" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnInsUsePoint" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnDelUsePoint" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="paging2" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <!-- E:scrollbox -->
                    <uc:paging ID="paging2" runat="server" OnPreRender="paging2_PreRender" />
                </div>
                <!-- E:datalist -->
            </div>                
            <!-- E:rightbox -->
        </div>
        <!-- E:multiplebox -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>