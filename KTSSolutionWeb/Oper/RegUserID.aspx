﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegUserID.aspx.cs" Inherits="KTSSolutionWeb.RegUserID" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사용자계정 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">
        function IdCheck() {
            if ($("#txbEmpNo").val() == "") {
                alert("로그인사번(ID)를 입력하세요");
                return false;
            }
        }

        function SetOrgCode(orgcd) {
            $("#hdfOrgCd").val(orgcd);

            __doPostBack("<%=hdfOrgCd.ClientID %>", "");
        }

        function PopupOrgTree() {
            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var ktsyn = $("#<%= ddlKtsYn.ClientID %> option:selected").val();

            var param = {
                pKTSYN: ktsyn
            };

            var Popupform = createForm("/Common/OrgTree_BComp", param);

            Popupform.target = "OrgTree_BComp";
            var win = window.open("", "OrgTree_BComp", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>사용자 등록</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th style="width:150px">구분</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="optionbox">
                                                            <asp:DropDownList ID="ddlKtsYn" style="width:150px;" runat="server" AutoPostBack="true" TabIndex="0" OnSelectedIndexChanged="DDLKtsYn_SelectedIndexChanged">
                                                                <asp:ListItem Text="kts" Value="Y" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="협력사" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:150px">소속</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbTeam" runat="server" Width="500px" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree();">+</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thEmpNo" runat="server" style="width:150px">로그인사번</th>
                                                    <td id="tdEmpNo" runat="server" style="text-align:left">
                                                        <p class="inpbox" style="text-align:left; width:160px;">
                                                            <asp:TextBox ID="txbEmpNo" runat="server" Width="150px" MaxLength="20" TabIndex="1"></asp:TextBox>
                                                        </p>
                                                        <asp:Button ID="btnIDChk" runat="server" OnClientClick="return IdCheck();" OnClick="BtnIDCheck_Click" class="btn-popupgray" Text="사번확인" />
                                                    </td>
                                                    <th id="thKtsEmpNo" runat="server" style="width:150px">kts사번</th>
                                                    <td id="tdKtsEmpNo" runat="server">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbKtsEmpno" runat="server" Width="230px" MaxLength="20" TabIndex="2"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trkts1" runat="server">
                                                    <th style="width:150px">이름</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbKtsEmpNm" runat="server" Width="230px" MaxLength="50" TabIndex="4"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:150px">연락처</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbKtsTelNo" runat="server" Width="230px" MaxLength="50" TabIndex="3"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trkts2" runat="server">
                                                    <th style="width:150px">IDMS사번</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbIdmsEmpNo" runat="server" Width="230px" MaxLength="20" TabIndex="4"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:150px">IDMS(구)사번</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbPreIdmsEmpNo" runat="server" Width="230px" MaxLength="20" TabIndex="5"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trEtc1" runat="server" visible="false">
                                                    <th style="width:150px">업체명</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEtcCompNm" runat="server" Width="230px" MaxLength="50" TabIndex="6"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:150px">업체코드</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEtcCompCd" runat="server" Width="230px" MaxLength="50" TabIndex="7"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trEtc2" runat="server" visible="false">
                                                    <th style="width:150px">이름</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEtcEmpNm" runat="server" Width="230px" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:150px">연락처</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEtcTelNo" runat="server" Width="230px" MaxLength="11" TabIndex="9" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:150px">권한</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="optionbox">
                                                            <asp:dropdownlist id="ddlAuth" Width="230px" runat="server"></asp:dropdownlist>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"/>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlKtsYn" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                                    </Triggers>
                                </asp:updatepanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <div class="btncenter">
                            <asp:Button id="btnReg" runat="server" OnClick="BtnRegedit_Click" class="btn-green" Text="등록" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
