﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Coefficient.aspx.cs" Inherits="KTSSolutionWeb.Coefficient" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function btnSaveCheck() {
            if (confirm("구역계수를 등록 하시겠습니까?(이미 등록한 구역계수가 있을 경우 입력된 값으로 변경됩니다.)")) {
                return true;
            }
            else {
                return false;
            }
        }

        function InitValue() {
            $("#<%= txbDeadLine.ClientID %>").val('');
        }
    </script>
    <%--<style>
        .ui-datepicker {display:none; width:200px; height:40px; overflow:hidden; margin:2px 0 0 0; padding:0; background:#fff; border-radius:0; box-shadow:2px 2px 4px 2px rgba(0,0,0,0.1)}
        table.ui-monthpicker-calendar {display:none;}
    </style>--%>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->

    <!-- S: contentsarea -->
    <div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>
                <span class="inpbox first">
                    <label>일자</label>
                    <asp:TextBox ID="txbDate" runat="server" class="month" MaxLength="7" TabIndex="1" ></asp:TextBox>
                </span>
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelMin" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>Min</label>
                            <asp:TextBox ID="txbMinValue" runat="server" MaxLength="10" TabIndex="2" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);"></asp:TextBox>     
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelMax" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>Max</label>
                            <asp:TextBox ID="txbMaxValue" runat="server" MaxLength="10" TabIndex="3" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);"></asp:TextBox>     
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelSum" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>조정한도(%)</label>
                            <asp:TextBox ID="txbSumValue" runat="server" MaxLength="10" TabIndex="4" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);"></asp:TextBox>   
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <span class="inpbox">
                    <label>수정마감일자</label>
                    <asp:TextBox ID="txbDeadLine" runat="server" class="date" Width="120px" MaxLength="10" TabIndex="5"></asp:TextBox>
                </span>

                <asp:Button id="btnSave" runat="server" OnClientClick="return btnSaveCheck();" OnClick="btnSave_ServerClick" class="btn-green" Text="등록" />
                <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>구역계수 조회 결과</strong>
		    </div>
		    <!-- E:list-top -->
		    <!-- S:scrollbox -->
		    <div class="scrollbox">            
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>일자</th>
                                    <th>Min</th>
                                    <th>Max</th>
                                    <th>조정한도(%)</th>
                                    <th>수정마감일자</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("COEFFICIENTDT") %></td>
                                            <td><%# Eval("MINVAL") %></td>
                                            <td><%# Eval("MAXVAL") %></td>
                                            <td><%# Eval("SUMVAL") %></td>
                                            <td><%# Eval("UPDATE_DEADLINE") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
		    <!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>
