﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class RegUserID : PageBase
    {
        public string bIDCheck
        {
            get
            {
                if (ViewState["IDCheck"] != null)
                {
                    return ViewState["IDCheck"].ToString();
                }
                else
                {
                    return "N";
                }
            }
            set
            {
                ViewState["IDCheck"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();
                    this.txbTeam.Attributes.Add("onclick", "PopupOrgTree();");
                    updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void PageInit()
        {
            DropDownListBinding();
        }

        private void DropDownListBinding()
        {
            DataSet ds = new DataSet();

            try
            {
                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetAuthGroup();

                    ddlAuth.Items.Add(new ListItem("선택하세요", ""));

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            ddlAuth.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                        }

                        ddlAuth.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ValidationCheck(ref string strMsg)
        {
            bool bCheck = true;

            string strEmpNo = Request.Form[txbEmpNo.UniqueID];
            string strTeamFullNm = Request.Form[txbTeam.UniqueID];
            string strOrgCd = Request.Form[hdfOrgCd.UniqueID];

            if (ddlKtsYn.SelectedValue.Equals("Y"))
            {
                string strEmpNm = Request.Form[txbKtsEmpNm.UniqueID];
                string strKtsTelNo = Request.Form[txbKtsTelNo.UniqueID];

                if (!bIDCheck.Equals("Y"))
                {
                    strMsg = "사번 확인을 해주세요.";
                    return false;
                }

                if (strEmpNo.Equals(""))
                {
                    strMsg = "로그인 사번을 입력해주세요.";
                    return false;
                }

                if (strEmpNm.Equals(""))
                {
                    strMsg = "이름을 입력해주세요.";
                    return false;
                }

                if (strKtsTelNo.Equals(""))
                {
                    strMsg = "연락처를 입력해주세요.";
                    return false;
                }
            }
            else
            {
                string strEtcCompNm = Request.Form[txbEtcCompNm.UniqueID];
                string strEtcCompCd = Request.Form[txbEtcCompCd.UniqueID];
                string strEtcEmpNm = Request.Form[txbEtcEmpNm.UniqueID];
                string strTelNo = Request.Form[txbEtcTelNo.UniqueID];

                if (!bIDCheck.Equals("Y"))
                {
                    strMsg = "ID 확인을 해주세요.";
                    return false;
                }

                if (strEmpNo.Equals(""))
                {
                    strMsg = "ID 를 입력해주세요.";
                    return false;
                }

                if (strEtcCompCd.Equals("") || strEtcCompNm.Equals(""))
                {
                    strMsg = "업체명, 업체코드를 입력해주세요.";
                    return false;
                }

                if (strEtcEmpNm.Equals(""))
                {
                    strMsg = "이름을 입력해주세요.";
                    return false;
                }

                if (strTelNo.Equals(""))
                {
                    strMsg = "휴대폰번호를 입력해주세요.";
                    return false;
                }
            }

            if (strTeamFullNm.Equals("") || strOrgCd.Equals(""))
            {
                strMsg = "조직을 선택해주세요.";
                return false;
            }

            if (ddlAuth.SelectedValue.Equals(""))
            {
                strMsg = "권한을 선택해주세요.";
                return false;
            }

            return bCheck;
        }

        protected void BtnIDCheck_Click(object sender, EventArgs e)
        {
            string strEmpNo = Request.Form[txbEmpNo.UniqueID];
            bool bExistUser = false;
            string strMsg = "";

            DataSet ds = new DataSet();

            try
            {
                if (strEmpNo.Trim().Length == 0)
                {
                    strMsg = "잘못된 접근입니다.";
                }
                else
                {
                    using (KTSUser ktsUser = new KTSUser())
                    {
                        ds = ktsUser.GetUserLogInfo(strEmpNo);

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bExistUser = true;
                            }
                        }
                    }

                    if (bExistUser)
                    {
                        if (ddlKtsYn.SelectedValue.Equals("Y"))
                        {
                            strMsg = "이미 등록된 사번 입니다.";
                        }
                        else
                        {
                            strMsg = "이미 등록된 ID 입니다.";
                        }
                    }
                    else
                    {
                        bIDCheck = "Y";

                        if (ddlKtsYn.SelectedValue.Equals("Y"))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "IDChkOK", "alert('등록 가능한 사번 입니다.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "IDChkOK", "alert('등록 가능한 ID 입니다.');", true);
                        }
                    }
                }

                if (strMsg.Length > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DDLKtsYn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bIDCheck = "N";
                this.txbTeam.Text = "";

                this.txbEmpNo.Text = "";
                this.txbKtsEmpno.Text = "";
                this.txbKtsTelNo.Text = "";
                this.txbKtsEmpNm.Text = "";
                this.txbIdmsEmpNo.Text = "";
                this.txbPreIdmsEmpNo.Text = "";

                this.txbEtcCompNm.Text = "";
                this.txbEtcCompCd.Text = "";
                this.txbEtcEmpNm.Text = "";
                this.txbEtcTelNo.Text = "";

                this.ddlAuth.SelectedIndex = 0;

                this.hdfOrgCd.Value = "";

                if (ddlKtsYn.SelectedValue.Equals("Y"))
                {
                    this.thEmpNo.InnerText = "로그인사번";
                    this.tdEmpNo.ColSpan = 1;
                    this.thKtsEmpNo.Visible = true;
                    this.tdKtsEmpNo.Visible = true;
                    this.btnIDChk.Text = "사번확인";

                    this.trkts1.Visible = true;
                    this.trkts2.Visible = true;

                    this.trEtc1.Visible = false;
                    this.trEtc2.Visible = false;
                }
                else
                {
                    this.thEmpNo.InnerText = "아이디";
                    this.tdEmpNo.ColSpan = 3;
                    this.thKtsEmpNo.Visible = false;
                    this.tdKtsEmpNo.Visible = false;
                    this.btnIDChk.Text = "ID확인";

                    this.trkts1.Visible = false;
                    this.trkts2.Visible = false;

                    this.trEtc1.Visible = true;
                    this.trEtc2.Visible = true;
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
                bool bValCheck = true;

                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetOrgInfo(strOrgCd);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.txbTeam.Text = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString();

                            this.updPanel1.Update();
                        }
                        else
                        {
                            bValCheck = false;
                        }
                    }
                    else
                    {
                        bValCheck = false;
                    }

                    if (!bValCheck)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void BtnRegedit_Click(object sender, EventArgs e)
        {
            try
            {
                string strMsg = "";

                if (ValidationCheck(ref strMsg))
                {
                    string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
                    string strEmpNo = Request.Form[txbEmpNo.UniqueID];
                    string strKtsEmpNo = "";
                    string strCompCd = "";
                    string strCompNm = "";
                    string strEmpNm = "";
                    string strTelNo = "";
                    string strAuth = "";
                    string strIdmsEmpNo = "";
                    string strPreIdmsEmpNo = "";

                    string strKTSYN = ddlKtsYn.SelectedValue;

                    if (strKTSYN.Equals("Y"))
                    {
                        strKtsEmpNo = Request.Form[txbKtsEmpno.UniqueID];
                        strEmpNm = Request.Form[txbKtsEmpNm.UniqueID];
                        strTelNo = Request.Form[txbKtsTelNo.UniqueID];

                        strIdmsEmpNo = Request.Form[txbIdmsEmpNo.UniqueID];
                        strPreIdmsEmpNo = Request.Form[txbPreIdmsEmpNo.UniqueID];

                        strAuth = ddlAuth.SelectedValue;
                    }
                    else
                    {
                        strEmpNm = Request.Form[txbEtcEmpNm.UniqueID];
                        strTelNo = Request.Form[txbEtcTelNo.UniqueID];

                        strCompCd = Request.Form[txbEtcCompCd.UniqueID];
                        strCompNm = Request.Form[txbEtcCompNm.UniqueID];

                        strAuth = ddlAuth.SelectedValue;
                    }

                    using (UserMgmt userReg = new UserMgmt())
                    {
                        userReg.InsertUserInfo(strEmpNo, strKtsEmpNo, strEmpNm, strTelNo, strIdmsEmpNo, strPreIdmsEmpNo, strOrgCd, strKTSYN, strCompCd, strCompNm, strAuth);
                        
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOK", "alert('사용자 계정등록이 완료되었습니다.'); window.close();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}