﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MenuAuthInfo.aspx.cs" Inherits="KTSSolutionWeb.MenuAuthInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function btnSaveCheck() {
            if (confirm("권한 정보를 저장하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDelCheck() {
            if (confirm("선택한 권한에 사용자가 존재 시 삭제가 불가합니다. 선택한 권한을 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->

    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S: page-menu-management -->
        <div class="page-menu-management">
            <div class="list-top">
                <strong>메뉴 관리</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <asp:Button ID="btnSelect" runat="server" class="btn-green" OnClick="btnSelect_ServerClick" Text="초기화" />
                        <asp:Button ID="btnAdd" runat="server" class="btn-green" OnClick="btnAdd_ServerClick" Text="추가" />
                        <asp:Button ID="btnSave" runat="server" class="btn-green" OnClientClick="return btnSaveCheck();" OnClick="btnSave_ServerClick" Text="저장" />
                        <asp:Button ID="btnDel" runat="server" class="btn-green" OnClientClick="return btnDelCheck();" OnClick="btnDel_Click" Text="권한삭제" />
                    </div>
                </div>
            </div>
            <div class="datalist">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width:230px;float:left;position:relative">
                            <table>
                                <thead>
                                    <tr style="height:53px;">
                                        <td style="width:230px;"></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="tdMenu" runat="server" style="width:230px"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <div style="overflow-x:auto;width:1580px;margin-left:230px;">
                            <table>
                                <thead>
                                    <tr>
                                        <asp:Repeater ID="rptHead" runat="server">
                                            <ItemTemplate>
                                                <td style="width:160px;text-align:center;padding-left:3px;padding-right:3px;">
                                                    <asp:Label ID="lblAuthId" runat="server" Visible="false" Text='<%# Eval("AUTHID") %>'></asp:Label>
                                                    <div style="width:160px">
                                                        <asp:TextBox ID="txbAuthNm" runat="server" Height="30px" Font-Size="15px" Font-Bold="true" style="width:135px;text-align:center;float:left;" Text='<%# Eval("AUTHNM") %>'></asp:TextBox>
                                                        <input id="chkDel" runat="server" type="checkbox" style="float:left;margin-top:5px;margin-left:5px;width:20px;height:20px;" tooltip="권한그룹삭제여부" />   
                                                    </div>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <asp:Repeater ID="rptBody" runat="server">
                                            <ItemTemplate>
                                                <td style="width:160px;text-align:center;">
                                                    <asp:Label ID="lblAuthId" runat="server" Visible="false" Text='<%# Eval("AUTHID") %>'></asp:Label>
                                                    <table style="border:0;">
                                                        <asp:Repeater ID="rptInTable" runat="server">
                                                            <ItemTemplate>
                                                                <tr style="height:25px">
                                                                    <td style="width:160px;text-align:center">
                                                                        <asp:Label ID="lblMenuId" runat="server" Visible="false"></asp:Label>
                                                                        <asp:RadioButton ID="rdY" runat="server" Text="Y" GroupName="rbYN" style="padding-right:10px;" />
                                                                        <asp:RadioButton ID="rdN" runat="server" Text="N" GroupName="rbYN"/>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" />
                        <asp:AsyncPostBackTrigger ControlID="btnAdd" />
                        <asp:AsyncPostBackTrigger ControlID="btnSave" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <!-- E: page-menu-management -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>