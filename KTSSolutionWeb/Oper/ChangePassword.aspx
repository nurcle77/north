﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="KTSSolutionWeb.ChangePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사용자계정 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">
        function EnterKeyPress(event) {
            event = event || window.event;
            var keyid = (event.which) ? event.which : event.keyCode;
            if (keyid == 13) {
                if (valCheck()) {
                    __doPostBack("<%=btnChgPwd.ClientID%>", "");
                }
            }
            return;
        }

        function valCheck() {
            if ($("#txbEmpNo").val() == "") {
                alert("사원번호를 입력하세요");
                $("#txbEmpNo").focus();
                return false;
            }

            if ($("#txbEmpPwd").val() == "") {
                alert("기존 비밀번호를 입력하세요");
                $("#txbEmpPwd").focus();
                return false;
            }

            if ($("#txbEmpPwdChg").val() == "") {
                alert("변경 비밀번호를 입력하세요");
                $("#txbEmpPwdChg").focus();
                return false;
            }

            if ($("#txbEmpPwdChK").val() == "") {
                alert("비밀번호 확인을 입력하세요");
                $("#txbEmpPwdChK").focus();
                return false;
            }

            if ($("#txbEmpPwd").val() == $("#txbEmpPwdChg").val()) {
                alert("기존 비밀번호와 변경 비밀번호가 동일합니다..");
                $("#txbEmpPwd").val('');
                $("#txbEmpPwdChg").val('');
                $("#txbEmpPwd").focus();
                return false;
            }

            if ($("#txbEmpPwdChg").val() != $("#txbEmpPwdChk").val()) {
                alert("변경 비밀번호와 비밀번호 확인이 일치하지 않습니다.");
                $("#txbEmpPwdChk").val('');
                $("#txbEmpPwdChk").focus();
                return false;
            }

            $("#hdfChk1").val($("#txbEmpNo").val());
            $("#hdfChk2").val($("#txbEmpPwd").val());
            $("#hdfChk3").val($("#txbEmpPwdChg").val());
            $("#hdfChk4").val($("#txbEmpPwdChk").val());

            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>비밀번호 변경</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
				                        <table>
                                            <tbody>
					                            <tr>
						                            <th style="width:120px;">사원번호</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpNo" runat="server" MaxLength="20" TabIndex="1"></asp:TextBox>
                                                        </p>
                                                    </td>
					                            </tr>
					                            <tr>
						                            <th style="width:120px;">기존 비밀번호</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpPwd" runat="server" MaxLength="50" TabIndex="2" TextMode="Password"></asp:TextBox>
                                                        </p>
                                                    </td>
					                            </tr>
					                            <tr>
						                            <th style="width:120px;">변경 비밀번호</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpPwdChg" runat="server" MaxLength="50" TabIndex="3" TextMode="Password"></asp:TextBox>
                                                        </p>
                                                    </td>
					                            </tr>
					                            <tr>
						                            <th style="width:120px;">비밀번호 확인</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpPwdChk" runat="server" MaxLength="50" TabIndex="4" TextMode="Password"></asp:TextBox>
                                                        </p>
                                                    </td>
					                            </tr>
                                            </tbody>
				                        </table>
                                        <asp:HiddenField ID="hdfChk1" runat="server" />
                                        <asp:HiddenField ID="hdfChk2" runat="server" />
                                        <asp:HiddenField ID="hdfChk3" runat="server" />
                                        <asp:HiddenField ID="hdfChk4" runat="server" />
                                    </ContentTemplate>
                                </asp:updatepanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:160px">
			        <div class="btnset">
                        <asp:Button ID="btnChgPwd" runat="server" OnClientClick="return valCheck();" OnClick="BtnChgPwd_Click" class="btn-green" Text="확인" />
			            <button onclick="window.close();" type="button" class="btn-green">닫기</button>
			        </div>
                </div>
            </div>
			<div class="list-top">
                <div style="background-color:#dfdfdf;border:0;text-align:left;font-size:14px;color:#000000;">
                    <div style="padding-top:3px;padding-bottom:3px;padding-left:5px;">* 귀중한 정보가 유출되지 않도록 주기적으로 암호를 변경하여 주시기 바랍니다.</div>
                    <div style="padding-bottom:3px;padding-left:5px;">* 신규자는 기존 비밀번호란에 <span style="color:red;font-weight:bold;">new1234!</span> 를 입력하세요.</div>
                    <div style="padding-bottom:3px;padding-left:5px;">* 비밀번호는 <span style="color:red;font-weight:bold;">숫자+영문+특수문자 조합 8자이상</span> 으로 입력하세요.</div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
