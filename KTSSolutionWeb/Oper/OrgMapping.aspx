﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrgMapping.aspx.cs" Inherits="KTSSolutionWeb.OrgMapping" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function postBackObject1() {
            var o = window.event.srcElement;

            if (o.tagName == "INPUT" || o.tagName == "checkbox") {
                __doPostBack("<%=updPanel1.ClientID %>", "");
            }
        }

        function postBackObject2() {
            var o = window.event.srcElement;

            if (o.tagName == "INPUT" || o.tagName == "checkbox") {
                __doPostBack("<%=updPanel2.ClientID %>", "");
            }
        }

        function btnSaveCheck() {
            if (confirm("조직 정보를 수정하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <!-- S:datalist -->
        <div class="treelist">
            <!-- S:list-top -->
            <div class="list-top">
                <div class="pull-right">
                    <div class="btnset">
                        <asp:Button ID="btnRegedit" runat="server" OnClick="btnRegedit_ServerClick" class="btn-green" Text="저장" />
                        <asp:Button ID="btnReset" runat="server" OnClick="btnReset_ServerClick" class="btn-green" Text="초기화" /> 
                    </div>
                </div>
            </div>
            <!-- E:list-top -->

            <div class="tree-wrapper">
                <!-- S:tree01 -->
                <div class="tree01">
                    <div class="organization-tree">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>변경조직</strong>
                        </div>
                        <!-- //E:list-top -->
                        <div style="overflow-y:auto;height:550px">
                            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:TreeView ID="tvBasicOrg" runat="server" Font-Size="14px" OnTreeNodeCheckChanged="tvBasicOrg_TreeNodeCheckChanged" ExpandImageUrl="/Resource/images/icon_plus_03.png" CollapseImageUrl="/Resource/images/icon_minus_02.png">

                                    </asp:TreeView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnRegedit" EventName="click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNode" EventName="click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <!-- E:tree01 -->
                <!-- S:tree02 -->
                <div class="tree02">
                    <div class="organization-tree">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>변경위치</strong>
                        </div>
                        <!-- //E:list-top -->
                        <div style="overflow-y:auto;height:550px">
                            <asp:UpdatePanel ID="updPanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:TreeView ID="tvTargetOrg" runat="server" Font-Size="14px" OnTreeNodeCheckChanged="tvTargetOrg_TreeNodeCheckChanged" ExpandImageUrl="/Resource/images/icon_plus_03.png" CollapseImageUrl="/Resource/images/icon_minus_02.png">

                                    </asp:TreeView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnRegedit" EventName="click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNode" EventName="click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <!-- E:tree02 -->
                <div class="button-wrapper">
                    <asp:Button id="btnAddNode" runat="server" OnClick="btnAddNode_ServerClick" class="move-btn btn-gray"/>
                </div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>