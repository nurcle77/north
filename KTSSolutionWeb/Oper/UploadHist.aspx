﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadHist.aspx.cs" Inherits="KTSSolutionWeb.UploadHist" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function OnlyNumber(event) {
            event = event || window.event;
            var keyid = (event.which) ? event.which : event.keyCode;
            if ((keyid < 48) || (keyid > 57)) {
                return false;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>                
			    <span class="inpbox">
				    <label>신청일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" MaxLength="10" TabIndex="1" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" ></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" MaxLength="10" TabIndex="2" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" ></asp:TextBox>
			    </span>
                <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>업로드 이력 조회 결과</strong>
		    </div>
		    <!-- S:scrollbox -->
		    <div class="scrollbox">            
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>명칭</th>
                                    <th>일자</th>
                                    <th>결과</th>
                                    <th>비고</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("UPLOADNM") %></td>
                                            <td><%# Eval("UPLOADDT") %></td>
                                            <td><%# Eval("RESULT") %></td>
                                            <td><%# Eval("MESSAGE") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel> 
            </div>
		    <!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>