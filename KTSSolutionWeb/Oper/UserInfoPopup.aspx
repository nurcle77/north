﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInfoPopup.aspx.cs" Inherits="KTSSolutionWeb.UserInfoPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사용자정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>사용자 정보</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
				                        <table>
                                            <tbody>
					                            <tr>
						                            <th style="width:120px;">사용자명</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:Label ID="lblEmpNm" runat="server" style="display:inline-block;margin-right:8px;vertical-align:middle;color:#000;font-size:15px;"></asp:Label>
                                                        </p>
                                                    </td>
						                            <th style="width:120px;">구분</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:Label ID="lblKtsYn" runat="server" style="display:inline-block;margin-right:8px;vertical-align:middle;color:#000;font-size:15px;"></asp:Label>
                                                        </p>
                                                    </td>
					                            </tr>
					                            <tr>
						                            <th style="width:120px;">소속</th>
                                                    <td colspan="3">
                                                        <p class="inpbox">
                                                            <asp:Label ID="lblOrgFullNm" runat="server" style="display:inline-block;margin-right:8px;vertical-align:middle;color:#000;font-size:15px;"></asp:Label>
                                                        </p>
                                                    </td>
					                            </tr>
					                            <tr>
						                            <th style="width:120px;">전화번호</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:Label ID="lblPhone" runat="server" style="display:inline-block;margin-right:8px;vertical-align:middle;color:#000;font-size:15px;"></asp:Label>
                                                        </p>
                                                    </td>
						                            <th style="width:120px;">휴대폰번호</th>
                                                    <td style="width:200px;">
                                                        <p class="inpbox">
                                                            <asp:Label ID="lblMobile" runat="server" style="display:inline-block;margin-right:8px;vertical-align:middle;color:#000;font-size:15px;"></asp:Label>
                                                        </p>
                                                    </td>
					                            </tr>
					                            <tr>
						                            <th style="width:120px;">이메일</th>
                                                    <td colspan="3">
                                                        <p class="inpbox">
                                                            <asp:Label ID="lblEmail" runat="server" style="display:inline-block;margin-right:8px;vertical-align:middle;color:#000;font-size:15px;"></asp:Label>
                                                        </p>
                                                    </td>
					                            </tr>
                                            </tbody>
				                        </table>
                                    </ContentTemplate>
                                </asp:updatepanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            <button onclick="window.close();" type="button" class="btn-green">닫기</button>
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
