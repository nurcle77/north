﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class UserInfoPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //세션 체크
                if (this.Page.Session.Count <= 5)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        GetDataList();
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            try
            {
                lblEmpNm.Text = Page.Session["EMPNM"].ToString();
                lblKtsYn.Text = Page.Session["KTSYN"].ToString() == "Y" ? "KTS" : "협력사";
                lblOrgFullNm.Text = Page.Session["ORGFULLNM"].ToString();
                lblPhone.Text = Page.Session["PHONENUM"].ToString();
                lblMobile.Text = Page.Session["MOBILE"].ToString();
                lblEmail.Text = Page.Session["EMAIL"].ToString();

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}