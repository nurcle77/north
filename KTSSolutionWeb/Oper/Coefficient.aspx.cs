﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class Coefficient : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetDataList();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (CoefficientMgmt coef = new CoefficientMgmt())
                {
                    ds = coef.GetCoefficientList();
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string strCoefficientDt = txbDate.Text.Replace("-", "");
                string strMinValue = txbMinValue.Text;
                string strMaxValue = txbMaxValue.Text;
                string strSumValue = txbSumValue.Text;
                string strDeadLine = txbDeadLine.Text.Replace("-", "");

                if (strCoefficientDt.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('일자를 입력해주세요.');", true);
                }
                else if (strMinValue.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('Min 값을 입력해주세요.');", true);
                }
                else if (strMaxValue.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('Max 값을 입력해주세요.');", true);
                }
                else if (strSumValue.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('조정한도를 입력해주세요.');", true);
                }
                else if (strDeadLine.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('수정마감일자를 입력해주세요.');", true);
                }
                else
                {
                    try
                    {
                        float fMinValue = float.Parse(strMinValue);
                        float fMaxValue = float.Parse(strMaxValue);
                        float fSumValue = float.Parse(strSumValue);
                    }
                    catch
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 값이 입력되었습니다.');", true);
                        return;
                    }

                    using (CoefficientMgmt coef = new CoefficientMgmt())
                    {
                        coef.SetCoefficient(strCoefficientDt, strMinValue, strMaxValue, strSumValue, strDeadLine);
                    }

                    GetDataList();

                    txbMinValue.Text = "";
                    txbMaxValue.Text = "";
                    txbSumValue.Text = "";
                    txbDeadLine.Text = "";

                    updPanelMin.Update();
                    updPanelMax.Update();
                    updPanelSum.Update();
                    updPanel1.Update();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('구역계수가 등록 되었습니다.');InitValue();", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('구역계수가 등록 중 오류가 발생했습니다.');", true);
            }
        }
    }
}