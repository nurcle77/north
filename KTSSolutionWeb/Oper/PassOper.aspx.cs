﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.Pass;

namespace KTSSolutionWeb
{
    public partial class PassOper : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private DataTable DtAuthList
        {
            get
            {
                if (ViewState["DtAuthList"] != null)
                    return (DataTable)ViewState["DtAuthList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtAuthList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                this.txbOperator.Attributes.Add("onclick", "PopupPassOper();");
                this.txtAuthOrgNm.Attributes.Add("onclick", "PopupPassOrgTree('', '', '', 'S');");

                this.hfOrgCd.Value = "000001";
                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('');");

                updPanelSearch.Update();

                SetAuthGroupList();

                SetOrgList();

            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetAuthGroupList()
        {
            DataSet ds = new DataSet();
            try
            {
                using (UserMgmt user = new UserMgmt())
                {
                    ds = user.GetAuthGroup();
                }

                if (ds.Tables.Count > 0)
                {
                    DtAuthList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hfOrgCd.Value.ToString();
                string strPassType = this.ddlPassTypeS.SelectedValue;
                string strOperNm = this.txbOperNm.Text;

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetPassAuthList(strOrgCd, strPassType, strOperNm);
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        private bool ValidationCheck(string strPassType, string strOperCd, string strAuthOrgCd, ref string strMsg)
        {
            bool bCheck = true;

            if (strPassType.Length == 0 || strOperCd.Length == 0)
            {
                strMsg = "잘못된 접근입니다.";
                bCheck = false;            
            }
            else if (strAuthOrgCd.Length == 0)
            {
                strMsg = "권한조직을 선택해 주세요.";
                bCheck = false;
            }

            return bCheck;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblPassType = (Label)rptResult.Items[i].FindControl("lblPassType");
                            Label lblOperCd = (Label)rptResult.Items[i].FindControl("lblOperCd");
                            Label lblAuthOrgCd = (Label)rptResult.Items[i].FindControl("lblAuthOrgCd");
                            HtmlButton btnOrg = (HtmlButton)rptResult.Items[i].FindControl("btnOrg");
                            Label lblUpperYn = (Label)rptResult.Items[i].FindControl("lblUpperYn");

                            string strPassType = lblPassType.Text;
                            string strOperator = lblOperCd.Text;
                            string strOrgCd = lblAuthOrgCd.Text;

                            btnOrg.Attributes.Add("onclick", "javascript: PopupPassOrgTree('" + strOrgCd + "', '" + strPassType + "', '" + strOperator + "', 'M');");

                            string strUpperYn = lblUpperYn.Text;

                            Button btnSave = (Button)rptResult.Items[i].FindControl("btnSave");
                            Button btnDelete = (Button)rptResult.Items[i].FindControl("btnDelete");                             

                            AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();
                            asyncBtnSave.ControlID = btnSave.UniqueID;
                            asyncBtnSave.EventName = "Click";

                            AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                            asyncBtnDelete.ControlID = btnDelete.UniqueID;
                            asyncBtnDelete.EventName = "Click";

                            updPanel1.Triggers.Add(asyncBtnSave);
                            updPanel1.Triggers.Add(asyncBtnDelete);

                            updPanel1.Update();
                        }
                    }
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strOrgCd = this.hfOrgCd.Value.Split(',');
                string strOrgNm = "";
                string strWhere = "";

                for (int i = 0; i < strOrgCd.Length; i++)
                {
                    if (strWhere.Length > 0)
                        strWhere += " OR ";

                    strWhere += string.Format("ORGCD = '{0}'", strOrgCd[i]);
                }

                DataRow[] dr = DtOrgList.Select(strWhere);
                DataTable dt = DtOrgList.Clone();

                if (dr.Length > 0)
                {
                    dt = dr.CopyToDataTable();
                }

                if(dt.Rows.Count == 1)
                {
                    strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                }
                else
                { 
                    foreach (DataRow drOrg in dt.Rows)
                    {
                        if (strOrgNm.Length > 0)
                            strOrgNm += ", ";

                        strOrgNm += drOrg["ORGNM"].ToString();
                    }
                }

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfAuthOrg_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strAuthOrg = hfAuthOrg.Value.Split('|');

                string strPassType = "";
                string strOperator = "";
                string strOrgCd = "";

                if (strAuthOrg.Length > 2)
                {
                    strPassType = strAuthOrg[0];
                    strOperator = strAuthOrg[1];
                    strOrgCd = strAuthOrg[2];
                }

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    Label lblPassType = (Label)rptResult.Items[i].FindControl("lblPassType");
                    Label lblOperCd = (Label)rptResult.Items[i].FindControl("lblOperCd");

                    if (lblPassType.Text.Equals(strPassType) && lblOperCd.Text.Equals(strOperator))
                    {
                        Label lblAuthOrgCd = (Label)rptResult.Items[i].FindControl("lblAuthOrgCd");
                        Label lblAuthOrgNm = (Label)rptResult.Items[i].FindControl("lblAuthOrgNm");
                        HtmlButton btnOrg = (HtmlButton)rptResult.Items[i].FindControl("btnOrg");


                        string[] strOrgList = strOrgCd.Split(',');

                        string strWhere = "";
                        string strOrgNm = "";

                        for (int j = 0; j < strOrgList.Length; j++)
                        {
                            if (strWhere.Length > 0)
                                strWhere += " OR ";

                            strWhere += string.Format("ORGCD = '{0}'", strOrgList[j]);
                        }

                        DataRow[] dr = DtOrgList.Select(strWhere);
                        DataTable dt = DtOrgList.Clone();

                        if (dr.Length > 0)
                        {
                            dt = dr.CopyToDataTable();
                        }

                        if (dt.Rows.Count == 1)
                        {
                            strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                        }
                        else
                        {
                            foreach (DataRow drOrg in dt.Rows)
                            {
                                if (strOrgNm.Length > 0)
                                    strOrgNm += ", ";

                                strOrgNm += drOrg["ORGNM"].ToString();
                            }
                        }

                        lblAuthOrgCd.Text = strOrgCd;
                        lblAuthOrgNm.Text = strOrgNm;

                        hfAuthOrg.Value = "";
                        btnOrg.Attributes.Remove("onclick");
                        btnOrg.Attributes.Add("onclick", "javascript: PopupPassOrgTree('" + strOrgCd + "', '" + strPassType + "', '" + strOperator + "', 'M');");

                        break;
                    }
                }
                
                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfOperator_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txbOperator.Text = Request.Form[txbOperator.UniqueID].ToString();

                hfOperator.Value = Request.Form[hfOperator.UniqueID].ToString();

                updPanelOperator.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfAuthOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strOrgVal = this.hfAuthOrgCd.Value.Split('|');
                string strOrgNm = "";

                if (strOrgVal.Length > 2)
                {
                    hfAuthOrgCd.Value = strOrgVal[2];
                    string[] strOrgCd = strOrgVal[2].Split(',');
                    string strWhere = "";

                    for (int i = 0; i < strOrgCd.Length; i++)
                    {
                        if (strWhere.Length > 0)
                            strWhere += " OR ";

                        strWhere += string.Format("ORGCD = '{0}'", strOrgCd[i]);
                    }

                    DataRow[] dr = DtOrgList.Select(strWhere);
                    DataTable dt = DtOrgList.Clone();

                    if (dr.Length > 0)
                    {
                        dt = dr.CopyToDataTable();
                    }

                    if (dt.Rows.Count == 1)
                    {
                        strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                    }
                    else
                    {
                        foreach (DataRow drOrg in dt.Rows)
                        {
                            if (strOrgNm.Length > 0)
                                strOrgNm += ", ";

                            strOrgNm += drOrg["ORGNM"].ToString();
                        }
                    }
                }
                txtAuthOrgNm.Text = strOrgNm;

                updPanelAuthOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlPassTypeR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txbOperator.Text = "";
                hfOperator.Value = "";

                txtAuthOrgNm.Text = "";
                hfAuthOrgCd.Value = "";

                string strPassType = this.ddlPassTypeR.SelectedValue;

                updPanelOperator.Update();
                updPanelAuthOrg.Update();
                updPanelUpperYnR.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupClosing", "PopupClosing();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblPassType = (Label)btnSave.Parent.FindControl("lblPassType");
                Label lblOperCd = (Label)btnSave.Parent.FindControl("lblOperCd");
                Label lblAuthOrgCd = (Label)btnSave.Parent.FindControl("lblAuthOrgCd");
                Label lblUpperYn = (Label)btnSave.Parent.FindControl("lblUpperYn");

                string strPassType = lblPassType.Text;
                string strOperCd = lblOperCd.Text;
                string strAuthOrgCd = lblAuthOrgCd.Text;
                string strUpperYn = lblUpperYn.Text;
                string strMsg = "";

                if (!ValidationCheck(strPassType, strOperCd, strAuthOrgCd, ref strMsg))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strMsg + "')", true);
                }
                else
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.SetPassAuthInfo(strPassType, strOperCd, strAuthOrgCd, strUpperYn);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('PASS 권한 정보가 수정되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('PASS 권한 정보 저장 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblPassType = (Label)btnSave.Parent.FindControl("lblPassType");
                Label lblOperCd = (Label)btnSave.Parent.FindControl("lblOperCd");

                string strPassType = lblPassType.Text;
                string strOperCd = lblOperCd.Text;

                if (strPassType.Length == 0 || strOperCd.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.DelPassAuthInfo(strPassType, strOperCd);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('PASS 권한 정보가 삭제되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('PASS 권한 정보 삭제 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnPassAuthReg_Click(object sender, EventArgs e)
        {
            try
            {
                string strPassType = ddlPassTypeR.SelectedValue;
                string strOperCd = hfOperator.Value;
                string strAuthOrgCd = hfAuthOrgCd.Value;
                string strUpperYn = chkUpperYnR.Checked ? "Y" : "N";

                if (strPassType.Length == 0 || strOperCd.Length == 0 || strAuthOrgCd.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.SetPassAuthInfo(strPassType, strOperCd, strAuthOrgCd, strUpperYn);
                    }

                    ddlPassTypeR.SelectedIndex = 0;

                    txbOperator.Text = "";
                    hfOperator.Value = "";

                    txtAuthOrgNm.Text = "";
                    hfAuthOrgCd.Value = "";

                    this.chkUpperYnR.Checked = false;

                    updPanelOperator.Update();
                    updPanelAuthOrg.Update();
                    updPanelUpperYnR.Update();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('PASS 권한 정보가 등록되었습니다.');", true);
                }

                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('PASS 권한 정보 등록 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('조회 중 오류가 발생했습니다.');", true);
            }
        }
    }
}