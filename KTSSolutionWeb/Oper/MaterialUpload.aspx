﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaterialUpload.aspx.cs" Inherits="KTSSolutionWeb.MaterialUpload" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-upload-management -->
        <div class="page-upload-management">
            <div class="list-top">
                <strong>자재정보</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" type="button" title="최종 등록 완료" class="submit-btn btn-green">최종 등록 완료</button>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="upload-list">
                        <ul>
                            <li>
					            <strong>자재마스터</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuMaster"/>
										<span class="file">
											<input type="file" id="fuMaster" name="fuMaster" onchange="document.getElementById('txtfuMaster').value=this.value;" /> 
											<label for="fu">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
									</p>
								</div>
							</li>
                            <li>
					            <strong>ktcommerce</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuCommerce"/>
										<span class="file">
											<input type="file" id="fuCommerce" name="fuCommerce" onchange="document.getElementById('txtfuCommerce').value=this.value;"/> 
											<label for="fuEvent">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
									</p>
								</div>
							</li>
                            <li>
					            <strong>단말마스터</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							            <input type="text" id="txtfuTerminal"/>
							            <span class="file">
								            <input type="file" id="fuTerminal" name="fuTerminal" onchange="document.getElementById('txtfuTerminal').value=this.value;" /> 
								            <label for="fu">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							            </span>
                                    </p>
                                </div>
                            </li>
						</ul>
					</div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </asp:UpdatePanel>
		</div>
		<!-- E:page-upload-management -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
