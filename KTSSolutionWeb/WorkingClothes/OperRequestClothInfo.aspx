﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OperRequestClothInfo.aspx.cs" Inherits="KTSSolutionWeb.OperRequestClothInfo"%>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function RegRequestClothPopup(strYears, strPointSeq, strEmpNo, strReqType) {
            var nWidth = 1000;
            var nHeight = 900;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                YEARS: strYears,
                POINTSEQ: strPointSeq,
                EMPNO: strEmpNo,
                REQTYPE: strReqType,
                SUBREQ:"Y"
            };

            var Popupform = createForm("/WorkingClothes/RegRequestCloth", param);

            Popupform.target = "RegRequestCloth";
            var win = window.open("", "RegRequestCloth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgTree(orgcd, empno) {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            if (years == "" || pointseq == "") {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: sumupmonth,
                    pType: "cloth"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SearchChk() {
            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();

            if (years == "" || pointseq == "") {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else if (OrgCd == "") {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function btnPermissionCheck() {
            if (confirm("체크한 추가신청을 승인하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnModifyCheck() {
            if (confirm("수정하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ReSearch() {
            __doPostBack("<%= btnSelect.UniqueID %>", "OnClick");
        }

        function SetOrgCd(orgcd, valtype) {

            this.focus();

            document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelSearch)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" name="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" name="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>       
                <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox" style="margin-right:20px;">
                            <label>연도</label>
                            <asp:DropDownList ID="ddlYears" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
			            </span> 
                        <span class="optionbox" style="margin-right:20px;">
                            <label>지급기준</label>
                            <asp:DropDownList ID="ddlPointSeq" runat="server" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="ddlPointSeq_SelectedIndexChanged">
                                <asp:ListItem Text="선택하세요" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
			            </span> 
				        <span class="inpbox">
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
					        <button id="btnplus" name="btnplus" type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
                            <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
				        </span>
                        <asp:HiddenField ID="hdfMonth" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPointSeq" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
                <br />
                <span class="inpbox" style="padding-bottom:10px;">
                    <label>이름</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" Width="150px" MaxLength="20" TabIndex="1"></asp:TextBox>
			    </span>
                <span class="inpbox" style="padding-bottom:10px;">
                    <label>사번</label>
                    <asp:TextBox ID="txbEmpNo" runat="server" Width="150px" MaxLength="20" TabIndex="2" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" ></asp:TextBox>
			    </span>
                <span class="optionbox" style="padding-bottom:10px;">
                    <label>상태</label>
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="150px">
                        <asp:ListItem Text="전체" Value=""></asp:ListItem>
                        <asp:ListItem Text="신규" Value="N"></asp:ListItem>
                        <asp:ListItem Text="기본" Value="B"></asp:ListItem>
                        <asp:ListItem Text="추가" Value="A"></asp:ListItem>
                        <asp:ListItem Text="미신청" Value="D"></asp:ListItem>
                    </asp:DropDownList>
			    </span>
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>피복신청 승인관리 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnPermission" runat="server" class="btn-green" OnClientClick="return btnPermissionCheck();" onClick="btnPermission_ServerClick" Text="추가신청승인" />
			            <asp:Button id="btnExcel" runat="server" class="btn-green" style="float:right;" OnClientClick="return SearchChk();" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>소속</th>
                                    <th>이름</th>
                                    <th>사번</th>
                                    <th>KTS사번</th>
                                    <th>직무</th>
                                    <th>지급기준</th>
                                    <th>신청구분</th>
                                    <th>신청상태</th>
                                    <th>작업복구분</th>
                                    <th>종류</th>
                                    <th>사이즈</th>
                                    <th>수량</th>
                                    <th>신청일자</th>
                                    <th>사용포인트</th>
                                    <th id="th" runat="server">대리신청</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <input id="cbNum" runat="server" type="checkbox" style="width:20px;height:20px;" />
                                                <asp:Label ID="lblNum" runat="server" Text='<%# Eval("NUM") %>'></asp:Label>   
                                            </td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td>
                                                <asp:Label ID="lblRequestID" runat="server" Visible="false" Text='<%# Eval("REQUESTID") %>'></asp:Label>
                                                <asp:Label ID="lblYears" runat="server" Visible="false" Text='<%# Eval("YEARS") %>'></asp:Label>
                                                <asp:Label ID="lblPointSeq" runat="server" Visible="false" Text='<%# Eval("POINTSEQ") %>'></asp:Label>
                                                <asp:Label ID="lblDetailSeq" runat="server" Visible="false" Text='<%# Eval("DETAILSEQ") %>'></asp:Label>
                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("JOBNM2") %></td>
                                            <td><%# Eval("TITLE") %></td>
                                            <td><%# Eval("PROVIDETYPE") %></td>
                                            <td><%# Eval("REQSTATUSNM") %>
                                                <asp:Label ID="lblReqStatus" runat="server" Visible="false" Text='<%# Eval("REQSTATUS") %>'></asp:Label>
                                                <asp:Label ID="lblReqType" runat="server" Visible="false" Text='<%# Eval("REQTYPE") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("CLOTHGBN") %></td>
                                            <td>
                                                <asp:Label ID="lblClothType" runat="server" Text='<%# Eval("CLOTHTYPE") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSize" runat="server" Visible="true" Text='<%# Eval("CLOTHSIZE") %>'></asp:Label>
                                                <span class="txtbox">
                                                    <asp:TextBox ID="txbSize" runat="server" Width="80px" Visible="false" ReadOnly="true" Text='<%# Eval("CLOTHSIZE") %>'></asp:TextBox>
                                                </span>
                                                <span class="optionbox">
                                                    <asp:DropDownList ID="ddlSize1" runat="server" Width="80px" Visible="false"></asp:DropDownList>
                                                    <asp:DropDownList ID="ddlSize2" runat="server" Width="115px" Visible="false"></asp:DropDownList>
                                                </span>
                                            </td>
                                            <td><%# Eval("REQCNT") %></td>
                                            <td><%# Eval("REGDT") %></td>
                                            <td><%# Eval("USEPOINT") %></td>
                                            <td> 
                                                <asp:Label ID="lblBtn" runat="server" Visible="false" Text='<%# Eval("BTNCHK") %>'></asp:Label>
                                                <button id="btnDeputy" runat="server" visible="false" class="btn-black">대리신청</button>
                                                <asp:Button id="btnModify" runat="server" OnClientClick="return btnModifyCheck();" OnClick="btnModify_Click" visible="false" class="btn-black" BorderStyle="None" Text="수정" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="btnPermission" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
