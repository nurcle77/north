﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class OperStandardClothStats : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetStandardInfo(string strYears, string strPointSeq)
        {
            DataSet ds = new DataSet();
            try
            {
                paging1.bPreRender = true;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetStandardInfo(strYears, strPointSeq);
                }

                paging1.PageNumber = 0;
                paging1.PageSize = 30;

                paging1.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging1.TotalRows = ds.Tables[0].Rows.Count;
                    paging1.Dt = ds.Tables[0];
                }

                paging1.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetRequestStatsList(string strYears, string strPointSeq)
        {
            DataSet ds = new DataSet();
            try
            {
                paging2.bPreRender = true;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetRequestStatsList(strYears, strPointSeq);
                }

                paging2.PageNumber = 0;
                paging2.PageSize = 30;

                paging2.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging2.TotalRows = ds.Tables[0].Rows.Count;
                    paging2.Dt = ds.Tables[0];
                }

                paging2.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[8];

            ArrHeader[0] = "JOBNM";
            ArrHeader[1] = "CLOTHGBN";
            ArrHeader[2] = "CLOTHTYPE";
            ArrHeader[3] = "PROVIDECNT";
            ArrHeader[4] = "COST";
            ArrHeader[5] = "EMPUSERCNT";
            ArrHeader[6] = "BUYCNT";
            ArrHeader[7] = "TOTALCOST";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[8];

            ArrHeader[0] = "직무";
            ArrHeader[1] = "구분";
            ArrHeader[2] = "종류";
            ArrHeader[3] = "수량";
            ArrHeader[4] = "단가";
            ArrHeader[5] = "인원";
            ArrHeader[6] = "구매수량";
            ArrHeader[7] = "금액";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void paging1_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging1.bPreRender)
                {
                    if (paging1.Dt != null)
                    {
                        paging1.RepeaterDataBind(rptResult1);
                        updPanel1.Update();
                    }

                    paging1.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void paging2_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging2.bPreRender)
                {
                    if (paging2.Dt != null)
                    {
                        paging2.RepeaterDataBind(rptResult2);
                        updPanel2.Update();
                    }
                    paging2.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {

                string strYears = this.ddlYears.SelectedValue;
                string strPointSeq = this.ddlPointSeq.SelectedValue;

                if (strYears.Length == 0 || strPointSeq.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    GetStandardInfo(strYears, strPointSeq);
                    GetRequestStatsList(strYears, strPointSeq);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel1_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging1.Dt != null)
                    dt = paging1.Dt.Copy();
                else
                {

                    string strYears = this.ddlYears.SelectedValue;
                    string strPointSeq = this.ddlPointSeq.SelectedValue;

                    if (strYears.Length == 0 || strPointSeq.Length == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                    }
                    else
                    {
                        using (PointMgmt point = new PointMgmt())
                        {
                            ds = point.GetStandardInfo(strYears, strPointSeq);
                        }

                        dt = ds.Tables[0];
                    }
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "피복지급내역통계-기준지급");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void btnExcel2_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging2.Dt != null)
                    dt = paging2.Dt.Copy();
                else
                {

                    string strYears = this.ddlYears.SelectedValue;
                    string strPointSeq = this.ddlPointSeq.SelectedValue;

                    if (strYears.Length == 0 || strPointSeq.Length == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                    }
                    else
                    {
                        using (PointMgmt point = new PointMgmt())
                        {
                            ds = point.GetRequestStatsList(strYears, strPointSeq);
                        }

                        dt = ds.Tables[0];
                    }
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "피복지급내역통계-연도지급기준 결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                ddlPointSeq.Items.Clear();

                string strYears = ddlYears.SelectedValue;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointInfo(strYears);
                }

                ddlPointSeq.Items.Add(new ListItem("선택하세요", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlPointSeq.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TITLE"].ToString(), ds.Tables[0].Rows[i]["POINTSEQ"].ToString()));
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
    }
}