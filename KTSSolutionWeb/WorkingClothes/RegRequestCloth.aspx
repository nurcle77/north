﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegRequestCloth.aspx.cs" Inherits="KTSSolutionWeb.RegRequestCloth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>피복신청</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />

    <script type="text/javascript">
        function RegChk() {
            if (confirm("신청(수정)하시겠습니까?")) {
                return true;
            } else {
                return false;
            }
        }

        function DelChk() {
            if (confirm("삭제 하시겠습니까?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-organization -->
            <div class="windowpop pop-clothes-request">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>피복 신청</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>

                    <!-- S:popcontents -->
                    <div class="popcontents" style="margin-bottom:20px;max-height:740px;overflow-y:auto;">

                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
                                        <table>
                                            <colgroup>
                                                <col style="width:10%"/>
                                                <col style="width:auto"/>
                                                <col style="width:10%"/>
                                                <col style="width:auto"/>
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th>소속</th>
                                                    <td colspan="3">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbOrgFullNm" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>제목</th>
                                                    <td colspan="3">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbTitle" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trPoint" runat="server" visible="false">
                                                    <th>총 포인트</th>
                                                    <td>
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbTotPoint" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th>사용</th>
                                                    <td>
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbUsePoint" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thRestPoint" runat="server" visible="false">잔여</th>
                                                    <td id="tdRestPoint" runat="server" visible="false">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbRestPoint" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th id="thSub" runat="server"></th>
                                                    <td id="tdSub" runat="server">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbSubstitute" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>신청자사번</th>
                                                    <td>
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpNo" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th>신청자</th>
                                                    <td>
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpNm" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>	
                                        </table>
                                    </ContentTemplate>
                                </asp:updatepanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:list-top -->
                            <div class="list-top">
                                <strong id="ReqTitle" runat="server" style="margin-top:7px;margin-right:10px;float:left">추가 신청</strong>
					            <asp:Button id="btnAddList" runat="server" onClick="btnAddList_ServerClick" type="button" class="btn-plus" Text="+" />
                            </div>
                            <!-- //E:list-top -->
                            <asp:updatepanel id="updPanel2" runat="server" UpdateMode="Conditional" >
                                <ContentTemplate>
                                    <!-- S:scrollbox -->
                                    <div class="scrollbox">
                                        <table class="tablelist">
                                            <tbody>
                                                <asp:Repeater ID="rptResult1" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <th>동/하복</th>
                                                            <td>
                                                                <asp:Label ID="lblRequestID" runat="server" Visible="false" Text='<%# Eval("REQUESTID") %>'></asp:Label>
                                                                <p class="inpbox">
                                                                    <asp:TextBox ID="txbClothGbn" runat="server" Width="80px" ReadOnly="true" Text='<%# Eval("CLOTHGBN") %>'></asp:TextBox>
                                                                </p>
                                                            </td>
                                                            <th>종류</th>
                                                            <td>
                                                                <asp:Label ID="lblDetailSeq" runat="server" Visible="false" Text='<%# Eval("DETAILSEQ") %>'></asp:Label>
                                                                <p class="optionbox">
                                                                    <asp:TextBox ID="txbClothType" runat="server" Width="80px" ReadOnly="true" Text='<%# Eval("CLOTHTYPE") %>'></asp:TextBox>
                                                                    <asp:DropDownList ID="ddlAddClothType" Width="80px" runat="server" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlAddClothType_SelectedIndexChanged"></asp:DropDownList>
                                                                </p>
                                                            </td>
                                                            <th>사이즈</th>
                                                            <td>
                                                                <p class="optionbox">
                                                                    <asp:TextBox ID="txbSize" runat="server" Width="80px" Visible="false" ReadOnly="true" Text='<%# Eval("CLOTHSIZE") %>'></asp:TextBox>
                                                                    <asp:DropDownList ID="ddlSize1" Width="80px" runat="server"></asp:DropDownList>
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p class="optionbox">
                                                                    <asp:DropDownList ID="ddlSize2" Width="115px" runat="server" Visible="false" ></asp:DropDownList>
                                                                </p>
                                                            </td>
                                                            <th>수량</th>
                                                            <td>
                                                                <p class="inpbox">
                                                                    <asp:TextBox ID="txbProvideCnt" runat="server" Width="60px" ReadOnly="true" Text='<%# Eval("REQCNT") %>'></asp:TextBox>
                                                                    <asp:TextBox ID="txbReqCnt" AutoPostBack="true" Width="60px" MaxLength="3" runat="server" Text='<%# Eval("REQCNT") %>' Visible="false" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" OnTextChanged="txbReqCnt_TextChanged"></asp:TextBox>
                                                                </p>
                                                            </td>
                                                            <th id="th" runat="server">포인트</th>
                                                            <td id="td" runat="server">
                                                                <p class="inpbox">
                                                                    <asp:TextBox ID="txbPoint" runat="server" Width="80px" ReadOnly="true" Text='<%# Eval("USEPOINT") %>'></asp:TextBox>
                                                                </p>
                                                            </td>
                                                            <th colspan="2"></th>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                <!-- //E:scrollbox -->
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddList" EventName="Click"/>
                                    <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click"/>
                                    <asp:AsyncPostBackTrigger ControlID="btnDel" EventName="Click"/>
                                </Triggers>
                            </asp:updatepanel>
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
			<div class="list-top">
				<div style="text-align:center;">
                    <asp:Button id="btnReg" runat="server" OnClientClick="return RegChk();" OnClick="btnReg_ServerClick" class="btn-green" Text="신청" />
                    <asp:Button id="btnDel" runat="server" OnClientClick="return DelChk();" OnClick="btnDel_ServerClick" class="btn-green" Text="삭제" />
                </div>
            </div>
        </div> 
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
