﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.WorkingClothes;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class OperRequestClothInfo : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }
        private DataTable DtPointSeq
        {
            get
            {
                if (ViewState["DtPointSeq"] != null)
                    return (DataTable)ViewState["DtPointSeq"];
                else
                    return null;
            }
            set
            {
                ViewState["DtPointSeq"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();

                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");

                updPanelSearch.Update();

                //GetDataList();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    DateTime chkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime month = new DateTime(int.Parse(strMonth.Substring(0, 4)), int.Parse(strMonth.Substring(4, 2)), 1);

                    if (chkDate > month)
                    {
                        ds = org.GetOperOrgMonthList("", "N", strMonth);
                    }
                    else
                    {
                        ds = org.GetOperOrgList("", "N");
                    }
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hdfOrgCd.Value;
                string strEmpNo = this.txbEmpNo.Text;
                string strEmpNm = this.txbEmpNm.Text; ;
                string strReqType = ddlStatus.SelectedValue;
                string strYears = ddlYears.SelectedValue;
                string strPointSeq = ddlPointSeq.SelectedValue;

                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetRequestOperClothesList(strOrgCd, strEmpNo, strEmpNm, strYears, strPointSeq, strReqType);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                updPanelSearch.Update();
                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetDDLSize(string strClothType, TextBox txbSize, DropDownList ddlSize1, DropDownList ddlSize2)
        {
            try
            {
                if (strClothType.Equals("상의") || strClothType.Equals("점퍼") || strClothType.Equals("조끼")
                    || strClothType.Equals("티셔츠") || strClothType.ToUpper().Equals("긴팔T") || strClothType.ToUpper().Equals("반팔T")
                    || strClothType.Equals("외피") || strClothType.Equals("내피"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("선택", ""));

                    ddlSize1.Items.Add(new ListItem("44", "44"));
                    ddlSize1.Items.Add(new ListItem("55", "55"));
                    ddlSize1.Items.Add(new ListItem("66", "66"));
                    ddlSize1.Items.Add(new ListItem("77", "77"));

                    for (int i = 90; i <= 125; i += 5)
                    {
                        ddlSize1.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                }
                else if (strClothType.Equals("하의") || strClothType.Contains("바지") || strClothType.Contains("방한바지"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = true;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("선택", ""));

                    for (int i = 24; i <= 46; i++)
                    {
                        ddlSize1.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }

                    ddlSize2.Items.Add(new ListItem("선택", ""));
                    ddlSize2.Items.Add(new ListItem("160cm이하", "SSS"));
                    ddlSize2.Items.Add(new ListItem("160~165cm", "SS"));
                    ddlSize2.Items.Add(new ListItem("165~170cm", "S"));
                    ddlSize2.Items.Add(new ListItem("170~175cm", "M"));
                    ddlSize2.Items.Add(new ListItem("175~180cm", "L"));
                    ddlSize2.Items.Add(new ListItem("180~185cm", "LL"));
                    ddlSize2.Items.Add(new ListItem("185~190cm", "LLL"));
                    ddlSize2.Items.Add(new ListItem("FREE", "FREE"));
                }
                else if (strClothType.Equals("신발"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("선택", ""));

                    for (int i = 230; i <= 300; i += 5)
                    {
                        ddlSize1.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                }
                else if (strClothType.Equals("모자") || strClothType.Equals("버프"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("FREE", "FREE"));
                }
                else if (strClothType.Equals(""))
                {
                    txbSize.Visible = true;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = false;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();
                }
                else
                {
                    txbSize.Visible = true;
                    txbSize.ReadOnly = false;

                    ddlSize1.Visible = false;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[16];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGFULLNM";
            ArrHeader[3] = "EMPNM";
            ArrHeader[4] = "EMPNO";
            ArrHeader[5] = "KTSEMPNO";
            ArrHeader[6] = "JOBNM2";
            ArrHeader[7] = "TITLE";
            ArrHeader[8] = "PROVIDETYPE";
            ArrHeader[9] = "REQSTATUSNM";
            ArrHeader[10] = "CLOTHGBN";
            ArrHeader[11] = "CLOTHTYPE";
            ArrHeader[12] = "CLOTHSIZE";
            ArrHeader[13] = "REQCNT";
            ArrHeader[14] = "REGDT";
            ArrHeader[15] = "USEPOINT";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[16];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "소속코드";
            ArrHeader[2] = "소속";
            ArrHeader[3] = "이름";
            ArrHeader[4] = "사번";
            ArrHeader[5] = "KTS사번";
            ArrHeader[6] = "직무";
            ArrHeader[7] = "지급기준";
            ArrHeader[8] = "신청구분";
            ArrHeader[9] = "신청상태";
            ArrHeader[10] = "작업복구분";
            ArrHeader[11] = "종류";
            ArrHeader[12] = "사이즈";
            ArrHeader[13] = "수량";
            ArrHeader[14] = "신청일자";
            ArrHeader[15] = "사용포인트";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            //HtmlTableRow tr = (HtmlTableRow)rptResult.Items[i].FindControl("tr");
                            Label lblNum = (Label)rptResult.Items[i].FindControl("lblNum");
                            HtmlInputCheckBox cbNum = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbNum");
                            Label lblYears = (Label)rptResult.Items[i].FindControl("lblYears");
                            Label lblPointSeq = (Label)rptResult.Items[i].FindControl("lblPointSeq");
                            Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");
                            Label lblReqType = (Label)rptResult.Items[i].FindControl("lblReqType"); 
                            Label lblReqStatus = (Label)rptResult.Items[i].FindControl("lblReqStatus");
                            Label lblClothType = (Label)rptResult.Items[i].FindControl("lblClothType");
                            Label lblSize = (Label)rptResult.Items[i].FindControl("lblSize");
                            TextBox txbSize = (TextBox)rptResult.Items[i].FindControl("txbSize");
                            DropDownList ddlSize1 = (DropDownList)rptResult.Items[i].FindControl("ddlSize1");
                            DropDownList ddlSize2 = (DropDownList)rptResult.Items[i].FindControl("ddlSize2");

                            Label lblBtn = (Label)rptResult.Items[i].FindControl("lblBtn");
                            HtmlButton btnDeputy = (HtmlButton)rptResult.Items[i].FindControl("btnDeputy");
                            Button btnModify = (Button)rptResult.Items[i].FindControl("btnModify");

                            if (lblReqType.Text.Equals("A") && lblReqStatus.Text.Equals("W"))
                            {
                                lblNum.Visible = false;
                                cbNum.Visible = true;
                            }
                            else
                            {
                                lblNum.Visible = true;
                                cbNum.Visible = false;
                            }

                            if (lblReqStatus.Text.Equals("D") && lblBtn.Text.Equals("D"))
                            {
                                lblSize.Visible = true;
                                txbSize.Visible = false;
                                ddlSize1.Visible = false;
                                ddlSize2.Visible = false;

                                btnDeputy.Visible = true;
                                btnDeputy.Attributes.Add("onclick", "RegRequestClothPopup('" + lblYears.Text + "', '" + lblPointSeq.Text + "', '" + lblEmpNo.Text + "', '" + lblReqType.Text + "')");

                                btnModify.Visible = false;
                                btnModify.Enabled = false;
                            }
                            else if (lblBtn.Text.Equals("M"))
                            {
                                lblSize.Visible = false;
                                string strClothType = lblClothType.Text;

                                SetDDLSize(strClothType, txbSize, ddlSize1, ddlSize2);

                                string[] strSize = lblSize.Text.Split('/');

                                if (strClothType.Equals("상의") || strClothType.Equals("점퍼") || strClothType.Equals("조끼")
                                    || strClothType.Equals("티셔츠") || strClothType.ToUpper().Equals("긴팔T") || strClothType.ToUpper().Equals("반팔T")
                                    || strClothType.Equals("외피") || strClothType.Equals("내피"))
                                {
                                    ddlSize1.SelectedValue = strSize[0];
                                }
                                else if (strClothType.Equals("하의") || strClothType.Contains("바지") || strClothType.Contains("방한바지"))
                                {
                                    if (strSize.Length > 1)
                                    {
                                        ddlSize1.SelectedValue = strSize[0];
                                        ddlSize2.SelectedValue = strSize[1];
                                    }
                                    else
                                    {
                                        ddlSize1.SelectedIndex = 0;
                                        ddlSize2.SelectedIndex = 0;
                                    }
                                }
                                else if (strClothType.Equals("신발"))
                                {
                                    ddlSize1.SelectedValue = strSize[0];
                                }
                                else if (strClothType.Equals("모자") || strClothType.Equals("버프"))
                                {
                                    ddlSize1.SelectedValue = "FREE";
                                }

                                btnDeputy.Visible = false;
                                btnDeputy.Attributes.Remove("onclick");

                                btnModify.Visible = true;
                                btnModify.Enabled = true;

                                AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
                                trigger1.ControlID = btnModify.UniqueID;
                                trigger1.EventName = "Click";
                                updPanel1.Triggers.Add(trigger1);
                            }
                            else
                            {
                                lblSize.Visible = true;
                                txbSize.Visible = false;
                                ddlSize1.Visible = false;
                                ddlSize2.Visible = false;

                                btnDeputy.Visible = false;
                                btnDeputy.Attributes.Remove("onclick");

                                btnModify.Visible = false;
                                btnModify.Enabled = false;
                            }
                        }
                    }
                    
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnPermission_ServerClick(object sender, EventArgs e)
        {
            try
            {
                List<string> lstData = new List<string>();
                bool bSave = true;

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    HtmlInputCheckBox cbNum = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbNum");
                    if (!cbNum.Visible || !cbNum.Checked)
                    {
                        continue;
                    }
                    else
                    {
                        Label lblRequestID = (Label)rptResult.Items[i].FindControl("lblRequestID");
                        Label lblYears = (Label)rptResult.Items[i].FindControl("lblYears");
                        Label lblPointSeq = (Label)rptResult.Items[i].FindControl("lblPointSeq");
                        Label lblDetailSeq = (Label)rptResult.Items[i].FindControl("lblDetailSeq");
                        Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");

                        if (lblYears.Text.Length == 0 || lblPointSeq.Text.Length == 0 || lblDetailSeq.Text.Length == 0 || lblEmpNo.Text.Length == 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                            bSave = false;
                            break;
                        }

                        string strData = lblRequestID.Text + "|" + lblYears.Text + "|" + lblPointSeq.Text + "|" + lblDetailSeq.Text + "|" + lblEmpNo.Text;
                        lstData.Add(strData);
                    }
                }

                if (bSave)
                {
                    using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                    {
                        mgmt.UpdRequestClothPermission(lstData);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "alert('추가신청 승인을 완료했습니다.');", true);
                    }

                    GetDataList();
                    this.updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    string strOrgCd = this.hdfOrgCd.Value;
                    string strEmpNo = this.txbEmpNo.Text;
                    string strEmpNm = this.txbEmpNm.Text; ;
                    string strReqType = ddlStatus.SelectedValue;
                    string strYears = ddlYears.SelectedValue;
                    string strPointSeq = ddlPointSeq.SelectedValue;

                    using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                    {
                        ds = mgmt.GetRequestOperClothesList(strOrgCd, strEmpNo, strEmpNm, strYears, strPointSeq, strReqType);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "피복신청 승인관리");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {

                string strYears = this.ddlYears.SelectedValue;

                ddlPointSeq.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointInfo(strYears);
                }

                ddlPointSeq.Items.Add(new ListItem("선택하세요", ""));

                if (ds.Tables.Count > 0)
                {
                    DtPointSeq = ds.Tables[0];

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlPointSeq.Items.Add(new ListItem(DtPointSeq.Rows[i]["TITLE"].ToString(), DtPointSeq.Rows[i]["POINTSEQ"].ToString()));
                    }
                }
                else
                {
                    DtPointSeq = null;
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnModify_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnModify = (Button)sender;
                Label lblRequestID = (Label)btnModify.Parent.FindControl("lblRequestID");
                Label lblYears = (Label)btnModify.Parent.FindControl("lblYears");
                Label lblPointSeq = (Label)btnModify.Parent.FindControl("lblPointSeq");
                Label lblDetailSeq = (Label)btnModify.Parent.FindControl("lblDetailSeq");
                Label lblEmpNo = (Label)btnModify.Parent.FindControl("lblEmpNo");
                Label lblClothType = (Label)btnModify.Parent.FindControl("lblClothType");

                TextBox txbSize = (TextBox)btnModify.Parent.FindControl("txbSize");
                DropDownList ddlSize1 = (DropDownList)btnModify.Parent.FindControl("ddlSize1");
                DropDownList ddlSize2 = (DropDownList)btnModify.Parent.FindControl("ddlSize2");
                Label lblBtn = (Label)btnModify.Parent.FindControl("lblBtn");

                string strRequestId = lblRequestID.Text;
                string strYears = lblYears.Text;
                string strPointSeq = lblPointSeq.Text;
                string strDetailSeq = lblDetailSeq.Text;
                string strEmpNo = lblEmpNo.Text;

                if (strRequestId.Length == 0 || strYears.Length == 0 || strPointSeq.Length == 0 || strDetailSeq.Length == 0 || strEmpNo.Length == 0 || !lblBtn.Text.Equals("M"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    string strClothType = lblClothType.Text;
                    string strSize = "";
                    string strMsg = "";

                    if (strClothType.Equals("상의") || strClothType.Equals("점퍼") || strClothType.Equals("조끼")
                        || strClothType.Equals("티셔츠") || strClothType.ToUpper().Equals("긴팔T") || strClothType.ToUpper().Equals("반팔T")
                        || strClothType.Equals("외피") || strClothType.Equals("내피")
                        || strClothType.Equals("신발") || strClothType.Equals("모자") || strClothType.Equals("버프"))
                    {
                        if (ddlSize1.SelectedValue.Length == 0)
                        {
                            strMsg = "사이즈를 선택해야합니다.";
                        }
                        else
                        {
                            strSize = ddlSize1.SelectedValue;
                        }
                    }
                    else if (strClothType.Equals("하의") || strClothType.Contains("바지") || strClothType.Contains("방한바지"))
                    {
                        if (ddlSize1.SelectedValue.Length == 0 || ddlSize2.SelectedValue.Length == 0)
                        {
                            strMsg = "사이즈 두가지 모두 선택해야합니다.";
                        }
                        else
                        {
                            strSize = ddlSize1.SelectedValue + "/" + ddlSize2.SelectedValue;
                        }
                    }
                    else
                    {
                        if (txbSize.Text.Length == 0)
                        {
                            strMsg = "사이즈를 입력해야합니다.";
                        }
                        else
                        {
                            strSize = txbSize.Text;
                        }
                    }

                    if (strMsg.Length > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('" + strMsg + "');", true);
                    }
                    else
                    {
                        using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                        {
                            mgmt.UdpWorkingClothSize(strRequestId, strYears, strPointSeq, strDetailSeq, strEmpNo, strSize);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Modify", "alert('수정되었습니다.');", true);
                        GetDataList();
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlPointSeq_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strPointSeq = this.ddlPointSeq.SelectedValue;

                if (DtPointSeq !=null && !strPointSeq.Equals(""))
                {
                    DataRow[] dr = DtPointSeq.Select(string.Format("POINTSEQ = '{0}'", strPointSeq));

                    if (dr.Length > 0)
                    {
                        string strMonth = dr[0]["STDT"].ToString().Substring(0, 6);

                        SetOrgList(strMonth);

                        using (KTSUser user = new KTSUser())
                        {
                            ds = user.GetUserViewOrgMonth(this.Session["EMPNO"].ToString(), strMonth);
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string strOrgCd = "";
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                if (strOrgCd.Length > 0)
                                    strOrgCd += ",";

                                strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                            }

                            this.hdfOrgCd.Value = strOrgCd;
                            this.txbTeam.Text = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);
                        }
                        else
                        {
                            this.hdfOrgCd.Value = "";
                            this.txbTeam.Text = "";
                        }

                        this.hdfMonth.Value = strMonth;
                    }
                    else
                    {
                        this.hdfMonth.Value = "";
                        this.hdfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hdfMonth.Value = "";
                    this.hdfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}