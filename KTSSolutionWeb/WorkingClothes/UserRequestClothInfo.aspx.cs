﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.WorkingClothes;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class UserRequestClothInfo : PageBase
    {
        private string ReqType
        {
            get
            {
                if (ViewState["ReqType"] != null)
                {
                    return ViewState["ReqType"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ReqType"] = value;
            }
        }
        private string REQYN
        {
            get
            {
                if (ViewState["REQYN"] != null)
                {
                    return ViewState["REQYN"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["REQYN"] = value;
            }
        }

        private string NEWREQYN
        {
            get
            {
                if (ViewState["NEWREQYN"] != null)
                {
                    return ViewState["NEWREQYN"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["NEWREQYN"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();

                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }

                UpdPanelDDL.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strEmpNo = Session["EMPNO"].ToString();
                string strYears = ddlYears.SelectedValue;
                string strPointSeq = ddlPointSeq.SelectedValue;
                ReqType = "N";
                REQYN = "N";
                NEWREQYN = "N";

                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetRequestUserClothesList(strEmpNo, strYears, strPointSeq);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ReqType = ds.Tables[1].Rows[0]["REQTYPE"].ToString();
                    }

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        REQYN = ds.Tables[2].Rows[0]["REQYN"].ToString();
                        NEWREQYN = ds.Tables[2].Rows[0]["NEWREQYN"].ToString();
                    }
                }

                bool bReq = false;

                if (!strYears.Equals("") && !strPointSeq.Equals("") && REQYN.Equals("Y") && NEWREQYN.Equals("Y"))
                {
                    if (ReqType.Equals("N"))
                    {
                        bReq = true;
                    }
                    if (ReqType.Equals("A"))
                    {
                        if (this.Session["JOBNM2"].ToString().ToLower().Equals("kte"))
                        {
                            bReq = true;
                        }
                        else
                        {
                            bReq = false;
                        }
                    }
                    else
                    {
                        bReq = true;
                    }
                }
                else
                {
                    bReq = false;
                }

                if (bReq)
                {
                    btnRequest.Visible = true;
                    btnRequest.Attributes.Remove("onclick");
                    btnRequest.Attributes.Add("onclick", "RegRequestClothPopup('" + strYears + "', '" + strPointSeq + "', '" + strEmpNo + "', '" + ReqType + "')");
                }
                else
                {
                    btnRequest.Visible = false;
                }

                //updPanelSearch.Update();
                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        bool bViewPoint = false;

                        if (this.Session["JOBNM2"].ToString().ToLower().Equals("kte") && !ReqType.Equals("N"))
                        {
                            bViewPoint = true;
                        }
                        else
                        {
                            bViewPoint = false;
                        }

                        th.Visible = bViewPoint;

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult.Items[i].FindControl("tr");
                            HtmlTableCell td = (HtmlTableCell)rptResult.Items[i].FindControl("td");

                            Label lblYears = (Label)rptResult.Items[i].FindControl("lblYears");
                            Label lblPointSeq = (Label)rptResult.Items[i].FindControl("lblPointSeq");
                            Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");
                            Label lblReqType = (Label)rptResult.Items[i].FindControl("lblReqType");

                            td.Visible = bViewPoint;

                            if (REQYN.Equals("Y"))
                            {
                                tr.Attributes.Add("onclick", "RegRequestClothPopup('" + lblYears.Text + "', '" + lblPointSeq.Text + "', '" + lblEmpNo.Text + "', '" + lblReqType.Text + "')");
                            }
                        }
                    }
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                ddlPointSeq.Items.Clear();

                string strYears = ddlYears.SelectedValue;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointInfo(strYears);
                }

                ddlPointSeq.Items.Add(new ListItem("선택하세요", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlPointSeq.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TITLE"].ToString(), ds.Tables[0].Rows[i]["POINTSEQ"].ToString()));
                }

                UpdPanelDDL.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
    }
}