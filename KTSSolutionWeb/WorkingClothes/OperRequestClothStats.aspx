﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OperRequestClothStats.aspx.cs" Inherits="KTSSolutionWeb.OperRequestClothStats" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function PopupOrgTree(orgcd, empno) {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            if (years.length == 0 || pointseq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: sumupmonth,
                    pType: "cloth"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SearchChk() {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();

            if (years.length == 0 || pointseq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function btnPermCheck() {
            if (confirm("추가신청자 일괄승인을 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function SetOrgCd(orgcd, valtype) {

            this.focus();

            document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelSearch)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul id="ulPage" runat="server">
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
            <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <fieldset>
                        <span class="optionbox first">
                            <label>연도</label>
                            <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged" Width="150px"></asp:DropDownList>
			            </span> 
                        <span class="optionbox">
                            <label>지급기준</label>
                            <asp:DropDownList ID="ddlPointSeq" runat="server" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="ddlPointSeq_SelectedIndexChanged"></asp:DropDownList>
			            </span> 
				        <span class="inpbox">
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
				        </span>
			            <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" onClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
                    </fieldset>
                    <asp:HiddenField ID="hdfMonth" runat="server" />
                    <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlPointSeq" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>조직별 조회 결과</strong>
			    <div class="pull-right">
				    <div class="btnset" style="margin-right:10px;">
                        <asp:UpdatePanel ID="UpdPanelBtn" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button id="btnPerm" runat="server" class="btn-green" Visible="false" Enabled="false" OnClientClick="return btnPermCheck();" OnClick="btnPermission_ServerClick" Text="추가신청자일괄승인" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
				    </div>
				    <div class="btnset">
                        <asp:Button id="btnExcel" runat="server" class="btn-green" OnClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>소속</th>
                                    <th>지급기준</th>
                                    <th>대상자(명)</th>
                                    <th>신청자(명)</th>
                                    <th>신청율(%)</th>
                                    <th>추가신청자(명)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <input id="cbNum" runat="server" type="checkbox" style="width:20px;height:20px;" />                           
                                                <asp:Label ID="lblNum" runat="server" Text='<%# Eval("NUM") %>'></asp:Label>   
                                            </td>
                                            <td style="text-align:left;"><%# Eval("ORGFULLNM") %>
                                                <asp:Label ID="lblOrgCd" runat="server" Visible="false" Text='<%# Eval("ORGCD") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("TITLE") %>
                                                <asp:Label ID="lblYears" runat="server" Visible="false" Text='<%# Eval("YEARS") %>'></asp:Label>
                                                <asp:Label ID="lblPointSeq" runat="server" Visible="false" Text='<%# Eval("POINTSEQ") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("TGTCNT") %></td>
                                            <td><%# Eval("REQCNT") %></td>
                                            <td><%# Eval("REQRATE") %></td>
                                            <td><%# Eval("ADDREQCNT") %>
                                                <asp:Label ID="lblAddReqCnt" runat="server" Visible="false" Text='<%# Eval("ADDREQWAITCNT") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table> 
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>        
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
