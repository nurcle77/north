﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.WorkingClothes;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class OperRequestClothStats : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }
        private DataTable DtPointSeq
        {
            get
            {
                if (ViewState["DtPointSeq"] != null)
                    return (DataTable)ViewState["DtPointSeq"];
                else
                    return null;
            }
            set
            {
                ViewState["DtPointSeq"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                if (this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002"))
                {
                    this.btnPerm.Visible = true;
                    this.btnPerm.Enabled = true;
                }
                else
                {
                    this.btnPerm.Visible = false;
                    this.btnPerm.Enabled = false;
                }

                UpdPanelBtn.Update();

                SetDDLYears();

                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");

                //this.hdfOrgCd.Value = this.Session["ORGCD"].ToString();
                //this.txbTeam.Text = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);
                updPanelSearch.Update();

                //txbStDt.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                //txbEnDt.Text = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

                GetDataList();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    DateTime chkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime month = new DateTime(int.Parse(strMonth.Substring(0, 4)), int.Parse(strMonth.Substring(4, 2)), 1);

                    if (chkDate > month)
                    {
                        ds = org.GetOperOrgMonthList("", "N", strMonth);
                    }
                    else
                    {
                        ds = org.GetOperOrgList("", "N");
                    }
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hdfOrgCd.Value;
                //string strStDt = txbStDt.Text.Replace("-", "");
                //string strEnDt = txbEnDt.Text.Replace("-", "");

                string strYears = this.ddlYears.SelectedValue;
                string strPointSeq = this.ddlPointSeq.SelectedValue;

                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetRequestOperClothStat(strOrgCd, strYears, strPointSeq);
                }

                paging.PageNumber = 0;
                paging.PageSize = 50;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                updPanelSearch.Update();
                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[8];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGFULLNM";
            ArrHeader[3] = "TITLE";
            ArrHeader[4] = "TGTCNT";
            ArrHeader[5] = "REQCNT";
            ArrHeader[6] = "REQRATE";
            ArrHeader[7] = "ADDREQCNT";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[8];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "소속코드";
            ArrHeader[2] = "소속";
            ArrHeader[3] = "지급기준";
            ArrHeader[4] = "대상자(명)";
            ArrHeader[5] = "신청자(명)";
            ArrHeader[6] = "신청율(%)";
            ArrHeader[7] = "추가신청자(명)";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblNum = (Label)rptResult.Items[i].FindControl("lblNum");
                            HtmlInputCheckBox cbNum = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbNum");
                            Label lblAddReqCnt = (Label)rptResult.Items[i].FindControl("lblAddReqCnt"); 

                            if (lblAddReqCnt.Text.Equals("0"))
                            {
                                lblNum.Visible = true;
                                cbNum.Visible = false;
                            }
                            else
                            {
                                lblNum.Visible = false;
                                cbNum.Visible = true;
                            }
                        }
                    }
                    
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnPermission_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (!this.Session["AUTHID"].ToString().Equals("AUTH0001") && !this.Session["AUTHID"].ToString().Equals("AUTH0002"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    List<string> lstData = new List<string>();
                    bool bSave = true;

                    for (int i = 0; i < rptResult.Items.Count; i++)
                    {
                        HtmlInputCheckBox cbNum = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbNum");
                        if (!cbNum.Visible || !cbNum.Checked)
                        {
                            continue;
                        }
                        else
                        {
                            Label lblOrgCd = (Label)rptResult.Items[i].FindControl("lblOrgCd");
                            Label lblYears = (Label)rptResult.Items[i].FindControl("lblYears");
                            Label lblPointSeq = (Label)rptResult.Items[i].FindControl("lblPointSeq");

                            if (lblOrgCd.Text.Length == 0 || lblYears.Text.Length == 0 || lblPointSeq.Text.Length == 0)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                                bSave = false;
                                break;
                            }

                            string strData = lblOrgCd.Text + "|" + lblYears.Text + "|" + lblPointSeq.Text;
                            lstData.Add(strData);
                        }
                    }

                    if (bSave)
                    {
                        using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                        {
                            mgmt.UpdRequestClothPermissionGroup(lstData);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "alert('추가신청 승인을 완료했습니다.');", true);
                        }

                        GetDataList();
                        this.updPanel1.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    string strOrgCd = this.hdfOrgCd.Value;
                    //string strStDt = txbStDt.Text.Replace("-", "");
                    //string strEnDt = txbEnDt.Text.Replace("-", "");

                    string strYears = this.ddlYears.SelectedValue;
                    string strPointSeq = this.ddlPointSeq.SelectedValue;

                    using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                    {
                        ds = mgmt.GetRequestOperClothStat(strOrgCd, strYears, strPointSeq);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "피복현황통계");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strYears = ddlYears.SelectedValue;

                ddlPointSeq.Items.Clear();
                ddlPointSeq.Items.Add(new ListItem("선택하세요", ""));

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointInfo(strYears);
                }

                if (ds.Tables.Count > 0)
                {
                    DtPointSeq = ds.Tables[0];

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlPointSeq.Items.Add(new ListItem(DtPointSeq.Rows[i]["TITLE"].ToString(), DtPointSeq.Rows[i]["POINTSEQ"].ToString()));
                    }
                }
                else
                {
                    DtPointSeq = null;
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void ddlPointSeq_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strPointSeq = ddlPointSeq.SelectedValue;

                if (DtPointSeq != null && !strPointSeq.Equals(""))
                {
                    DataRow[] dr = DtPointSeq.Select(string.Format("POINTSEQ = '{0}'", strPointSeq));

                    if (dr.Length > 0)
                    {
                        string strMonth = dr[0]["STDT"].ToString().Substring(0, 6);

                        SetOrgList(strMonth);

                        using (KTSUser user = new KTSUser())
                        {
                            ds = user.GetUserViewOrgMonth(this.Session["EMPNO"].ToString(), strMonth);
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string strOrgCd = "";
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                if (strOrgCd.Length > 0)
                                    strOrgCd += ",";

                                strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                            }

                            this.hdfOrgCd.Value = strOrgCd;
                            this.txbTeam.Text = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);
                        }
                        else
                        {
                            this.hdfOrgCd.Value = "";
                            this.txbTeam.Text = "";
                        }

                        this.hdfMonth.Value = strMonth;
                    }
                    else
                    {
                        this.hdfMonth.Value = "";
                        this.hdfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hdfMonth.Value = "";
                    this.hdfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}