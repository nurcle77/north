﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkingClothPointInfo.aspx.cs" Inherits="KTSSolutionWeb.WorkingClothPointInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function PopupOrgTree(orgcd, empno) {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            if (years.length == 0) {
                alert("연도를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: sumupmonth,
                    pType: "cloth"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SearchChk() {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();

            if (Years.length == 0) {
                alert("연도를 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SetOrgCd(orgcd, valtype) {

            this.focus();

            document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelSearch)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>       
                <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox" style="margin-right:20px">
                            <label>연도</label>
                            <asp:DropDownList ID="ddlYears" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged"></asp:DropDownList>
			            </span>
				        <span class="inpbox">
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
				        </span>
                        <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
                        <asp:HiddenField ID="hdfMonth" runat="server" />
                        <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                    </Triggers>
                </asp:UpdatePanel>
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>피복포인트 관리조회 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" style="float:right;" OnClientClick="return SearchChk();" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>소속</th>
                                    <th>사번</th>
                                    <th>kts사번</th>
                                    <th>이름</th>
                                    <th>연도</th>
                                    <th>총포인트</th>
                                    <th>사용포인트</th>
                                    <th>잔여포인트</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td><%# Eval("EMPNO") %></td>
                                            <td><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td><%# Eval("YEARS") %></td>
                                            <td><%# Eval("TOTALPOINT") %></td>
                                            <td><%# Eval("USEPOINT") %></td>
                                            <td><%# Eval("RESTPOINT") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
