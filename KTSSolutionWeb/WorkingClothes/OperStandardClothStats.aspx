﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OperStandardClothStats.aspx.cs" Inherits="KTSSolutionWeb.OperStandardClothStats" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function SearchChk() {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (years.length == 0 || pointseq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-bottom:20px;">
		    <fieldset>       
                <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox">
                            <label>연도</label>
                            <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged" Width="150px"></asp:DropDownList>
			            </span> 
                        <span class="optionbox" style="margin-left:10px;">
                            <label>지급기준</label>
                            <asp:DropDownList ID="ddlPointSeq" runat="server" Width="250px"></asp:DropDownList>
			            </span> 
                        <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <!-- S:leftbox -->
            <div class="leftbox">    
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>기준지급</strong>
                        <div class="pull-right">
                            <div class="btnset">
                                <asp:Button id="btnExcel1" runat="server" class="btn-green" OnClientClick="return SearchChk();"  OnClick="btnExcel1_ServerClick" Text="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->
    
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>직무</th>
                                            <th>구분</th>
                                            <th>종류</th>
                                            <th>수량</th>
                                            <th>단가</th>
                                            <th>인원</th>
                                            <th>구매수량</th>
                                            <th>금액</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult1" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("JOBNM") %></td>
                                                    <td><%# Eval("CLOTHGBN") %></td>
                                                    <td><%# Eval("CLOTHTYPE") %></td>
                                                    <td><%# Eval("PROVIDECNT") %></td>
                                                    <td><%# Eval("COST") %></td>
                                                    <td><%# Eval("EMPUSERCNT") %></td>
                                                    <td><%# Eval("BUYCNT") %></td>
                                                    <td><%# Eval("TOTALCOST") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="paging1" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>    
                    <!-- E:scrollbox -->
                    <uc:paging ID="paging1" runat="server" OnPreRender="paging1_PreRender" />
                </div>
            </div>
            <!-- E:leftbox -->
            <!-- S:rightbox -->
            <div class="rightbox">
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>연도/지급기준 결과</strong>
                        <div class="pull-right">
                            <div class="btnset">
                                <asp:Button id="btnExcel2" runat="server" class="btn-green" OnClientClick="return SearchChk();"  OnClick="btnExcel2_ServerClick" Text="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->                    
                        
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <asp:UpdatePanel ID="updPanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>직무</th>
                                            <th>구분</th>
                                            <th>종류</th>
                                            <th>수량</th>
                                            <th>단가</th>
                                            <th>인원</th>
                                            <th>구매수량</th>
                                            <th>금액</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult2" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("JOBNM") %></td>
                                                    <td><%# Eval("CLOTHGBN") %></td>
                                                    <td><%# Eval("CLOTHTYPE") %></td>
                                                    <td><%# Eval("PROVIDECNT") %></td>
                                                    <td><%# Eval("COST") %></td>
                                                    <td><%# Eval("EMPUSERCNT") %></td>
                                                    <td><%# Eval("BUYCNT") %></td>
                                                    <td><%# Eval("TOTALCOST") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="paging2" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <!-- E:scrollbox -->
                    <uc:paging ID="paging2" runat="server" OnPreRender="paging2_PreRender" />
                </div>
                <!-- E:datalist -->
            </div>                
            <!-- E:rightbox -->
        </div>
        <!-- E:multiplebox -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>