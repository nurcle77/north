﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserRequestClothInfo.aspx.cs" Inherits="KTSSolutionWeb.UserRequestClothInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function RegRequestClothPopup(strYears, strPointSeq, strEmpNo, strReqType) {
            var nWidth = 1000;
            var nHeight = 900;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                YEARS: strYears,
                POINTSEQ: strPointSeq,
                EMPNO: strEmpNo,
                REQTYPE: strReqType,
                SUBREQ: "N"
            };

            var Popupform = createForm("/WorkingClothes/RegRequestCloth", param);

            Popupform.target = "RegRequestCloth";
            var win = window.open("", "RegRequestCloth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SearchChk() {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length > 0 && PointSeq.length) {
                return true;
            } else {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            }
        }

        function ReSearch() {
            <%=Page.GetPostBackEventReference(btnSelect)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>       
                <asp:UpdatePanel ID="UpdPanelDDL" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox">
                            <label>연도</label>
                            <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged" Width="150px"></asp:DropDownList>
			            </span> 
                        <span class="optionbox" style="margin-left:10px;">
                            <label>지급기준</label>
                            <asp:DropDownList ID="ddlPointSeq" runat="server" Width="250px"></asp:DropDownList>
			            </span> 
                        <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>조직별 조회 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
                        <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button id="btnRequest" Visible="false" runat="server" class="btn-green" style="float:right;" Text="신청" />                                
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
		    <!-- S:scrollbox -->
		    <div class="scrollbox">            
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>소속</th>
                                    <th>이름</th>
                                    <th>사번</th>
                                    <th>KTS사번</th>
                                    <th>지급기준</th>
                                    <th>신청구분</th>
                                    <th>신청상태</th>
                                    <th>작업복구분</th>
                                    <th>종류</th>
                                    <th>사이즈</th>
                                    <th>수량</th>
                                    <th>신청일자</th>
                                    <th id="th" runat="server" visible="false">사용포인트</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr id="tr" runat="server">
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td>
                                                <asp:Label ID="lblYears" runat="server" Visible="false" Text='<%# Eval("YEARS") %>'></asp:Label>
                                                <asp:Label ID="lblPointSeq" runat="server" Visible="false" Text='<%# Eval("POINTSEQ") %>'></asp:Label>
                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("TITLE") %></td>
                                            <td><%# Eval("PROVIDETYPE") %></td>
                                            <td><%# Eval("REQSTATUS") %>
                                                <asp:Label ID="lblReqType" runat="server" Visible="false" Text='<%# Eval("REQTYPE") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("CLOTHGBN") %></td>
                                            <td><%# Eval("CLOTHTYPE") %></td>
                                            <td><%# Eval("CLOTHSIZE") %></td>
                                            <td><%# Eval("REQCNT") %></td>
                                            <td><%# Eval("REGDT") %></td>
                                            <td id="td" runat="server" visible="false"><%# Eval("USEPOINT") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>  
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="btnRequest" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel> 
            </div>      
		    <!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
