﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Car;

namespace KTSSolutionWeb
{
    public partial class CarListInfo : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetOrgList();

                this.hfOrgCd.Value = this.Session["ORGCD"].ToString();
                this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hfOrgCd.Value.ToString();

                using (CarMgmt mgmt = new CarMgmt())
                {
                    ds = mgmt.GetCarInfoList(strOrgCd);
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[8];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGFULLNM";
            ArrHeader[2] = "CARNUM";
            ArrHeader[3] = "STKM";
            ArrHeader[4] = "EMPNO";
            ArrHeader[5] = "KTSEMPNO";
            ArrHeader[6] = "EMPNM";
            ArrHeader[7] = "NOTICE";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[8];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "소속";
            ArrHeader[2] = "차량정보";
            ArrHeader[3] = "시작km";
            ArrHeader[4] = "사번";
            ArrHeader[5] = "KTS사번";
            ArrHeader[6] = "담당자";
            ArrHeader[7] = "공지";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    string strOrgCd = this.hfOrgCd.Value.ToString();

                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        ds = mgmt.GetCarInfoList(strOrgCd);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "차량관리");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Button btnSave = (Button)rptResult.Items[i].FindControl("btnSave");
                            Button btnDelete = (Button)rptResult.Items[i].FindControl("btnDelete");                             

                            AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();
                            asyncBtnSave.ControlID = btnSave.UniqueID;
                            asyncBtnSave.EventName = "Click";

                            AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                            asyncBtnDelete.ControlID = btnDelete.UniqueID;
                            asyncBtnDelete.EventName = "Click";

                            updPanel1.Triggers.Add(asyncBtnSave);
                            updPanel1.Triggers.Add(asyncBtnDelete);

                            updPanel1.Update();
                        }
                    }
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblCarNo = (Label)btnSave.Parent.FindControl("lblCarNo");
                TextBox txbNoti = (TextBox)btnSave.Parent.FindControl("txbNoti");

                if (lblCarNo.Text.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        mgmt.SetCarNotiInfo(lblCarNo.Text, txbNoti.Text);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('차량에 대한 공지가 수정되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('차량에 대한 공지 저장 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            try
            {
                Button btnSave = (Button)sender;
                Label lblCarNo = (Label)btnSave.Parent.FindControl("lblCarNo");

                if (lblCarNo.Text.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        ds = mgmt.GetDelCarInfoCheck(lblCarNo.Text);
                    }

                    bool bDel = true;

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        bDel = ds.Tables[0].Rows[0]["DELYN"].ToString() == "Y" ? true : false;
                    }

                    if (bDel)
                    {
                        using (CarMgmt mgmt = new CarMgmt())
                        {
                            mgmt.DelCarInfo(lblCarNo.Text);
                        }

                        GetDataList();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('차량정보가 삭제되었습니다.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('등록된 운행일지가 있어서 삭제가 불가능합니다.');", true);
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('차량정보 삭제 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnRegCar_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strCarOrg = this.hfCarOrg.Value;
                string strCarNum = this.txbCarNum.Text;
                string strStkm = this.txbStkm.Text;
                string strEmpNo = this.hfEmpNo.Value;
                string strNotice = this.txbNotice.Text;

                if (strCarNum.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('차량번호를 입력해 주세요.');", true);
                }
                else if (strCarOrg.Length == 0)
                {
                    if (strEmpNo.Length == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('담당자나 차량소속을 입력해 주세요.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                    }
                }
                else if (strStkm.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('시작km를 입력해 주세요.');", true);
                }
                else
                {
                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        ds = mgmt.GetCarNumberCheck(strCarNum);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strCarOrgNm = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strCarOrgNm + " 에 등록된 차량번호입니다.');", true);
                    }
                    else
                    {
                        using (CarMgmt mgmt = new CarMgmt())
                        {
                            mgmt.SetCarInfo(strCarOrg, strCarNum, strStkm, strEmpNo, strNotice);
                        }

                        this.txbCarOrg.Text = "";
                        this.hfCarOrg.Value = "";
                        this.txbCarNum.Text = "";
                        this.txbStkm.Text = "";
                        this.txbEmpNm.Text = "";
                        this.hfEmpNo.Value = "";
                        this.txbNotice.Text = "";

                        updPanelRegCar.Update();

                        GetDataList();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('차량정보가 등록 되었습니다.');", true);
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('차량정보 저장 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void hfEmpNo_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = this.hfEmpNo.Value;

                if (!strEmpNo.Equals(""))
                {
                    this.txbEmpNm.Text = Request.Form[txbEmpNm.UniqueID].ToString();

                    using (CarMgmt mgmt = new CarMgmt())
                    {
                        ds = mgmt.GetUserOrgInfo(strEmpNo);
                    }

                    string strOrgCd = "";
                    string strOrgNm = "";

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (strOrgCd.Length > 0)
                        {
                            strOrgCd += ",";
                            strOrgNm += ",";
                        }

                        strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        strOrgNm += ds.Tables[0].Rows[i]["ORGNM"].ToString();
                    }

                    this.hfCarOrg.Value = strOrgCd;
                    this.txbCarOrg.Text = strOrgNm;
                }
                else
                {
                    this.txbEmpNm.Text = "";
                    this.txbCarOrg.Text = "";
                    this.hfCarOrg.Value = "";
                }

                updPanelRegCar.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('차량정보 저장 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void hfCarOrg_ValueChanged(object sender, EventArgs e)
        {
            this.txbCarOrg.Text = Request.Form[txbCarOrg.UniqueID].ToString();

            this.hfEmpNo.Value = "";
            this.txbEmpNm.Text = "";

            updPanelRegCar.Update();
        }
    }
}