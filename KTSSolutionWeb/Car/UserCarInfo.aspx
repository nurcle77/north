﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserCarInfo.aspx.cs" Inherits="KTSSolutionWeb.UserCarInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno
            };

            var Popupform = createForm("/Common/OrgTree_Oper", param);

            Popupform.target = "OrgTree_Oper";
            var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetOrgCd(orgcd, chk) {
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            document.getElementById("<%=hfEmpNo.ClientID %>").value = empno;
            document.getElementById("<%=txbEmpNm.ClientID %>").value = empnm;

            __doPostBack("<%=updPanelEmpNo.ClientID %>", "");
        }

        function btnRegCheck() {
            var CarNum = $("#<%= ddlCarNum.ClientID %> option:selected").val();
            var WorkDt = $("#<%= txbWorkDt.ClientID %>").val();
            var Enkm = $("#<%= txbEnkm.ClientID %>").val();
            var Leaveyn = $("#<%= chkReqLeave.ClientID %>").is(":checked");

            if (CarNum.length == 0) {
                alert("차량을 선택하세요");
                return false;
            }
            else if (WorkDt.length == 0) {
                alert("운행일자를 입력하세요");
                return false;
            }
            else if (Enkm.length == 0) {
                alert("종료km를 입력하세요");
                return false;
            }
            else {
                if (Leaveyn) {
                    if (confirm("운행일지 등록 및 직퇴승인 요청하시겠습니까?")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (confirm("운행일지를 등록 하시겠습니까?")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        }

        function SearchChk() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();
            var EmpNo = $("#<%= hfEmpNo.ClientID %>").val();
            var CarNum = $("#<%= txbCarNum.ClientID %>").val();

            if (OrgCd.length == 0 && EmpNo.length == 0 && CarNum.length == 0 ) {
                alert("조회할 조직이나 사원을 선택하거나 차량번호를 입력해 주세요.");
                return false;
            }else {
                return true;
            }
        }

        function btnSaveCheck(obj) {

            var text = obj.value;

            if (text == "저장") {
                if (confirm("저장하시겠습니까?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                if (confirm("수정하시겠습니까?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function btnConfirmCheck() {
            if (confirm("직퇴를 승인하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnReturnCheck() {
            if (confirm("직퇴를 반려하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDeleteCheck() {
            if (confirm("삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function InitValue() {
            $("#<%= txbWorkDt.ClientID %>").val('');
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">            
		    <fieldset>       
                <span class="optionbox first">
                    <asp:UpdatePanel ID="updPanelCarNum" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>차량</label>
                            <asp:DropDownList ID="ddlCarNum" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCarNum_SelectedIndexChanged"></asp:DropDownList>
                            <asp:HiddenField ID="hfStKm" runat="server" />
                            <asp:HiddenField ID="hfRegYn" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCarNum" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="inpbox">
					<label>운행일자</label>
                    <asp:TextBox ID="txbWorkDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span>
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelFuels" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>주유(ℓ)</label>
                            <asp:TextBox ID="txbFuels" runat="server" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Width="120px" MaxLength="11"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelEnKm" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>종료km</label>
                            <asp:TextBox ID="txbEnkm" runat="server" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Width="120px" MaxLength="11"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <span class="ickbox" style="margin-right:150px;">
                    <asp:UpdatePanel ID="updPanelLeave" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>직퇴요청</label>
                                <input id="chkReqLeave" runat="server" type="checkbox" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelNoti" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>공지</label>
                            <asp:TextBox ID="txbNoti" runat="server"  Width="250px" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <asp:Button id="btnRegCar" runat="server" OnClientClick="return btnRegCheck();" OnClick="btnRegCar_Click" class="btn-green last" style="float:right;" Text="등록" />
            </fieldset>
        </div>
        <div class="searchbox" style="margin-top:20px;">    
		    <fieldset>     
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" style="margin-right:10px;width:40px;height:40px;" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="inpbox" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelEmpNo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>이름</label>
                            <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnEmpUser" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" style="margin-right:10px;width:40px;height:40px;" />
                            <asp:HiddenField ID="hfEmpNo" runat="server" OnValueChanged="hfEmpNo_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfEmpNo" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
                <span class="inpbox">
					<label>차량번호</label>
                    <asp:TextBox ID="txbCarNum" runat="server" Width="100px"></asp:TextBox>
                </span>
                <span class="inpbox">
					<label>운행일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>차량관리 조회 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" style="float:right;" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <th>No.</th>
                                <th>소속</th>
                                <th>사번</th>
                                <th>KTS사번</th>
                                <th>이름</th>
                                <th>차량정보</th>
                                <th>운행일자</th>
                                <th>시작km</th>
                                <th>종료km</th>
                                <th>운행거리</th>
                                <th>직퇴결과</th>
                                <th>직퇴거리</th>
                                <th>최종km</th>
                                <th>주유(ℓ)</th>
                                <th>수정/직퇴요청</th>
                                <th>직퇴요청상태</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td>
                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                                <asp:Label ID="lblLeaveYn" runat="server" Text='<%# Eval("LEAVEYN") %>' visible="false"></asp:Label>
                                            </td>
                                            <td><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td>
                                                <asp:Label ID="lblCarNum" runat="server" Text='<%# Eval("CARNUM") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblWorkDt" runat="server" Text='<%# Eval("WORKDATE") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblStKm" runat="server" Text='<%# Eval("STKM") %>'></asp:Label>
                                            </td>
                                            <td style="width:80px">
                                                <asp:Label ID="lblEnKm" runat="server" Text='<%# Eval("ENKM") %>'></asp:Label>
                                                <asp:TextBox id="txbEnKm2" runat="server" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Width="80px" Height="40px" style="text-align:center;" Font-Size="15px" Visible="false" Text='<%# Eval("ENKM") %>'></asp:TextBox>
                                            </td>
                                            <td><%# Eval("WORKDISTANCE") %></td>
                                            <td style="width:80px">
                                                <asp:Label ID="lblLeaveKm" runat="server" Text='<%# Eval("LEAVEKM") %>'></asp:Label>
                                                <asp:TextBox id="txbLeaveKm" runat="server" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Width="80px" Height="40px" style="text-align:center;" Font-Size="15px" Visible="false" Text='<%# Eval("LEAVEKM") %>'></asp:TextBox>
                                            </td>
                                            <td><%# Eval("LEAVEDISTANCE") %></td>
                                            <td><%# Eval("RESULTKM") %></td>
                                            <td style="width:80px">
                                                <asp:Label ID="lblFuels" runat="server" Text='<%# Eval("FUELS") %>'></asp:Label>
                                                <asp:TextBox id="txbFuels2" runat="server" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Width="80px" Height="40px" style="text-align:center;" Font-Size="15px" Visible="false" Text='<%# Eval("FUELS") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblModifyYn" runat="server" Visible="false" Text='<%# Eval("MODIYN") %>'></asp:Label>
                                                <asp:Button id="btnSave" runat="server" OnClientClick="return btnSaveCheck(this);" OnClick="btnSave_ServerClick" Visible="false" Enabled="false" class="btn-save" Text="저장" />
                                                <asp:Button id="btnConfirm" runat="server" OnClientClick="return btnConfirmCheck();" OnClick="btnConfirm_Click" Visible="false" Enabled="false" class="btn-save" Text="승인" />
                                                <asp:Button id="btnReturn" runat="server" OnClientClick="return btnReturnCheck();" OnClick="btnConfirm_Click" Visible="false" Enabled="false" class="btn-del" Text="반려" />
                                                <asp:Button id="btnDelete" runat="server" OnClientClick="return btnDeleteCheck();" OnClick="btnDelete_Click" Visible="false" Enabled="false" class="btn-del" Text="삭제" />
                                            </td>
                                            <td id="td" runat="server">
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRegCar" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
