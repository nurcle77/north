﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarListInfo.aspx.cs" Inherits="KTSSolutionWeb.CarListInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function PopupOrgCd() {

            var nWidth = 600;
            var nHeight = 400;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGLV: "F"
            };

            var Popupform = createForm("/Common/SearchOrgCd", param);

            Popupform.target = "SearchOrgCd";
            var win = window.open("", "SearchOrgCd", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno
            };

            var Popupform = createForm("/Common/OrgTree_Oper", param);

            Popupform.target = "OrgTree_Oper";
            var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetOrgCd(orgcd, chk) {
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelSearch.ClientID %>", "");
        }

        function SetDispOrgCd(orgcd, orgnm) {
            document.getElementById("<%=hfCarOrg.ClientID %>").value = orgcd;
            document.getElementById("<%=txbCarOrg.ClientID %>").value = orgnm;

            __doPostBack("<%=updPanelRegCar.ClientID %>", "");
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            document.getElementById("<%=hfEmpNo.ClientID %>").value = empno;
            document.getElementById("<%=txbEmpNm.ClientID %>").value = empnm;

            __doPostBack("<%=updPanelRegCar.ClientID %>", "");
        }

        function btnRegCarCheck() {
            if (confirm("차량정보를 등록하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnSaveCheck() {
            if (confirm("차량에 대한 공지를 수정하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDelCheck() {
            if (confirm("차량정보를 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">            
		    <fieldset>       
                <asp:UpdatePanel ID="updPanelRegCar" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="inpbox first" style="margin-right:20px">
					        <label>번호</label>
                            <asp:TextBox ID="txbCarNum" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                        </span>
                        <span class="inpbox" style="margin-right:20px">
					        <label>시작km</label>
                            <asp:TextBox ID="txbStkm" runat="server" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Width="150px" MaxLength="10"></asp:TextBox>
                        </span>
                        <span class="inpbox" style="margin-right:20px">
					        <label>담당자</label>
                            <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                            <asp:HiddenField ID="hfEmpNo" runat="server" OnValueChanged="hfEmpNo_ValueChanged" />
                        </span>
                        <span class="inpbox" style="margin-right:20px">
					        <label>차량소속</label>
                            <asp:TextBox ID="txbCarOrg" runat="server" Width="150px" style="margin-right:10px" onclick="PopupOrgCd();" ReadOnly="true"></asp:TextBox>
				            <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgCd();">+</button>
                            <asp:HiddenField ID="hfCarOrg" runat="server" OnValueChanged="hfCarOrg_ValueChanged" />
				        </span>
                        <span class="inpbox">
					        <label>공지</label>
                            <asp:TextBox ID="txbNotice" runat="server" Width="250px"></asp:TextBox>
                        </span>
                        <asp:Button id="btnRegCar" runat="server" OnClick="btnRegCar_Click" OnClientClick="return btnRegCarCheck();" class="btn-green last" style="float:right;" Text="등록" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hfEmpNo" EventName="ValueChanged" />
                        <asp:AsyncPostBackTrigger ControlID="hfCarOrg" EventName="ValueChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </fieldset>
        </div>
        <div class="searchbox" style="margin-top:20px;">    
		    <fieldset>       
                <span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
				            <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '', '');">+</button>
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>차량관리 조회 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" style="float:right;" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <th>No.</th>
                                <th>소속</th>
                                <th>차량정보</th>
                                <th>시작km</th>
                                <th>사번</th>
                                <th>KTS사번</th>
                                <th>담당자</th>
                                <th>공지</th>
                                <th>수정/삭제</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td><asp:Label ID="lblCarNo" runat="server" Text='<%# Eval("CARNUM") %>'></asp:Label></td>
                                            <td><%# Eval("STKM") %></td>
                                            <td><%# Eval("EMPNO") %></td>
                                            <td><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td>
                                                <asp:TextBox id="txbNoti" runat="server" Width="100%" Height="40px" style="padding-left:5px" Text='<%# Eval("NOTICE") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button id="btnSave" runat="server" OnClientClick="return btnSaveCheck();" OnClick="btnSave_ServerClick" class="btn-save" Text="저장" />
                                                <asp:Button id="btnDelete" runat="server" OnClientClick="return btnDelCheck();" OnClick="btnDelete_ServerClick" class="btn-del" Text="삭제" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
