﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PassInfoSts.aspx.cs" Inherits="KTSSolutionWeb.PassInfoSts" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function PopupPassOrgTree(passtype, operator, upperyn) {
            
            var orgcd = $("#<%= hfAuthOrgCd.ClientID %>").val();

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pPASSTYPE: passtype,
                pOPERATOR: operator,
                pUPPERYN: upperyn
            };

            var Popupform = createForm("/Common/OrgTree_Pass", param);

            Popupform.target = "OrgTree_PassAuth";
            pop3 = window.open("", "OrgTree_PassAuth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop3.focus();
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetPassOrgCd(orgcd) {

            this.focus();
            
            $("#<%= hfAuthOrgCd.ClientID %>").val(orgcd);
            __doPostBack("<%=updPanelAuthOrg.ClientID %>", "");
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=txbEmpNo.ClientID %>").val(empno);
            $("#<%=txbEmpNm.ClientID %>").val(empnm);
        }

        function btnSearchCheck() {
            var passtype = $("#<%= ddlPassType.ClientID %> option:selected").val();

            if (passtype == "") {
                alert("구분을 선택해주세요");
                return false;
            }
            else {
                return true;
            }
        }

        function rbDateCheck(obj) {
            if (obj.value == "date") {
                $("#<%=txbStDt.ClientID %>").show();
            }
            else {
                $("#<%=txbStDt.ClientID %>").hide();
            }
        }

        function chkLvCheck(obj) {
            if (obj.value == "lv1") {
                $("#<%=chkLv2.ClientID %>").prop("checked", false);
                $("#<%=chkLv3.ClientID %>").prop("checked", false);
                $("#<%=chkLv4.ClientID %>").prop("checked", false);
            }
            else if (obj.value == "lv2") {
                $("#<%=chkLv1.ClientID %>").prop("checked", false);
                $("#<%=chkLv3.ClientID %>").prop("checked", false);
                $("#<%=chkLv4.ClientID %>").prop("checked", false);
            }
            else if (obj.value == "lv3") {
                $("#<%=chkLv1.ClientID %>").prop("checked", false);
                $("#<%=chkLv2.ClientID %>").prop("checked", false);
                $("#<%=chkLv4.ClientID %>").prop("checked", false);
            }
            else {
                $("#<%=chkLv1.ClientID %>").prop("checked", false);
                $("#<%=chkLv2.ClientID %>").prop("checked", false);
                $("#<%=chkLv3.ClientID %>").prop("checked", false);
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>   
				<span class="optionbox">
                    <asp:UpdatePanel ID="updPanelPassType" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>구분</label>
                            <asp:DropDownList ID="ddlPassType" style="width:150px;" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlPassType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>    
                <span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelAuthOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                            <asp:HiddenField ID="hfAuthOrgCd" runat="server" OnValueChanged="hfAuthOrgCd_ValueChanged"  />
				            <button id="btnOrg" runat="server" type="button" class="btn-plus" title="검색">+</button>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfAuthOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelEmp" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>사번</label>
                            <asp:TextBox ID="txbEmpNo" runat="server" Width="100px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
					        <label>이름</label>
                            <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <br />
                <span class="inpbox">
					<input type="radio" name="rbDateChk" id="rbDate" value="date" runat="server" style="width:20px; height:20px;margin-right:5px;" checked="true" onclick="rbDateCheck(this);" /><label>일자</label>
					<input type="radio" name="rbDateChk" id="rbMonth" value="month" runat="server" style="width:20px;height:20px;margin-right:5px;" onclick="rbDateCheck(this);" /><label>당월 현재</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span>
                <span class="inpbox">
                    <input type="checkbox" name="chkLvChk" id="chkLv1" value="lv1" runat="server" style="width:20px;height:20px;margin-right:5px;" onclick="chkLvCheck(this);" /><label>1레벨(본부)</label>
                    <input type="checkbox" name="chkLvChk" id="chkLv2" value="lv2" runat="server" style="width:20px;height:20px;margin-right:5px;" onclick="chkLvCheck(this);" /><label>2레벨(지사)</label>
                    <input type="checkbox" name="chkLvChk" id="chkLv3" value="lv3" runat="server" style="width:20px;height:20px;margin-right:5px;" onclick="chkLvCheck(this);" /><label>3레벨(지점)</label>
                    <input type="checkbox" name="chkLvChk" id="chkLv4" value="trtEmp" runat="server" style="width:20px;height:20px;margin-right:5px;" onclick="chkLvCheck(this);" /><label>담당자별</label>
                </span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return btnSearchCheck();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조직별 조회 결과</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <th>순번</th>
                                <th>년월</th>
                                <th id="thOrgNm" runat="server">소속</th>
                                <th id="thEmpNo" runat="server">사번</th>
                                <th id="thEmpNm" runat="server">이름</th>
                                <th>상품구분</th>
                                <th>상품</th>
                                <th>PASS건</th>
                                <th>슈팅건</th>
                                <th>성공건</th>
                                <th>실패건</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td id="tdNo" runat="server"><%# Eval("NUM") %></td>
                                            <td id="tdPassDt" runat="server"><%# Eval("PASSDT") %></td>
                                            <td id="tdOrgNm" runat="server"><%# Eval("ORGNM") %></td>
                                            <td id="tdEmpNo" runat="server"><%# Eval("EMPNO") %></td>
                                            <td id="tdEmpNm" runat="server"><%# Eval("EMPNM") %></td>
                                            <td id="tdPassType" runat="server"><%# Eval("PASSTYPENM") %></td>
                                            <td id="tdSvcType" runat="server"><%# Eval("SVCTYPE") %></td>
                                            <td><%# Eval("PASSCNT") %></td>
                                            <td><%# Eval("SHOOTINGCNT") %></td>
                                            <td><%# Eval("SUCCCNT") %></td>
                                            <td><%# Eval("FAILCNT") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
