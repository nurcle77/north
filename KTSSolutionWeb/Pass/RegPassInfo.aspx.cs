﻿using System;
using System.Data;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Pass;
using System.Web.UI.WebControls;

namespace KTSSolutionWeb
{
    public partial class RegPassInfo : PageBase
    {
        #region fields

        #region DtSvc
        /// <summary>
        /// DtSvc
        /// </summary>
        private DataTable DtSvc
        {
            get
            {
                if (ViewState["DtSvc"] != null)
                {
                    return (DataTable)ViewState["DtSvc"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["DtSvc"] = value;
            }
        }
        #endregion

        #region DtConsult
        /// <summary>
        /// DtConsult
        /// </summary>
        private DataTable DtConsult
        {
            get
            {
                if (ViewState["DtConsult"] != null)
                {
                    return (DataTable)ViewState["DtConsult"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["DtConsult"] = value;
            }
        }
        #endregion

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('잘못된 접근입니다.');window.close();", true);
            }
            else
            {
                initializeControls();

                if (!IsPostBack)
                {
                    this.txbHopeDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }
        }
        #endregion

        #region ddlPassType_SelectedIndexChanged
        /// <summary>
        /// ddlPassType_SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPassType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetDropDownList();

                this.txbCompEmpNm.Text = "";
                this.hfCompEmpNo.Value = "";

                this.txbCompEmpNm.Attributes.Add("onClick", "ModalShow();");

                this.updPanelSvc.Update();
                this.updPanelConsult.Update();
                this.updPanelComp.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnRegPass_Click
        /// <summary>
        /// btnRegPass_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegPass_Click(object sender, EventArgs e)
        {
            try
            {
                string strEmpNo = this.Session["EMPNO"].ToString();
                string strCustNm = this.txbCustNm.Text;
                string strCustTelNo = this.txbTelNo.Text.Replace("-", "");
                string strHopeDt = Request.Form[this.txbHopeDt.UniqueID].Replace("-", "");
                string strRelType = this.txbRelType.Text;
                string strPassType = this.ddlPassType.SelectedValue;
                string strSvcType = this.ddlSvcType.SelectedValue;
                string strMotType = this.ddlMotType.SelectedValue;
                string strConsult = this.ddlConsult.SelectedValue;
                string strCompEmpNo = this.hfCompEmpNo.Value;
                string strNoti = this.txbNoti.Text.Replace("<", "(").Replace(">", ")");

                string strMsg = "";

                if (strPassType.Equals("COMP"))
                {
                    if (strSvcType.Equals("직원"))
                    {
                        strCustNm = "칭찬";
                    }

                    strRelType = strSvcType;
                }

                if (ValidationCheck(strCustNm, strCustTelNo, strHopeDt, strRelType, strPassType, strSvcType, strCompEmpNo, strConsult, strMotType, ref strMsg))
                {
                    if (!strPassType.Equals("MOT"))
                    {
                        strMotType = "";
                    }

                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.SetPassProcess(strEmpNo, strCustNm, strCustTelNo, strHopeDt, strRelType, strPassType, strSvcType, strMotType, strConsult, strNoti, strCompEmpNo, "", "-1");
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('PASS가 저장되었습니다.');window.close(self);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strMsg + "');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('저장 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnEmpNo_Click
        /// <summary>
        /// btnEmpNo_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnEmpNo_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNm = this.txbEmpNm.Text;

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetUserList(strEmpNm);
                }

                rptResult3.DataSource = ds.Tables[0];
                rptResult3.DataBind();

                for (int i = 0; i < rptResult3.Items.Count; i++)
                {
                    Button btnConfirm = (Button)rptResult3.Items[i].FindControl("btnConfirm");

                    AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
                    trigger.ControlID = btnConfirm.UniqueID;
                    trigger.EventName = "Click";
                    updPanelModal.Triggers.Add(trigger);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region btnConfirm_Click
        /// <summary>
        /// btnConfirm_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnConfirm = (Button)sender;
                Label lblOrgFullNm = (Label)btnConfirm.Parent.FindControl("lblOrgFullNm");
                Label lblEmpNm = (Label)btnConfirm.Parent.FindControl("lblEmpNm");
                Label lblEmpNo = (Label)btnConfirm.Parent.FindControl("lblEmpNo");

                string strOrgFullNm = lblOrgFullNm.Text;
                string strEmpNm = lblEmpNm.Text;
                string strEmpNo = lblEmpNo.Text;

                this.txbCompEmpNm.Text = strOrgFullNm + " " + strEmpNm;
                this.hfCompEmpNo.Value = strEmpNo;

                this.updPanelComp.Update();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalClose", "ModalClose();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region ddlSvcType_SelectedIndexChanged
        /// <summary>
        /// ddlSvcType_SelectedIndexChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void ddlSvcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strScript = "";

                string strPassType = this.ddlPassType.SelectedValue;
                string strSvcType = this.ddlSvcType.SelectedValue;

                if (strPassType.Equals("COMP"))
                {                
                    if (strSvcType.Equals("직원"))
                    {
                        strScript = "$('#thCustNm').hide();$('#tdCustNm').hide();$('#thCustTel').hide();$('#tdCustTel').hide();$('#tdComp').show();$('#thConsult').show();";
                    }
                    else
                    {
                        strScript = "$('#thCustNm').show();$('#tdCustNm').show();$('#thCustTel').show();$('#tdCustTel').show();$('#tdComp').hide();$('#thConsult').hide();";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "controlResize", strScript, true);
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region initializeControls
        /// <summary>
        /// initializeControls
        /// </summary>
        private void initializeControls()
        {
            DataSet dsSvc = null;
            DataSet dsConsult = null;

            try
            {
                if (!IsPostBack)
                {
                    string strEmpNo = this.Session["EMPNO"].ToString();

                    using (PassMgmt pass = new PassMgmt())
                    {
                        dsSvc = pass.GetPassSvcList();
                        dsConsult = pass.GetPassBizAuthist(strEmpNo);
                    }

                    if (dsSvc.Tables.Count > 0)
                    {
                        DtSvc = dsSvc.Tables[0];
                    }

                    if (dsConsult.Tables.Count > 0)
                    {
                        DtConsult = dsConsult.Tables[0];
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (dsSvc != null)
                    dsSvc.Dispose();

                if (dsConsult != null)
                    dsConsult.Dispose();
            }
        }
        #endregion

        #region SetDropDownList
        /// <summary>
        /// SetDropDownList
        /// </summary>
        private void SetDropDownList()
        {
            try
            {
                this.ddlSvcType.Items.Clear();
                this.ddlConsult.Items.Clear();

                string strPassType = this.ddlPassType.SelectedValue;
                string strWhere = string.Format("PASSTYPE = '{0}'", strPassType);

                DataRow[] drSvc = DtSvc.Select(strWhere);

                if (!strPassType.Equals("COMP"))
                {
                    this.ddlSvcType.Items.Add(new ListItem("선택하세요", ""));

                    this.txbRelType.Text = "";
                }
                else
                {
                    this.txbRelType.Text = "";
                }

                foreach (DataRow dr in drSvc)
                {
                    this.ddlSvcType.Items.Add(new ListItem(dr["SVCTYPE"].ToString(), dr["SVCTYPE"].ToString()));
                }

                this.ddlSvcType.SelectedIndex = 0;

                if (strPassType.Equals("MOT"))
                {
                    this.ddlConsult.Visible = false;
                    this.ddlMotType.Visible = true;
                }
                else if (strPassType.Equals("BIZ"))
                {
                    this.ddlConsult.Visible = true;
                    this.ddlMotType.Visible = false;

                    this.ddlConsult.Items.Add(new ListItem("선택하세요", ""));

                    foreach (DataRow dr in DtConsult.Rows)
                    {
                        this.ddlConsult.Items.Add(new ListItem(dr["AUTHOPERNM"].ToString(), dr["AUTHOPERATOR"].ToString()));
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "controlResize", "javascript:controlResize('" + strPassType + "');", true);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strCustNm">string</param>
        /// <param name="strCustTelNo">string</param>
        /// <param name="strHopeDt">string</param>
        /// <param name="strRelType">string</param>
        /// <param name="strPassType">string</param>
        /// <param name="strSvcType">string</param>
        /// <param name="strCompEmpNo">string</param>
        /// <param name="strMotType">string</param>
        /// <param name="strMsg">ref string</param>
        /// <param name="strMsg">ref string</param>
        /// <returns></returns>
        private bool ValidationCheck(string strCustNm, string strCustTelNo, string strHopeDt, string strRelType, string strPassType, string strSvcType, string strCompEmpNo, string strConsult, string strMotType, ref string strMsg)
        {
            bool bResult = true;

            try
            {
                if (strPassType.Length == 0)
                {
                    strMsg = "잘못된 접근입니다.";
                    bResult = false;
                }
                else if (strCustNm.Length == 0 && !(strPassType.Equals("COMP") && strSvcType.Equals("직원")))
                {
                    strMsg = "성명을 입력해주세요.";
                    bResult = false;
                }
                else if (strCustTelNo.Length == 0 && !(strPassType.Equals("COMP") && strSvcType.Equals("직원")))
                {
                    strMsg = "TEL을 입력해주세요.";
                    bResult = false;
                }
                else if (strRelType.Length == 0)
                {
                    strMsg = "관계를 입력해주세요.";
                    bResult = false;
                }
                else if (strHopeDt.Length == 0)
                {
                    strMsg = "일자를 선택해주세요.";
                    bResult = false;
                }
                else if (strSvcType.Length == 0)
                {
                    if (strPassType.Equals("COMP"))
                    {
                        strMsg = "관계를 선택해주세요.";
                    }
                    else
                    {
                        strMsg = "희망상품을 선택해주세요.";
                    }
                    bResult = false;
                }
                else if (strPassType.Equals("COMP") && strSvcType.Equals("직원") && strCompEmpNo.Length == 0)
                {
                    strMsg = "칭찬 대상자를 선택해주세요.";
                    bResult = false;
                }
                else if (strPassType.Equals("BIZ") && strConsult.Length == 0)
                {
                    strMsg = "컨설팅을 선택해주세요.";
                    bResult = false;
                }
                else if (strPassType.Equals("MOT") && strMotType.Length == 0)
                {
                    strMsg = "타입를 선택해주세요.";
                    bResult = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bResult;
        }
        #endregion

        #endregion
    }
}