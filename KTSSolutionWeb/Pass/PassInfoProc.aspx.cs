﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Pass;
using System.Linq;

namespace KTSSolutionWeb
{
    public partial class PassInfoProc : PageBase
    {
        #region fields

        #region DtOrgList
        /// <summary>
        /// DtOrgList
        /// </summary>
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }
        #endregion

        #region DtAuthList
        /// <summary>
        /// DtAuthList
        /// </summary>
        private DataTable DtAuthList
        {
            get
            {
                if (ViewState["DtAuthList"] != null)
                    return (DataTable)ViewState["DtAuthList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtAuthList"] = value;
            }
        }
        #endregion

        #region UPPERYN
        /// <summary>
        /// UPPERYN
        /// </summary>
        private string UPPERYN
        {
            get
            {
                if (ViewState["UPPERYN"] != null)
                    return ViewState["UPPERYN"].ToString();
                else
                    return null;
            }
            set
            {
                ViewState["UPPERYN"] = value;
            }
        }
        #endregion

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetOrgList();

                SetPassAuthList();

                this.txbTeam.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");
                this.btnOrg.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");

                this.txbStDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.txbFinMonth.Text = DateTime.Now.AddMonths(-1).ToString("yyyy-MM");
            }
        }
        #endregion

        #region ddlPassType_SelectedIndexChanged
        /// <summary>
        /// ddlPassType_SelectedIndexChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void ddlPassType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strPassType = this.ddlPassType.SelectedValue;
                string strAuthOper = "";
                UPPERYN = "N";

                this.txbTeam.Attributes.Remove("onclick");
                this.btnOrg.Attributes.Remove("onclick");

                if (strPassType.Equals(""))
                {
                    this.hfAuthOrgCd.Value = "";
                    this.txbTeam.Text = "";
                    this.txbTeam.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");
                    this.btnOrg.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");
                }
                else
                {
                    DataRow[] drUpper = DtAuthList.Select(string.Format("PASSTYPE = '{0}' AND UPPERYN = '{1}'", strPassType, "Y"));

                    if (strPassType.Equals("BIZ") || drUpper.Length > 0)
                    {
                        strAuthOper = Session["EMPNO"].ToString();
                    }
                    else
                    {
                        strAuthOper = Session["DEPTCD"].ToString();
                    }

                    DataRow[] dr = DtAuthList.Select(string.Format("PASSTYPE = '{0}' AND AUTHOPERATOR = '{1}'", strPassType, strAuthOper));

                    string strOrgCd = "";

                    for (int i = 0; i < dr.Length; i++)
                    {
                        if(i == 0)
                            UPPERYN = dr[i]["UPPERYN"].ToString();

                        if (strOrgCd.Length > 0)
                        {
                            strOrgCd += ",";
                        }

                        strOrgCd += dr[i]["ORGCD"].ToString();
                    }

                    this.hfAuthOrgCd.Value = strOrgCd;
                    this.txbTeam.Text = Utility.GetOrgNm(strOrgCd, DtOrgList);

                    this.txbTeam.Attributes.Add("onclick", "javascript: PopupPassOrgTree('" + strPassType + "', '" + strAuthOper + "', '" + UPPERYN + "');");
                    this.btnOrg.Attributes.Add("onclick", "javascript: PopupPassOrgTree('" + strPassType + "', '" + strAuthOper + "', '" + UPPERYN + "');");

                    //월마감 작업예정 버튼도 추가 할것

                    if (UPPERYN.Equals("Y"))
                    {
                        txbFinMonth.Text = DateTime.Now.AddMonths(-1).ToString("yyyy-MM");
                        btnFinMonth.Enabled = true;
                        btnFinMonthCancel.Enabled = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show", "$('#spanFinMonth').show();", true);
                    }
                    else
                    {
                        txbFinMonth.Text = "";
                        btnFinMonth.Enabled = false;
                        btnFinMonthCancel.Enabled = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show", "$('#spanFinMonth').hide();", true);
                    }
                }

                this.updPanelAuthOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnSelect_ServerClick
        /// <summary>
        /// btnSelect_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnExcel_ServerClick
        /// <summary>
        /// btnExcel_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    string strPassType = this.ddlPassType.SelectedValue;
                    string strOrgCd = this.hfAuthOrgCd.Value;
                    string strEmpNo = Request.Form[this.txbEmpNo.UniqueID];
                    string strStdt = Request.Form[this.txbStDt.UniqueID];
                    string strEndt = Request.Form[this.txbEnDt.UniqueID];

                    strStdt = strStdt + " 00:00:00";
                    strEndt = strEndt + " 23:59:59";

                    string strPassStat = this.ddlPassStat.SelectedValue;
                    string strSuccYn = this.ddlSuccYn.SelectedValue;

                    string strTrtEmpNo = Session["EMPNO"].ToString();


                    using (PassMgmt pass = new PassMgmt())
                    {
                        ds = pass.GetPassProcList(strPassType, strOrgCd, strEmpNo, strStdt, strEndt, strPassStat, strSuccYn, strTrtEmpNo);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    string strPassType = this.ddlPassType.SelectedValue;

                    excel.HeaderColumn = GetHeaderColumn(strPassType);
                    excel.HeaderTop = GetHeaderTop(strPassType);
                    excel.HeaderBom = GetHeaderBottom(strPassType);

                    excel.ExcelDownLoad(this.Page, dt, "PASS 현황");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }
        #endregion

        #region paging_PreRender
        /// <summary>
        /// paging_PreRender
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    string strPassType = this.ddlPassType.SelectedValue;
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        bool bChoice = true;

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblProcId = (Label)rptResult.Items[i].FindControl("lblProcId");
                            Label lblPassType = (Label)rptResult.Items[i].FindControl("lblPassType");
                            Label lblMonthFin = (Label)rptResult.Items[i].FindControl("lblMonthFin");
                            
                            HtmlTableCell tdComp = (HtmlTableCell)rptResult.Items[i].FindControl("tdComp");
                            HtmlTableCell tdCompTel = (HtmlTableCell)rptResult.Items[i].FindControl("tdCompTel");
                            HtmlTableCell tdSvcType = (HtmlTableCell)rptResult.Items[i].FindControl("tdSvcType");
                            HtmlTableCell tdHopeDt = (HtmlTableCell)rptResult.Items[i].FindControl("tdHopeDt");
                            HtmlTableCell tdNoti1 = (HtmlTableCell)rptResult.Items[i].FindControl("tdNoti1");
                            HtmlTableCell tdMotType = (HtmlTableCell)rptResult.Items[i].FindControl("tdMotType");
                            HtmlTableCell tdPassStat = (HtmlTableCell)rptResult.Items[i].FindControl("tdPassStat");
                            HtmlTableCell tdEmpType = (HtmlTableCell)rptResult.Items[i].FindControl("tdEmpType");
                            HtmlTableCell tdCreateDt = (HtmlTableCell)rptResult.Items[i].FindControl("tdCreateDt");
                            HtmlTableCell tdShootingDt = (HtmlTableCell)rptResult.Items[i].FindControl("tdShootingDt");
                            
                            Label lblCustNm = (Label)rptResult.Items[i].FindControl("lblCustNm"); 
                            Label lblCustSignYn = (Label)rptResult.Items[i].FindControl("lblCustSignYn");
                            Label lblImgData = (Label)rptResult.Items[i].FindControl("lblImgData");
                            Label lblNoti1 = (Label)rptResult.Items[i].FindControl("lblNoti1");
                            Label lblPassStat = (Label)rptResult.Items[i].FindControl("lblPassStat");
                            Label lblSuccYn = (Label)rptResult.Items[i].FindControl("lblSuccYn");
                            DropDownList ddlSuccYn = (DropDownList)rptResult.Items[i].FindControl("ddlSuccYn");
                            Label lblEmpType = (Label)rptResult.Items[i].FindControl("lblEmpType");
                            DropDownList ddlEmpType = (DropDownList)rptResult.Items[i].FindControl("ddlEmpType");
                            HtmlTableCell tdPassChoice = (HtmlTableCell)rptResult.Items[i].FindControl("tdPassChoice");
                            Button btnPassChoice = (Button)rptResult.Items[i].FindControl("btnPassChoice");
                            Label lblPassChoiceYn = (Label)rptResult.Items[i].FindControl("lblPassChoiceYn");
                            HtmlTableCell tdTrtEmpNo = (HtmlTableCell)rptResult.Items[i].FindControl("tdTrtEmpNo");
                            HtmlTableCell tdDelete = (HtmlTableCell)rptResult.Items[i].FindControl("tdDelete");
                            Label lblTrtEmpNo = (Label)rptResult.Items[i].FindControl("lblTrtEmpNo");
                            HtmlButton btnTrtEmpNo = (HtmlButton)rptResult.Items[i].FindControl("btnTrtEmpNo");

                            Button btnSave = (Button)rptResult.Items[i].FindControl("btnSave");
                            Button btnDelete = (Button)rptResult.Items[i].FindControl("btnDelete");

                            string strMonthFin = lblMonthFin.Text;
                            string strSuccYn = lblSuccYn.Text;
                            ddlSuccYn.SelectedValue = strSuccYn;

                            string strPassTypeT = lblPassType.Text;
                            string strProcId = lblProcId.Text;

                            string strEmpType = lblEmpType.Text;
                            ddlEmpType.SelectedValue = strEmpType;

                            string strTrtEmpNo = lblTrtEmpNo.Text;

                            btnTrtEmpNo.Attributes.Remove("onclick");

                            string strNoti1 = lblNoti1.Text.Replace("\r\n", "<br/>");
                            tdNoti1.InnerHtml = strNoti1;

                            if (strTrtEmpNo.Length > 0)
                            {
                                lblPassChoiceYn.Text = "선택완료";
                            }
                            else
                            {
                                lblPassChoiceYn.Text = "선택안됨";
                            }

                            if (strMonthFin.Equals("Y"))
                            {
                                if (strPassType.Equals("COMP"))
                                {
                                    tdMotType.Visible = false;
                                    tdEmpType.Visible = false;

                                    tdComp.Visible = true;
                                    tdCompTel.Visible = true;
                                    tdSvcType.Visible = false;
                                    tdHopeDt.Visible = false;
                                    tdPassStat.Visible = false;
                                    tdCreateDt.Visible = false;
                                    tdShootingDt.Visible = false;

                                    tdPassChoice.Visible = true;
                                    tdPassChoice.Visible = false;
                                }
                                else
                                {
                                    if (strPassType.Equals("MOT"))
                                    {
                                        tdMotType.Visible = true;
                                        tdEmpType.Visible = true;
                                        lblEmpType.Visible = true;
                                        ddlEmpType.Visible = false;
                                        ddlEmpType.Enabled = false;
                                    }
                                    else
                                    {
                                        tdMotType.Visible = false;
                                        tdEmpType.Visible = false;

                                        lblEmpType.Visible = false;
                                        ddlEmpType.Visible = false;
                                        ddlEmpType.Enabled = false;
                                    }
                                    tdComp.Visible = false;
                                    tdCompTel.Visible = false;
                                    tdSvcType.Visible = true;
                                    tdHopeDt.Visible = true;
                                    tdPassStat.Visible = true;
                                    tdCreateDt.Visible = true;
                                    tdShootingDt.Visible = true;

                                    tdPassChoice.Visible = true;
                                    lblPassChoiceYn.Visible = true;
                                }

                                btnPassChoice.Visible = false;
                                btnPassChoice.Enabled = false;

                                btnSave.Enabled = false;
                                btnSave.Visible = false;

                                btnDelete.Enabled = false;
                                btnDelete.Visible = false;
                            }
                            else
                            {
                                AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();
                                asyncBtnSave.ControlID = btnSave.UniqueID;
                                asyncBtnSave.EventName = "Click";

                                updPanel1.Triggers.Add(asyncBtnSave);

                                if (strPassType.Equals("COMP"))
                                {
                                    tdMotType.Visible = false;
                                    tdEmpType.Visible = false;

                                    lblEmpType.Visible = false;
                                    ddlEmpType.Visible = false;
                                    ddlEmpType.Enabled = false;

                                    tdComp.Visible = true;
                                    tdCompTel.Visible = true;
                                    tdSvcType.Visible = false;
                                    tdHopeDt.Visible = false;
                                    tdPassStat.Visible = false;
                                    tdCreateDt.Visible = false;
                                    tdShootingDt.Visible = false;

                                    tdPassChoice.Visible = false;
                                    btnPassChoice.Visible = false;
                                    btnPassChoice.Enabled = false;
                                    lblPassChoiceYn.Visible = false;

                                    tdTrtEmpNo.Visible = false;

                                    if (UPPERYN.Equals("Y"))
                                    {
                                        tdDelete.Visible = true;
                                        btnDelete.Enabled = true;
                                        btnDelete.Visible = true;

                                        AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                                        asyncBtnDelete.ControlID = btnDelete.UniqueID;
                                        asyncBtnDelete.EventName = "Click";

                                        updPanel1.Triggers.Add(asyncBtnDelete);
                                    }
                                    else
                                    {
                                        tdDelete.Visible = false;
                                        btnDelete.Enabled = false;
                                    }
                                }
                                else
                                {
                                    if (strPassType.Equals("MOT"))
                                    {
                                        tdMotType.Visible = true;
                                        tdEmpType.Visible = true;

                                        if (UPPERYN.Equals("Y"))
                                        {
                                            lblEmpType.Visible = false;
                                            ddlEmpType.Visible = true;
                                            ddlEmpType.Enabled = true;
                                        }
                                        else
                                        {
                                            lblEmpType.Visible = true;
                                            ddlEmpType.Visible = false;
                                            ddlEmpType.Enabled = false;
                                        }
                                    }
                                    else
                                    {
                                        tdMotType.Visible = false;
                                        tdEmpType.Visible = false;

                                        lblEmpType.Visible = false;
                                        ddlEmpType.Visible = false;
                                        ddlEmpType.Enabled = false;
                                    }

                                    tdComp.Visible = false;
                                    tdCompTel.Visible = false;
                                    tdSvcType.Visible = true;
                                    tdHopeDt.Visible = true;
                                    tdPassStat.Visible = true;
                                    tdCreateDt.Visible = true;
                                    tdShootingDt.Visible = true;

                                    if (UPPERYN.Equals("Y"))
                                    {
                                        tdPassChoice.Visible = false;

                                        btnPassChoice.Visible = false;
                                        btnPassChoice.Enabled = false;
                                        lblPassChoiceYn.Visible = false;

                                        tdTrtEmpNo.Visible = true;

                                        string strEmpNo = Session["EMPNO"].ToString();

                                        btnTrtEmpNo.Attributes.Add("onclick", "PopupPassUser('" + strPassTypeT + "', '" + strEmpNo + "', '" + strProcId + "');");

                                        tdDelete.Visible = true;
                                        btnDelete.Enabled = true;
                                        btnDelete.Visible = true;

                                        AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                                        asyncBtnDelete.ControlID = btnDelete.UniqueID;
                                        asyncBtnDelete.EventName = "Click";

                                        updPanel1.Triggers.Add(asyncBtnDelete);
                                    }
                                    else
                                    {
                                        tdPassChoice.Visible = true;

                                        if (strTrtEmpNo.Length > 0)
                                        {
                                            lblPassChoiceYn.Visible = true;
                                            btnPassChoice.Visible = false;
                                            btnPassChoice.Enabled = false;
                                        }
                                        else
                                        {
                                            if (bChoice)
                                            {
                                                lblPassChoiceYn.Visible = false;
                                                btnPassChoice.Visible = true;
                                                btnPassChoice.Enabled = true;
                                                bChoice = false;
                                            }
                                            else
                                            {
                                                lblPassChoiceYn.Visible = true;
                                                btnPassChoice.Visible = false;
                                                btnPassChoice.Enabled = false;
                                            }
                                        }

                                        tdTrtEmpNo.Visible = false;

                                        tdDelete.Visible = false;
                                        btnDelete.Enabled = false;

                                        AsyncPostBackTrigger asyncBtnChoice = new AsyncPostBackTrigger();
                                        asyncBtnChoice.ControlID = btnPassChoice.UniqueID;
                                        asyncBtnChoice.EventName = "Click";

                                        updPanel1.Triggers.Add(asyncBtnChoice);
                                    }
                                }
                            }

                            if (!strPassType.Equals("COMP"))
                            {
                                string strCustSignYn = lblCustSignYn.Text;
                                string strImgData = lblImgData.Text;

                                if (strCustSignYn.Equals("Y"))
                                {
                                    lblCustNm.Attributes.Add("onmouseover", "ModalCustSign('show', '" + strImgData + "', event, this);");
                                    lblCustNm.Attributes.Add("onmouseout", "ModalCustSign('hide', '" + strImgData + "', event, this);");
                                }
                            }

                            updPanel1.Update();
                        }
                    }
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region hfAuthOrgCd_ValueChanged
        /// <summary>
        /// hfAuthOrgCd_ValueChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void hfAuthOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strOrgCd = this.hfAuthOrgCd.Value.Split(',');
                string strOrgNm = "";
                string strWhere = "";

                for (int i = 0; i < strOrgCd.Length; i++)
                {
                    if (strWhere.Length > 0)
                        strWhere += " OR ";

                    strWhere += string.Format("ORGCD = '{0}'", strOrgCd[i]);
                }

                DataRow[] dr = DtOrgList.Select(strWhere);
                DataTable dt = DtOrgList.Clone();

                if (dr.Length > 0)
                {
                    dt = dr.CopyToDataTable();
                }

                if (dt.Rows.Count == 1)
                {
                    strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                }
                else
                {
                    foreach (DataRow drOrg in dt.Rows)
                    {
                        if (strOrgNm.Length > 0)
                            strOrgNm += ", ";

                        strOrgNm += drOrg["ORGNM"].ToString();
                    }
                }

                this.txbTeam.Text = strOrgNm;

                this.updPanelAuthOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnPassChoice_Click
        /// <summary>
        /// btnPassChoice_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnPassChoice_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnPassChoice = (Button)sender;
                Label lblProcId = (Label)btnPassChoice.Parent.FindControl("lblProcId");
                Label lblMonthFin = (Label)btnPassChoice.Parent.FindControl("lblMonthFin");

                Label lblPassChoiceYn = (Label)btnPassChoice.Parent.FindControl("lblPassChoiceYn");
                Label lblTrtEmpNo = (Label)btnPassChoice.Parent.FindControl("lblTrtEmpNo");

                string strProcID = lblProcId.Text;
                string strMonthFin = lblMonthFin.Text;

                if (strProcID.Length == 0 || strMonthFin.Equals("Y") || !btnPassChoice.Enabled)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    lblTrtEmpNo.Text = Session["EMPNO"].ToString();

                    lblPassChoiceYn.Text = "선택";
                    lblPassChoiceYn.Visible = true;
                    btnPassChoice.Visible = false;
                    btnPassChoice.Enabled = false;

                    this.updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region hfTrtEmpInfo_ValueChanged
        /// <summary>
        /// hfTrtEmpInfo_ValueChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void hfTrtEmpInfo_ValueChanged(object sender, EventArgs e)
        {
            //담당자 배정 작업예정
            try
            {
                string[] strTrtEmpInfo = this.hfTrtEmpInfo.Value.Split('|');

                string strProcIdValue = "";
                string strEmpNo = "";
                string strEmpNm = "";

                if (strTrtEmpInfo.Length > 2)
                {
                    strProcIdValue = strTrtEmpInfo[0];
                    strEmpNo = strTrtEmpInfo[1];
                    strEmpNm = strTrtEmpInfo[2];
                }

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    Label lblProcId = (Label)rptResult.Items[i].FindControl("lblProcId");
                    Label lblTrtEmpNo = (Label)rptResult.Items[i].FindControl("lblTrtEmpNo");
                    Label lblTrtEmpNm = (Label)rptResult.Items[i].FindControl("lblTrtEmpNm");

                    string strProcId = lblProcId.Text;

                    if (strProcId.Equals(strProcIdValue))
                    {
                        lblTrtEmpNo.Text = strEmpNo;
                        lblTrtEmpNm.Text = strEmpNm;
                        break;
                    }
                }

                this.hfTrtEmpInfo.Value = "";

                this.updPanel1.Update();
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnSave_ServerClick
        /// <summary>
        /// btnSave_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblProcId = (Label)btnSave.Parent.FindControl("lblProcId");
                Label lblMonthFin = (Label)btnSave.Parent.FindControl("lblMonthFin");
                Label lblPassType = (Label)btnSave.Parent.FindControl("lblPassType");

                DropDownList ddlSuccYn = (DropDownList)btnSave.Parent.FindControl("ddlSuccYn");
                Label lblTrtEmpNo = (Label)btnSave.Parent.FindControl("lblTrtEmpNo");
                DropDownList ddlEmpType = (DropDownList)btnSave.Parent.FindControl("ddlEmpType");
                TextBox txbNoti2 = (TextBox)btnSave.Parent.FindControl("txbNoti2");

                string strProcID = lblProcId.Text;
                string strMonthFin = lblMonthFin.Text;
                string strPassType = lblPassType.Text;

                string strSuccYn = ddlSuccYn.SelectedValue;
                string strTrtEmpNo = lblTrtEmpNo.Text;
                string strTrtEmpType = ddlEmpType.SelectedValue;
                string strNoti2 = txbNoti2.Text;

                if (strProcID.Length == 0 || strMonthFin.Equals("Y") || !btnSave.Visible)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else if (strTrtEmpNo.Equals("") && !strSuccYn.Equals("") && !strPassType.Equals("COMP"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('담당자가 지정되지 않은 상태입니다.');", true);
                }
                else
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.SetPassProcInfo(strProcID, strTrtEmpNo, strSuccYn, strNoti2, strTrtEmpType);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('PASS 정보가 수정되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('PASS 정보 저장 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnDelete_ServerClick
        /// <summary>
        /// btnDelete_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnDelete = (Button)sender;
                Label lblProcId = (Label)btnDelete.Parent.FindControl("lblProcId");
                Label lblMonthFin = (Label)btnDelete.Parent.FindControl("lblMonthFin");

                string strProcID = lblProcId.Text;
                string strMonthFin = lblMonthFin.Text;

                if (lblProcId.Text.Length == 0 || strMonthFin.Equals("Y"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.DelPassProcess(strProcID);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('PASS 정보가 삭제되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('PASS 정보 삭제 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnFinMonth_Click
        /// <summary>
        /// btnFinMonth_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnFinMonth_Click(object sender, EventArgs e)
        {
            try
            {
                string strFinMonth = Request.Form[this.txbFinMonth.UniqueID];
                DateTime FinMonth = DateTime.Parse(strFinMonth);
                DateTime NowMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                if (!UPPERYN.Equals("Y"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else if (FinMonth >= NowMonth)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('지난 달만 월마감 할 수 있습니다.');", true);
                }
                else
                {
                    string strPassType = this.ddlPassType.SelectedValue;
                    string strEmpNo = Session["EMPNO"].ToString();
                    string strStDt = FinMonth.ToString("yyyy-MM") + "-01 00:00:00";
                    string strEnDt = FinMonth.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + " 23:59:59";

                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.SetPassProcMonthFin(strPassType, strEmpNo, strStDt, strEnDt, "Y");
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "MonthFinish", "alert('월마감 되었습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('월마감 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnFinMonthCancel_Click
        /// <summary>
        /// btnFinMonthCancel_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnFinMonthCancel_Click(object sender, EventArgs e)
        {
            try
            {
                string strFinMonth = Request.Form[this.txbFinMonth.UniqueID];
                DateTime FinMonth = DateTime.Parse(strFinMonth);
                DateTime NowMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                if (!UPPERYN.Equals("Y"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else if (FinMonth >= NowMonth)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('지난 달만 월마감 취소 할 수 있습니다.');", true);
                }
                else
                {
                    string strPassType = this.ddlPassType.SelectedValue;
                    string strEmpNo = Session["EMPNO"].ToString();
                    string strStDt = FinMonth.ToString("yyyy-MM") + "-01 00:00:00";
                    string strEnDt = FinMonth.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + " 23:59:59";

                    using (PassMgmt pass = new PassMgmt())
                    {
                        pass.SetPassProcMonthFin(strPassType, strEmpNo, strStDt, strEnDt, "N");
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "MonthFinish", "alert('월마감이 취소 되었습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('월마감취소 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region MenuTree_Load
        /// <summary>
        /// MenuTree_Load
        /// </summary>
        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region SetOrgList
        /// <summary>
        /// SetOrgList
        /// </summary>
        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region SetPassAuthList
        /// <summary>
        /// SetPassAuthList
        /// </summary>
        private void SetPassAuthList()
        {
            DataSet ds = new DataSet();
            try
            {
                string strEmpNo = Session["EMPNO"].ToString();

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetPassAuthList(strEmpNo);
                }

                if (ds.Tables.Count > 0)
                {
                    DtAuthList = ds.Tables[0];
                }

                if (DtAuthList.Rows.Count > 0)
                {
                    ddlPassType.Items.Add(new ListItem("선택하세요", ""));

                    DataTable dt = DtAuthList.AsEnumerable()
                        .GroupBy(
                            row => new 
                            { 
                                PASSTYPE = row.Field<string>("PASSTYPE"), 
                                PASSTYPENM = row.Field<string>("PASSTYPENM") 
                            })
                        .Select(g => 
                            { 
                                DataRow row = DtAuthList.NewRow();

                                row["PASSTYPE"] = g.Key.PASSTYPE;
                                row["PASSTYPENM"] = g.Key.PASSTYPENM;

                                return row;
                            }).CopyToDataTable();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ddlPassType.Items.Add(new ListItem(dt.Rows[i]["PASSTYPENM"].ToString(), dt.Rows[i]["PASSTYPE"].ToString()));
                    }
                }
                else
                {                   
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('PASS권한이 없습니다. 관리자에게 문의하세요.');window.location.href = '/Default';", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region GetDataList
        /// <summary>
        /// GetDataList
        /// </summary>
        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strPassType = this.ddlPassType.SelectedValue;
                string strOrgCd = this.hfAuthOrgCd.Value;
                string strEmpNo = Request.Form[this.txbEmpNo.UniqueID];
                string strStdt = Request.Form[this.txbStDt.UniqueID];
                string strEndt = Request.Form[this.txbEnDt.UniqueID];

                strStdt = strStdt + " 00:00:00";
                strEndt = strEndt + " 23:59:59";

                string strPassStat = this.ddlPassStat.SelectedValue;
                string strSuccYn = this.ddlSuccYn.SelectedValue;

                string strTrtEmpNo = Session["EMPNO"].ToString();

                if (strPassType.Equals("COMP"))
                {
                    this.thMotType.Visible = false;
                    this.thEmpType.Visible = false;

                    this.thComp.Visible = true;
                    this.thCompTel.Visible = true;
                    this.thSvcType.Visible = false;
                    this.thHopeDt.Visible = false;
                    this.thPassStat.Visible = false;
                    this.thCreateDt.Visible = false;
                    this.thShootingDt.Visible = false;

                    this.thPassChoice.Visible = false;
                    this.thTrtEmpChoice.Visible = false;

                    if (UPPERYN.Equals("Y"))
                    {
                        this.thdel.Visible = true;
                    }
                    else
                    {
                        this.thdel.Visible = false;
                    }
                }
                else
                {
                    if (strPassType.Equals("MOT"))
                    {
                        this.thMotType.Visible = true;
                        this.thEmpType.Visible = true;
                    }
                    else
                    {
                        this.thMotType.Visible = false;
                        this.thEmpType.Visible = false;
                    }

                    this.thComp.Visible = false;
                    this.thCompTel.Visible = false;
                    this.thSvcType.Visible = true;
                    this.thHopeDt.Visible = true;
                    this.thPassStat.Visible = true;
                    this.thCreateDt.Visible = true;
                    this.thShootingDt.Visible = true;

                    if (UPPERYN.Equals("Y"))
                    {
                        this.thPassChoice.Visible = false;
                        this.thTrtEmpChoice.Visible = true;
                        this.thdel.Visible = true;
                    }
                    else
                    {
                        this.thPassChoice.Visible = true;
                        this.thTrtEmpChoice.Visible = false;
                        this.thdel.Visible = false;
                    }
                }

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetPassProcList(strPassType, strOrgCd, strEmpNo, strStdt, strEndt, strPassStat, strSuccYn, strTrtEmpNo);
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region GetHeaderColumn
        /// <summary>
        /// GetHeaderColumn
        /// </summary>
        /// <returns>string[]</returns>
        private string[] GetHeaderColumn(string strPassType)
        {
            string[] ArrHeader = null;

            if (strPassType.Equals("MOT"))
            {
                ArrHeader = new string[20];

                ArrHeader[0] = "NUM";
                ArrHeader[1] = "PASSDT";
                ArrHeader[2] = "CUSTNM";
                ArrHeader[3] = "CUSTTELNO";
                ArrHeader[4] = "RELATIONTYPE";
                ArrHeader[5] = "ORGFULLNM";
                ArrHeader[6] = "EMPNM";
                ArrHeader[7] = "EMPNO";
                ArrHeader[8] = "PASSTYPENM";
                ArrHeader[9] = "SVCTYPE";
                ArrHeader[10] = "HOPEDT";
                ArrHeader[11] = "MOTTYPE";
                ArrHeader[12] = "NOTI1";
                ArrHeader[13] = "PASSSTATNM";
                ArrHeader[14] = "SUCCYN";
                ArrHeader[15] = "NOTI2";
                ArrHeader[16] = "TRTEMPTYPE";
                ArrHeader[17] = "CREATEDT";
                ArrHeader[18] = "SHOOTINGDT";
                ArrHeader[19] = "RESULTDT";
            }
            else if (strPassType.Equals("COMP"))
            {
                ArrHeader = new string[13];

                ArrHeader[0] = "NUM";
                ArrHeader[1] = "PASSDT";
                ArrHeader[2] = "COMPEMPNM";
                ArrHeader[3] = "COMPTELNO";
                ArrHeader[4] = "RELATIONTYPE";
                ArrHeader[5] = "ORGFULLNM";
                ArrHeader[6] = "EMPNM";
                ArrHeader[7] = "EMPNO";
                ArrHeader[8] = "PASSTYPENM";
                ArrHeader[9] = "NOTI1";
                ArrHeader[10] = "SUCCYN";
                ArrHeader[11] = "NOTI2";
                ArrHeader[12] = "RESULTDT";
            }
            else
            {
                ArrHeader = new string[18];

                ArrHeader[0] = "NUM";
                ArrHeader[1] = "PASSDT";
                ArrHeader[2] = "CUSTNM";
                ArrHeader[3] = "CUSTTELNO";
                ArrHeader[4] = "RELATIONTYPE";
                ArrHeader[5] = "ORGFULLNM";
                ArrHeader[6] = "EMPNM";
                ArrHeader[7] = "EMPNO";
                ArrHeader[8] = "PASSTYPENM";
                ArrHeader[9] = "SVCTYPE";
                ArrHeader[10] = "HOPEDT";
                ArrHeader[11] = "NOTI1";
                ArrHeader[12] = "PASSSTATNM";
                ArrHeader[13] = "SUCCYN";
                ArrHeader[14] = "NOTI2";
                ArrHeader[15] = "CREATEDT";
                ArrHeader[16] = "SHOOTINGDT";
                ArrHeader[17] = "RESULTDT";
            }

            return ArrHeader;
        }
        #endregion

        #region GetHeaderTop
        /// <summary>
        /// GetHeaderTop
        /// </summary>
        /// <returns></returns>
        private string[] GetHeaderTop(string strPassType)
        {
            string[] ArrHeader = null;

            if (strPassType.Equals("MOT"))
            {
                ArrHeader = new string[20];

                ArrHeader[0] = "No.";
                ArrHeader[1] = "PASS일";
                ArrHeader[2] = "고객명";
                ArrHeader[3] = "TEL";
                ArrHeader[4] = "관계";
                ArrHeader[5] = "부서";
                ArrHeader[6] = "사원명";
                ArrHeader[7] = "사번";
                ArrHeader[8] = "구분";
                ArrHeader[9] = "희망상품";
                ArrHeader[10] = "희망일자";
                ArrHeader[11] = "타입";
                ArrHeader[12] = "참고사항";
                ArrHeader[13] = "상태";
                ArrHeader[14] = "성공여부";
                ArrHeader[15] = "비고";
                ArrHeader[16] = "담당자타입";
                ArrHeader[17] = "PASS접수일";
                ArrHeader[18] = "슈팅일";
                ArrHeader[19] = "결과일";
            }
            else if (strPassType.Equals("COMP"))
            {
                ArrHeader = new string[13];

                ArrHeader[0] = "No.";
                ArrHeader[1] = "PASS일";
                ArrHeader[2] = "대상자";
                ArrHeader[3] = "대상자TEL";
                ArrHeader[4] = "관계";
                ArrHeader[5] = "부서";
                ArrHeader[6] = "사원명";
                ArrHeader[7] = "사번";
                ArrHeader[8] = "구분";
                ArrHeader[9] = "참고사항";
                ArrHeader[10] = "성공여부";
                ArrHeader[11] = "비고";
                ArrHeader[12] = "결과일";
            }
            else
            {
                ArrHeader = new string[18];

                ArrHeader[0] = "No.";
                ArrHeader[1] = "PASS일";
                ArrHeader[2] = "고객명";
                ArrHeader[3] = "TEL";
                ArrHeader[4] = "관계";
                ArrHeader[5] = "부서";
                ArrHeader[6] = "사원명";
                ArrHeader[7] = "사번";
                ArrHeader[8] = "구분";
                ArrHeader[9] = "희망상품";
                ArrHeader[10] = "희망일자";
                ArrHeader[11] = "참고사항";
                ArrHeader[12] = "상태";
                ArrHeader[13] = "성공여부";
                ArrHeader[14] = "비고";
                ArrHeader[15] = "PASS접수일";
                ArrHeader[16] = "슈팅일";
                ArrHeader[17] = "결과일";
            }

            return ArrHeader;
        }
        #endregion

        #region GetHeaderBottom
        /// <summary>
        /// GetHeaderBottom
        /// </summary>
        /// <returns>string[]</returns>
        private string[] GetHeaderBottom(string strPassType)
        {
            string[] ArrHeader = new string[0];

            return ArrHeader;
        }
        #endregion

        #endregion
    }
}