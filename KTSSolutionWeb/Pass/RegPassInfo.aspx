﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegPassInfo.aspx.cs" Inherits="KTSSolutionWeb.RegPassInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Pass등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />

    <script type="text/javascript">     
        function RegChk() {
            var strPasstype = $("#<%= ddlPassType.ClientID %>  option:selected").val();

            if (strPasstype == "") {
                alert("구분을 선택해주세요.");
                return false;
            }
            else {
                if (confirm("PASS를 저장 하시겠습니까?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function controlResize(obj) {
            
            if (obj == "COMP") {
                $("#thCustNm").hide();
                $("#tdCustNm").hide();
                $("#thCustTel").hide();
                $("#tdCustTel").hide();
                $("#tdHopeDt").attr("colspan", 3);
                $("#thRel").hide();
                $("#tdRel").hide();
             
                $("#thHopeDt").text("일자");
                $("#thSvc").text("관계");
                $("#thConsult").show();
                $("#thConsult").text("대상자");

                $("#tdConsult").hide();
                $("#tdComp").show();
			}
            else {
                $("#thCustNm").show();
                $("#tdCustNm").show();
                $("#thCustTel").show();
                $("#tdCustTel").show();
                $("#tdHopeDt").attr("colspan", 1);
                $("#thRel").show();
                $("#tdRel").show();

                $("#thHopeDt").text("희망일자");
                $("#thSvc").text("희망상품");

                if (obj == "MOT") {
                    $("#thConsult").show();
                    $("#tdConsult").show();
                    $("#thConsult").text("타입");
                }
                else if (obj == "BIZ") {
                    $("#thConsult").show();
                    $("#tdConsult").show();
                    $("#thConsult").text("컨설팅");
                }
                else {
                    $("#thConsult").hide();
                    $("#tdConsult").hide();
                }

                $("#tdComp").hide();
            }
        }

        function ModalShow() {
            $("#divModal").show();
            $("#<%= txbEmpNm.ClientID %>").focus();
            return false;
        }

        function ModalClose() {
            $("#divModal").hide();
            return false;
        }

        function EmpNmCheck() {

            var CompEmpNm = $("#<%= txbEmpNm.ClientID %>").val();

            if (CompEmpNm == "") {
                alert("사원명을 입력해주세요.");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</head>
<body id="divBody">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-organization -->
            <div class="windowpop pop-clothes-request">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>PASS 등록</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self);">닫기</button>

                    <!-- S:popcontents -->
                    <div class="popcontents" style="margin-bottom:20px;max-height:740px;overflow-y:auto;">

                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <colgroup>
                                        <col style="width:10%"/>
                                        <col style="width:40%"/>
                                        <col style="width:10%"/>
                                        <col style="width:40%"/>
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th>구분</th>
                                            <td style="text-align:left;">
                                                <p class="optionbox">
								                    <asp:DropDownList ID="ddlPassType" runat="server" style="width:92% !important" AutoPostBack="true" OnSelectedIndexChanged="ddlPassType_SelectedIndexChanged">
									                    <asp:ListItem Text="선택하세요" Value="" Selected="True"></asp:ListItem>
									                    <asp:ListItem Text="MOT" Value="MOT"></asp:ListItem>
									                    <asp:ListItem Text="신사업" Value="BIZ"></asp:ListItem>
									                    <asp:ListItem Text="공간" Value="SPACE"></asp:ListItem>
									                    <asp:ListItem Text="칭찬" Value="COMP"></asp:ListItem>
								                    </asp:DropDownList>
                                                </p>
                                            </td>
                                            <th id="thSvc">희망상품</th>
                                            <td style="text-align:left;">
						                        <asp:UpdatePanel ID="updPanelSvc" runat="server" UpdateMode="Conditional">
							                        <ContentTemplate>
                                                        <p class="optionbox">
								                            <asp:DropDownList ID="ddlSvcType" runat="server" style="width:92% !important" AutoPostBack="true" OnSelectedIndexChanged="ddlSvcType_SelectedIndexChanged">
								                            </asp:DropDownList>
                                                        </p>
                                                    </ContentTemplate>
							                        <Triggers>
								                        <asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="ddlSvcType" EventName="SelectedIndexChanged" />
							                        </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thCustNm">성명</th>
                                            <td id="tdCustNm" style="text-align:left;">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCustNm" runat="server" MaxLength="50" style="width:92% !important"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th id="thCustTel">TEL</th>
                                            <td id="tdCustTel" style="text-align:left;">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbTelNo" runat="server" MaxLength="20" style="width:92% !important" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thHopeDt">희망일자</th>
                                            <td id="tdHopeDt" style="text-align:left;">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbHopeDt" runat="server" class="date" ReadOnly="true" style="width:120px !important"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th id="thRel">관계</th>
                                            <td id="tdRel" style="text-align:left;">
                                                <p class="inpbox">
								                    <asp:TextBox ID="txbRelType" runat="server" MaxLength="50" style="width:92% !important"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thConsult" style="display:none;">컨설팅</th>
                                            <td id="tdConsult" colspan="3" style="display:none;text-align:left;">
						                        <asp:UpdatePanel ID="updPanelConsult" runat="server" UpdateMode="Conditional">
							                        <ContentTemplate>
                                                        <p class="optionbox">
								                            <asp:DropDownList ID="ddlConsult" runat="server" Visible="false" style="width:96% !important">
                                                            </asp:DropDownList>
								                            <asp:DropDownList ID="ddlMotType" runat="server" Visible="false" style="width:50% !important">
									                            <asp:ListItem Text="B" Value="B"></asp:ListItem>
									                            <asp:ListItem Text="P" Value="P"></asp:ListItem>
								                            </asp:DropDownList>
                                                        </p>
							                        </ContentTemplate>
							                        <Triggers>
								                        <asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
							                        </Triggers>
						                        </asp:UpdatePanel>
                                            </td>
                                            <td id="tdComp" colspan="3" style="display:none;text-align:left;">
						                        <asp:UpdatePanel ID="updPanelComp" runat="server" UpdateMode="Conditional">
							                        <ContentTemplate>
                                                        <p class="inpbox">
								                            <asp:TextBox ID="txbCompEmpNm" runat="server" ReadOnly="true" style="width:96.5% !important"></asp:TextBox>
								                            <asp:HiddenField ID="hfCompEmpNo" runat="server" />
							                            </p>
                                                    </ContentTemplate>
							                        <Triggers>
								                        <asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />					
							                        </Triggers>
						                        </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>참고사항</th>
                                            <td colspan="3" style="text-align:left;">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbNoti" runat="server" Width="96.5%" Height="240px" MaxLength="8000" TextMode="MultiLine"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>	
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
			
            <div id="divModal" style="display:none;position:absolute;width:100%;height:750px;left:0;top:0%;background-color:gray; border-radius:10px;">
				<div class="scrollbox" style="position:absolute;margin-left:2%;width:98%;height:420px;top:20%;overflow-y:auto; border-radius:10px;">
                    <div class="requestlist" style="text-align:center;background-color:rgb(255, 255, 255);padding-bottom:10px;">
					    <strong style="margin-top:2px;margin-right:10px;margin-bottom:10px;float:left;">사원 조회</strong>
					    <span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						    <label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">사원명</label>
						    <asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
						    <asp:Button id="btnEmpNm" runat="server" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;color:white" OnClientClick="return EmpNmCheck();" OnClick="btnEmpNo_Click" Text="조회" />
					    </span>
                        <div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						    <asp:updatepanel id="updPanelModal" runat="server" UpdateMode="Conditional">
							    <ContentTemplate>	
								    <table>
									    <thead>
										    <th>부서</th>
										    <th>성명</th>
										    <th>직책</th>
										    <th></th>
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptResult3" runat="server">
											    <ItemTemplate>
												    <tr>
													    <td>
														    <asp:Label ID="lblOrgFullNm" runat="server" Text='<%# Eval("ORGFULLNM") %>'></asp:Label>
													    </td>
													    <td>
														    <asp:Label ID="lblEmpNm" runat="server" Text='<%# Eval("EMPNM") %>'></asp:Label>
														    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>' Visible="false"></asp:Label>														
													    </td>
													    <td><%# Eval("TITLENM") %></td>
													    <td>
														    <asp:Button id="btnConfirm" runat="server" class="btn-save" Text="선택" OnClick="btnConfirm_Click"/>
													    </td>
												    </tr>
											    </ItemTemplate>
										    </asp:Repeater>
									    </tbody>
								    </table>
							    </ContentTemplate>
							    <Triggers>
								    <asp:AsyncPostBackTrigger ControlID="btnEmpNm" EventName="Click" />
							    </Triggers>
						    </asp:updatepanel>
                        </div>
					    <asp:Button id="btnModalClose" runat="server" class="btn-black" BorderStyle="None" style="width:33px;margin:0 !important;" OnClientClick="return ModalClose();" Text="닫기" />
				    </div>
                </div>
			</div>
			<div class="list-top">
				<div style="text-align:center;">
                    <asp:Button id="btnReg" runat="server" OnClientClick="return RegChk();" OnClick="btnRegPass_Click" class="btn-green" Text="등록" />
                </div>
            </div>
        </div> 
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
