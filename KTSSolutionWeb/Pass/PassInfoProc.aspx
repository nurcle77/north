﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PassInfoProc.aspx.cs" Inherits="KTSSolutionWeb.PassInfoProc" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function PopupPassOrgTree(passtype, operator, upperyn) {
            
            var orgcd = $("#<%= hfAuthOrgCd.ClientID %>").val();

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pPASSTYPE: passtype,
                pOPERATOR: operator,
                pUPPERYN: upperyn
            };

            var Popupform = createForm("/Common/OrgTree_Pass", param);

            Popupform.target = "OrgTree_PassAuth";
            pop3 = window.open("", "OrgTree_PassAuth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop3.focus();
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupPassUser(passtype, empno, procid) {

            var sPtype = "EMPNO";

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pPASSTYPE: passtype,
                pEMPNO: empno,
                pPTYPE: sPtype,
                pPROCID: procid
            };

            var Popupform = createForm("/Common/SearchPassOper", param);

            Popupform.target = "SearchPassOper";
            var win = window.open("", "SearchPassOper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function RegPassInfo() {
            var nWidth = 700;
            var nHeight = 750;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Pass/RegPassInfo", null);

            Popupform.target = "RegPassInfo";
            var win = window.open("", "RegPassInfo", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetPassOrgCd(orgcd) {

            this.focus();
            
            $("#<%= hfAuthOrgCd.ClientID %>").val(orgcd);
            __doPostBack("<%=updPanelAuthOrg.ClientID %>", "");
        }

        function SetOperCd(opercd, opernm, procid) {

            this.focus();

            $("#<%= hfTrtEmpInfo.ClientID %>").val(procid + "|" + opercd + "|" + opernm);
            __doPostBack("<%=updPanel1.ClientID %>", "");
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=txbEmpNo.ClientID %>").val(empno);
            $("#<%=txbEmpNm.ClientID %>").val(empnm);
        }

        function btnFinMonthCheck() {
            if (confirm("월마감 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnFinMonthCancelCheck() {
            if (confirm("당일 마감한 PASS 건 만 취소 됩니다. 월마감을 취소 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        } 

        function btnSearchCheck() {
            var passtype = $("#<%= ddlPassType.ClientID %> option:selected").val();

            if (passtype == "") {
                alert("구분을 선택해주세요");
                return false;
            }
            else {
                return true;
            }
        }

        function btnPassChoiceCheck() {
            if (confirm("PASS를 선택 하시겠습니끼?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnSaveCheck() {
            if (confirm("PASS 정보를 저장하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDelCheck() {
            if (confirm("PASS 정보를 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ModalCustSign(stype, imgval, event, obj) {
            var img = $("#imgSign");

            if (stype == "show") {  
                img.attr("src", imgval);

                var curX = event.pageX;
                var curY = event.pageY - 200;

                $("#divModal").css({
                    top: curY,
                    left: curX
                });
                $("#divModal").show();
            }
            else {
                img.src = "";
                $("#divModal").hide();
            }
            return false;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>   
				<span class="optionbox">
                    <asp:UpdatePanel ID="updPanelPassType" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>구분</label>
                            <asp:DropDownList ID="ddlPassType" style="width:150px;" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlPassType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>    
                <span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelAuthOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                            <asp:HiddenField ID="hfAuthOrgCd" runat="server" OnValueChanged="hfAuthOrgCd_ValueChanged"  />
				            <button id="btnOrg" runat="server" type="button" class="btn-plus" title="검색">+</button>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPassType" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfAuthOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelEmp" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>사번</label>
                            <asp:TextBox ID="txbEmpNo" runat="server" Width="100px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
					        <label>이름</label>
                            <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
				<span id="spanFinMonth" class="inpbox" style="float:right; text-align:right;display:none;" >
					<label>마감 년월</label>
                    <asp:TextBox ID="txbFinMonth" runat="server" class="month" Width="120px" ReadOnly="true"></asp:TextBox>
                    <asp:Button id="btnFinMonth" runat="server" OnClientClick="return btnFinMonthCheck();" OnClick="btnFinMonth_Click" class="btn-black" style="float:right;color:white;width:100px;margin-left:10px" Text="월마감" />
                    <asp:Button id="btnFinMonthCancel" runat="server" OnClientClick="return btnFinMonthCancelCheck();" OnClick="btnFinMonthCancel_Click" class="btn-black last" style="float:right;color:white;width:100px;margin-left:10px" Text="마감취소" />
                </span>
                <br />
                <span class="inpbox">
					<label>일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span>
				<span class="optionbox">
					<label>상태</label>
                    <asp:DropDownList ID="ddlPassStat" style="width:250px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pass 대기중" Value="P"></asp:ListItem>
                        <asp:ListItem Text="Shooting 진행중" Value="S"></asp:ListItem>
                        <asp:ListItem Text="Shooting 완료" Value="F"></asp:ListItem>
                    </asp:DropDownList>
                </span>
				<span class="optionbox">
					<label>성공여부</label>
                    <asp:DropDownList ID="ddlSuccYn" style="width:100px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="성공" Value="Y"></asp:ListItem>
                        <asp:ListItem Text="실패" Value="N"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return btnSearchCheck();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조직별 조회 결과</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <button id="btnRegPass" type="button" onclick="RegPassInfo();" class="btn-green">등록</button>
                        <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <th>No.</th>
                                <th>PASS일</th>
                                <th>고객명</th>
                                <th>TEL</th>
                                <th id="thComp" runat="server">대상자</th>
                                <th id="thCompTel" runat="server">대상자TEL</th>
                                <th>관계</th>
                                <th>부서</th>
                                <th>사원명</th>
                                <th>사번</th>
                                <th>구분</th>
                                <th id="thSvcType" runat="server">희망상품</th>
                                <th id="thHopeDt" runat="server">희망일자</th>
                                <th id="thMotType" runat="server">타입</th>
                                <th>참고사항</th>
                                <th id="thPassStat" runat="server">상태</th>
                                <th>성공여부</th>
                                <th>비고</th>
                                <th id="thEmpType" runat="server">담당자타입</th>
                                <th id="thCreateDt" runat="server">PASS접수일</th>
                                <th id="thShootingDt" runat="server">슈팅일</th>
                                <th>결과일</th>
                                <th id="thPassChoice" runat="server" visible="true">PASS건 선택</th>
                                <th id="thTrtEmpChoice" runat="server" visible="false">담당자지정</th>
                                <th>저장</th>
                                <th id="thdel" runat="server" visible="false">삭제</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%# Eval("NUM") %>
                                                <asp:Label ID="lblProcId" runat="server" Text='<%# Eval("PROCID") %>' Visible="false"></asp:Label>   
                                                <asp:Label ID="lblMonthFin" runat="server" Text='<%# Eval("MONTHFIN") %>' Visible="false"></asp:Label>   
                                                <asp:Label ID="lblPassType" runat="server" Text='<%# Eval("PASSTYPE") %>' Visible="false"></asp:Label>     
                                            </td>
                                            <td><%# Eval("PASSDT") %></td>
                                            <td>
                                                <asp:Label ID="lblCustNm" runat="server" Text='<%# Eval("CUSTNM") %>'></asp:Label>  
                                                <asp:Label ID="lblCustSignYn" runat="server" Text='<%# Eval("CUSTSIGNYN") %>' Visible="false"></asp:Label> 
                                                <asp:Label ID="lblImgData" runat="server" Text='<%# Eval("IMGDATA") %>' Visible="false"></asp:Label> 
                                            </td>
                                            <td><%# Eval("CUSTTELNO") %></td>
                                            <td id="tdComp" runat="server"><%# Eval("COMPEMPNM") %></td>
                                            <td id="tdCompTel" runat="server"><%# Eval("COMPTELNO") %></td>
                                            <td><%# Eval("RELATIONTYPE") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td><%# Eval("EMPNO") %></td>
                                            <td><%# Eval("PASSTYPENM") %></td>
                                            <td id="tdSvcType" runat="server"><%# Eval("SVCTYPE") %></td>
                                            <td id="tdHopeDt" runat="server"><%# Eval("HOPEDT") %></td>
                                            <td id="tdMotType" runat="server"><%# Eval("MOTTYPE") %></td>
                                            <td id="tdNoti1" runat="server" style="text-align:left">
                                                <asp:Label ID="lblNoti1" runat="server" Text='<%# Eval("NOTI1") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td id="tdPassStat" runat="server">
                                                <%# Eval("PASSSTATNM") %>
                                                <asp:Label ID="lblPassStat" runat="server" Text='<%# Eval("PASSSTAT") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSuccYn" runat="server" Text='<%# Eval("SUCCYN") %>' Visible="false"></asp:Label>
                                                <span class="optionbox">
                                                    <asp:DropDownList ID="ddlSuccYn" runat="server">
                                                        <asp:ListItem Text="선택" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="성공" Value="Y"></asp:ListItem>
                                                        <asp:ListItem Text="실패" Value="N"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="txtbox">
                                                    <asp:TextBox ID="txbNoti2" runat="server" Text='<%# Eval("Noti2") %>'></asp:TextBox>
                                                </div>
                                            </td>
                                            <td id="tdEmpType" runat="server">
                                                <asp:Label ID="lblEmpType" runat="server" Text='<%# Eval("TRTEMPTYPE") %>' Visible="true"></asp:Label>
                                                <span class="optionbox">
                                                    <asp:DropDownList ID="ddlEmpType" runat="server" Enabled="false" Visible="false">
                                                        <asp:ListItem Text="선택" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                        <asp:ListItem Text="P" Value="P"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </span>
                                            </td>
                                            <td id="tdCreateDt" runat="server"><%# Eval("CREATEDT") %></td>
                                            <td id="tdShootingDt" runat="server"><%# Eval("SHOOTINGDT") %></td>
                                            <td><%# Eval("RESULTDT") %></td>
                                            <td id="tdPassChoice" runat="server">
                                                <asp:Label ID="lblPassChoiceYn" runat="server" Visible="false"></asp:Label>
                                                <asp:Button id="btnPassChoice" runat="server" OnClientClick="return btnPassChoiceCheck();" OnClick="btnPassChoice_Click" class="btn-save" Text="선택" />
                                            </td>
                                            <td id="tdTrtEmpNo" runat="server" visible="false">
                                                <asp:Label ID="lblTrtEmpNo" runat="server" Visible="false" Text='<%# Eval("TRTEMPNO") %>'></asp:Label>
                                                <asp:Label ID="lblTrtEmpNm" runat="server" Text='<%# Eval("TRTEMPNM") %>'></asp:Label>
                                                <button id="btnTrtEmpNo" runat="server" type="button" class="btn-plus" style="padding-left:5px;">+</button>
                                            </td>
                                            <td>
                                                <asp:Button id="btnSave" runat="server" OnClientClick="return btnSaveCheck();" OnClick="btnSave_ServerClick" class="btn-save" Text="저장" />
                                            </td>
                                            <td id="tdDelete" runat="server" visible="false">
                                                <asp:Button id="btnDelete" runat="server" OnClientClick="return btnDelCheck();" OnClick="btnDelete_ServerClick" class="btn-del" Text="삭제" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hfTrtEmpInfo" runat="server" OnValueChanged="hfTrtEmpInfo_ValueChanged" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="btnFinMonth" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="btnFinMonthCancel" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="hfTrtEmpInfo" EventName="ValueChanged" />
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
        <div id="divModal" style="display:none;position:absolute;width:300px;height:200px;background:rgb(255, 255, 255); border-width:2px;border-color:black;border-radius:10px;">
            <img id="imgSign" src="" style="border:1px solid black; width:99%; height:99%;" />
        </div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>
