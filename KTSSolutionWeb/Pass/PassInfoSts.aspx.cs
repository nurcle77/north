﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Pass;
using System.Linq;

namespace KTSSolutionWeb
{
    public partial class PassInfoSts : PageBase
    {
        #region fields

        #region DtOrgList
        /// <summary>
        /// DtOrgList
        /// </summary>
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }
        #endregion

        #region DtAuthList
        /// <summary>
        /// DtAuthList
        /// </summary>
        private DataTable DtAuthList
        {
            get
            {
                if (ViewState["DtAuthList"] != null)
                    return (DataTable)ViewState["DtAuthList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtAuthList"] = value;
            }
        }
        #endregion

        #region UPPERYN
        /// <summary>
        /// UPPERYN
        /// </summary>
        private string UPPERYN
        {
            get
            {
                if (ViewState["UPPERYN"] != null)
                    return ViewState["UPPERYN"].ToString();
                else
                    return null;
            }
            set
            {
                ViewState["UPPERYN"] = value;
            }
        }
        #endregion

        #region SELECTTYPE
        /// <summary>
        /// SELECTTYPE
        /// </summary>
        private string SELECTTYPE
        {
            get
            {
                if (ViewState["SELECTTYPE"] != null)
                    return ViewState["SELECTTYPE"].ToString();
                else
                    return null;
            }
            set
            {
                ViewState["SELECTTYPE"] = value;
            }
        }
        #endregion

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetOrgList();

                SetPassAuthList();

                this.txbTeam.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");
                this.btnOrg.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");

                this.txbStDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        #endregion

        #region ddlPassType_SelectedIndexChanged
        /// <summary>
        /// ddlPassType_SelectedIndexChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void ddlPassType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strPassType = this.ddlPassType.SelectedValue;
                string strAuthOper = "";
                UPPERYN = "N";

                this.txbTeam.Attributes.Remove("onclick");
                this.btnOrg.Attributes.Remove("onclick");

                if (strPassType.Equals(""))
                {
                    this.hfAuthOrgCd.Value = "";
                    this.txbTeam.Text = "";
                    this.txbTeam.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");
                    this.btnOrg.Attributes.Add("onclick", "javascript: alert('구분을 선택은 필수입니다.');");
                }
                else
                {
                    DataRow[] drUpper = DtAuthList.Select(string.Format("PASSTYPE = '{0}' AND UPPERYN = '{1}'", strPassType, "Y"));

                    if (strPassType.Equals("BIZ") || drUpper.Length > 0)
                    {
                        strAuthOper = Session["EMPNO"].ToString();
                    }
                    else
                    {
                        strAuthOper = Session["DEPTCD"].ToString();
                    }

                    DataRow[] dr = DtAuthList.Select(string.Format("PASSTYPE = '{0}' AND AUTHOPERATOR = '{1}'", strPassType, strAuthOper));

                    string strOrgCd = "";

                    for (int i = 0; i < dr.Length; i++)
                    {
                        if(i == 0)
                            UPPERYN = dr[i]["UPPERYN"].ToString();

                        if (strOrgCd.Length > 0)
                        {
                            strOrgCd += ",";
                        }

                        strOrgCd += dr[i]["ORGCD"].ToString();
                    }

                    this.hfAuthOrgCd.Value = strOrgCd;
                    this.txbTeam.Text = Utility.GetOrgNm(strOrgCd, DtOrgList);

                    this.txbTeam.Attributes.Add("onclick", "javascript: PopupPassOrgTree('" + strPassType + "', '" + strAuthOper + "', '" + UPPERYN + "');");
                    this.btnOrg.Attributes.Add("onclick", "javascript: PopupPassOrgTree('" + strPassType + "', '" + strAuthOper + "', '" + UPPERYN + "');");

                }

                this.updPanelAuthOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnSelect_ServerClick
        /// <summary>
        /// btnSelect_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnExcel_ServerClick
        /// <summary>
        /// btnExcel_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;
            DataTable dtExcel = null;

            try
            {
                string strSelectType = "";
                bool bExcelChk = true;

                if (paging.Dt != null)
                {
                    strSelectType = SELECTTYPE;
                    dt = paging.Dt.Copy();
                }
                else
                {
                    string strPassType = this.ddlPassType.SelectedValue;
                    string strOrgCd = this.hfAuthOrgCd.Value;
                    string strEmpNo = Request.Form[this.txbEmpNo.UniqueID];
                    string strStdt = Request.Form[this.txbStDt.UniqueID];
                    string strEndt = Request.Form[this.txbEnDt.UniqueID];

                    bool bRbDate = rbDate.Checked;
                    bool bValChk = true;

                    if (bRbDate)
                    {
                        strStdt = strStdt + " 00:00:00";
                        strEndt = strEndt + " 23:59:59";
                    }
                    else
                    {
                        strStdt = DateTime.Now.ToString("yyyy-MM") + "-01 00:00:00";
                        strEndt = strEndt + " 23:59:59";

                        if (!strStdt.Substring(0, 5).Equals(strEndt.Substring(0, 5)))
                        {
                            bValChk = false;
                        }
                    }

                    strSelectType = chkLv1.Checked ? "1LV" : chkLv2.Checked ? "2LV" : chkLv3.Checked ? "3LV" : chkLv4.Checked ? "TRTEMPNO" : "EMPNO";

                    if (bValChk)
                    {
                        using (PassMgmt pass = new PassMgmt())
                        {
                            if (SELECTTYPE.Equals("EMPNO"))
                            {
                                //개인별 조회
                                this.thOrgNm.InnerText = "소속";
                                this.thEmpNo.InnerText = "사번";
                                this.thEmpNm.InnerText = "이름";

                                ds = pass.GetPassProcStsByEmpNo(strPassType, strOrgCd, strEmpNo, strStdt, strEndt);
                            }
                            else if (SELECTTYPE.Equals("TRTEMPNO"))
                            {
                                //담당자별 조회
                                this.thOrgNm.InnerText = "소속";
                                this.thEmpNo.InnerText = "담당자사번";
                                this.thEmpNm.InnerText = "담당자이름";

                                ds = pass.GetPassProcStsByTrtEmpNo(strPassType, strOrgCd, strEmpNo, strStdt, strEndt);
                            }
                            else
                            {
                                //레벨별 조회
                                this.thOrgNm.InnerText = "조직명";
                                this.thEmpNo.InnerText = "담당자사번";
                                this.thEmpNm.InnerText = "담당자이름";

                                ds = pass.GetPassProcStsByOrgLv(strPassType, strOrgCd, strEmpNo, strStdt, strEndt, strSelectType);
                            }

                            dt = ds.Tables[0];
                        }
                    }
                    else
                    {
                        bExcelChk = false;
                    }
                }

                if (bExcelChk)
                {
                    dtExcel = dt.Clone();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.NewRow();
                        dr = dt.Rows[i];

                        if (dr["PASSDT"].ToString().Equals("9999-12"))
                        {
                            dr["PASSDT"] = "";
                            dr["ORGNM"] = "계";
                        }

                        if (dr["PASSTYPENM"].ToString().Equals("") && dr["SVCTYPE"].ToString().Equals(""))
                        {
                            dr["PASSDT"] = "";
                        }

                        dtExcel.ImportRow(dr);
                    }

                    using (ExcelUtil excel = new ExcelUtil())
                    {
                        excel.HeaderColumn = GetHeaderColumn(strSelectType);
                        excel.HeaderTop = GetHeaderTop(strSelectType);
                        excel.HeaderBom = GetHeaderBottom();

                        excel.ExcelDownLoad(this.Page, dtExcel, "PASS 현황");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('당월 현재 체크 조회 시 이번 달만 엑셀 저장 가능합니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();

                if (dtExcel != null)
                    dtExcel.Dispose();
            }
        }
        #endregion

        #region paging_PreRender
        /// <summary>
        /// paging_PreRender
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            HtmlTableCell tdNo = (HtmlTableCell)rptResult.Items[i].FindControl("tdNo");
                            HtmlTableCell tdPassDt = (HtmlTableCell)rptResult.Items[i].FindControl("tdPassDt");
                            HtmlTableCell tdOrgNm = (HtmlTableCell)rptResult.Items[i].FindControl("tdOrgNm");
                            HtmlTableCell tdEmpNo = (HtmlTableCell)rptResult.Items[i].FindControl("tdEmpNo");
                            HtmlTableCell tdEmpNm = (HtmlTableCell)rptResult.Items[i].FindControl("tdEmpNm");
                            HtmlTableCell tdPassType = (HtmlTableCell)rptResult.Items[i].FindControl("tdPassType");
                            HtmlTableCell tdSvcType = (HtmlTableCell)rptResult.Items[i].FindControl("tdSvcType");

                            string strPassDt = tdPassDt.InnerText;
                            string strPassType = tdPassType.InnerText;
                            string strOrgNm = tdOrgNm.InnerText;

                            if (SELECTTYPE.Equals("EMPNO") || SELECTTYPE.Equals("TRTEMPNO"))
                            {
                                if (strPassDt.Equals("9999-12"))
                                {
                                    tdNo.ColSpan = 7;
                                    tdPassDt.Visible = false;
                                    tdOrgNm.Visible = false;
                                    tdEmpNo.Visible = false;
                                    tdEmpNm.Visible = false;
                                    tdPassType.Visible = false;
                                    tdSvcType.Visible = false;

                                    tdNo.InnerText = "계";
                                }
                                else if (strPassType.Equals(""))
                                {
                                    tdNo.ColSpan = 7;
                                    tdPassDt.Visible = false;
                                    tdOrgNm.Visible = false;
                                    tdEmpNo.Visible = false;
                                    tdEmpNm.Visible = false;
                                    tdPassType.Visible = false;
                                    tdSvcType.Visible = false;

                                    tdNo.InnerText = strOrgNm;
                                }
                            }
                            else
                            {
                                tdEmpNo.Visible = false;
                                tdEmpNm.Visible = false;

                                if (strPassDt.Equals("9999-12"))
                                {
                                    tdNo.ColSpan = 5;
                                    tdPassDt.Visible = false;
                                    tdOrgNm.Visible = false;
                                    tdPassType.Visible = false;
                                    tdSvcType.Visible = false;

                                    tdNo.InnerText = "계";
                                }
                                else if (strPassType.Equals(""))
                                {
                                    tdNo.ColSpan = 5;
                                    tdPassDt.Visible = false;
                                    tdOrgNm.Visible = false;
                                    tdPassType.Visible = false;
                                    tdSvcType.Visible = false;

                                    tdNo.InnerText = strOrgNm;
                                }
                            }

                            updPanel1.Update();
                        }
                    }
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region hfAuthOrgCd_ValueChanged
        /// <summary>
        /// hfAuthOrgCd_ValueChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void hfAuthOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strOrgCd = this.hfAuthOrgCd.Value.Split(',');
                string strOrgNm = "";
                string strWhere = "";

                for (int i = 0; i < strOrgCd.Length; i++)
                {
                    if (strWhere.Length > 0)
                        strWhere += " OR ";

                    strWhere += string.Format("ORGCD = '{0}'", strOrgCd[i]);
                }

                DataRow[] dr = DtOrgList.Select(strWhere);
                DataTable dt = DtOrgList.Clone();

                if (dr.Length > 0)
                {
                    dt = dr.CopyToDataTable();
                }

                if (dt.Rows.Count == 1)
                {
                    strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                }
                else
                {
                    foreach (DataRow drOrg in dt.Rows)
                    {
                        if (strOrgNm.Length > 0)
                            strOrgNm += ", ";

                        strOrgNm += drOrg["ORGNM"].ToString();
                    }
                }

                this.txbTeam.Text = strOrgNm;

                this.updPanelAuthOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region MenuTree_Load
        /// <summary>
        /// MenuTree_Load
        /// </summary>
        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region SetOrgList
        /// <summary>
        /// SetOrgList
        /// </summary>
        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region SetPassAuthList
        /// <summary>
        /// SetPassAuthList
        /// </summary>
        private void SetPassAuthList()
        {
            DataSet ds = new DataSet();
            try
            {
                string strEmpNo = Session["EMPNO"].ToString();

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetPassAuthList(strEmpNo);
                }

                if (ds.Tables.Count > 0)
                {
                    DtAuthList = ds.Tables[0];
                }

                if (DtAuthList.Rows.Count > 0)
                {
                    ddlPassType.Items.Add(new ListItem("선택하세요", ""));

                    DataTable dt = DtAuthList.AsEnumerable()
                        .GroupBy(
                            row => new 
                            { 
                                PASSTYPE = row.Field<string>("PASSTYPE"), 
                                PASSTYPENM = row.Field<string>("PASSTYPENM") 
                            })
                        .Select(g => 
                            { 
                                DataRow row = DtAuthList.NewRow();

                                row["PASSTYPE"] = g.Key.PASSTYPE;
                                row["PASSTYPENM"] = g.Key.PASSTYPENM;

                                return row;
                            }).CopyToDataTable();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ddlPassType.Items.Add(new ListItem(dt.Rows[i]["PASSTYPENM"].ToString(), dt.Rows[i]["PASSTYPE"].ToString()));
                    }
                }
                else
                {                   
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('PASS권한이 없습니다. 관리자에게 문의하세요.');window.location.href = '/Default';", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region GetDataList
        /// <summary>
        /// GetDataList
        /// </summary>
        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strPassType = this.ddlPassType.SelectedValue;
                string strOrgCd = this.hfAuthOrgCd.Value;
                string strEmpNo = Request.Form[this.txbEmpNo.UniqueID];
                string strStdt = Request.Form[this.txbStDt.UniqueID];
                string strEndt = Request.Form[this.txbEnDt.UniqueID];

                bool bRbDate = rbDate.Checked;
                bool bValChk = true;

                SELECTTYPE = chkLv1.Checked ? "1LV" : chkLv2.Checked ? "2LV" : chkLv3.Checked ? "3LV" : chkLv4.Checked ? "TRTEMPNO" : "EMPNO";

                if (bRbDate)
                {
                    strStdt = strStdt + " 00:00:00";
                    strEndt = strEndt + " 23:59:59";
                }
                else
                {
                    strStdt = DateTime.Now.ToString("yyyy-MM") + "-01 00:00:00";
                    strEndt = strEndt + " 23:59:59";

                    if (!strStdt.Substring(0, 5).Equals(strEndt.Substring(0, 5)))
                    {
                        bValChk = false;
                    }
                }

                if (bValChk)
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        if (SELECTTYPE.Equals("EMPNO"))
                        {
                            //개인별 조회
                            this.thOrgNm.InnerText = "소속";
                            this.thEmpNo.InnerText = "사번";
                            this.thEmpNm.InnerText = "이름";
                            this.thEmpNo.Visible = true;
                            this.thEmpNm.Visible = true;

                            ds = pass.GetPassProcStsByEmpNo(strPassType, strOrgCd, strEmpNo, strStdt, strEndt);
                        }
                        else if (SELECTTYPE.Equals("TRTEMPNO"))
                        {
                            //담당자별 조회
                            this.thOrgNm.InnerText = "소속";
                            this.thEmpNo.InnerText = "담당자사번";
                            this.thEmpNm.InnerText = "담당자이름";
                            this.thEmpNo.Visible = true;
                            this.thEmpNm.Visible = true;

                            ds = pass.GetPassProcStsByTrtEmpNo(strPassType, strOrgCd, strEmpNo, strStdt, strEndt);
                        }
                        else
                        {
                            //레벨별 조회
                            this.thOrgNm.InnerText = "조직명";
                            //this.thEmpNo.InnerText = "담당자사번";
                            //this.thEmpNm.InnerText = "담당자이름";
                            this.thEmpNo.Visible = false;
                            this.thEmpNm.Visible = false;

                            ds = pass.GetPassProcStsByOrgLv(strPassType, strOrgCd, strEmpNo, strStdt, strEndt, SELECTTYPE);
                        }
                    }

                    paging.PageNumber = 0;
                    paging.PageSize = 10;

                    paging.Dt = null;

                    if (ds.Tables.Count > 0)
                    {
                        paging.TotalRows = ds.Tables[0].Rows.Count;
                        paging.Dt = ds.Tables[0];
                    }

                    paging.SetPagingDataList();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('당월 현재 체크 조회 시 이번 달만 조회 가능합니다.');", true);
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region GetHeaderColumn
        /// <summary>
        /// GetHeaderColumn
        /// </summary>
        /// <returns>string[]</returns>
        private string[] GetHeaderColumn(string strSelectType)
        {
            string[] ArrHeader = null;

            if (strSelectType.Equals("EMPNO") || strSelectType.Equals("TRTEMPNO"))
            {
                ArrHeader = new string[11];

                ArrHeader[0] = "NUM";
                ArrHeader[1] = "PASSDT";
                ArrHeader[2] = "ORGNM";
                ArrHeader[3] = "EMPNO";
                ArrHeader[4] = "EMPNM";
                ArrHeader[5] = "PASSTYPENM";
                ArrHeader[6] = "SVCTYPE";
                ArrHeader[7] = "PASSCNT";
                ArrHeader[8] = "SHOOTINGCNT";
                ArrHeader[9] = "SUCCCNT";
                ArrHeader[10] = "FAILCNT";
            }
            else
            {
                ArrHeader = new string[9];

                ArrHeader[0] = "NUM";
                ArrHeader[1] = "PASSDT";
                ArrHeader[2] = "ORGNM";
                ArrHeader[3] = "PASSTYPENM";
                ArrHeader[4] = "SVCTYPE";
                ArrHeader[5] = "PASSCNT";
                ArrHeader[6] = "SHOOTINGCNT";
                ArrHeader[7] = "SUCCCNT";
                ArrHeader[8] = "FAILCNT";
            }

            return ArrHeader;
        }
        #endregion

        #region GetHeaderTop
        /// <summary>
        /// GetHeaderTop
        /// </summary>
        /// <returns></returns>
        private string[] GetHeaderTop(string strSelectType)
        {
            string[] ArrHeader = null;

            if (strSelectType.Equals("EMPNO") || strSelectType.Equals("TRTEMPNO"))
            {
                ArrHeader = new string[11];

                ArrHeader[0] = "순번";
                ArrHeader[1] = "년월";
                if (strSelectType.Equals("EMPNO"))
                {
                    ArrHeader[2] = "소속";
                    ArrHeader[3] = "사번";
                    ArrHeader[4] = "이름";
                }
                else if (strSelectType.Equals("TRTEMPNO"))
                {
                    ArrHeader[2] = "소속";
                    ArrHeader[3] = "담당자사번";
                    ArrHeader[4] = "담당자이름";
                }

                ArrHeader[5] = "상품구분";
                ArrHeader[6] = "상품";
                ArrHeader[7] = "PASS건";
                ArrHeader[8] = "슈팅건";
                ArrHeader[9] = "성공건";
                ArrHeader[10] = "실패건";

            }
            else
            {
                ArrHeader = new string[9];

                ArrHeader[0] = "순번";
                ArrHeader[1] = "년월";
                ArrHeader[2] = "조직명";
                ArrHeader[3] = "상품구분";
                ArrHeader[4] = "상품";
                ArrHeader[5] = "PASS건";
                ArrHeader[6] = "슈팅건";
                ArrHeader[7] = "성공건";
                ArrHeader[8] = "실패건";
            }

            return ArrHeader;
        }
        #endregion

        #region GetHeaderBottom
        /// <summary>
        /// GetHeaderBottom
        /// </summary>
        /// <returns>string[]</returns>
        private string[] GetHeaderBottom()
        {
            string[] ArrHeader = new string[0];

            return ArrHeader;
        }
        #endregion

        #endregion
    }
}