﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Runtime.CompilerServices;
using System.Text;

namespace KTSSolutionWeb
{
    public partial class SiteMainMaster : MasterPage
    {
        List<KtsMenuItem> menuitems { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            //SSL 적용시 처리
            if (!Request.Url.Port.Equals(443))
            {
                //Response.Redirect(Request.Url.ToString().Replace("hhttp:", "https:"));
            }

            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!GetPageAuthYn())
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/Default");
            }
            else
            {
                UserInfo userInfo = new UserInfo();

                userInfo.SetUserInfo(this.Page.Session["EMPNO"].ToString(), "", this.Page);

                if (!IsPostBack)
                {
                    SetMenuData();
                }
            }
        }

        #region GetPageAuthYn
        /// <summary>
        /// GetPageAuthYn
        /// </summary>
        /// <returns>bool</returns>
        public bool GetPageAuthYn()
        {
            DataSet ds = new DataSet();

            bool bPageAuth = false;

            try
            {
                string strEmpNo = Page.Session["EMPNO"].ToString();
                string strDefaultPage = "/Default";
                string strUrl = Request.Url.AbsolutePath.ToString();

                if (strDefaultPage.Equals(strUrl))
                {
                    bPageAuth = true;
                }
                else
                {
                    using (KTSUser biz = new KTSUser())
                    {
                        ds = biz.GetUserPageAuth(strEmpNo, strUrl);

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bPageAuth = ds.Tables[0].Rows[0]["PAGEAUTHYN"].ToString() == "Y" ? true : false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bPageAuth;
        }
        #endregion

        #region SetMenuData
        /// <summary>
        /// SetMenuData
        /// </summary>
        private void SetMenuData()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = Page.Session["EMPNO"].ToString();
                string strEmpNm = Page.Session["EMPNM"].ToString();

                strUserNm.InnerText = strEmpNm;

                using (KTSUser biz = new KTSUser())
                {
                    ds = biz.GetUserMenu(strEmpNo);

                    if (ds.Tables.Count > 0)
                    {
                        menuitems = new List<KtsMenuItem>();

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            int idx = 0;
                            int order = 0;
                            KtsMenuItem item = new KtsMenuItem(row);
                            if (!item.MENULEVEL.Equals("1"))
                            {
                                idx = menuitems.FindIndex(t => t.MENUID == item.UPPERMENUID);
                                order = menuitems.FindLastIndex(t => t.UPPERMENUID == item.UPPERMENUID);
                                if (order > -1)
                                {
                                    menuitems.Insert(order + 1, item);
                                }
                                else
                                {
                                    menuitems.Insert(idx + 1, item);
                                }
                            }
                            else
                            {
                                menuitems.Add(item);
                            }
                        }
                        
                        //menuitems = menuitems.OrderBy(t => t.UPPERMENUID).ToList();

                        StringBuilder strMenu = new StringBuilder();
                        int i = 0;
                        int lv2idx = 0;
                        foreach (KtsMenuItem item in menuitems)
                        {
                            if (item.MENULEVEL.Equals("1"))
                            {

                                i++;

                                if (i > 1)
                                {
                                    strMenu.AppendLine("</ul>");
                                    strMenu.AppendLine("</li>");
                                }

                                strMenu.AppendLine("<li class=\"menu-" + i.ToString() + "\">");
                                strMenu.AppendLine("<a href=\"#\">" + item.MENUNM + "</a>");
                                strMenu.AppendLine("<ul class=\"two-depth\">");
                            }
                            else
                            {
                                if (item.LEAFMENUYN.Equals("Y"))
                                {
                                    lv2idx = menuitems.FindLastIndex(t => t.UPPERMENUID == item.MENUID);
                                    strMenu.AppendLine("<li><a href=\"#\">" + item.MENUNM + "</a>");
                                    strMenu.AppendLine("<ul class=\"three-depth\">");
                                }
                                else
                                {
                                    strMenu.AppendLine("<li><a href=\"" + (item.URL.Length == 0 ? "#" : item.URL) + "\">" + item.MENUNM + "</a></li>");

                                    int nowIdx = menuitems.FindIndex(t => t.MENUID == item.MENUID);
                                    if (lv2idx == nowIdx)
                                    {
                                        strMenu.AppendLine("</ul>");
                                        strMenu.AppendLine("</li>");
                                    }
                                }
                            }
                        }

                        strMenu.AppendLine("</ul>");
                        strMenu.AppendLine("</li>");

                        ulMenuList.InnerHtml = strMenu.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if(ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        internal string GetMenuTree(string url)
        {
            KtsMenuItem menuItem = menuitems.Where(t => t.URL == url).First();

            string menuTree = GetMenuName(menuItem, menuItem.MENUNM);

            return menuTree;
        }

        private string GetMenuName(KtsMenuItem menuItem, string childName)
        {

            if (menuitems.Where(t => t.MENUID == menuItem.UPPERMENUID).Any())
            {
                KtsMenuItem parentMenu = menuitems.Where(t => t.MENUID == menuItem.UPPERMENUID).First();

                return GetMenuName(parentMenu, parentMenu.MENUNM + ">" + childName);
            }
            else
            {
                return childName;
            }
        }

        protected void aLogout_ServerClick(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("/Login");
        }
    }
}