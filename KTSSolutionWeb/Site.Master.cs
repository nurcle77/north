﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Runtime.CompilerServices;
using System.Text;

namespace KTSSolutionWeb
{
    public partial class SiteMaster : MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //SSL 적용시 처리
            if (!Request.Url.Port.Equals(443))
            {
                //Response.Redirect(Request.Url.ToString().Replace("hhttp:", "https:"));
            }

            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }
            
            if (!GetPageAuthYn())
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/Default");
            }
            else
            {
                UserInfo userInfo = new UserInfo();

                userInfo.SetUserInfo(this.Page.Session["EMPNO"].ToString(), "", this.Page);

                if (!IsPostBack)
                {
                    //SetMenuData();
                }
            }
        }

        #region GetPageAuthYn
        /// <summary>
        /// GetPageAuthYn
        /// </summary>
        /// <returns>bool</returns>
        private bool GetPageAuthYn()
        {
            DataSet ds = new DataSet();

            bool bPageAuth = false;

            try
            {
                string strEmpNo = Page.Session["EMPNO"].ToString();
                string strDefaultPage = "/Default";
                string strUrl = Request.Url.AbsolutePath.ToString();

                if (strDefaultPage.Equals(strUrl))
                {
                    bPageAuth = true;
                }
                else
                {
                    using (KTSUser biz = new KTSUser())
                    {
                        ds = biz.GetUserPageAuth(strEmpNo, strUrl);

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bPageAuth = ds.Tables[0].Rows[0]["PAGEAUTHYN"].ToString() == "Y" ? true : false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bPageAuth;
        }
        #endregion
    }
}