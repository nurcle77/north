﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class MobileDefault : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();


            if (!IsPostBack)
            {
                Response.Buffer = true;

                bool bCheckAuth = false;
                string strtoken = Request.QueryString["token"] == null ? "" : Request.QueryString["token"].ToString();
                string strcorpFlag = Request.QueryString["corpFlag"] == null ? "" : Request.QueryString["corpFlag"].ToString();

                // aes256 으로 포탈에서 받아오는  sso  토큰 값
                string strPortalSSOCookie = Request.QueryString["sso_token"] == null ? string.Empty : Request.QueryString["sso_token"].ToString();

                if ((strtoken != "" && strcorpFlag != "") || strPortalSSOCookie != "")
                {
                    UserInfo user = new UserInfo();
                    bCheckAuth = user.CheckMobileAuth(this.Page, strtoken, strcorpFlag, strPortalSSOCookie);
                }

                //UserInfo user = new UserInfo();
                //bCheckAuth = user.CheckMobileAuthTest(this.Page);

                PageUtility pageUtil = new PageUtility(this.Page);

                string strUrl = "/MobileMain";

                if (bCheckAuth)
                {
                    //메인 페이지로 이동
                    using (KTSUser ktsUser = new KTSUser())
                    {
                        ktsUser.InsertLoginLog(Page.Session["EMPNO"].ToString(), Page.Request.UserHostAddress, Page.Request.UserAgent);
                    }

                    strUrl = "/MobileMain";
                }
                else
                {
                    //로그인실패 페이지로 이동
                    strUrl = "/MobileLoginFail";
                }

                pageUtil.PageMove(strUrl);
            }
            //}
            //catch (Exception ex)
            //{
            //    KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            //}
        }
    }
}