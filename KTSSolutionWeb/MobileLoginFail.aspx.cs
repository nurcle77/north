﻿using System;
using System.Reflection;
using System.Web.UI;
using System.Web.SessionState;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class MobileLoginFail : PageBase
    {
        #region Fields
        #endregion


        #region Event

        #region Page_load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string strUserAgent = Request.UserAgent.ToLower();

                    if (strUserAgent.IndexOf("iphone") > -1 || strUserAgent.IndexOf("android") > -1)
                    {
                        btnRefer.Visible = false;
                    }
                    else
                    {
                        btnRefer.Text = "이전페이지";
                    }

                    updPanel1.Update();
                }
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region btnRefer_Click
        /// <summary>
        /// btnRefer_Click
        /// </summary>
        protected void btnRefer_Click(object sender, EventArgs e)
        {
            string referUrl = "";

            if (Request.UrlReferrer != null)
            {
                referUrl = Request.UrlReferrer.ToString();
            }
            else
            {
                string strUserAgent = Request.UserAgent.ToLower();

                if (strUserAgent.IndexOf("iphone") > -1 || strUserAgent.IndexOf("android") > -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "close", "window.close();", true);
                }
                else
                {
                    referUrl = "/Login";
                }
            }

            if (referUrl.Length > 0)
            {
                PageUtility pageUtil = new PageUtility(this.Page);

                pageUtil.PageMove(referUrl);
            }
        }
        #endregion

        #endregion
    }
}