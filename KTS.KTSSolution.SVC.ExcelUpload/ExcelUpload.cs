﻿using System;
using System.ComponentModel;
using System.ServiceProcess;
using System.Threading;
using System.Runtime.Remoting;
using System.Configuration;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Util;
using KTS.KTSSolution.BSL.ExcelImport;

namespace KTS.KTSSolution.SVC.ExcelUpload
{
    public partial class ExcelUpload : ServiceBase
    {
        #region fields

        private Timer timer;
        private bool bTimerStat = false;
        private bool bRunSvcStat = true;
        private ExcelUpload oSvc;

        #endregion

        #region Constructor

        public ExcelUpload()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region OnStart
        /// <summary>
        /// OnStart
        /// </summary>
        /// <param name="args">string[]</param>
        protected override void OnStart(string[] args)
        {
            string strConfig = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            RemotingConfiguration.Configure(strConfig, false);

            oSvc = new ExcelUpload();
            oSvc.Start();
        }
        #endregion

        #region OnStop
        /// <summary>
        /// OnStop
        /// </summary>
        protected override void OnStop()
        {
            if (bTimerStat)
            {
                timer.Dispose();
                bTimerStat = false;
            }
        }
        #endregion

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        public void Start()
        {
            int ntimerTick = 60 * 1000;

            try
            {
//#if DEBUG
//                StartService(null);
//#else
                int.TryParse(ConfigurationSettings.AppSettings["timerTick"], out ntimerTick);

                timer = new Timer(new TimerCallback(StartService), null, 100, ntimerTick * 1000);
                bTimerStat = true;
//#endif
            }
            catch(Exception ex)
            {
                KTSException.HandleSVCException(SubSystemType.KTSSolution, ex, this.GetType(), false);
            }
        }
        #endregion

        #region StartService
        /// <summary>
        /// StartService
        /// </summary>
        /// <param name="s">object</param>
        public void StartService(object s)
        {
            bool bResult = false;
            TimeStamp timestamp = new TimeStamp();

            try
            {
                timestamp.TimeStampStart();

                if (bRunSvcStat)
                {
                    bRunSvcStat = false;
                    timestamp.TimeStampEnd(this, "=====서비스 시작");

                    using (ExcelImport oXlsxImport = new ExcelImport())
                    {
                        bResult = oXlsxImport.XlsxImport();
                    }

                    bRunSvcStat = true;
                }
                else
                {
                    timestamp.TimeStampEnd(this, ">> 종전 서비스 수행중으로 서비스 종료.");
                }
            }
            catch (Exception ex)
            {
                bRunSvcStat = true;

                timestamp = new TimeStamp();
                timestamp.TimeStampStart();
                timestamp.TimeStampEnd(this, "=====오류 발생 >> " + ex.ToString());
                KTSException.HandleSVCException(SubSystemType.KTSSolution, ex, this.GetType(), false);
            }
            finally
            {
                timestamp = new TimeStamp();
                timestamp.TimeStampStart();
                timestamp.TimeStampEnd(this, "=====서비스종료 결과 >>" + bResult.ToString());
            }
        }
        #endregion

        #endregion
    }
}
