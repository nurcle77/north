﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Text;
using KTS.KTSSolution.Framework.SharedType;
using MySql.Data.MySqlClient;

namespace KTS.KTSSolution.Common.DAL
{
    public class DBConn : System.IDisposable
    {
        #region fields

        private MySqlConnection Conn;
        private SqlConnection ConnByMs;
        private int commandTimeout = 30;

        #endregion

        #region Constructor

        public DBConn()
        {
        }

        #endregion

        #region Method

        #region ExecuteDataSet(CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        /// <summary>
        /// ExecuteDataSet(CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        /// </summary>
        /// <param name="commandType">CommandType</param>
        /// <param name="cammandText">string</param>
        /// <param name="commandParameters">params MySqlParameter[]</param>
        /// <returns></returns>
        public DataSet ExecuteDataSet(CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        {            
            DataSet ds = null;
            MySqlDataAdapter da = null;
            MySqlCommand Comm = null;
            string strSqlConn = GetConnectionString("SQLServer-KTSSolution");

            Conn = new MySqlConnection(strSqlConn);

            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }

                Comm = new MySqlCommand(commandText)
                {
                    CommandType = commandType,
                    Connection = Conn,
                    CommandTimeout = this.commandTimeout
                };

                this.AttachParameters(Comm, commandParameters);

                ds = new DataSet();
                da = new MySqlDataAdapter(Comm);

                Conn.Open();
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (da != null)
                    da.Dispose();
                if (Comm != null)
                    Comm.Dispose();
                if (Conn != null)
                    Conn.Close();
            }

            return ds;
        }
        #endregion

        #region ExecuteReader(CommandType commandType, string cammandText, MySqlParameter[] parameters)
        /// <summary>
        /// ExecuteReader(CommandType commandType, string cammandText, MySqlParameter[] parameters)
        /// </summary>
        /// <param name="strCommand">CommandType</param>
        /// <param name="commandType">string</param>
        /// <param name="parameters">MySqlParameter[]</param>
        /// <returns></returns>
        public MySqlDataReader ExecuteReader(CommandType commandType, string cammandText, MySqlParameter[] parameters)
        {
            MySqlCommand Comm = null;
            MySqlDataReader reader = null;
            string strSqlConn = GetConnectionString("SQLServer-KTSSolution");

            Conn = new MySqlConnection(strSqlConn);

            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }

                Comm = new MySqlCommand(cammandText)
                {
                    Connection = Conn,
                    CommandType = commandType,
                    CommandTimeout = this.commandTimeout
                };

                this.AttachParameters(Comm, parameters);

                Conn.Open();
                reader = Comm.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (Comm != null)
                    Comm.Dispose();
                if (Conn != null)
                    Conn.Close();
            }

            return reader;
        }
        #endregion

        #region ExecuteNonQuery(CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        /// <summary>
        /// ExecuteNonQuery(CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        /// </summary>
        /// <param name="commandType">CommandType</param>
        /// <param name="commandText">string</param>
        /// <param name="commandParameters">params MySqlParameter[]</param>
        /// <returns></returns>
        public int ExecuteNonQuery(CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        {
            int iResult = 0;
            MySqlCommand Comm = null;
            string strSqlConn = GetConnectionString("SQLServer-KTSSolution");

            Conn = new MySqlConnection(strSqlConn);

            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }

                Comm = new MySqlCommand(commandText)
                {
                    CommandType = commandType,
                    Connection = Conn,
                    CommandTimeout = this.commandTimeout
                };

                AttachParameters(Comm, commandParameters);

                Conn.Open();
                iResult = Comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (Comm != null)
                    Comm.Dispose();
                if (Conn != null)
                    Conn.Close();
            }

            return iResult;
        }
        #endregion

        #region ExecuteMultiDataSet(List<CommonData> dataList, bool bSleep)
        /// <summary>
        /// ExecuteMultiDataSet
        /// </summary>
        /// <param name="dataList">List<CommonData></param>
        /// <returns>int</returns>
        public int ExecuteMultiDataSet(List<CommonData> dataList, bool bSleep)
        {
            int iResult = 0;
            MySqlCommand Comm = null;
            string strSqlConn = GetConnectionString("SQLServer-KTSSolution");

            Conn = new MySqlConnection(strSqlConn);

            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }

                Conn.Open();

                foreach (CommonData data in dataList)
                {
                    Comm = new MySqlCommand(data.strQry)
                    {
                        CommandType = data.commandType,
                        Connection = Conn,
                        CommandTimeout = this.commandTimeout
                    };

                    AttachParameters(Comm, data.parameters);

                    iResult = Comm.ExecuteNonQuery();

                    if (bSleep)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (Comm != null)
                    Comm.Dispose();
                if (Conn != null)
                    Conn.Close();
            }

            return iResult;
        }
        #endregion

        #region ExecuteBulk(string strTableName, string strFilePath)
        /// <summary>
        /// ExecuteBulk(string strTableName, string strFilePath)
        /// </summary>
        /// <param name="strTableName">string</param>
        /// <param name="strFilePath">string</param>
        public void ExecuteBulk(string strTableName, string strFilePath)
        {
            try
            {
                string strSqlConn = GetConnectionString("SQLServer-KTSSolution");

                Conn = new MySqlConnection(strSqlConn);

                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }

                Conn.Open();

                MySqlBulkLoader objBulk = new MySqlBulkLoader(Conn)
                {
                    TableName = strTableName,
                    Timeout = this.commandTimeout,
                    FieldTerminator = ",",
                    LineTerminator = "\r\n",
                    FileName = strFilePath,
                    NumberOfLinesToSkip = 1
                };

                objBulk.Load();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (Conn != null)
                    Conn.Close();
            }
        }
        #endregion

        #region ExecuteDataSetByMsSql(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        /// <summary>
        /// ExecuteDataSetByMsSql(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        /// </summary>
        /// <param name="commandType">CommandType</param>
        /// <param name="cammandText">string</param>
        /// <param name="commandParameters">params SqlParameter[]</param>
        /// <returns></returns>
        public DataSet ExecuteDataSetByMsSql(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            DataSet ds = null;
            SqlDataAdapter da = null;
            SqlCommand Comm = null;
            string strSqlConn = "DSN=kts_north;Uid=im_user;Pwd=unierp@im#5;";//GetConnectionString("SQLServer-KTSERP");

            OdbcConnection ConnByMs = new OdbcConnection(strSqlConn);

            try
            {
                if (ConnByMs.State == ConnectionState.Open)
                {
                    ConnByMs.Close();
                }

                //Comm = new SqlCommand(commandText)
                //{
                //    CommandType = commandType,
                //    Connection = ConnByMs,
                //    CommandTimeout = this.commandTimeout
                //};
                OdbcCommand odbcCommand = new OdbcCommand(commandText);
                odbcCommand.Connection = ConnByMs;
                odbcCommand.CommandTimeout = this.commandTimeout;

                this.AttachParameters(Comm, commandParameters);

                ds = new DataSet();
                da = new SqlDataAdapter(Comm);

                Conn.Open();
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (da != null)
                    da.Dispose();
                if (Comm != null)
                    Comm.Dispose();
                if (Conn != null)
                    Conn.Close();
            }

            return ds;
        }
        #endregion

        #region AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        /// <summary>
        /// AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        /// </summary>
        /// <param name="command">MySqlCommand</param>
        /// <param name="commandParameters">MySqlParameter[]</param>
        private void AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        {
            if (commandParameters == null) return;

            foreach (MySqlParameter p in commandParameters)
            {
                //인자값이 없으면 Null 할당
                if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                {
                    p.Value = DBNull.Value;
                }

                command.Parameters.Add(p);
            }

            MySqlParameter param = new MySqlParameter("@pDEBUG", MySqlDbType.VarChar, 1);
            param.Value = "F";

            command.Parameters.Add(param);

            //return Value
            //MySqlParameter rtnParam = command.Parameters.Add("_RETURN_VALUE_", MySqlDbType.VarChar);
            //rtnParam.Direction = ParameterDirection.ReturnValue;
        }
        #endregion

        #region AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        /// <summary>
        /// AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        /// </summary>
        /// <param name="command">MySqlCommand</param>
        /// <param name="commandParameters">MySqlParameter[]</param>
        private void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            if (commandParameters == null) return;

            foreach (SqlParameter p in commandParameters)
            {
                //인자값이 없으면 Null 할당
                if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                {
                    p.Value = DBNull.Value;
                }

                command.Parameters.Add(p);
            }
            
            SqlParameter rtnParam = command.Parameters.Add("_RETURN_VALUE_", SqlDbType.VarChar);
            rtnParam.Direction = ParameterDirection.ReturnValue;
        }
        #endregion

        #region GetConnectionString(string Source)
        /// <summary>
        /// GetConnectionString(string Source)
        /// </summary>
        /// <param name="Source">string</param>
        /// <returns></returns>
        private string GetConnectionString(string Source)
        {
            string strSource = ConfigurationSettings.AppSettings[Source];
            byte[] str2byte = Encoding.UTF8.GetBytes(ConfigurationSettings.AppSettings["ServerNum"].ToString());

            string StrConn = DeCryption.Decryption.DecryptString(strSource, ConfigurationSettings.AppSettings["ServerNum"].ToString(), str2byte);

            try
            {
                string strTimeOut = ConfigurationSettings.AppSettings["SqlTimeOut"];
                this.commandTimeout = Convert.ToInt32(strTimeOut);
            }
            catch
            {
                this.commandTimeout = 30;
            }

            return StrConn;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
                if (this.Conn != null)
                    Conn.Close();
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
