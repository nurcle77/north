﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Data;
using System.Reflection;
using MySql.Data.MySqlClient;

using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Collections.Generic;

namespace KTS.KTSSolution.Common.DAL
{
    public class DSBase : System.IDisposable
    {
        #region Constructor
        public DSBase()
        {
        }
        #endregion

        #region Method

        #region Inquiry
        /// <summary>
        /// Inquiry
        /// </summary>
        /// <param name="data">CommonData</param>
        /// <returns>DataSet</returns>
        public DataSet Inquiry(CommonData data)
        {
            DataSet ds = null;
            try
            {
                using (DBConn dbConn = new DBConn())
                {
                    ds = dbConn.ExecuteDataSet(data.commandType, data.strQry, data.parameters);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleDSLException(SubSystemType.KTSSolution, ex, this.GetType(), MethodInfo.GetCurrentMethod().Name);
            }
            return ds;
        }
        #endregion

        #region Reader
        /// <summary>
        /// Reader
        /// </summary>
        /// <param name="data">CommonData</param>
        /// <returns>MySqlDataReader</returns>
        public MySqlDataReader Reader(CommonData data)
        {
            MySqlDataReader reader = null;
            try
            {
                using (DBConn dbConn = new DBConn())
                {
                    reader = dbConn.ExecuteReader(data.commandType, data.strQry, data.parameters);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleDSLException(SubSystemType.KTSSolution, ex, this.GetType(), MethodInfo.GetCurrentMethod().Name);
            }
            return reader;
        }
        #endregion

        #region Register
        /// <summary>
        /// Register
        /// </summary>
        /// <param name="data">CommonData</param>
        /// <returns>int</returns>
        public int Register(CommonData data)
        {
            int iResult = 0;
            try
            {
                using (DBConn dbConn = new DBConn())
                {
                    iResult = dbConn.ExecuteNonQuery(data.commandType, data.strQry, data.parameters);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleDSLException(SubSystemType.KTSSolution, ex, this.GetType(), MethodInfo.GetCurrentMethod().Name);
            }
            return iResult;
        }
        #endregion

        #region MultiRegister
        /// <summary>
        /// MultiRegister
        /// </summary>
        /// <param name="datalist">List<CommonData></param>
        /// <returns>DataSet</returns>
        public int MultiRegister(List<CommonData> datalist, bool bSleep = false)
        {
            int iResult = 0;
            try
            {
                using (DBConn dbConn = new DBConn())
                {
                    iResult = dbConn.ExecuteMultiDataSet(datalist, bSleep);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleDSLException(SubSystemType.KTSSolution, ex, this.GetType(), MethodInfo.GetCurrentMethod().Name);
            }
            return iResult;
        }
        #endregion

        #region Bulk
        /// <summary>
        /// Bulk
        /// </summary>
        /// <param name="strTableName">string</param>
        /// <param name="strFilePath">string</param>
        public void Bulk(string strTableName, string strFilePath)
        {
            try
            {
                using (DBConn dbConn = new DBConn())
                {
                    dbConn.ExecuteBulk(strTableName, strFilePath);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleDSLException(SubSystemType.KTSSolution, ex, this.GetType(), MethodInfo.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region InquiryByMs
        /// <summary>
        /// InquiryByMs
        /// </summary>
        /// <param name="data">CommonData</param>
        /// <returns>DataSet</returns>
        public DataSet InquiryByMs(CommonDataByMsSql data)
        {
            DataSet ds = null;
            try
            {
                using (DBConn dbConn = new DBConn())
                {
                    ds = dbConn.ExecuteDataSetByMsSql(data.commandType, data.strQry, data.parameters);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleDSLException(SubSystemType.KTSSolution, ex, this.GetType(), MethodInfo.GetCurrentMethod().Name);
            }
            return ds;
        }
        #endregion
        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
