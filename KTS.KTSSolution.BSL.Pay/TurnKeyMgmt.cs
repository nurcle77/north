﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Pay
{
    public class TurnKeyMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public TurnKeyMgmt()
        {
        }

        #endregion

        #region Method

        #region GetSumupMonth
        /// <summary>
        /// GetSumupMonth
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetSumupMonth()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_TURNKEYSUMUPMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetTurnkeyTotalList
        /// <summary>
        /// GetTurnkeyTotalList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetTurnkeyTotalList(string strOrgCd, string strSumupMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_TURNKEYTOTALLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetTurnkeyStatByMonth
        /// <summary>
        /// GetTurnkeyStatByMonth
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetTurnkeyStatByMonth(string strOrgCd, string strStMonth, string strEnMonth, string strValtype)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_TURNKEYSTATBYMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSTMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strStMonth;

                parameters[2] = new MySqlParameter("@pENMONTH", MySqlDbType.VarChar, 6);
                parameters[2].Value = strEnMonth;

                parameters[3] = new MySqlParameter("@pVALTYPE", MySqlDbType.VarChar, 10);
                parameters[3].Value = strValtype;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetTurnkeyDetail
        /// <summary>
        /// GetTurnkeyDetail
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strOrgChk">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetTurnkeyDetail(string strOrgCd, string strOrgChk, string strSumupMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_TURNKEYDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pORGCHK", MySqlDbType.VarChar, 1);
                parameters[2].Value = strOrgChk;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetTurnkeyEventDetail
        /// <summary>
        /// GetTurnkeyEventDetail
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strOrgChk">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetTurnkeyEventDetail(string strOrgCd, string strOrgChk, string strSumupMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_TURNKEYEVENTDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pORGCHK", MySqlDbType.VarChar, 1);
                parameters[2].Value = strOrgChk;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
