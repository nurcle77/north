﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Pay
{
    public class BCompanyPayMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public BCompanyPayMgmt()
        {
        }

        #endregion

        #region Method

        #region GetSumupMonth
        /// <summary>
        /// GetSumupMonth
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetSumupMonth()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_BCOMPANYSUMUPMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetBCompanyState
        /// <summary>
        /// GetBCompanyState
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetBCompanyState(string strOrgCd, string strSumupMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_BCOMPANYSTATE_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetBCompanyState
        /// <summary>
        /// GetBCompanyState
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetBCompanyDetailList(string strOrgCd, string strOrgNm, string strSumupMonth, string strWorkType)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_BCOMPANYSTATEDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pORGNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strOrgNm;

                parameters[2] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[2].Value = strSumupMonth;

                parameters[3] = new MySqlParameter("@pWORKTYPE", MySqlDbType.VarChar, 10);
                parameters[3].Value = strWorkType;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
