﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Oper
{
    public class UserMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public UserMgmt()
        {
        }

        #endregion

        #region Method

        #region GetAuthGroup
        /// <summary>
        /// GetUserLogOn
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetAuthGroup()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_USERAUTHGROUP_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOrgInfo
        /// <summary>
        /// GetOrgInfo
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <returns></returns>
        public DataSet GetOrgInfo(string strOrgCd)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_OPERORGINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 6);
                parameters[0].Value = strOrgCd;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsertUserInfo
        /// <summary>
        /// InsertUserInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strKtsEmpno">string</param>
        /// <param name="strEmpNm">string</param>
        /// <param name="strTelNo">string</param>
        /// <param name="strIdmsEmpNo">string</param>
        /// <param name="strPreIdmsEmpNo">string</param>
        /// <param name="strOrgCd">string</param>
        /// <param name="strKTSYN">string</param>
        /// <param name="strCompCd">string</param>
        /// <param name="strCompNm">string</param>
        public void InsertUserInfo(string strEmpNo, string strKtsEmpno, string strEmpNm, string strTelNo, string strIdmsEmpNo, string strPreIdmsEmpNo, string strOrgCd
                                    , string strKTSYN, string strCompCd, string strCompNm, string strAuth)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[11];

                CommonData data = new CommonData("UP_USERINFO_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pKTSEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strKtsEmpno;

                parameters[2] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strEmpNm;

                parameters[3] = new MySqlParameter("@pMOBILE", MySqlDbType.VarChar, 20);
                parameters[3].Value = strTelNo;

                parameters[4] = new MySqlParameter("@pIDMSEMPNO", MySqlDbType.VarChar, 20);
                parameters[4].Value = strIdmsEmpNo;

                parameters[5] = new MySqlParameter("@pPREIDMSEMPNO", MySqlDbType.VarChar, 20);
                parameters[5].Value = strPreIdmsEmpNo;

                parameters[6] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 6);
                parameters[6].Value = strOrgCd;

                parameters[7] = new MySqlParameter("@pKTSYN", MySqlDbType.VarChar, 1);
                parameters[7].Value = strKTSYN;

                parameters[8] = new MySqlParameter("@pCOMPANYCD", MySqlDbType.VarChar, 10);
                parameters[8].Value = strCompCd;

                parameters[9] = new MySqlParameter("@pCOMPANYNM", MySqlDbType.VarChar, 50);
                parameters[9].Value = strCompNm;

                parameters[10] = new MySqlParameter("@pAUTHID", MySqlDbType.VarChar, 8);
                parameters[10].Value = strAuth;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetUserPassWord
        /// <summary>
        /// SetUserPassWord
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strEmpPwd">string</param>
        /// <param name="strEmpPwdChg">string</param>
        public void SetUserPassWord(string strEmpNo, string strEmpPwdChg)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_USERPASSWORD_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pEMPPWDCHG", MySqlDbType.VarChar, 256);
                parameters[1].Value = strEmpPwdChg;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetUserExfireYn
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEmpNo"></param>
        /// <returns></returns>
        public DataSet GetUserExfireYn(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_USEREXFIREYN_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperUserList
        /// <summary>
        /// GetOperUserList 
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strEmpNm">string</param>
        /// <param name="strKtsYn">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperUserList(string strOrgCd, string strEmpNm, string strKtsYn)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_OPERUSERINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strEmpNm;

                parameters[2] = new MySqlParameter("@pKTSYN", MySqlDbType.VarChar, 1);
                parameters[2].Value = strKtsYn;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetOperUserInfo
        /// <summary>
        /// SetOperUserInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strPhoneNum">string</param>
        /// <param name="strAuthId">string</param>
        /// <param name="strOrgCd">string</param>
        /// <param name="strJobNm">string</param>
        /// <param name="JobGrade">string</param>
        /// <param name="strJobNm2">string</param>
        public void SetOperUserInfo(string strEmpNo, string strAuthId, string strOrgCd, string strJobNm2, string strPwdChgYn, string strIdmsEmpNo, string strPreIdmsEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[7];

                CommonData data = new CommonData("UP_OPERUSERINFO_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pAUTHID", MySqlDbType.VarChar, 8);
                parameters[1].Value = strAuthId;

                parameters[2] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strOrgCd;

                parameters[3] = new MySqlParameter("@pJOBNM2", MySqlDbType.VarChar, 30);
                parameters[3].Value = strJobNm2;

                parameters[4] = new MySqlParameter("@pPWDCHGYN", MySqlDbType.VarChar, 1);
                parameters[4].Value = strPwdChgYn;

                parameters[5] = new MySqlParameter("@pIDMSEMPNO", MySqlDbType.VarChar, 20);
                parameters[5].Value = strIdmsEmpNo;

                parameters[6] = new MySqlParameter("@pPREIDMSEMPNO", MySqlDbType.VarChar, 20);
                parameters[6].Value = strPreIdmsEmpNo;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelOperUserInfo
        /// <summary>
        /// DelOperUserInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        public void DelOperUserInfo(string strEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_OPERUSERINFO_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetOperUserInfoExcel
        /// <summary>
        /// SetOperUserInfoExcel
        /// </summary>
        public void SetOperUserInfoExcel()
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_OPERUSERINFOEXCEL_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region GetOperBCompanyUserList
        /// <summary>
        /// GetOperBCompanyUserList 
        /// </summary>
        /// <param name="strCompNm">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperBCompanyUserList(string strCompNm)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_OPERUSERINFO_BCOMPANY_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCOMPNM", MySqlDbType.VarChar, 50);
                parameters[0].Value = strCompNm;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperBcompUserInfo
        /// <summary>
        /// GetOperBcompUserInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        public DataSet GetOperBcompUserInfo(string strEmpNo)
        {
            DataSet ds = null; 

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_OPERUSERIDCHECK_BCOMPANY_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetBCompUserInfo
        /// <summary>
        /// SetBCompUserInfo
        /// </summary>
        /// <param name="strCompNm"></param>
        /// <param name="strEmpNo"></param>
        /// <param name="strEmpNm"></param>
        /// <param name="strBBNm"></param>
        /// <param name="strJSNm"></param>
        /// <param name="strOfficeNm"></param>
        public void InsertBCompUserInfo(string strCompNm, string strEmpNo, string strEmpNm, string strBBNm, string strJSNm, string strOfficeNm)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_OPERUSERINFO_BCOMPANY_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCOMPNM", MySqlDbType.VarChar, 50);
                parameters[0].Value = strCompNm;

                parameters[1] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strEmpNo;

                parameters[2] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strEmpNm;

                parameters[3] = new MySqlParameter("@pBBNM", MySqlDbType.VarChar, 50);
                parameters[3].Value = strBBNm;

                parameters[4] = new MySqlParameter("@pJSNM", MySqlDbType.VarChar, 50);
                parameters[4].Value = strJSNm;

                parameters[5] = new MySqlParameter("@pOFFICENM", MySqlDbType.VarChar, 50);
                parameters[5].Value = strOfficeNm;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelOperBCompUserInfo
        /// <summary>
        /// DelOperBCompUserInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        public void DelOperBCompUserInfo(string strEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_OPERUSERINFO_BCOMPANY_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetOperBCompUserInfoExcel
        /// <summary>
        /// SetOperBCompUserInfoExcel
        /// </summary>
        public void SetOperBCompUserInfoExcel()
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_OPERUSERINFOEXCEL_BCOMPANY_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetDelAuthCheck
        /// <summary>
        /// GetDelAuthCheck
        /// </summary>
        /// <param name="strAuthId">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetDelAuthCheck(string strAuthId)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_OPERDELETEAUTHCHECK_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pAUTHID", MySqlDbType.VarChar, 8);
                parameters[0].Value = strAuthId;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelAuthGroup
        /// <summary>
        /// DelAuthGroup
        /// </summary>
        /// <param name="strAuthId">string</param>
        public void DelAuthGroup(string strAuthId)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_OPERAUTHGROUP_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pAUTHID", MySqlDbType.VarChar, 8);
                parameters[0].Value = strAuthId;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
