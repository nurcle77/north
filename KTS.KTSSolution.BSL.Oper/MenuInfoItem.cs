﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTS.KTSSolution.BSL.Oper
{
    public class MenuInfoItem
    {
        public string MENUID { get; set; }
        public string MENUNM { get; set; }
        public string UPPERMENUID { get; set; }
        public int MENULEVEL { get; set; }
        public int MENUORDER { get; set; }

        public MenuInfoItem(DataRow row)
        {
            this.MENUID = row["MENUID"].ToString();
            this.MENUNM = row["MENUNM"].ToString();
            this.UPPERMENUID = row["UPPERMENUID"].ToString();
            this.MENULEVEL = Convert.ToInt32(row["MENULEVEL"]);
            this.MENUORDER = Convert.ToInt32(row["MENUORDER"]);
        }
    }

    public class MenuAuthItem
    {
        public string MENUID { get; set; }
        public string UPPERMENUID { get; set; }
        public string AUTHID { get; set; }
        public string VIEWYN { get; set; }
        public int MENUORDER { get; set; }

        public MenuAuthItem(DataRow row)
        {
            this.MENUID = row["MENUID"].ToString();
            this.UPPERMENUID = row["UPPERMENUID"].ToString();
            this.AUTHID = row["AUTHID"].ToString();
            this.VIEWYN = row["VIEWYN"].ToString();
            this.MENUORDER = Convert.ToInt32(row["MENUORDER"]);
        }
    }
}
