﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Common
{
    public class Notice : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public Notice()
        {
        }

        #endregion

        #region Method

        #region GetNotiPopUpList
        /// <summary>
        /// GetNotiPopUpList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetNotiPopUpList()
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_NOTICEPOPUPLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region GetNotiList
        /// <summary>
        /// GetNotiList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetNotiList(string strNoticeId, string strTitle, string strEmpNm, string strPopYn)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];
                parameters[0] = new MySqlParameter("@pNOTICEID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strNoticeId;
                parameters[1] = new MySqlParameter("@pTITLE", MySqlDbType.VarChar, 150);
                parameters[1].Value = strTitle;
                parameters[2] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strEmpNm;
                parameters[3] = new MySqlParameter("@pPOPUPYN", MySqlDbType.VarChar, 1);
                parameters[3].Value = strPopYn;

                CommonData data = new CommonData("UP_NOTICELIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region UpdNotiList
        /// <summary>
        /// UpdNotiList
        /// </summary>
        public void UpdNotiList(string strNoticeId, string strTitle, string strContents, string strStDt, string strEnDt, string strYiewYn, string strPopupYn, string strEmpno)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[8];
                parameters[0] = new MySqlParameter("@pNOTICEID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strNoticeId;
                parameters[1] = new MySqlParameter("@pTITLE", MySqlDbType.VarChar, 150);
                parameters[1].Value = strTitle;
                parameters[2] = new MySqlParameter("@pCONTENTS", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strContents;
                parameters[3] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 10);
                parameters[3].Value = strStDt;
                parameters[4] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 10);
                parameters[4].Value = strEnDt;
                parameters[5] = new MySqlParameter("@pVIEWYN", MySqlDbType.VarChar, 1);
                parameters[5].Value = strYiewYn;
                parameters[6] = new MySqlParameter("@pPOPUPYN", MySqlDbType.VarChar, 1);
                parameters[6].Value = strPopupYn;
                parameters[7] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[7].Value = strEmpno;

                CommonData data = new CommonData("UP_NOTICELIST_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InsNotiList
        /// <summary>
        /// InsNotiList
        /// </summary>
        public DataSet InsNotiList(string strTitle, string strContents, string strStDt, string strEnDt, string strYiewYn, string strPopupYn, string strEmpno)
        {
            DataSet ds = new DataSet();
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[7];
                parameters[0] = new MySqlParameter("@pTITLE", MySqlDbType.VarChar, 150);
                parameters[0].Value = strTitle;
                parameters[1] = new MySqlParameter("@pCONTENTS", MySqlDbType.VarChar, 8000);
                parameters[1].Value = strContents;
                parameters[2] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 10);
                parameters[2].Value = strStDt;
                parameters[3] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 10);
                parameters[3].Value = strEnDt;
                parameters[4] = new MySqlParameter("@pVIEWYN", MySqlDbType.VarChar, 1);
                parameters[4].Value = strYiewYn;
                parameters[5] = new MySqlParameter("@pPOPUPYN", MySqlDbType.VarChar, 1);
                parameters[5].Value = strPopupYn;
                parameters[6] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[6].Value = strEmpno;

                CommonData data = new CommonData("UP_NOTICELIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelNotiList
        /// <summary>
        /// DelNotiList
        /// </summary>
        public void DelNotiList(string strNoticeId)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pNOTICEID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strNoticeId;

                CommonData data = new CommonData("UP_NOTICELIST_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetNoticeFile
        /// <summary>
        /// GetNoticeFile
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetNoticeFile(string strNoticeId, string strSeq)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pNOTICEID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strNoticeId;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strSeq;

                CommonData data = new CommonData("UP_NOTICEFILES_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region InsNoticeFile
        /// <summary>
        /// InsNoticeFile
        /// </summary>
        public void InsNoticeFile(string strNoticeId, string strFileNm, byte[] bfile, string strFileType, int nFileSize)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];
                parameters[0] = new MySqlParameter("@pNOTICEID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strNoticeId;
                parameters[1] = new MySqlParameter("@pFILENM", MySqlDbType.VarChar, 150);
                parameters[1].Value = strFileNm;
                parameters[2] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[2].Value = bfile;
                parameters[3] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[3].Value = strFileType;
                parameters[4] = new MySqlParameter("@pFILESIZE", MySqlDbType.VarChar, 20);
                parameters[4].Value = nFileSize;

                CommonData data = new CommonData("UP_NOTICEFILES_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelNoticeFile
        /// <summary>
        /// DelNoticeFile
        /// </summary>
        public void DelNoticeFile(string strNoticeId, string strSeq)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pNOTICEID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strNoticeId;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strSeq;

                CommonData data = new CommonData("UP_NOTICEFILES_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetBoardList
        /// <summary>
        /// GetBoardList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetBoardList(string strBoardId, string strTitle, string strEmpNm)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pTITLE", MySqlDbType.VarChar, 150);
                parameters[1].Value = strTitle;
                parameters[2] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strEmpNm;


                CommonData data = new CommonData("UP_BOARDLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region UpdBoardList
        /// <summary>
        /// UpdBoardList
        /// </summary>
        public void UpdBoardList(string strBoardId, string strTitle, string strContents)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pTITLE", MySqlDbType.VarChar, 150);
                parameters[1].Value = strTitle;
                parameters[2] = new MySqlParameter("@pCONTENTS", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strContents;

                CommonData data = new CommonData("UP_BOARDLIST_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InsBoardList
        /// <summary>
        /// InsBoardList
        /// </summary>
        public DataSet InsBoardList(string strTitle, string strContents, string strEmpno)
        {
            DataSet ds = new DataSet();
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];
                parameters[0] = new MySqlParameter("@pTITLE", MySqlDbType.VarChar, 150);
                parameters[0].Value = strTitle;
                parameters[1] = new MySqlParameter("@pCONTENTS", MySqlDbType.VarChar, 8000);
                parameters[1].Value = strContents;
                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpno;

                CommonData data = new CommonData("UP_BOARDLIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelBoardList
        /// <summary>
        /// DelBoardList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet DelBoardList(string strBoardId)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;

                CommonData data = new CommonData("UP_BOARDLIST_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region GetBoardFile
        /// <summary>
        /// GetBoardFile
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetBoardFile(string strBoardId, string strSeq)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strSeq;

                CommonData data = new CommonData("UP_BOARDFILES_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region InsBoardFile
        /// <summary>
        /// InsBoardFile
        /// </summary>
        public void InsBoardFile(string strBoardId, string strFileNm, byte[] bfile, string strFileType, int nFileSize)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pFILENM", MySqlDbType.VarChar, 150);
                parameters[1].Value = strFileNm;
                parameters[2] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[2].Value = bfile;
                parameters[3] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[3].Value = strFileType;
                parameters[4] = new MySqlParameter("@pFILESIZE", MySqlDbType.VarChar, 20);
                parameters[4].Value = nFileSize;

                CommonData data = new CommonData("UP_BOARDFILES_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelBoardFile
        /// <summary>
        /// DelBoardFile
        /// </summary>
        /// <returns>DataSet</returns>
        public void DelBoardFile(string strBoardId, string strSeq)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strSeq;

                CommonData data = new CommonData("UP_BOARDFILES_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetBoardReplyList
        /// <summary>
        /// GetBoardReplyList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetBoardReplyList(string strBoardId)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;

                CommonData data = new CommonData("UP_BOARDREPLY_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region InsBoardReply
        /// <summary>
        /// InsBoardReply
        /// </summary>
        public void InsBoardReply(string strBoardId, string strContents, string strEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pCONTENTS", MySqlDbType.VarChar, 8000);
                parameters[1].Value = strContents;
                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                CommonData data = new CommonData("UP_BOARDREPLY_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdBoardReply
        /// <summary>
        /// UpdBoardReply
        /// </summary>
        public void UpdBoardReply(string strBoardId, string strReplySeq, string strContents)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pREPLYSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strReplySeq;
                parameters[2] = new MySqlParameter("@pCONTENTS", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strContents;

                CommonData data = new CommonData("UP_BOARDREPLY_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelBoardReply
        /// <summary>
        /// DelBoardReply
        /// </summary>
        public void DelBoardReply(string strBoardId, string strReplySeq)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pBOARDID", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBoardId;
                parameters[1] = new MySqlParameter("@pREPLYSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strReplySeq;

                CommonData data = new CommonData("UP_BOARDREPLY_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion

    }
}
