﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Framework.Util;

namespace KTS.KTSSolution.BSL.Erp_Yd
{
    public class Erp_Yd : IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public Erp_Yd()
        {
        }

        #endregion

        #region Method

        #region GetYdLIst()
        /// <summary>
        /// GetYdLIst
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetYdLIst()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_ERPYDLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetYdData()
        /// <summary>
        /// GetYdData
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetYdData(string strSql)
        {
            DataSet ds = null;

            try
            {
                CommonDataByMsSql data = new CommonDataByMsSql(strSql);
                data.commandType = CommandType.Text;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.InquiryByMs(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetYdData()
        /// <summary>
        /// SetYdData
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet SetYdData(string strSql)
        {
            DataSet ds = null;

            try
            {
                CommonData data = new CommonData(strSql);
                data.commandType = CommandType.Text;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
