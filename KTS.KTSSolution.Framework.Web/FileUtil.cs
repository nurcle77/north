﻿using MySqlX.XDevAPI;
using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

namespace KTS.KTSSolution.Framework.Web
{
    public class FileUtil : IDisposable
    {
        #region Fields

        private bool disposedValue;

        #endregion

        #region Properties

        #endregion

        #region Constructor
        public FileUtil()
        {
        }

        #endregion

        #region Method

        #region

        public void FileDownLoad(Page oPage, byte[] bFile, string strFileNm, int nFileLen)
        {
            try
            {
                oPage.Response.Clear();
                oPage.Response.ClearHeaders();
                oPage.Response.ContentType = "application/octet-stream";
                oPage.Response.AddHeader("Content-Disposition", "attachment; filename=" + strFileNm);

                oPage.Response.BinaryWrite(bFile);
                oPage.Response.Flush();
                oPage.Response.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FileDownLoad_M(Page oPage, byte[] bFile, string strFileNm, int nFileLen)
        {
            try
            {
                string strEmpNo = oPage.Session["EMPNO"].ToString();

                string strSource = ConfigurationSettings.AppSettings["mKateDownloadPath"];
                byte[] str2byte = Encoding.UTF8.GetBytes(ConfigurationSettings.AppSettings["ServerNum"].ToString());

                string strPath = DeCryption.Decryption.DecryptString(strSource, ConfigurationSettings.AppSettings["ServerNum"].ToString(), str2byte);

                strPath = strPath + strEmpNo;

                DirectoryInfo di = new DirectoryInfo(strPath);

                if (!di.Exists)
                {
                    di.Create();
                }

                string strFilePath = strPath + "\\" + strFileNm;

                FileInfo fi = new FileInfo(strFilePath);

                if (!fi.Exists)
                {
                    fi.Delete();
                }

                using (FileStream fs = new FileStream(strFilePath, FileMode.OpenOrCreate))
                {
                    fs.Write(bFile, 0, nFileLen);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region
        public bool IsFileRun(string strFilePath)
        {
            FileStream fs = null;

            try
            {
                fs = new FileStream(strFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch
            {
                return true;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }

            return false;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
                }

                // TODO: 비관리형 리소스(비관리형 개체)를 해제하고 종료자를 재정의합니다.
                // TODO: 큰 필드를 null로 설정합니다.
                disposedValue = true;
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ExcelUtil()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
