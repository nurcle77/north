﻿using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;

namespace KTS.KTSSolution.Framework.Web
{
    public static class Utility
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructor
        #endregion

        #region Method

        #region ReplaceChar
        /// <summary>
        /// ReplaceChar
        /// </summary>
        /// <param name="strData">string</param>
        /// <returns>string</returns>
        public static string ReplaceChar(string strData)
        {
            strData = strData.Replace("<", "←");
            strData = strData.Replace(">", "→");
            strData = strData.Replace("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
            strData = strData.Replace("script", "");
            strData = strData.Replace("meta", "");
            strData = strData.Replace("eval\\((.*)\\)", "");

            return strData;
        }
        #endregion

        #region SHA256Hash
        /// <summary>
        /// SHA256Hash
        /// </summary>
        /// <param name="data">string</param>
        /// <returns>string</returns>
        public static string SHA256Hash(string data)
        {
            SHA256 sha = new SHA256Managed();
            byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(data));

            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
            {
                sb.AppendFormat("{0:x2}", b);
            }

            return sb.ToString();
        }
        #endregion

        #region GetOrgNm
        /// <summary>
        /// GetOrgNm
        /// </summary>
        /// <param name="OrgCd">string</param>
        /// <returns>string</returns>
        public static string GetOrgNm(string OrgCd, DataTable DtOrgList)
        {
            string strOrgNm = "";
            try
            {
                string[] strOrgCd = OrgCd.Split(',');
                string strWhere = "";

                for (int i = 0; i < strOrgCd.Length; i++)
                {
                    if (strWhere.Length > 0)
                        strWhere += " OR ";

                    strWhere += string.Format("ORGCD = '{0}'", strOrgCd[i]);
                }

                DataRow[] dr = DtOrgList.Select(strWhere);
                DataTable dt = DtOrgList.Clone();

                if (dr.Length > 0)
                {
                    dt = dr.CopyToDataTable();
                }

                if (dt.Rows.Count == 1)
                {
                    strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                }
                else
                {
                    foreach (DataRow drOrg in dt.Rows)
                    {
                        if (strOrgNm.Length > 0)
                            strOrgNm += ", ";

                        strOrgNm += drOrg["ORGNM"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOrgNm;
        }
        #endregion

        #endregion
    }
}
