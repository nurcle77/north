﻿using System;
using System.Web;
using System.Web.UI;

namespace KTS.KTSSolution.Framework.Web
{
    public class PageUtility
    {
        #region Fields
        private Page _page;
        private Random _random;
        #endregion

        #region Properties
        #endregion

        #region Constructor
        public PageUtility(Page page)
        {
            _page = page;
            _random = new Random(unchecked((int)DateTime.Now.Ticks));
        }
        #endregion

        #region Method

        #region PageMove
        /// <summary>
        /// PageMove
        /// </summary>
        /// <param name="strUrl">string</param>
        public void PageMove(string strUrl)
        {
            string strScript = "window.location.href = '" + strUrl + "';";

            this.ExecuteScript(strScript, false);

        }
        #endregion

        #region
        public void ExecuteScript(string strScript, bool bBeforPageLoad)
        {
            strScript = "<script type=\"text/javascript\">" + strScript + "</script>";

            if (bBeforPageLoad)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(strScript);
                HttpContext.Current.Response.Flush();
            }
            else
            {
                string strScriptID = DateTime.Now.ToString("yyyyMMddhhmmssfff") + _random.Next().ToString();
                if (!_page.ClientScript.IsClientScriptBlockRegistered(strScriptID))
                {
                    ScriptManager.RegisterStartupScript(_page, this.GetType(), strScriptID, strScript, false);
                }

            }
        }
        #endregion

        #endregion
    }
}
