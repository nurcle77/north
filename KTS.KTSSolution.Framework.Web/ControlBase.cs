﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System.Collections.Generic;
using System.Web.UI;

namespace KTS.KTSSolution.Framework.Web
{
    public class ControlBase : UserControl
    {
        #region Fields

        private bool _refreshState;
        private bool _isRefresh;

        #endregion

        #region Properties

        #region PageDictionary
        /// <summary>
        /// PageDictionary
        /// </summary>
        public Dictionary<string, object> PageDictionary
        {
            get
            {
                return ((PageBase)this.Page).PageDictionary;
            }
        }
        #endregion

        #region IsRefresh
        /// <summary>
        /// IsRefresh
        /// </summary>
        public bool IsRefresh
        {
            get
            {
                return _isRefresh;
            }
        }
        #endregion

        #endregion

        #region Constructor

        public ControlBase() : base()
        {
        }

        #endregion

        #region Method

        #region LoadViewState(object savedState)
        /// <summary>
        /// LoadViewState(object savedState)
        /// </summary>
        /// <param name="savedState">object</param>
        protected override void LoadViewState(object savedState)
        {
            object[] allStates = (object[])savedState;
            base.LoadViewState(allStates[0]);
            _refreshState = (bool)allStates[1];
            _isRefresh = _refreshState == (bool)(Session["__ISREFRESH"] == null ? false : Session["__ISREFRESH"]);
        }
        #endregion

        #region SaveViewState()
        /// <summary>
        /// SaveViewState()
        /// </summary>
        /// <returns>object</returns>
        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _refreshState;
            object[] allStates = new object[2];
            allStates[0] = base.SaveViewState();
            allStates[1] = !_refreshState;
            return allStates;
        }
        #endregion

        #endregion
    }
}
