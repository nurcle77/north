﻿using System;
using System.Linq;
using System.Collections;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.SharedType;

using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace KTS.KTSSolution.Framework.Web
{
    public class ExcelUtil : IDisposable
    {
        #region Fields

        string[] headertop = null;
        string[] headerbom = null;
        string[] headerColumn = null;
        string[] totalColumn = null;
        string[] mergeRow = null;

        string mergecell = "";
        string mergegrid = "";

        private bool disposedValue;

        #endregion

        #region Properties

        public string[] HeaderTop
        {
            get
            {
                return headertop;
            }
            set
            {
                this.headertop = value;
            }

        }

        public string[] HeaderBom
        {
            get
            {
                return headerbom;
            }
            set
            {
                this.headerbom = value;
            }

        }

        public string[] HeaderColumn
        {
            get
            {
                return headerColumn;
            }
            set
            {
                this.headerColumn = value;
            }

        }

        public string[] MergeRow
        {
            get
            {
                return mergeRow;
            }
            set
            {
                mergeRow = value;
            }
        }

        public string[] TotalColumn
        {
            get
            {
                return totalColumn;
            }
            set
            {
                totalColumn = value;
            }
        }

        public string MergeCell
        {
            get
            {
                return mergecell;
            }
            set
            {
                mergecell = value;
            }
        }

        public string MergeGrid
        {
            get
            {
                return mergegrid;
            }
            set
            {
                mergegrid = value;
            }
        }

        #endregion

        #region Constructor

        public ExcelUtil()
        {
        }

        #endregion

        #region Method

        #region ExcelDownLoad(Page oPage, DataTable dt, string strFileName) // ExcelWorkSheet 직접제어
        /// <summary>
        /// ExcelDownLoad(Page oPage, DataTable dt, string strFileName)
        /// </summary>
        /// <param name="oPage"></param>
        /// <param name="dt"></param>
        /// <param name="strFileName"></param>
        public void ExcelDownLoad(Page oPage, DataTable dt, string strFileName)
        {
            try
            {
                DataTable dtData = new DataTable();

                for (int i = 0; i < HeaderColumn.Length; i++)
                {
                    dtData.Columns.Add(HeaderColumn[i]);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].ToString().Length == 0)
                        continue;

                    DataRow dr = dtData.NewRow();

                    for (int j = 0; j < HeaderColumn.Length; j++)
                    {
                        dr[HeaderColumn[j]] = dt.Rows[i][HeaderColumn[j]];
                    }

                    dtData.Rows.Add(dr);
                }
                
                using (ExcelPackage package = new ExcelPackage())
                {
                    bool bSpanChk = false;
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("sheet1");

                    #region header binding
                    int HeaderRow = 0;

                    if ((HeaderColumn != null && HeaderColumn.Length > 0) && (HeaderTop != null && HeaderTop.Length > 0))
                    {
                        int n = 0;
                        HeaderRow += 1;

                        for (int i = 0; i < this.HeaderTop.Length; i++)
                        {
                            string[] type = HeaderColumn[i].Split('/');

                            string strHeaderType = type[0];
                            string strHeaderText = HeaderTop[i];

                            HeaderColumn[i] = HeaderTop[i];

                            if (MergeCell.Length > 0)
                            {
                                string[] merge = MergeCell.Split(',');
                                int index = 0;
                                int len = 0;

                                worksheet.Cells[HeaderRow, n + 1].Value = strHeaderText;

                                for (int j = 0; j < merge.Length; j++)
                                {
                                    string[] mergeInfo = merge[j].Split('/');

                                    index = int.Parse(mergeInfo[0]);

                                    if (index == i)
                                    {
                                        bSpanChk = true;

                                        len = int.Parse(mergeInfo[1]);

                                        string strMergeCell1 = worksheet.Cells[1, n + 1].Address;
                                        string strMergeCell2 = worksheet.Cells[1, n + len].Address;

                                        worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                        worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                        n += (len - 1);

                                        len = 0;

                                        break;
                                    }
                                }

                                n++;
                            }
                            else
                            {
                                worksheet.Cells[HeaderRow, i + 1].Value = strHeaderText;
                                worksheet.Cells[HeaderRow, i + 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Cells[HeaderRow, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                        }
                    }
                    if ((HeaderBom != null && HeaderBom.Length > 0) && (HeaderColumn != null && HeaderColumn.Length > 0))
                    {
                        HeaderRow += 1;

                        for (int i = 0; i < this.HeaderBom.Length; i++)
                        {
                            string[] type = HeaderColumn[i].Split('/');

                            string strHeaderType = type[0];
                            string strHeaderText = HeaderBom[i].Replace("/", "");

                            HeaderColumn[i] = HeaderBom[i];

                            bool bMerge = false;

                            for (int j = 0; j < this.HeaderTop.Length; j++)
                            {
                                if (HeaderBom[i] == HeaderTop[j])
                                {
                                    string strMergeCell1 = worksheet.Cells[1, i + 1].Address;
                                    string strMergeCell2 = worksheet.Cells[2, i + 1].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    bMerge = true;

                                    break;
                                }
                            }

                            if (bMerge)
                            {
                                continue;
                            }
                            else
                            {
                                worksheet.Cells[HeaderRow, i + 1].Value = strHeaderText;
                                worksheet.Cells[HeaderRow, i + 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Cells[HeaderRow, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                        }
                    }
                    #endregion

                    #region HeaderSpan
                    //if ((HeaderTop != null && HeaderTop.Length > 0) && (HeaderBom != null && HeaderBom.Length > 0))
                    //{
                    //    this.HeaderSpan(ref worksheet, MergeCell, HeaderTop, HeaderBom, ref bSpanChk);
                    //}
                    #endregion

                    #region Data Binding
                    //int nCols = dt.Columns.Count;
                    //int nRows = dt.Rows.Count;

                    int nStartRow = 2;

                    if (bSpanChk)
                    {
                        nStartRow = 3;
                        worksheet.Cells[1, 1, 2, worksheet.Dimension.End.Column].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[1, 1, 2, worksheet.Dimension.End.Column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[1, 1, 2, worksheet.Dimension.End.Column].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                    }
                    else
                    {
                        worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                    }

                    worksheet.Cells[1, 1, dtData.Rows.Count + nStartRow, worksheet.Dimension.End.Column].Style.Numberformat.Format = "@";

                    worksheet.Cells[1, 1, dtData.Rows.Count + nStartRow, worksheet.Dimension.End.Column].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[1, 1, dtData.Rows.Count + nStartRow, worksheet.Dimension.End.Column].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[1, 1, dtData.Rows.Count + nStartRow, worksheet.Dimension.End.Column].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[1, 1, dtData.Rows.Count + nStartRow, worksheet.Dimension.End.Column].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    worksheet.Cells[nStartRow, 1].LoadFromDataTable(dtData, false);

                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    int nCol = 1;
                    //    foreach (string colNm in headerColumn)
                    //    {
                    //        worksheet.Cells[nStartRow, nCol].Value = dr[colNm].ToString();
                    //        nCol++;
                    //    }
                    //}

                    //using (MemoryStream stream = new MemoryStream())
                    //{
                        oPage.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        oPage.Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(strFileName + ".xlsx").Replace("+", "").Replace("%20", ""));
                        //package.SaveAs(stream);
                        //stream.WriteTo(oPage.Response.OutputStream);
                        //oPage.Response.Flush();
                        //oPage.Response.Close();
                        //oPage.Response.End();
                        //oPage.Response.TransmitFile(strFileName);
                        oPage.Response.BinaryWrite(package.GetAsByteArray());
                        oPage.Response.Flush();
                        oPage.Response.Close();
                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ExcelDownLoad(Page oPage, DataTable dt, string strFileName) // DataGrid 에서 제어 후 ExcelWorkSheet로 처리
        /// <summary>
        /// ExcelDownLoad(Page oPage, DataTable dt, string strFileName)
        /// </summary>
        /// <param name="oPage"></param>
        /// <param name="dt"></param>
        /// <param name="strFileName"></param>
        public void ExcelDownLoad2(Page oPage, DataTable dt, string strFileName)
        {
            try
            {
                DataGrid oGrid = new DataGrid();

                oGrid.ID = "grid";
                oGrid.AutoGenerateColumns = false;
                oGrid.ShowHeader = true;
                oGrid.DataSource = null;
                oGrid.Columns.Clear();

                #region 엑셀 데이터 바인딩
                if ((HeaderBom != null && HeaderBom.Length > 0) && (HeaderColumn != null && HeaderColumn.Length > 0))
                {
                    for (int i = 0; i < this.HeaderBom.Length; i++)
                    {
                        BoundColumn column = new BoundColumn();

                        string[] type = HeaderColumn[i].Split('/');

                        column.DataField = type[0];
                        column.HeaderText = type[0];

                        HeaderColumn[i] = HeaderBom[i];

                        if (type.Length > 1)
                        {
                            if (type[1] == "0")
                            {
                                column.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                            }

                            else if (type[1] == "1")
                            {
                                column.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                            }
                            else
                            {
                                column.DataFormatString = "{0:#,##0}";
                                column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                            }
                        }

                        oGrid.Columns.Add(column);
                    }
                }
                else if ((HeaderColumn != null && HeaderColumn.Length > 0) && (HeaderTop != null && HeaderTop.Length > 0))
                {
                    for (int i = 0; i < this.HeaderTop.Length; i++)
                    {
                        BoundColumn column = new BoundColumn();

                        string[] type = HeaderColumn[i].Split('/');

                        column.DataField = type[0];
                        column.HeaderText = HeaderTop[i];

                        if (type.Length > 1)
                        {
                            if (type[1] == "0")
                            {
                                column.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                            }

                            else if (type[1] == "1")
                            {
                                column.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                            }
                            else
                            {
                                column.DataFormatString = "{0:#,##0}";
                                column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                            }
                        }

                        oGrid.Columns.Add(column);
                    }
                }

                oGrid.DataSource = dt;
                oGrid.DataBind();
                #endregion

                #region HeaderSpan

                if ((HeaderTop != null && HeaderTop.Length > 0)
                    && (HeaderBom != null && HeaderBom.Length > 0))
                {
                    this.HeaderSpan(ref oGrid, MergeCell, HeaderTop, HeaderBom);
                }

                oGrid.HeaderStyle.BackColor = System.Drawing.Color.AliceBlue;
                oGrid.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                oGrid.Attributes.Add("style", "font-famaily:돋음; font-size:12;");
                oGrid.FooterStyle.BackColor = System.Drawing.Color.AliceBlue;

                if (MergeRow != null && MergeRow.Length > 0)
                {
                    for (int i = 0; i < MergeRow.Length; i++)
                    {
                        this.RowSpan(ref oGrid, MergeRow[i]);
                    }
                }

                #endregion

                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("sheet1");

                    #region header binding
                    if ((HeaderBom != null && HeaderBom.Length > 0) && (HeaderColumn != null && HeaderColumn.Length > 0))
                    {
                        for (int i = 0; i < this.HeaderBom.Length; i++)
                        {
                            string[] type = HeaderColumn[i].Split('/');

                            string strHeaderType = type[0];
                            string strHeaderText = type[0];

                            HeaderColumn[i] = HeaderBom[i];

                            if (type.Length > 1)
                            {
                                if (strHeaderType == "0")
                                {
                                    worksheet.Column(i + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                }
                                else if (strHeaderType == "1")
                                {
                                    worksheet.Column(i + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Column(i + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                }
                            }

                            worksheet.Column(i + 1).Style.Numberformat.Format = "@";
                            worksheet.Cells[1, i + 1].Value = strHeaderText;
                        }
                    }
                    else if ((HeaderColumn != null && HeaderColumn.Length > 0) && (HeaderTop != null && HeaderTop.Length > 0))
                    {
                        for (int i = 0; i < this.HeaderTop.Length; i++)
                        {
                            string[] type = HeaderColumn[i].Split('/');

                            string strHeaderType = type[0];
                            string strHeaderText = HeaderTop[i];

                            HeaderColumn[i] = HeaderBom[i];

                            if (type.Length > 1)
                            {
                                if (strHeaderType == "0")
                                {
                                    worksheet.Column(i + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                }
                                else if (strHeaderType == "1")
                                {
                                    worksheet.Column(i + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Column(i + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                }
                            }

                            worksheet.Column(i + 1).Style.Numberformat.Format = "@";
                            worksheet.Cells[1, i + 1].Value = strHeaderText;
                        }
                    }
                    #endregion

                    #region HeaderSpan

                    if ((HeaderTop != null && HeaderTop.Length > 0) && (HeaderBom != null && HeaderBom.Length > 0))
                    {
                        this.HeaderSpan(ref oGrid, MergeCell, HeaderTop, HeaderBom);
                    }

                    oGrid.HeaderStyle.BackColor = System.Drawing.Color.AliceBlue;
                    oGrid.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    oGrid.Attributes.Add("style", "font-famaily:돋음; font-size:12;");
                    oGrid.FooterStyle.BackColor = System.Drawing.Color.AliceBlue;

                    if (MergeRow != null && MergeRow.Length > 0)
                    {
                        for (int i = 0; i < MergeRow.Length; i++)
                        {
                            this.RowSpan(ref oGrid, MergeRow[i]);
                        }
                    }
                    #endregion

                    #region Data Binding
                    //worksheet.Cells["A1"].lo


                    using (MemoryStream stream = new MemoryStream())
                    {
                        oPage.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        oPage.Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(strFileName + ".xlsx").Replace("+", "").Replace("%20", ""));
                        package.SaveAs(stream);
                        stream.WriteTo(oPage.Response.OutputStream);
                        //oPage.Response.Flush();
                        //oPage.Response.Close();
                        //oPage.Response.End();
                        //oPage.Response.TransmitFile(strFileName);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region HeaderSpan(ref ExcelWorksheet worksheet, string ArrColumn, string[] ArrHeaderTop, string[] ArrHeaderBom)
        /// <summary>
        /// HeaderSpan(ref ExcelWorksheet worksheet, string ArrColumn, string[] ArrHeaderTop, string[] ArrHeaderBom)
        /// </summary>
        /// <param name="worksheet">ref ExcelWorksheet</param>
        /// <param name="ArrColumn">string</param>
        /// <param name="ArrHeaderTop">string[]</param>
        /// <param name="ArrHeaderBom">string[]</param>
        private void HeaderSpan(ref ExcelWorksheet worksheet, string ArrColumn, string[] ArrHeaderTop, string[] ArrHeaderBom, ref bool bSpanChk)
        {
            int iCurrIndex = 0;
            Hashtable m_htblRowspanIndex = new Hashtable();

            try
            {
                string[] merge = ArrColumn.Split(',');
                int index = 0;
                int len = 0;

                for (int i = 0; i < ArrHeaderTop.Length; i++)
                { 
                    for (int j = 0; j < merge.Length; j++)
                    {
                        string[] mergeInfo = merge[j].Split('/');

                        index = int.Parse(mergeInfo[0]);

                        if (index == i)
                        {
                            bSpanChk = true;

                            len = int.Parse(mergeInfo[1]);

                            string strMergeCell1 = worksheet.Cells[1, i + 1].Address;
                            string strMergeCell2 = worksheet.Cells[1, i + 1 + len].Address;

                            worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                            worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            iCurrIndex += len - 1;
                            len = 0;

                            break;
                        }
                    }

                    if (len <= 1)
                    {
                        string strMergeCell1 = worksheet.Cells[1, i + 1].Address;
                        string strMergeCell2 = worksheet.Cells[2, i + 1].Address;

                        worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                        worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        m_htblRowspanIndex.Add(iCurrIndex + i, iCurrIndex + i);
                    }
                }

                for (int i = 0; i < ArrHeaderBom.Length; i++)
                {
                    if (m_htblRowspanIndex.Contains(i))
                        continue;

                    worksheet.Cells[2, i + 1].Value = ArrHeaderBom[i].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region HeaderSpan(ref DataGrid grid, string ArrColumn, string[] ArrHeaderTop, string[] ArrHeaderBom)
        /// <summary>
        /// <b>DataGrid의 HeaderSpan</b><br/>
        /// </summary>
        /// <param name="grid">출력할 데이터가 있는 Grid</param>
        /// <param name="ArrColumn">Column</param>
        /// <param name="ArrHeaderTop">Header</param>
        /// <param name="ArrHeaderBom">Bottom</param>
        private void HeaderSpan(ref DataGrid grid, string ArrColumn, string[] ArrHeaderTop, string[] ArrHeaderBom)
        {

            DataGridItem dgItem = new DataGridItem(0, 0, ListItemType.Header);
            DataGridItem dgItem1 = new DataGridItem(0, 0, ListItemType.Header);

            int iCurrIndex = 0;
            Hashtable m_htblRowspanIndex = new Hashtable();

            try
            {
                TableCell oCell = null;
                string[] merge = ArrColumn.Split(',');
                int index = 0;
                int len = 0;

                for (int i = 0; i < ArrHeaderTop.Length; i++)
                {
                    oCell = new TableCell();
                    oCell.Text = ArrHeaderTop[i];

                    for (int j = 0; j < merge.Length; j++)
                    {
                        string[] mergeInfo = merge[j].Split('/');

                        index = int.Parse(mergeInfo[0]);
                        len = int.Parse(mergeInfo[1]);

                        if (index == i)
                        {
                            oCell.ColumnSpan = len;
                            iCurrIndex += len - 1;
                            break;
                        }
                    }

                    if (oCell.ColumnSpan <= 1)
                    {
                        oCell.RowSpan = 2;
                        m_htblRowspanIndex.Add(iCurrIndex + i, iCurrIndex + i);
                    }

                    oCell.VerticalAlign = VerticalAlign.Middle;

                    dgItem.Cells.Add(oCell);
                }

                for (int i = 0; i < ArrHeaderBom.Length; i++)
                {
                    oCell = new TableCell();
                    oCell.Text = ArrHeaderBom[i];

                    if (m_htblRowspanIndex.Contains(i))
                        oCell.Visible = false;

                    dgItem1.Cells.Add(oCell);
                }

                grid.Controls[0].Controls.AddAt(0, dgItem);
                grid.Controls[0].Controls.AddAt(1, dgItem1);

                grid.Controls[0].Controls.RemoveAt(2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region RowSpan(ref DataGrid dataGrid, string ArrColumn)
        /// <summary>
        /// <b>DataGrid의 내용을 Rowspan</b><br/>
        /// </summary>
        /// <param name="dataGrid">출력할 데이터가 있는 Grid</param>
        /// <param name="ArrColumn">컬럼</param>
        private void RowSpan(ref DataGrid dataGrid, string ArrColumn)
        {
            int rowCount = 1;
            string partString0;
            string partString1;
            int start;
            string DefaultText0;
            string DefaultText1;

            string[] ArrCol = ArrColumn.Split(',');

            start = int.Parse(ArrCol[0]);

            for (int j = 0; j < ArrCol.Length; j++)
            {
                int columnIndex = int.Parse(ArrCol[j]);

                rowCount = 1;

                for (int i = 0; i < dataGrid.Items.Count; i++)
                {

                    if (dataGrid.Items[i].ItemType != ListItemType.Header)
                    {

                        if (i.Equals(dataGrid.Items.Count - 1))
                            dataGrid.Items[i - rowCount + 1].Cells[columnIndex].RowSpan = rowCount;
                        else
                        {
                            partString0 = dataGrid.Items[i].Cells[columnIndex].Text;
                            partString1 = dataGrid.Items[i + 1].Cells[columnIndex].Text;

                            DefaultText0 = dataGrid.Items[i].Cells[start].Text;
                            DefaultText1 = dataGrid.Items[i + 1].Cells[start].Text;

                            if ((partString0.Equals(partString1) && DefaultText0.Equals(DefaultText1))
                                //&& (j == DefaultCell || !dataGrid.Items[i + 1].Cells[start].Visible))
                                )
                            {
                                rowCount++;
                                dataGrid.Items[i + 1].Cells[columnIndex].Visible = false;
                            }
                            else
                            {
                                dataGrid.Items[i - rowCount + 1].Cells[columnIndex].RowSpan = rowCount;
                                rowCount = 1;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
                }

                // TODO: 비관리형 리소스(비관리형 개체)를 해제하고 종료자를 재정의합니다.
                // TODO: 큰 필드를 null로 설정합니다.
                disposedValue = true;
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ExcelUtil()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
