﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System.Collections.Generic;
using System.Web.UI;

namespace KTS.KTSSolution.Framework.Web
{
    public class PageBase : Page
    {
        #region Properties

        #region  PageDictionary
        /// <summary>
        /// PageDictionary
        /// </summary>
        public Dictionary<string, object> PageDictionary
        {
            get
            {
                if (ViewState["PageDictionary"] == null)
                {
                    ViewState.Add("PageDictionary", new Dictionary<string, object>());
                }
                return ViewState["PageDictionary"] as Dictionary<string, object>;
            }
        }
        #endregion

        #endregion

        #region Constructor

        public PageBase()
        {
        }

        #endregion
    }
}
